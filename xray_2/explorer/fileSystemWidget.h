#ifndef FILESYSTEMWIDGET_H
#define FILESYSTEMWIDGET_H


class TFileModel;
class TFileProxyModel;

class TFileSystemWidget : public QWidget
{
	Q_OBJECT
public:
	TFileSystemWidget( TFileModel *model, QAction *act = NULL, QWidget *parent = NULL );
	//
	void getSelectedFiles( QList<QString> &list ) const;
	void expandToPath( const QString &path );

protected slots:
	void setRoot( const QModelIndex &index );
	void detailedActivated( const QModelIndex &index );
	void focusChanged( QWidget *old, QWidget *now );
	void backspaceActSlot();

protected:
	virtual void showEvent( QShowEvent *event );
	virtual void hideEvent( QHideEvent *event );

private slots:
	void slotPathUpdated( QString path );
	void slotPathAboutToBeUpdated( QString path );

private:
	QAction *action;
	//
	QSplitter  *splitter;
	QTreeView  *folderTree, *detailedTree;
	TFileModel *fileModel;
	TFileProxyModel *folderModel, *detailedModel;
	//
	QTreeView *lastactiveView;
	//
//	QString currentPath;
};



#endif