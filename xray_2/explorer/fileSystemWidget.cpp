#include "StdAfx.h"

#include "fileSystemWidget.h"
#include "fileModel.h"

////////////////////////////////////////////////////////////////////
// TFileSystemWidget ///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TFileSystemWidget::TFileSystemWidget( TFileModel *model, QAction *act, QWidget *parent )
: QWidget(parent)
, action(act)
, fileModel(model)
{
	folderModel = new TFileProxyModel;
	folderModel->setSourceModel( fileModel );
	
	detailedModel = new TFileProxyModel;
	detailedModel->setFilteringEnable(false);
	detailedModel->setSourceModel( fileModel );
	
	folderTree = new QTreeView;
	folderTree->setModel( folderModel );
	folderTree->setMinimumWidth( 100 );
	folderTree->setMaximumWidth( 400 );
	folderTree->hideColumn(1);
	folderTree->hideColumn(2);
	folderTree->hideColumn(3);
	folderTree->setUniformRowHeights( true );

	folderTree->addAction( action );
	folderTree->setContextMenuPolicy( Qt::ActionsContextMenu );

	detailedTree = new QTreeView;
	detailedTree->setModel( detailedModel );
	detailedTree->header()->setMovable( false );
	detailedTree->setItemsExpandable( false );
	detailedTree->setRootIsDecorated( false );
	detailedTree->setSelectionMode( QAbstractItemView::ExtendedSelection );
	detailedTree->addAction( action );
	detailedTree->setContextMenuPolicy( Qt::ActionsContextMenu );
	detailedTree->setUniformRowHeights( true );
	detailedTree->setSortingEnabled( true );
	detailedTree->sortByColumn(0, Qt::AscendingOrder);
	detailedTree->setRootIndex( detailedModel->index(0,0,QModelIndex()) );

/**
	QAction *backspaceAct = new QAction( detailedTree );
	backspaceAct->setShortcut(Qt::Key_Backspace);
	//backspaceAct->setVisible(false);
	//backspaceAct->setEnabled(true);
	connect( backspaceAct, SIGNAL(triggered(bool)), SLOT(backaspaceActSlot()) );
	detailedTree->addAction( backspaceAct );
/**/

	splitter = new QSplitter( Qt::Horizontal );
	splitter->addWidget( folderTree );
	folderTree->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	//splitter->addWidget( detailedTree );

	QHBoxLayout *l = new QHBoxLayout;
	l->addWidget( splitter );
	setLayout( l );

	lastactiveView = folderTree;

	connect( folderTree->selectionModel(), SIGNAL(currentChanged(const QModelIndex&,const QModelIndex&))
										 , SLOT(setRoot(const QModelIndex&)) );

//	connect( folderTree->selectionModel(), SIGNAL(currentChanged(const QModelIndex&,const QModelIndex&))
//		   , folderTree					 , SLOT(expand(const QModelIndex&)) );
	connect( folderTree, SIGNAL(activated(const QModelIndex&))
		   , folderTree, SLOT(expand(const QModelIndex&)) );

	connect( detailedTree, SIGNAL(activated(const QModelIndex&))
		   	 			 , SLOT(detailedActivated(const QModelIndex&)) );

	folderTree->selectionModel()->setCurrentIndex( folderModel->index(0,0,QModelIndex())
												 , QItemSelectionModel::ClearAndSelect );
	folderTree->expand( folderModel->index(0,0) );

	connect( folderModel, SIGNAL(pathAboutToBeUpdated(QString)), SLOT(slotPathAboutToBeUpdated(QString)) );
	connect( folderModel, SIGNAL(pathUpdated(QString)), SLOT(slotPathUpdated(QString)) );

	setFocusProxy(folderTree);
	folderTree->setFocus();
}

//
void TFileSystemWidget::slotPathAboutToBeUpdated( QString path )
{
	if( path == "." ){
		QModelIndex prCmIn = folderModel->mapFromSource( fileModel->myComputer() );
		for( int i = 0; i < folderModel->rowCount(prCmIn); i++ ){
			folderTree->collapse( folderModel->index(i,0,prCmIn) );
		}
		folderTree->collapse( folderModel->mapFromSource( fileModel->myComputer() ) );
		folderTree->collapse( folderModel->mapFromSource( fileModel->myDesktop() ) );
	}
	else{
	}
}

/**/
//
void TFileSystemWidget::slotPathUpdated( QString path )
{
	if( path == "." ){
		folderTree->selectionModel()->setCurrentIndex( folderModel->mapFromSource( fileModel->myComputer() ), QItemSelectionModel::ClearAndSelect );
	}
	else{
	}
}
/**/

//
void TFileSystemWidget::backspaceActSlot()
{
	QModelIndex parent = detailedTree->rootIndex().parent();
	
	if( parent.isValid() ){
		detailedTree->setRootIndex( parent );
		QModelIndex sourceParent = detailedModel->mapToSource(parent);
		QModelIndex folderParent = folderModel->mapFromSource(sourceParent);
		folderTree->selectionModel()->setCurrentIndex( folderParent, QItemSelectionModel::ClearAndSelect );
	}
}

//
void TFileSystemWidget::detailedActivated( const QModelIndex &index )
{
	QModelIndex sourceIndex = detailedModel->mapToSource(index);
	TFileItem *item = fileModel->getItem( sourceIndex );
	
	if( item->isDir() ){
		QModelIndex folderIndex = folderModel->mapFromSource(sourceIndex);
		folderTree->selectionModel()->setCurrentIndex( folderIndex, QItemSelectionModel::ClearAndSelect );
	}
	else if( item->isSymLink() ){
		QString target = item->symLinkTarget();
		if( !target.isEmpty() )
			expandToPath( target );
	}
	else
		action->trigger();
}

//
void TFileSystemWidget::setRoot( const QModelIndex &index )
{
/**/
	const QSortFilterProxyModel *proxyModel = dynamic_cast<const QSortFilterProxyModel*>( index.model() );
	Q_ASSERT(proxyModel);

	QModelIndex sourceIndex = proxyModel->mapToSource( index );
	QModelIndex detailedIndex = detailedModel->mapFromSource( sourceIndex );

	//fileModel->fetchMore(sourceIndex);
	folderModel->fetchMore(index);
	folderTree->viewport()->update();
	detailedTree->setRootIndex( detailedIndex );
/**/
//	currentPath = folderModel->getItemFromSource( sourceIndex )->filePath();
//	currentPath = folderModel->getItemFromProxy( index )->filePath();
}

//
void TFileSystemWidget::getSelectedFiles( QList<QString> &list ) const
{
	if( lastactiveView == folderTree )
		list << fileModel->getItem( folderModel->mapToSource(folderTree->currentIndex()) )->filePath();
	else if( lastactiveView == detailedTree ){
		QModelIndexList indexList = detailedTree->selectionModel()->selectedRows();
		foreach(QModelIndex index, indexList )
			list << fileModel->getItem( detailedModel->mapToSource(index) )->filePath();
	}
}

//
void TFileSystemWidget::expandToPath( const QString &path )
{
	QString myDocumentsPath = fileModel->getItem(fileModel->myDocuments())->filePath();
	QString myDesktopPath   = fileModel->getItem(fileModel->myDesktop())->filePath();

	expandToPathInTreeView( folderTree
		                  , path
						  , myDocumentsPath
						  , folderModel->mapFromSource(fileModel->myDocuments())
						  , myDesktopPath
						  , folderModel->mapFromSource(fileModel->myDesktop())
						  , folderModel->mapFromSource(fileModel->myComputer())  );
	folderTree->setFocus();
}

//
void TFileSystemWidget::focusChanged(QWidget *old, QWidget *now)
{
	QTreeView *tmp = dynamic_cast<QTreeView*>( now );
	if( tmp ) lastactiveView = tmp;
}

//
void TFileSystemWidget::showEvent( QShowEvent *event )
{
	connect( qApp, SIGNAL(focusChanged(QWidget*,QWidget*)), SLOT(focusChanged(QWidget*,QWidget*)) );
}

//
void TFileSystemWidget::hideEvent( QHideEvent *event )
{
	disconnect( qApp, SIGNAL(focusChanged(QWidget*,QWidget*)), this, SLOT(focusChanged(QWidget*,QWidget*)) );
}