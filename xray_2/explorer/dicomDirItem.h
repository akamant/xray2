#ifndef DICOMDIRITEM_H
#define DICOMDIRITEM_H


////////////////////////////////////////////////////////////////////
// TDicomDirItem ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
class TDicomDirItem
{
	friend class TDicomDirModel;
public:
	TDicomDirItem( const QString     &value  );
	TDicomDirItem( const QStringList &values );
	~TDicomDirItem();
	//
	void addChild( TDicomDirItem *child );
	TDicomDirItem *child( int i ) const { return children[i]; }
	int childCount()			  const { return children.count(); }
	TDicomDirItem *parent()		  const { return par; }
	//
	void setPixmap( const char * data, int width, int height
		           , QImage::Format format = QImage::Format_RGB32 );
	void setPath( const QString &p ) { filmPath = p; }
	//
	const QPixmap &pixmap()	const { return previewPixmap; }
	const QString &path()   const { return filmPath; }

private:
	char * data;
	QPixmap previewPixmap;
	QString filmPath;
	//
	TDicomDirItem			*par;
	QVector<QString>		values;
	QList<TDicomDirItem*>	children;
};

////////////////////////////////////////////////////////////////////
// TDicomDirModel///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//

struct THeaderSectionInfo
{
	THeaderSectionInfo( QVariant::Type _type = QVariant::String, const QString &_label = "<empty>" )
		: type(_type), label(_label) {}
	//
	QVariant::Type type;
	QString		   label;
};
typedef QVector<THeaderSectionInfo> THeaderInfoList;

////////////////////////////////////////////////////////////////////
// TDicomDirModel //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
class TDicomDirModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	//
	TDicomDirModel();
	~TDicomDirModel() { delete rootItem; }
  
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	virtual QModelIndex parent(const QModelIndex &index) const;
	virtual bool hasChildren(const QModelIndex &index = QModelIndex()) const;
	
	virtual Qt::ItemFlags flags(const QModelIndex &index)              const { return Qt::ItemIsSelectable | Qt::ItemIsEnabled; }
	virtual int rowCount(const QModelIndex &parent = QModelIndex())    const { return getItem(parent)->childCount(); }
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const { return headerInfo.count(); }

	TDicomDirItem *getRootItem(){ return rootItem; }

	//
	TDicomDirItem *getItem( const QModelIndex index ) const;
	//
	void addTopLevelItem( TDicomDirItem *item );
	void setHeader( const THeaderInfoList &header ) { headerInfo = header; }
	const THeaderInfoList &header(){ return headerInfo; } 
	void clear();

private:
	int rowInParentItem( TDicomDirItem *item ) const;	
	//
	TDicomDirItem *rootItem;
	THeaderInfoList headerInfo;
};

////////////////////////////////////////////////////////////////////
// TDicomDirProxyModel /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
class TDicomDirProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT
public:
	TDicomDirProxyModel( QObject *parent = 0 ): QSortFilterProxyModel(parent) {}

public:
	TDicomDirItem *getItemFromSource(QModelIndex index) const;
	TDicomDirItem *getItemFromProxy(QModelIndex index)  const;

	virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const;
};



#endif