#include "StdAfx.h"
#include "dicomDirWidget.h"
#include "fileModel.h"
#include "dicomDirItem.h"

////////////////////////////////////////////////////////////////////
// TAspectLabel ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TAspectLabel::TAspectLabel( QWidget *parent, Qt::WindowFlags f )
: QLabel(parent, f)
{
	setLineWidth( 2 );
	setFrameShape( QFrame::Panel );
	setAlignment( Qt::AlignCenter );
	setText( "No Image" );
}

//
void TAspectLabel::paintEvent( QPaintEvent *event )
{
	QLabel::paintEvent( event );

	if( origPixmap.isNull() )
		return;
	
	QPainter painter(this);
	int w = width()  - 10
	  , h = height() - 10;
	QPixmap tmp = origPixmap.scaled( w, h, Qt::KeepAspectRatio );
	int px = (w - tmp.width())  >> 1
	  , py = (h - tmp.height()) >> 1;
	painter.drawPixmap( px + 5, py + 5, tmp );
}

//
void TAspectLabel::slotSetAspectPixmap( const QPixmap &pxmap )
{
	clear();
	origPixmap = pxmap;
	update();
}

//
void TAspectLabel::slotSetText( const QString &text )
{
	origPixmap = 0;
	setText( text );
	update();
}

////////////////////////////////////////////////////////////////////
// TDicomDirWidget /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TDicomDirWidget::TDicomDirWidget( TFileModel *model, QAction *act, QWidget *parent )
: QWidget(parent)
, action(act)
, fileModel(model)
{
	dicomModel = new TDicomDirModel;
	dicomProxyModel = new TDicomDirProxyModel;
	dicomProxyModel->setSourceModel( dicomModel );
	
	dicomView  = new QTreeView;
	dicomView->setModel( dicomProxyModel );
	dicomView->header()->setMovable( false );
	dicomView->setUniformRowHeights( true );
	dicomView->setSortingEnabled( true );
	dicomView->setSelectionBehavior( QAbstractItemView::SelectRows);
	dicomView->setSelectionMode( QAbstractItemView::ExtendedSelection );
	connect( dicomView, SIGNAL(clicked(const QModelIndex&)),   SLOT(slotDicomDirItemClicked(const QModelIndex&)) );
	connect( dicomView, SIGNAL(expanded(const QModelIndex&)),  SLOT(resizeColumns()) );
	connect( dicomView, SIGNAL(collapsed(const QModelIndex&)), SLOT(resizeColumns()) );

	pixmapLabel = new TAspectLabel;
	pixmapLabel->setMinimumWidth( 50 );
	pixmapLabel->setMaximumWidth( 150 );

	dirView = new QTreeView;
	dirView->setModel( fileModel );
	dirView->setSelectionBehavior( QAbstractItemView::SelectRows);
	dirView->setSelectionMode( QAbstractItemView::SingleSelection );
	dirView->setUniformRowHeights( true );
	dirView->setMinimumWidth( 100 );
	dirView->setMaximumWidth( 400 );
	dirView->hideColumn(1);
	dirView->hideColumn(2);
	dirView->hideColumn(3);
	connect( dirView, SIGNAL(clicked(const QModelIndex &)), SLOT(slotFileItemChoosed(const QModelIndex &)) );
	dirView->expand( fileModel->index(0,0,QModelIndex()) );

	dirView->addAction( action );
	dirView->setContextMenuPolicy( Qt::ActionsContextMenu );
	dicomView->addAction( action );
	dicomView->setContextMenuPolicy( Qt::ActionsContextMenu );

	splitter1 = new QSplitter( Qt::Horizontal );
	splitter2 = new QSplitter( Qt::Horizontal );

	splitter1->addWidget( dirView );
	splitter2->addWidget( dicomView );
	splitter2->addWidget( pixmapLabel );
	splitter1->addWidget( splitter2 );
	
	QHBoxLayout *l = new QHBoxLayout;
	l->addWidget( splitter1 );
	setLayout( l );
}

//
void TDicomDirWidget::addTopLevelItem(TDicomDirItem *item)
{
	dicomModel->addTopLevelItem(item);
	resizeColumns();
}

//
void TDicomDirWidget::resizeColumns()
{
	int n = dicomView->header()->count();
	for( int i = 0; i < n; ++i ){
		dicomView->resizeColumnToContents(i);
		dicomView->show();
	}
}

//
void TDicomDirWidget::slotDicomDirItemClicked( const QModelIndex &index )
{
/**/
	TDicomDirItem *dicItem = dicomProxyModel->getItemFromProxy(index);
	if( dicItem ){
		pixmapLabel->slotSetText( "Loading.." );
		pixmapLabel->slotSetAspectPixmap( dicItem->pixmap() );
	}
	else
		pixmapLabel->slotSetText( "No Image" );
/**/
}

//
void TDicomDirWidget::slotFileItemChoosed( const QModelIndex &index )
{
	pixmapLabel->slotSetText( "No Image" );
	//
	TFileItem *tmp = static_cast<TFileItem *>(index.internalPointer());
	emit signalDirChosed( tmp->filePath() );
}

//
void TDicomDirWidget::setTopLevelHeaders( const THeaderInfoList &header )
{
	dicomModel->setHeader( header );
}

//
void TDicomDirWidget::clear()
{
	dicomModel->clear();
}

//
void TDicomDirWidget::expandToPath( const QString &path )
{
	QString myDocumentsPath = fileModel->getItem(fileModel->myDocuments())->filePath();
	QString myDesktopPath   = fileModel->getItem(fileModel->myDesktop())->filePath();
	
	QModelIndex rt = expandToPathInTreeView( dirView
						  , path
						  , myDocumentsPath
						  , fileModel->myDocuments()
						  , myDesktopPath
						  , fileModel->myDesktop()
						  , fileModel->myComputer()  );
	
	slotFileItemChoosed( rt );
	dirView->setFocus();
}


//
void TDicomDirWidget::getSelectedFiles( QStringList &list ) const
{
	TDicomDirItem *item;
	QModelIndexList indexList = dicomView->selectionModel()->selectedRows();
	if( indexList.size() != 0 ){
		foreach(QModelIndex index, indexList ){
			item = dicomProxyModel->getItemFromProxy( index );
			if( item )
				addPathFromItem( item, list );
		}
	}
	else{
		int n = dicomProxyModel->rowCount();
		for( int i = 0; i < n; ++i ){
			item = dicomProxyModel->getItemFromProxy( dicomProxyModel->index(i,0) );
			if( item )
				addPathFromItem( item, list );
		}
	}

	QSet<QString> set = list.toSet();
	list = set.toList();
}

//
void TDicomDirWidget::addPathFromItem( const TDicomDirItem *item, QStringList &list ) const 
{
	if( !item->path().isEmpty() ){
		list << item->path();
		return;
	}

	int n = item->childCount();
	for( int i = 0; i < n; ++i ){
		addPathFromItem( item->child(i), list );
	}
}

