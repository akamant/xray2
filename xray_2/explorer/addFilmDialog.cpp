#include "StdAfx.h"

#include "addFilmDialog.h"
#include "fileSystemWidget.h"
#include "dicomDirWidget.h"
#include "dicomDirItem.h"
#include "filemodel.h"

////////////////////////////////////////////////////////////////////
// TAddFilmDialog //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TAddFilmDialog::TAddFilmDialog( QWidget *parent, Qt::WindowFlags f )
:	QMainWindow( parent, f )
{
	setWindowTitle("������� ����...");
	addItems = new QAction( "Add", this );
	connect( addItems, SIGNAL(triggered()), SLOT(slotAddSelected()) );
	
	tabWidget = new QTabWidget;
	fileModel = new TFileModel;
	
	fsw = new TFileSystemWidget(fileModel, addItems);

	ddw = new TDicomDirWidget(fileModel, addItems);
	connect( ddw, SIGNAL(signalDirChosed(QString)), SIGNAL(signalDirChosed(QString)) );

	tabWidget->addTab( fsw, "Explorer" );
	tabWidget->addTab( ddw, "Dicom Dir" );

/*
	QWidget * widget = new QWidget();
	QHBoxLayout *vh0 = new QHBoxLayout;	
	QPushButton * getFilesFromServ  = new QPushButton("GetFilesFromServer");
	connect( getFilesFromServ, SIGNAL(clicked()), SLOT(slotGetFilesFromServ()) ); 
	editAdd = new QLineEdit();
	editAdd->setText("1.2.826.0.1.3680043.2.1545.1.2.1.7.1257956221.375.23");
	vh0->addWidget(editAdd);
	vh0->addWidget(getFilesFromServ);
	widget->setLayout( vh0 );
	tabWidget->addTab( widget, "Ivan Server" );
*/

	buttonAdd  = new QPushButton( "Add selected" );
	connect( buttonAdd, SIGNAL(clicked()), SLOT(slotAddSelected()) ); 
	
	buttonExit = new QPushButton( "Exit" );
	connect( buttonExit, SIGNAL(clicked()), SLOT(hide()) ); 
	
	QVBoxLayout *vl = new QVBoxLayout;
	QHBoxLayout *hl = new QHBoxLayout;

	hl->addWidget( buttonAdd );
	hl->addWidget( buttonExit );

	vl->addWidget( tabWidget );
	vl->addLayout( hl );

	QWidget *tmp = new QWidget;
	tmp->setLayout( vl );
	setCentralWidget( tmp );

	fsw->setFocus();
//	refresh = new QAction( "Refresh", this );
//	refresh->setShortcut( Qt::Key_F5 );
//	addAction( refresh );

//	connect( refresh, SIGNAL(triggered()), fileModel, SLOT(slotUpdateDrives()) );
//	connect( refresh, SIGNAL(triggered()), fileModel, SLOT(slotRefreshIndex()) );
//	connect( refresh, SIGNAL(triggered()), fileModel, SLOT(slotRefreshIndex(const QModelIndex&)) );
}

//
void TAddFilmDialog::addTopLevelItem(TDicomDirItem *item)
{
	ddw->addTopLevelItem(item);
}

//
void TAddFilmDialog::setTopLevelHeaders( const THeaderInfoList &headers )
{
	ddw->setTopLevelHeaders(headers);
}

//
void TAddFilmDialog::slotAddSelected()
{
	filesToLoad.clear();
	switch( tabWidget->currentIndex() ){
		case 0:{ 
			fsw->getSelectedFiles( filesToLoad );
			break;
		}
		case 1:{ 
			ddw->getSelectedFiles( filesToLoad );
			break;
		}
	}
	emit signalFilesChosed( filesToLoad );
	hide();
}

void TAddFilmDialog::slotGetFilesFromServ(){
	QStringList  studyList;
	studyList << editAdd->text();
	emit signalGetFileFromServer(studyList);
}

//
void TAddFilmDialog::clear()
{
	ddw->clear();
}

//
void TAddFilmDialog::setRootPath( const QString &path )
{
	rootPath = path;
	fsw->expandToPath( rootPath );
	ddw->expandToPath( rootPath );
}