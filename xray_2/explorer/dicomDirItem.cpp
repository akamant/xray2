#include "StdAfx.h"

#include "dicomDirItem.h"

////////////////////////////////////////////////////////////////////
// TDicomDirItem ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TDicomDirItem::TDicomDirItem( const QString &value )
: par(0)
{
	data = NULL;
	values.push_back( value );
}

//
TDicomDirItem::TDicomDirItem( const QStringList &strings )
: par(0)
{
	data = NULL;
	foreach(QString tmp, strings){
		values.push_back(tmp);
	}
}

//
TDicomDirItem::~TDicomDirItem()
{
	foreach( TDicomDirItem *tmp, children)
		delete tmp;
	//delete this->data;
}

//
void TDicomDirItem::setPixmap(const char * data, int width, int height, QImage::Format format ){
	QImage img = QImage( (const unsigned char*)data, width, height, format );

	void * p = img.bits();
	void * pp = img.bits();
	previewPixmap = previewPixmap.fromImage( img );
}

//
void TDicomDirItem::addChild( TDicomDirItem *child )
{
	children << child;
	child->par = this;
}

////////////////////////////////////////////////////////////////////
// TDicomDirModel //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TDicomDirModel::TDicomDirModel()
{
	rootItem = new TDicomDirItem("");
}

//
QVariant TDicomDirModel::data(const QModelIndex &index, int role) const
{
	TDicomDirItem *item = getItem( index );
	
	if( role == Qt::DisplayRole ){
		int col = index.column();
		if( item->values.count() - 1 >= col )
			return item->values[index.column()];
		else
			return "";
	}

	return QVariant();
}

//
QVariant TDicomDirModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if( role == Qt::DisplayRole ){
		return headerInfo[section].label;
	}

	return QVariant();
}

//
QModelIndex TDicomDirModel::index(int row, int column, const QModelIndex &parent ) const
{
	TDicomDirItem *parentItem = getItem(parent);
	if( !parentItem->childCount() )
		return QModelIndex();

	return createIndex( row, column, parentItem->children[row] );
}

//
QModelIndex TDicomDirModel::parent(const QModelIndex &index) const
{
	TDicomDirItem *itemParent = getItem(index)->par;
	if( itemParent != rootItem ){
		return createIndex( rowInParentItem(itemParent), 0, itemParent );
	}
	
	return QModelIndex();
}

//
int TDicomDirModel::rowInParentItem( TDicomDirItem *item ) const
{
	TDicomDirItem *parent =item->par;
	int i = 0;
	foreach( TDicomDirItem *tmp, parent->children ){
		if( tmp == item ) return i;
		++i;
	}
	return -1;
}

//
bool TDicomDirModel::hasChildren(const QModelIndex &index) const
{
	TDicomDirItem *tmp = getItem(index);
	return tmp->childCount();
}

//
TDicomDirItem *TDicomDirModel::getItem(QModelIndex index) const
{
	TDicomDirItem *tmp = static_cast<TDicomDirItem*>(index.internalPointer());
	return tmp ? tmp : rootItem;
}

//
void TDicomDirModel::addTopLevelItem( TDicomDirItem *item )
{
	rootItem->addChild( item );
	emit layoutChanged();
}

//
void TDicomDirModel::clear()
{
	int n = rootItem->childCount();
	if( n ) --n;;
	beginRemoveRows( QModelIndex(), 0, n );
	
	foreach( TDicomDirItem *tmp, rootItem->children){
		delete tmp;
	}
	rootItem->children.clear();

	endRemoveRows();
}

////////////////////////////////////////////////////////////////////
// TDicomDirProxyModel /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TDicomDirItem *TDicomDirProxyModel::getItemFromSource(QModelIndex index) const
{
	TDicomDirItem *tmp = static_cast<TDicomDirItem *>(index.internalPointer());
	return tmp ? tmp : static_cast<TDicomDirModel*>(sourceModel())->getRootItem();
}

//
TDicomDirItem *TDicomDirProxyModel::getItemFromProxy(QModelIndex index) const
{
	QModelIndex sourceIndex = mapToSource( index );
	return getItemFromSource( sourceIndex );
}

//
bool TDicomDirProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
	if( left.parent().isValid() )
		return true;

	TDicomDirModel *dicomModel = dynamic_cast<TDicomDirModel*>( sourceModel() );
	QVariant::Type type = dicomModel->header()[ left.column() ].type;

	QVariant leftData  = dicomModel->data(left);
	QVariant rightData = dicomModel->data(right);

	if( leftData.canConvert(type) && rightData.canConvert(type) ){
		switch(type){
			case QVariant::Int:
				return leftData.toInt() < rightData.toInt();
			case QVariant::Double:
				return leftData.toDouble() < rightData.toDouble();
			case QVariant::Time:
				return leftData.toTime() < rightData.toTime();
			case QVariant::Date:{
				
				QDate leftDate = QDate::fromString( leftData.toString(), "d.M.yyyy" )
				,	  rightDate = QDate::fromString( rightData.toString(), "d.M.yyyy" );
				return leftDate < rightDate;
			}
			case QVariant::DateTime:
				::MessageBox( NULL, "������� ��������� QDateTime! � TDicomDirProxyModel", "", MB_OK );
		}
	}



	return leftData.toString() < rightData.toString();
}