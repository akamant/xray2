#include "StdAfx.h"

#include "filemodel.h"

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
//
QPixmap convertHIconToPixmap( const HICON icon, bool large  = false )
{
    HDC screenDevice = GetDC(0);
    HDC hdc = CreateCompatibleDC(screenDevice);
    ReleaseDC(0, screenDevice);

    int size = large ? 64 : 32;

    BITMAPINFO bmi;
    memset(&bmi, 0, sizeof(bmi));
    bmi.bmiHeader.biSize        = sizeof(BITMAPINFO);
    bmi.bmiHeader.biWidth       = size;
    bmi.bmiHeader.biHeight      = -size;
    bmi.bmiHeader.biPlanes      = 1;
    bmi.bmiHeader.biBitCount    = 32;
    bmi.bmiHeader.biCompression = BI_RGB;
    bmi.bmiHeader.biSizeImage   = size*size*4;

    uchar* bits;

    HBITMAP winBitmap = CreateDIBSection(hdc, &bmi, DIB_RGB_COLORS, (void**) &bits, 0, 0);
    if (winBitmap )
        memset(bits, 0xff, size*size*4);
    if (!winBitmap) {
        qWarning("convertHIconToPixmap(), failed to CreateDIBSection()");
        return QPixmap();
    }

    HGDIOBJ oldhdc = (HBITMAP)SelectObject(hdc, winBitmap);
    if (!DrawIconEx( hdc, 0, 0, icon, size, size, 0, 0, DI_NORMAL))
        qWarning("convertHIconToPixmap(), failed to DrawIcon()");

    uint mask = 0xff000000;
    // Create image and copy data into image.
    QImage image(size, size, QImage::Format_ARGB32);

    if (!image.isNull()) { // failed to alloc?
        int bytes_per_line = size * sizeof(QRgb);
        for (int y=0; y<size; ++y) {
            QRgb *dest = (QRgb *) image.scanLine(y);
            const QRgb *src = (const QRgb *) (bits + y * bytes_per_line);
            for (int x=0; x<size; ++x) {
                dest[x] = src[x];
            }
        }
    }
    if (!DrawIconEx( hdc, 0, 0, icon, size, size, 0, 0, DI_MASK))
        qWarning("convertHIconToPixmap(), failed to DrawIcon()");
    if (!image.isNull()) { // failed to alloc?
        int bytes_per_line = size * sizeof(QRgb);
        for (int y=0; y<size; ++y) {
            QRgb *dest = (QRgb *) image.scanLine(y);
            const QRgb *src = (const QRgb *) (bits + y * bytes_per_line);
            for (int x=0; x<size; ++x) {
                if (!src[x])
                    dest[x] = dest[x] | mask;
            }
        }
    }
    SelectObject(hdc, oldhdc); //restore state
    DeleteObject(winBitmap);
    DeleteDC(hdc);
    return QPixmap::fromImage(image);
}


////////////////////////////////////////////////////////////////////
// TFileItem ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TFileItem::TFileItem(const QFileInfo &fInfo, bool virtFolder, TFileItem *parent)
: info(fInfo)
, itemParent(parent)
, fetched(true)
, virtualFolder(virtFolder)
{
	if( info.isDir() )
		fetched = false;
}

//
TFileItem::TFileItem(QString filePath, bool virtFolder, TFileItem *parent)
: info(filePath)
, itemParent(parent)
, fetched(true)
, virtualFolder(virtFolder)
{
	if( info.isDir() )
		fetched = false;
}

//
TFileItem::~TFileItem()
{
	foreach(TFileItem *child, children)
		delete child;
}

//
QString TFileItem::fileSize() const
{
	if( isDir() )
		return "";

	qint64 size = info.size();
	if( size < 1024 )
		return QString("%1 B").arg(size);
	int tmp = size >> 10;
	if( tmp < 1024 )
		return QString("%1 KB").arg(tmp);
	tmp = tmp >> 10;
	if( tmp < 1024 )
		return QString("%1 MB").arg(tmp);
	tmp = tmp >> 10;
	if( tmp < 1024 )
		return QString("%1 GB").arg(tmp);
	return QString("%1 TB").arg(tmp);
}

//
QString TFileItem::fileName() const
{
	if( info.isRoot() ) 
		return info.filePath();
	if( info.isSymLink() ) 
		return info.fileName().remove(".lnk", Qt::CaseInsensitive);

	return info.fileName();
}

//
void TFileItem::addChild( TFileItem *item ) 
{ 
	children.push_back(item);
	item->itemParent = this;
}

//
void TFileItem::insertChild( TFileItem *item, int pos ) 
{ 
	children.insert( pos, item );
	item->itemParent = this;
}

//
bool fileInfoLessThan(TFileItem *f1, TFileItem *f2)
{
	if( f1->isDir() != f2->isDir() )
		return f1->isDir();
	return QString::compare( f1->fileName(), f2->fileName(), Qt::CaseInsensitive ) < 0;
}

//
void TFileItem::fetchChildren()
{
	QString path = info.filePath();
	QDir dir( info.absoluteFilePath() );
	
	QFileInfoList list = dir.entryInfoList( QDir::AllEntries | QDir::NoDotAndDotDot );
	foreach(QFileInfo tmp, list){
		addChild( new TFileItem(tmp, false, this) );
	}

	qSort( children.begin(), children.end(), fileInfoLessThan );
	fetched = true;
}

//
bool TFileItem::synchronizeWithDir( const QFileInfoList &fileList )
{
	int fileListPos = 0
	,	itemsCount  = children.count()
	,	filesCount  = fileList.count();
	
	bool found 
	,	 needUpdate = false;
	
	TFileItem *curItem;
	
	for( int i = 0; i < itemsCount; ++i ){
		curItem = children[i];
		found = false;

		for( int j = fileListPos; j < filesCount; ++j )
			if( curItem->fileName() == fileList[j].filePath() ){
				fileListPos = j++;
				found = true;
				break;
			}

		if( !found ){
			children.removeAt(i);
			--itemsCount;
			needUpdate = true;
		}
	}

	for( int j = 0; j < filesCount; ++j )
		if( j >= itemsCount || children[j]->fileName() != fileList[j].filePath() ){
			insertChild( new TFileItem(fileList[j]), j );
			++itemsCount;
			needUpdate = true;
		}

	return needUpdate;
}

////////////////////////////////////////////////////////////////////
// TFileModel //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
TFileModel::TFileModel()
{
	rootItem = new TFileItem("");
	
	itemDesktop     = createWinSysFolder( CSIDL_DESKTOP );
	itemMyDocuments = createWinSysFolder( CSIDL_PERSONAL );
	itemMyComputer  = createWinSysFolder( CSIDL_DRIVES );
	
	itemDesktop->fetchChildren();

	itemDesktop->insertChild( itemMyDocuments, 0 );   
	itemDesktop->insertChild( itemMyComputer,  1 );

	rootItem->addChild( itemDesktop );

	indexDesktop	 = createIndex( 0, 0, itemDesktop );
	indexMyDocuments = createIndex( 0, 0, itemMyDocuments );
	indexMyComputer	 = createIndex( 1, 0, itemMyComputer );

//	fetchMore( indexDesktop );
	fetchMore( indexMyComputer );

	initDrivesTimer();
}

//
void TFileModel::initDrivesTimer()
{
	connect( &drivesTimer, SIGNAL(timeout()), SLOT(slotDrivesTimeout()) );
	drivesTimer.start( 2000 );
}

//
void TFileModel::slotDrivesTimeout()
{
	if( itemMyComputer->synchronizeWithDir( QDir::drives() ) ){
		emit pathAboutToBeUpdated(".");
		emit layoutChanged();
		emit pathUpdated(".");
	}
}

//
TFileItem *TFileModel::createWinSysFolder( int folder )
{
	char path[MAX_PATH];
	path[0] = '\0';
	
	SHGetFolderPath( NULL, folder,  NULL, 0, path );
	
	TFileItem *folderItem = new TFileItem( path, true );
	folderItem->fetched = false;

	PIDLIST_ABSOLUTE pidl = 0;
	DWORD hr = SHGetSpecialFolderLocation( NULL, folder, &pidl );

	if( SUCCEEDED(hr) ){
		SHFILEINFO sfi = {0};
		hr = SHGetFileInfo((LPCTSTR)pidl, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_ICON );
		if( SUCCEEDED(hr) )
			folderItem->icon = convertHIconToPixmap(sfi.hIcon);
	}
	return folderItem;
}
 
//
bool TFileModel::canFetchMore( const QModelIndex &index ) const
{
	TFileItem *item = getItem(index);
	return !getItem(index)->getFetched();
}

//
void TFileModel::fetchMore(const QModelIndex &index)
{
	TFileItem *item = getItem(index);

	if( !item->getFetched() ){

		if( item == itemMyComputer ){
			QFileInfoList drives = QDir::drives();
			foreach(QFileInfo tmp, drives){
		//		if( tmp.filePath() != "A:/" )
				itemMyComputer->addChild( new TFileItem(tmp) );
			}
			item->fetched = true;
		}
		else
			item->fetchChildren();
/**		
		int count = item->childCount();
		if( count ){
			QModelIndex endIndex = this->index( count - 1 , 0, index );
//			dataChanged( index, endIndex );
//			layoutChanged();
		}
/**/
	}
}

//
QVariant TFileModel::data(const QModelIndex &index, int role) const
{
	TFileItem *item = getItem( index );
	
	if( item == itemMyComputer || item == itemMyDocuments ){
		if( role == Qt::DisplayRole ){
			if( index.column() == NameColumn )
				if( item == itemMyComputer ) return "��� ���������";
			if( index.column() == TypeColumn )
				return "��������� �����";
		}
	}

	if( role == Qt::DisplayRole ){
		switch( index.column() ){
			case NameColumn: return item->fileName();
			case SizeColumn: return item->fileSize();
			case DateColumn: return item->fileDateTime();
			case TypeColumn: 
			{	
/**
				SHFILEINFO info = {};
				QString vv = item->filePath().toAscii().constData();
				SHGetFileInfo( QDir::toNativeSeparators( vv ).toAscii().constData()
						     , FILE_ATTRIBUTE_NORMAL, &info,  sizeof(info)
							 , SHGFI_TYPENAME );
				return QString( info.szTypeName );
/**/
/**/
				if( item->isSymLink() )	return "�����";
				if( item->isDir() )		return "����� � �������";
				
				QString ext( item->fileExt().toUpper() );
				if( extToType.contains( ext ) )
					return extToType.value( ext );
				else{
					SHFILEINFO info = {};
					if( SHGetFileInfo( QString("*."+ ext).toLatin1().constData()
						              , FILE_ATTRIBUTE_NORMAL, &info,  sizeof(info)
									  , SHGFI_TYPENAME | SHGFI_USEFILEATTRIBUTES) != 0 ){
						extToType[ext] = info.szTypeName;
					}
					else
						extToType[ext] = "����";

					return extToType[ext];
				}	
/**/
			}
		}
	}
	
	if( role == Qt::TextAlignmentRole ){
		if( index.column() == 1 || index.column() == 3 ){
			return Qt::AlignRight;
		}
	}

	if( role == Qt::DecorationRole && index.column() == 0 ){
		if( !item->icon.isNull() )
			return item->icon;

		QString ext( item->fileExt().toUpper() );
		if( ext.isEmpty() || ext == "EXE" || ext == "LNK" ) {
			item->icon = iconProvider.icon( item->fileInfo() );
		}
		else{
			if( extToIcon.contains( ext ) )
				item->icon = extToIcon.value( ext );
			else{
				extToIcon[ext] = iconProvider.icon( item->fileInfo() );
				item->icon = extToIcon[ext];
			}
		}
		
		return item->icon;
	}

	return QVariant();
}

//
QVariant TFileModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if( role == Qt::DisplayRole ){
		switch( section ){
			case NameColumn: return "Name";
			case SizeColumn: return "Size";
			case TypeColumn: return "Type";
			case DateColumn: return "Date";
		}
	}
	return QVariant();
}
 


QModelIndex TFileModel::index(int row, int column, const QModelIndex &parent ) const
{
	TFileItem *parentItem = getItem(parent);
return createIndex( row, column, parentItem->child(row) );
} 

//
QModelIndex TFileModel::parent(const QModelIndex &index) const
{
	TFileItem *itemParent = getItem(index)->parent();
	if( itemParent != rootItem ){
		TFileItem *itemParentParent = itemParent->itemParent;
		int n = itemParentParent->childCount();
		for( int i = 0; i < n; ++i )
			if( itemParentParent->children[i] == itemParent )
				return createIndex( i, 0, itemParent );	
	}
	
	return QModelIndex();
}

//
bool TFileModel::hasChildren(const QModelIndex &index) const
{
	TFileItem *tmp = getItem(index);
	return tmp->getFetched() ? tmp->childCount() : true;
}

//
TFileItem *TFileModel::getItem(QModelIndex index) const
{
	TFileItem *tmp = static_cast<TFileItem*>(index.internalPointer());
	return tmp ? tmp : rootItem;
}

////////////////////////////////////////////////////////////////////
// TFileProxyModel /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
bool TFileProxyModel::filterAcceptsRow( int source_row, const QModelIndex &source_parent ) const
{
	if( !filter ) return true;

	QModelIndex index = sourceModel()->index( source_row, 0, source_parent );
	TFileItem *fileItem = static_cast<TFileItem*>( index.internalPointer() );
	
	return fileItem->isDir() && !fileItem->isSymLink();
}

//
bool TFileProxyModel::canFetchMore(const QModelIndex &index) const
{
	return !getItemFromProxy(index)->getFetched();
}

//
void TFileProxyModel::fetchMore(const QModelIndex &index)
{
	QModelIndex sourceIndex = mapToSource(index);
	sourceModel()->fetchMore( sourceIndex );
}

//
bool TFileProxyModel::hasChildren(const QModelIndex &index) const
{
	if( canFetchMore(index) ){
		QModelIndex sourceIndex = mapToSource( index );
		return sourceModel()->hasChildren( sourceIndex );
	}
	else
		return rowCount(index) != 0;
}

//
TFileItem *TFileProxyModel::getItemFromSource(QModelIndex index) const
{
	TFileItem *tmp = static_cast<TFileItem *>(index.internalPointer());
	return tmp ? tmp : static_cast<TFileModel*>(sourceModel())->getRootItem();
}

//
TFileItem *TFileProxyModel::getItemFromProxy(QModelIndex index) const
{
	QModelIndex sourceIndex = mapToSource( index );
	return getItemFromSource( sourceIndex );
}

//
bool TFileProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
	QVariant leftData  = sourceModel()->data(left);
	QVariant rightData = sourceModel()->data(right);
	
	TFileItem *leftItem  = getItemFromSource(left)
			, *rightItem = getItemFromSource(right);
	
	QModelIndex sysParent = fileModel->myComputer().parent();
	if( sysParent == left.parent() || sysParent == right.parent() ){
		if( left.row()  == fileModel->myComputer().row() ) return true;
		if( right.row() == fileModel->myComputer().row() ) return false;

		if( left.row()  == fileModel->myDocuments().row() ) return true;
		if( right.row() == fileModel->myDocuments().row() ) return false;
	}
	
	if( leftItem->isDir() != rightItem->isDir() )
		return leftItem->isDir() > rightItem->isDir();

	switch( left.column() ){
		case TFileModel::NameColumn:
		case TFileModel::TypeColumn:
			return QString::compare( leftData.toString(), rightData.toString(), Qt::CaseInsensitive ) < 0;

		case TFileModel::SizeColumn:
			return leftItem->fileSizeInBytes() < rightItem->fileSizeInBytes();
		case TFileModel::DateColumn:
			return leftData.toDateTime() < rightData.toDateTime();
	};

	Q_ASSERT(false);
	return false;
}

//
void TFileProxyModel::setSourceModel( TFileModel *sourceModel )
{
	fileModel = sourceModel;
	QSortFilterProxyModel::setSourceModel( sourceModel );

	connect( fileModel, SIGNAL(pathAboutToBeUpdated(QString)), SIGNAL(pathAboutToBeUpdated(QString)) );
	connect( fileModel, SIGNAL(pathUpdated(QString)), SIGNAL(pathUpdated(QString)) );
}


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//
QModelIndex findChild( QAbstractItemModel *model, const QString &part, const QModelIndex &parent )
{
	int num = model->rowCount( parent );
	
QStringList lst;	
	for( int i = 0; i < num; ++i ){
		QModelIndex current = model->index( i, 0, parent );
		lst << model->data(current).toString();
	}
	
	
	for( int i = 0; i < num; ++i ){
		QModelIndex current = model->index( i, 0, parent );
		if( model->data(current).toString().compare( part, Qt::CaseInsensitive ) == 0 )
			return current;
	}
	return QModelIndex();
}

//
QModelIndex expandToPathInTreeView( QTreeView *view
								  , const QString &path
								  , const QString &myDocumentsPath
								  , QModelIndex myDocumentsIndex
								  , const QString &myDesktopPath
								  , QModelIndex myDesktopIndex
								  , QModelIndex myComputerIndex )
{
	QAbstractItemModel *model = view->model();
	
	QString newPath = QDir::fromNativeSeparators(path);
	
	QModelIndex begIndex = myComputerIndex;

	if( newPath.contains( myDocumentsPath, Qt::CaseInsensitive ) ){
		newPath.remove(   myDocumentsPath, Qt::CaseInsensitive );
		begIndex = myDocumentsIndex;
	}
	else 
		if( newPath.contains( myDesktopPath, Qt::CaseInsensitive ) ){
			newPath.remove(   myDesktopPath, Qt::CaseInsensitive );
			begIndex = myDesktopIndex;
		}
	
	QStringList	list = newPath.split( '/', QString::SkipEmptyParts );
	if( list[0].contains(':') )
		list[0] += ('/');

	view->expand( begIndex );
	
	QModelIndex current = begIndex, lastValid = begIndex;
	foreach( QString part, list ){
		model->fetchMore(current);
		current = findChild( model, part, current );
		if( !current.isValid() )
			break;
		view->expand( current );
		lastValid = current;
	}
	
	view->selectionModel()->setCurrentIndex( lastValid, QItemSelectionModel::ClearAndSelect );
	return lastValid;
}