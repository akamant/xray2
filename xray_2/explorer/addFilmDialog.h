#ifndef ADDFILMDIALOG_H
#define ADDFILMDIALOG_H

//
class TFileModel;
class TDicomDirItem;
class TDicomDirWidget;
struct THeaderSectionInfo;
class TFileSystemWidget;

////////////////////////////////////////////////////////////////////
// TAddFilmDialog //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
class TAddFilmDialog : public QMainWindow
{
	Q_OBJECT

public:
	TAddFilmDialog( QWidget *parent = 0, Qt::WindowFlags f = 0 );
	//
	void addTopLevelItem( TDicomDirItem *item );
	void setTopLevelHeaders( const QVector<THeaderSectionInfo> &header );
	void setRootPath( const QString &path );
	void clear();
	
signals:
	void signalFilesChosed(const QStringList &files);
	void signalGetFileFromServer(const QStringList &files);
	void signalDirChosed( QString dir );

private slots:
	void slotGetFilesFromServ();
	void slotAddSelected();

private:
	QAction				*addItems, *refresh;
	QTabWidget			*tabWidget;
	TFileSystemWidget	*fsw;
	TDicomDirWidget		*ddw;
	QStringList			filesToLoad;
	QPushButton			*buttonAdd;
	QLineEdit			* editAdd;
	QPushButton			*buttonExit;
	TFileModel			*fileModel;
	QString				rootPath;
};


#endif