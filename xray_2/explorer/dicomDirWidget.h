#ifndef DICOMDIRWIDGET_H
#define DICOMDIRWIDGET_H

//
class TFileModel;
struct THeaderSectionInfo;
class TDicomDirItem;
class TDicomDirModel;
class TDicomDirProxyModel;

////////////////////////////////////////////////////////////////////
// TAspectLabel ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
class TAspectLabel : public QLabel
{
	Q_OBJECT
public:
	TAspectLabel( QWidget *parent = 0, Qt::WindowFlags f = 0 );

public slots:
	void slotSetAspectPixmap( const QPixmap &pxmap );
	void slotSetText( const QString &text );

protected:
	virtual void paintEvent ( QPaintEvent * event );
	//
	QPixmap origPixmap;
};

////////////////////////////////////////////////////////////////////
// TDicomDirWidget /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
class TDicomDirWidget : public QWidget
{
	Q_OBJECT
public:
	TDicomDirWidget( TFileModel *model, QAction *act = NULL, QWidget *parent = NULL );
	//
	void addTopLevelItem( TDicomDirItem *item );
	void setTopLevelHeaders( const QVector<THeaderSectionInfo> &header );
	void clear();
	void getSelectedFiles( QStringList &list ) const;
	void expandToPath( const QString &path );

signals:
	void signalDirChosed( QString dir );

private slots:
	void slotDicomDirItemClicked( const QModelIndex &index );
	void slotFileItemChoosed( const QModelIndex &index );
	void resizeColumns();

private:
	void addPathFromItem( const TDicomDirItem *item, QStringList &list ) const;
	//	
	QAction *action;
	//
	QSplitter *splitter1
		    , *splitter2;
	QTreeView    *dirView;
	QTreeView    *dicomView;
	TFileModel	 *fileModel;
	TDicomDirModel		*dicomModel;
	TDicomDirProxyModel *dicomProxyModel;
	TAspectLabel *pixmapLabel;
};



#endif