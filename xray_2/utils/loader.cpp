#pragma once
#include "StdAfx.h"
#include "loader.h"
QString createFileName(int i)
{
	QString s;
	s.setNum(i);

	QString ss;
	switch (s.count())
	{
	case 1:
		ss.append(QString("000%1").arg(i));
		break;
	case 2:
		ss.append(QString("00%1").arg(i));
		break;
	case 3:
		ss.append(QString("0%1").arg(i));
		break;
	default:
		ss.append(QString("%1").arg(i));
		break;
	}

	return ss;
}
Loader::Loader(){
}

QStringList Loader::getListFiles(const QString & filePath){
	QStringList listFiles;
	QFile file(filePath);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return listFiles;
	QTextStream in(&file);	
	while (!in.atEnd()) {
		QString line = in.readLine();
		if(line.indexOf("//") != 0){
			QFile file(line);
			if(file.exists())
				listFiles << line;
		}
	}

	return listFiles;
}