#include "stdafx.h"
#include "toolbuttonwithmenu.h"

ToolButtonWithMenu::ToolButtonWithMenu(QActionGroup * actionGroup, QWidget *parent)
	: QToolButton(parent)
	, activeAction(NULL)
{
	if(actionGroup->actions().size())	
		changeSplitMenu(actionGroup->actions()[0]);

	
	QPen pen;
	QPalette palette;
	palette.setColor( QPalette::Background, QColor( 255, 0, 0 ) );
	palette.setColor( QPalette::Text, QColor( 255, 0, 255 ) );
	palette.setColor( QPalette::Foreground,  QColor( 255, 255, 0 ) );	
	setPalette( palette );
	setPopupMode(QToolButton::InstantPopup);


	splitMenu = new QMenu();

	splitMenu->addActions(actionGroup->actions());		

	connect	(	splitMenu,	SIGNAL	( triggered			( QAction * )	),
				this,		SLOT	( changeSplitMenu	( QAction * )	)
			);
		

	this->setMenu(splitMenu);
}

ToolButtonWithMenu::~ToolButtonWithMenu()
{

}
void ToolButtonWithMenu::changeSplitMenu(QAction * act)
{
	setIcon(act->icon());
	setText(act->text());
	activeAction = act;
}

int ToolButtonWithMenu::count()
{	
	if(activeAction)
		return splitMenu->actions().lastIndexOf(activeAction);
	return -1;
}




