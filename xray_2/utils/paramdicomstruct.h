#ifndef PARAMDICOMSTRUCT_H
#define PARAMDICOMSTRUCT_H

#include "dicomData.h"
#include <QObject>

class ParamDicomStruct : public DicomData
{	

public:
	ParamDicomStruct(QString _ID);
	~ParamDicomStruct();

	ViewParameters viewParameters(){return _viewParameters;};
private:
	ViewParameters _viewParameters;
};

#endif // PARAMDICOMSTRUCT_H
