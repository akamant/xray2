#ifndef DICOMDATA_H
#define DICOMDATA_H

#include <QObject>
#include "dicomDict.h"
#include "elemCont.h"
#include "elementParser.h"
#include "dicomdata.h"
#include "normalizedImage.h"
#include "command.h"
#include "TLUT.h"
#include "planeInfo.h"
//#include "TLUT.h"

#define PATIENT 0
#define STUDY 1
#define SERIES 2
#define IMAGE 3
#define MONOCHROME1 1
#define MONOCHROME2 2
#define PALETTE_COLOR 3
#define RGB_INTERPETATION 4
#define ARGB_INTERPETATION 5
#define YBR_FULL 6
#define YBR_FULL_422 7
#define YBR_PARTIAL_422 8
#define YBR_ICT_422 9
#define YBR_RCT_422 10

#define ExplicitVRBigEndian 0
#define ExplicitVRLittleEndian 1
#define ImplicitVRBigEndian 2
#define ImplicitVRLittleEndian 3
#define RLELossless 4
#define JPEGBaseline 5
#define JPEGExtended 6
#define JPEGLossless 7
#define JPEGLosslessFirstOrderPrediction 8
#define JPEGLSLossless 9
#define JPEGLSNearLossless 10
#define JPEG2000Lossless 11
#define JPEG2000LosslessAndLossy 12

#define NotMediaStorageDirectoryStorage 0
#define MediaStorageDirectoryStorage 1


#define ID_ADDIMAGETOPRINT 15005
#define ID_ADDIMAGETOPRINTSERIES 15006
#define ID_SAVEALLSTUDYTOJPEG 15007
#define ID_SAVEALLSTUDYTODICOM 15008
//#define ID_DOMPR 15008
#define ID_DELETE 15010
#define ID_DELETE_ALL 15011

#define PortraitFilm 2
#define LandscapeFilm 3

BOOLEAN operator != (RECT , RECT);

#pragma pack(push, _Struct, 1) // ���������� ������� ������������ � 1 ����

struct MYPOINT{
	double x;
	double y;
	MYPOINT(){};
	MYPOINT(double _x, double _y):x(_x), y(_y){};	
	bool operator == ( const MYPOINT &p ) const { return x == p.x && y == p.y; }
	bool operator != ( const MYPOINT &p ) const { return !operator==(p); }
};

struct DIAGN_VidarMSG {

public:
	////////////////////////////////////////////////////////////
	/////////////////////// Variables //////////////////////////
	////////////////////////////////////////////////////////////
	unsigned short  Cmd;
	char            Str1[256];
	char            Str2[256];
};

struct TimeDate
{
	unsigned short tm_sec;    // �������
	unsigned short tm_min;    // ������
	unsigned short tm_hour;   // ���
	unsigned short tm_mday;   // ����
	unsigned short tm_mon;    // �����
	unsigned short tm_year;   // ���
}; 
struct FileHeader{
	char  VidarName[6];	//  {'W','I','N','V','D','R'};
	char TypeFormat; 		// 0 - �� �����������
	int NumberPatient;     //  ����� ��������
	TimeDate TimDat; // ��. ����
	short FlArrow;           // = TRUE ���� �� ����������� ���� ������� ���� �������
	short FlDimen;           // = TRUE ���� �� ����������� ���� ������� ���� ����������
	short FlText;            // = TRUE ���� �� ����������� ���� ������� ���� ����
	short FlRect;            // = TRUE ���� �� ����������� ���� ������� ���� 4-� ��������
	short FlEllips;          // = TRUE ���� �� ����������� ���� ������� ���� ������
	short FlArea;           // = TRUE ���� �� ����������� ���� ������� ����  �����- ��������
	short FlAngle;           // = TRUE ���� �� ����������� ���� ������� ���� ����	
	//char Rezerv2[31];
	unsigned char Rezerv[512];
	//char Rezerv3[1];
	unsigned short x;       // ������ ����������� - ������;
	unsigned short y;       // ������ ����������� - ������;

	unsigned char BPerPix;// ��� ����������, = 0 -> 256-�� ������� ��������
	//			  = 1 -> 2_���� �� ������ ��������
	//			  = 3 -> ������� three-color
	short new_x;   // ���������� ������
	short new_y;   // ���������� ������

	unsigned char Orient; // 0 = ���������� (� ������� ����������� ������� �����, ������� � 0)
	// 1 = �������� (� �������  �������� ������� �����, ������� � y-1) ��� �
	// DIB �������

};

struct ImageLow{
	BYTE Low; 		// ����� ����������������/�������
	//	 = 0  - �������� ��  Min �� �ax
	//  = 1  - �������� ��  Left �� Right
	//  = 2  - �������-��������  Min -  Left - �ax - Right

	short Min;   		// �������  !!!������ ��� 256-��������
	short Max;   		// �������� !!!������ ��� 256-��������
	short Left;  		//  �� Low !!!������ ��� 256-��������
	short Right; 		//  �� Low !!!������ ��� 256-��������
	short CountContr;   //  ���������� ���������
	short CountBritness;//  ���������� �������
	short CountLevel;   //  ���������� ������
	int  Hist[256];  //  ����������� !!!������ ��� 256-�������� �
	//  �������� 	�������������  2BYTE per PIX
};

///////////////////////////////////////////////////////////
//����� ������  ��������� ImagePar

struct ImagePar{
	float	ScaleX;	// ������� ��������� �� X
	float	ScaleY;  	// ������� ��������� �� Y
	// ����� ����� ���������, ������������ ������ ��� 2BYTE per Pix
	// ��� �������� 	�������������  2BYTE per PIX � ���� BYTE per PIX
	short  WindowWidth;      // ������ ����
	short   WindowCenterPos;  // ��������� ������ ����
	short   GMax;			// ������� ������������� � �����������
	short   GMin;             // �������� ������������� � �����������
	unsigned char  NumOfColor;       // ���������� ������, ���������� ��� ����
	unsigned char  D_Color;          // ���������� ������, ���������� ��� ��� ��������� (��� ����)
	unsigned char  Rezerved[22];     //- ����������������
};
struct ImageHeader{	
	short x;       // ������ ����������� - ������;
	short y;       // ������ ����������� - ������;


	char BPerPix;// ��� ����������, = 0 -> 256-�� ������� ��������
	//			  = 1 -> 2_���� �� ������ ��������
	//			  = 3 -> ������� three-color
	short new_x;   // ���������� ������
	short new_y;   // ���������� ������

	char Orient; // 0 = ���������� (� ������� ����������� ������� �����, ������� � 0)
	// 1 = �������� (� �������  �������� ������� �����, ������� � y-1) ��� �
	// DIB �������
};
#pragma pack(pop, _Struct) // ������������ �� �������


class RepresentationParameters{
public:
	int sharping;
	int positionX;	
	int positionY;
	int deltaXcenter;
	int deltaYcenter;
	float windowValue;
	float levelValue;
	float windowValueConst;
	float LevelValueConst;
	float relativeDeltaWidthSum;
	float relativeDeltaHeightSum;
	double scale;	
	int numberOfRotations;
	int numberOfHorTurns;
	int numberOfVerTurns;	
	BOOLEAN isReflected;
	RepresentationParameters();
	bool operator== (const RepresentationParameters& other) const{
		if(this->sharping == other.sharping &&
			this->positionX == other.positionX &&	
			this->positionY == other.positionY &&
			//	this->deltaXcenter == other.deltaXcenter &&
			//	this->deltaYcenter == other.deltaYcenter &&
			this->windowValue == other.windowValue &&
			this->levelValue == other.levelValue &&
			this->windowValueConst == other.windowValueConst &&
			this->LevelValueConst == other.LevelValueConst &&
			this->relativeDeltaWidthSum == other.relativeDeltaWidthSum &&
			this->relativeDeltaHeightSum == other.relativeDeltaHeightSum &&
			this->scale == other.scale &&	
			this->numberOfRotations == other.numberOfRotations &&
			this->numberOfHorTurns == other.numberOfHorTurns &&
			this->numberOfVerTurns == other.numberOfVerTurns &&	
			this->isReflected == other.isReflected
			) return true;
		else return false;
	};
	bool operator != (const RepresentationParameters& other) const{	return !operator==(other);}; 

	//RepresentationParameters();	
};

struct Indentifying{
	//int modality;
	char * modalityStr;
	char * manufacturersModelName;
	char * seriesDescripion;
	TimeDate acquisitionTimeDate;

};

struct ImageParameters{
	int PhotometricInterpretation;	
	float rescaleIntercept;
	float rescaleSlope;	
	char planarConfiguration;
	int bitsAllocated;
	int bitsStored;
	int highBit;
	int samplesPerPixel;
	BOOLEAN pixelRepresentation;
	short width;
	short height;

};

struct Acquisitions{	
	float sliceThickness;
	int KVP;	
	int exposureTime;
	int xRayTubeCurrent;
	char * bodyPartExamined;

};

struct Relationship{
	int studyID;  	
	int instanceNumber;
	int frameNumber;
	int	acquisitionNumber;
	int seriesNumber;
	char * StudyInstanceUID;
	//char * SeriesInstanceUID;	
	float sliceLocation;
	//char * laterality;
};

struct PatientInformation{
	TimeDate birthDate;

};

struct BasicDirectoryInformation{	
	QString referencedFileID;
};

class BodyCreatedEmiter;
class VAbstractGraphicItem;
class DicomData;

class ViewParameters
{
public:
	ViewParameters();
	~ViewParameters();
	void setWLVector( );
	void setDicomData( DicomData * dicomData );
	void resetParameters();
	void processCommand(const Command & command );

	QTransform transform(){return _transform;};
	float scale(){return _scale;};
	QVector2D relativeDisplacement(){return _relativeDisplacement;};
	QVector2D WLVector(){return _WLVector;};
	bool isViewNegative(){return _isViewInNegative;};
		
private:
	DicomData * _dicomData;
	QTransform _transform;
	float _scale;
	QVector2D _relativeDisplacement;
	QVector2D _WLVector;
	bool _isViewInNegative;
};


class DicomData
{

private:	
	VNormalizedImage * normalizedImage;
	BOOLEAN * isNormalized;
	void * body;	
	void * normImageBody;	
	
	QMutex * mutex;
	BodyCreatedEmiter * emiter;
	QString ID;
	int a;
	void * pImage;
	
	BOOLEAN isBlocked;
	
	QString UID;
	
	int priority;
	
	BOOLEAN isChosen;
	BOOLEAN nonChosenAllInGroup;
	
	int sizeBody;
	unsigned int sizeImage;
	unsigned int  sizeFile;

public:
	//DicomData(QObject *parent);
	TWLLUT wlLUT;
	TElemCont  ELEM_CONT;
	QList <VAbstractGraphicItem *> graphicsItems;
	DicomData( QString _ID );


	DicomData(const DicomData& DicomData);
	DicomData(const DicomData& DicomData, void * body, int sizeImage);


	void setNormalizedImage(VNormalizedImage * _normalizedImage){QMutexLocker locker(mutex); normalizedImage = _normalizedImage;};
	VNormalizedImage * getNormalizedImage(){QMutexLocker locker(mutex); return normalizedImage;};

	void setNormImageBody(void * body){QMutexLocker locker(mutex); normImageBody = body;};
	void * getNormImageBody(){ QMutexLocker locker(mutex); return normImageBody;}

	void setBody(void * body, int sizeBody);
	void * getBody(){QMutexLocker locker(mutex); return body; };

	void setID( QString _ID){QMutexLocker locker(mutex); ID = _ID;};
	QString getID(){QMutexLocker locker(mutex); return this->ID;};

	void setisNormalized(bool isNorm) { QMutexLocker locker(mutex); *isNormalized = isNorm;};
	bool getisNormalized() { QMutexLocker locker(mutex); return *isNormalized;};

	TElemCont & getELEM_CONT() { QMutexLocker locker(mutex); return ELEM_CONT;};

	int getDataTransfer(){QMutexLocker locker(mutex); return transferSyntax;};	

	~DicomData();

	BodyCreatedEmiter * getEmiter();

	ImageParameters imageParameters;

	RepresentationParameters representationParameters;
	int numberOfRotations;
	int numberOfHorTurns;
	int numberOfVerTurns;	
	float	ScaleX;	// ������� ��������� �� X
	float	ScaleY;

	char orient;
	char orientHor;	
	int maxValue;	
	int minValue;	

	BOOLEAN isVDRFile;
	QString idFile;
	QString filePath;
	int transferSyntax;
	int MediaStorageSOPClassUID;
	BOOLEAN withoutErrors;
	QString descriptionError;

	void doEmptyTags();
	//void setImageBuffer(void * imageBuffer, int sizeImage);
	/////////////////////////////////
	TPlaneInfo planeInfo;
	void fillPlaneInfo();
	
	
	//void clearAllMarks(){distSizes.clear(); areas.clear(); textMarks.clear(); angles.clear(); ovals.clear();}	
	/*std::vector<TPoint> distSizes;
	std::vector<MprPlane> mprPlanes;

	std::vector<Area> areas;
	//std::vector<TextMark> textMarks;
	QList<TextMark> textMarks;
	std::vector<Angle> angles;
	std::vector<ExtAngle> extAngles;
	std::vector<Oval> ovals;*/
};


static int getSizePixel (DicomData * dicomStruct){
	int value = 1;
	if((dicomStruct->imageParameters.bitsAllocated + 7) / 8 > 2)
		value = 4;
	if((dicomStruct->imageParameters.bitsAllocated + 7) / 8 != 0){
		value = (dicomStruct->imageParameters.bitsAllocated + 7) / 8;
	}else{
		value = (dicomStruct->imageParameters.bitsStored + 7) / 8;
	}
	return value;
}

static int getSizePixel (int bitsAllocated, int bitsStored){
	int value = 1;
	if((bitsAllocated + 7) / 8 > 2)
		value = 4;
	if((bitsAllocated + 7) / 8 != 0){
		value = (bitsAllocated + 7) / 8;		
	}else{
		value = (bitsStored + 7) / 8;
	}
	return value;
}

class BodyCreatedEmiter	 : public QObject
{
	Q_OBJECT
signals:
	void bodyCreated(DicomData *);
public:
	BodyCreatedEmiter(DicomData * _dicomData) : dicomData(_dicomData){		};
	void emitBodyCreateSignal();
private:
	DicomData * dicomData;
};


#endif // DICOMDATA_H
