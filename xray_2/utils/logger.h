#ifndef VLOGGER_H
#define VLOGGER_H

#include <QObject>
#include <QFile>

class VLogger : public QObject
{
	Q_OBJECT

public:
	VLogger(QObject *parent = NULL);
	~VLogger();


void operator << (const QString & );
static void myMessageOutput(QtMsgType type, const char *msg);
private:
static QFile curDayFile;
};
//void myMessageOutput(QtMsgType type, const char *msg);
#endif // VLOGGER_H
