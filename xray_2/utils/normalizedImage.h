#pragma once
class VNormalizedImage
{
public:
	VNormalizedImage(char * bitmap, int width, int height, int colorsPerPixel, int bytePerColor, float rescaleSlope = 1, int rescaleIntercept = 0, bool colorPresentation = false);
	VNormalizedImage();
	VNormalizedImage(const VNormalizedImage& normImage);
	//VNormalizedImage(int * _bitmap, int _width, int _height, int _colorsPerPixel, int _bytePerColor, float _rescaleSlope /*= 1*/, int _rescaleIntercept /*= 0*/ , bool _colorPresentation/* = 0*/);
//	VNormalizedImage(const VNormalizedImage& normImage, char * bitmap, int * _bitmap, int _width, int _height);
	VNormalizedImage(const VNormalizedImage& normImage, char * _bitmap, int * bitmapSrc, int _width, int _height);
	~VNormalizedImage(void); 
	char * bitmap;	
	int getWidth();
	int getHeight();
	int getColorsPerPixel();
	int getBytePerColor();
	QRect significantRect;
	void * getBitmap();	
	void grabRegion(VNormalizedImage * normalizedImage, int x, int y, int widthSrc, int heightSrc, int widthDst, int heightDst, bool isQuickly);	
	void setBitmap(char * bitmap, int width, int height, int colorsPerPixel, int bytePerColor, float rescaleSlope = 1,	int rescaleIntercept = 0);
	void setBitmap(char * bitmap, int width, int height);
	void setBitmap(char * bitmap);	
	int getValue(unsigned int offset);
	void setValue( unsigned int offset, int value);
	bool operator== (const VNormalizedImage& other) const;
	bool operator!= (const VNormalizedImage& other) const;
	
private:
	//void ScaleNearest(char * dist, int widthDst, int heightDst, int widthSrc, int heightSrc, int xDst, int yDst);
	void ScaleNearest(char * dist, int widthDst, int heightDst, int xDst, int yDst, int widthSrc, int heightSrc);
	void copyRegionFromImage(char * dst, int left, int top, int newWidth, int newHeight);		
	int colorsPerPixel;
	bool isUpdated;
	int bytePerColor;	
	float rescaleSlope;
	int rescaleIntercept;
	bool colorPresentation;
	unsigned int oldBitmapSize;
	int width;
	int height;
	int prevX;
	int prevY;
	VNormalizedImage * prevNormalizedImageSrc;
	QByteArray prevByteArray;
	unsigned int newBitmapSize;
	int offsetImage;
};

