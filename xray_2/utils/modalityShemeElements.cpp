#include "stdafx.h"
#include "ModalityShemeElements.h"

ModalityShemeElements::ModalityShemeElements(QObject *parent)
	: QObject(parent)
{

}

QVariant ModalityShemeElements::modalityElement(QString modality, QString attribute, uint level)	
{
	return QVariant();
}


void ModalityShemeElements::serializeObjectTree(QObject * topObject, QString modality)
{
	QDomDocument doc("Sheme");
	
	QDomElement root = doc.createElement("modality");

	root.setAttribute("name", modality);

	doc.appendChild(root);
	serializeObject(doc, root, topObject);


	QFile output("note.xml");
	output.open(QIODevice::WriteOnly);
	QTextStream stream(&output);
	doc.save(stream, 2);
}

 void ModalityShemeElements::serializeObject(QDomDocument &doc, QDomNode & docNode, QObject * object)
{
	QDomElement el = doc.createElement(object->metaObject()->className());

	docNode.appendChild(el);

	bool haveXrayProp = false;
	for(int i = 0; i < object->metaObject()->propertyCount(); i++)
	{
		QMetaProperty prop = object->metaObject()->property(i);
		QString propName = prop.name();
		if(propName == "objectName" )
			continue;
		if(propName.contains("xray_")){
			QVariant value = object->property(propName.toAscii().data());		
			el.setAttribute(propName.remove("xray_"), value.toString());
			haveXrayProp = true;
		}
		
	}

	//if(haveXrayProp)
	//	docNode.appendChild(el);

	for (int i = 0; i < object->children().size(); i++)
	{
		//if(haveXrayProp)
			serializeObject(doc, el, object->children()[i]);
		//else
		//	serializeObject(doc, docNode, object->children()[i]);
	}
}

ModalityShemeElements::~ModalityShemeElements()
{

}
