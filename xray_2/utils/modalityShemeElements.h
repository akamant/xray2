#ifndef ModalityShemeElements_H
#define ModalityShemeElements_H

#include <QObject>

class ModalityShemeElements : public QObject
{
	Q_OBJECT

public:
	ModalityShemeElements(QObject *parent = NULL);
	~ModalityShemeElements();
	QVariant modalityElement(QString modality, QString attribute, uint level);
	static void serializeObject(QDomDocument &doc, QDomNode & docNode, QObject * object);
	static void serializeObjectTree(QObject * topObject, QString modality);
private:
	
};

#endif // ModalityShemeElements_H
