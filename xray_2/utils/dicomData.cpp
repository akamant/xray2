#include "stdafx.h"
#include "dicomdata.h"
#include "command.h"

RepresentationParameters::RepresentationParameters():
numberOfRotations (0)
,numberOfHorTurns (0)
,numberOfVerTurns (0)	
,isReflected (false)
,deltaXcenter (0)
,deltaYcenter (0)	
,levelValue (-999999)
,windowValue (-999999)
,positionX (0)
,positionY (0)
,scale (1)
,sharping (0)
,relativeDeltaWidthSum (0)
,relativeDeltaHeightSum (0)
{

}

DicomData::~DicomData()
{
	delete mutex;
	delete isNormalized ;
	delete emiter;

	delete normalizedImage;	
	delete body;	
	delete normImageBody;	

	delete pImage;
}
// 
// QString DicomData::getID()
// {
// 	return ID;
// }

DicomData::DicomData( QString _ID )
	: transferSyntax(-1)
	
	,ID(_ID)
	,body(NULL)
	,normalizedImage(NULL)
	,normImageBody(NULL)	
	//,normImageRegion(NULL)
	,isBlocked(false)
	,idFile("")
	,descriptionError("")	
	,pImage(NULL)	
{
	mutex = new QMutex();
	QMutexLocker locker (mutex);
	emiter = new BodyCreatedEmiter(this);
	QVariant var;	
	UID =  QDateTime::currentDateTime().toString("ddMMyyyy_hhmmsszzz") + QString("_%1").arg(qrand());
	var.setValue(-1);
	{	
		TElement newElem( 0x00010007, var);
		ELEM_CONT.add(0x001, 0x007, newElem);
	}
	var.setValue(0);
	TElement newElem( 0x00200013, var);
	ELEM_CONT.add(0x0020, 0x0013, newElem);	
	isNormalized = new BOOLEAN;
	*isNormalized = false;
	sizeImage = 0;
	sizeFile = 0;
	body = NULL; 	
	priority = 0;
	isVDRFile = false;
	var.setValue(QString("unknown"));
	{
		TElement newElem( 0x000800060, var);
		ELEM_CONT.add(0x0008, 0x0060, newElem);	
	}
	maxValue = 255;

	var.setValue(QString("noNameStudy"));
	{	
		TElement newElem( StudyUID, var);//SeriesInstanceUID
		ELEM_CONT.add(0x0020, 0x000D, newElem);
	}

	var.setValue(QString("noNameSeries"));
	{	
		TElement newElem( SeriesUID, var);//SeriesInstanceUID
		ELEM_CONT.add(0x0020, 0x000E, newElem);
	}
	var.setValue(QString("noPatientName"));
	{	
		TElement newElem( 0x00100010, var);//patientsName
		ELEM_CONT.add(0x0010, 0x0010, newElem);
	}
	var.setValue(QString("NOID"));
	{	
		TElement newElem( 0x00100020, var);//patientId
		ELEM_CONT.add(0x0010, 0x0020, newElem);
	}

	var.setValue(QString("NOSEX"));
	{	
		TElement newElem( 0x00100040, var);//patientId
		ELEM_CONT.add(0x0010, 0x0040, newElem);
	}	
	numberOfRotations = 0;
	numberOfHorTurns = 0;
	numberOfVerTurns = 0;
	orient = 1;
	ScaleX = 0;
	ScaleY = 0;	
	var.setValue(-1);
	{	
		TElement newElem( 0x00010007, var);//SeriesInstanceUID
		ELEM_CONT.add(0x0001, 0x0007, newElem);
	}
	var.setValue(-1);
	{	
		TElement newElem( 0x00010007, var);//SeriesInstanceUID
		ELEM_CONT.add(0x0001, 0x0007, newElem);
	}	
	var.setValue(0);
	{	
		TElement newElem( 0x00200013, var);//SeriesInstanceUID
		ELEM_CONT.add(0x0020, 0x0013, newElem);
	}
	representationParameters.deltaXcenter = 0;
	representationParameters.deltaYcenter = 0;	
	representationParameters.levelValue = 0;
	representationParameters.windowValue = 0;
	representationParameters.positionX = 0;
	representationParameters.positionY = 0;
	representationParameters.scale = 1;
	representationParameters.isReflected = false;
	imageParameters.samplesPerPixel = 1;
	imageParameters.pixelRepresentation = 0;
	imageParameters.planarConfiguration = 0;
	imageParameters.rescaleIntercept = 0;
	imageParameters.rescaleSlope = 0;	
	imageParameters.width = 0;
	imageParameters.height = 0;	
	imageParameters.bitsAllocated = 0;
	imageParameters.bitsStored = 0;
	imageParameters.bitsAllocated = 0;
	withoutErrors = true;
	body = NULL;	
	isChosen = false;
	nonChosenAllInGroup = true;	
	MediaStorageSOPClassUID = NotMediaStorageDirectoryStorage;	
	//!MEMManager::addDicomStruct(this); 

}


DicomData::DicomData(const DicomData& dicomData, void * body, int sizeImage)
{	
	QMutexLocker locker (dicomData.mutex);
	*this = dicomData;
	mutex = new QMutex();
	QMutexLocker lockerOwn(mutex);
	emiter = new BodyCreatedEmiter(this);
	isNormalized = new BOOLEAN;	
	*isNormalized = false;
	normalizedImage = NULL;
	normImageBody = NULL;

	pImage = NULL;
	this->body = NULL;
	
	
}

DicomData::DicomData(const DicomData& dicomData)
{	
	QMutexLocker locker (dicomData.mutex);
	*this = dicomData;	
	mutex = new QMutex();
	QMutexLocker lockerOwn (mutex);
	emiter = new BodyCreatedEmiter(this);
	isNormalized = new BOOLEAN;	
	*isNormalized = false;
	this->body = NULL;
	normalizedImage = NULL;
	normImageBody = NULL;
	pImage = NULL;	

}


void DicomData::doEmptyTags(){
	if(ELEM_CONT.get(SeriesUID).toString() == QString("noNameSeries"))
	{		
		QVariant var;						
		var.setValue(QString("NoUID, number =") + ELEM_CONT.get(0x00200011).toString());
		TElement newElem( SeriesUID, var);
		ELEM_CONT.add(0x0020, 0x000E, newElem);	
	}
	if(representationParameters.windowValue == 0 && imageParameters.bitsStored == 8){
		representationParameters.windowValue = 255;		
		representationParameters.levelValue = 127;
		QVariant var;
		var.setValue(representationParameters.windowValue);
		TElement newElem( 0x00281051, var);
		ELEM_CONT.add(0x0028, 0x1051, newElem);
		var.setValue(representationParameters.levelValue);
		TElement newElem2( 0x00281050, var);
		ELEM_CONT.add(0x0028, 0x1050, newElem2);
	}
	representationParameters.LevelValueConst = representationParameters.levelValue;
	representationParameters.windowValueConst = representationParameters.windowValue;

	if(QString("noNameStudy") == ELEM_CONT.get(StudyUID).toString()){
		QString separator("_");
		QString a = ELEM_CONT.get(0x00080020).toString();
		a.replace(QString(":"), QString(" "));

		QString a0 = ELEM_CONT.get(StudyUID).toString();
		QString a1 = ELEM_CONT.get(0x00100010).toString();
		QString a2 = ELEM_CONT.get(0x00100020).toString();
		QString a3 = ELEM_CONT.get(0x00080030).toString();
		QString a4 = ELEM_CONT.get(0x00080020).toString();

		QString valueVar = a0 + separator +  
			a1 + separator  +  a2 + separator  + idFile + separator + a3 + separator
			+ a4 + separator;
		QString valueVar2 = a0 + separator + a1 + separator + idFile;
		QString valueVar3 = a0 + separator + idFile + separator + a1;
		//QString x("123 ");
		//QString y("123");
		//QString z = x + x + y + y;
		valueVar = valueVar.replace(QString("\\"), QString(" "));
		valueVar = valueVar.replace(QString("/"), QString(" "));
		QVariant var(valueVar);
		//	var.replace(QString("\\"), QString(" ").replace(QString("/"), QString(" ")
		TElement newElem( StudyUID, var);
		ELEM_CONT.add(0x0020, 0x000D, newElem);
	}

	QVariantList list = ELEM_CONT.get(0x00181164).toList();
	if(list.size() > 0)		
		ScaleX = list[0].toFloat();
	if(list.size() > 1)		
		ScaleY = list[1].toFloat();
	list = ELEM_CONT.get(0x00182010).toList();
	if(list.size() > 0)		
		ScaleX = list[0].toFloat();
	if(list.size() > 1)		
		ScaleY = list[1].toFloat();
	list = ELEM_CONT.get(0x00280030).toList();
	if(list.size() > 0)		
		ScaleX = list[0].toFloat();
	if(list.size() > 1)		
		ScaleY = list[1].toFloat();
	////////////////////////////
	fillPlaneInfo();
}

void DicomData::fillPlaneInfo()
{
	if(		ELEM_CONT.hasElement(0x00200032) 
		&&	ELEM_CONT.hasElement(0x00200037)
		)
	{
		QVariantList posList	= ELEM_CONT.get( 0x00200032 ).toList()
			,		orientList = ELEM_CONT.get( 0x00200037 ).toList();

		planeInfo.pos		= QVector3D( posList[0].toFloat(),	posList[1].toFloat(),	 posList[2].toFloat() );
		planeInfo.orientRow = QVector3D( orientList[0].toFloat(), orientList[1].toFloat(), orientList[2].toFloat() );
		planeInfo.orientCol = QVector3D( orientList[3].toFloat(), orientList[4].toFloat(), orientList[5].toFloat() );
		planeInfo.normal	= QVector3D::crossProduct( planeInfo.orientRow, planeInfo.orientCol );
	}	

	if(ELEM_CONT.hasElement(0x00180050))
		planeInfo.thickness = ELEM_CONT.get( 0x00180050 ).toFloat();
	if(ELEM_CONT.hasElement(0x00280030))
		planeInfo.spacing	= ELEM_CONT.get( 0x00280030 ).toFloat();
	if(ELEM_CONT.hasElement(0x00280011))
		planeInfo.width		= ELEM_CONT.get( 0x00280011 ).toUShort() * planeInfo.spacing;
	if(ELEM_CONT.hasElement(0x00280010))
		planeInfo.height	= ELEM_CONT.get( 0x00280010 ).toUShort() * planeInfo.spacing;
}

void DicomData::setBody( void * body, int sizeBody )
{
	mutex->lock();
	if(this->body != NULL)
		delete this->body;

	this->body = body;
	this->sizeBody = sizeBody;
	mutex->unlock();
	emiter->emitBodyCreateSignal();
}

BodyCreatedEmiter * DicomData::getEmiter()
{
	QMutexLocker locker(mutex);
	return emiter;
}

void BodyCreatedEmiter::emitBodyCreateSignal(){

	emit bodyCreated(dicomData);	
	//qDebug() << "bodyCreate signal emited";
}

ViewParameters::ViewParameters()
	:_dicomData(NULL)
{
	resetParameters();
}

void ViewParameters::setWLVector( )
{
	if(_dicomData)
	{
		_WLVector.setX(_dicomData->representationParameters.windowValue);
		_WLVector.setY(_dicomData->representationParameters.levelValue);
	}
}

void ViewParameters::resetParameters()
{
	setWLVector();
	_scale = 1;
	_relativeDisplacement = QVector2D(1, 1);
	_transform.reset();
	_isViewInNegative = false;
}

ViewParameters::~ViewParameters()
{

}
void ViewParameters::processCommand(const Command & command )
{

	switch(command.commandName)
	{
	case Command::translate:
		{				
			_relativeDisplacement = command.vector2DPar;
			_scale = command.floatPar;
			break;
		}
	case Command::contrast:
		{
			//presentImageWithWL(command.vector2DPar);		
			_WLVector = command.vector2DPar;
			break;
		}
	case Command::rotate:
		{
			_transform.rotate(command.intPar);
		
			break;
		}

	case Command::turn:
		{
			_transform.scale(command.vector2DPar.x(), command.vector2DPar.y());
			//_transform.rotate(command.intPar);
			break;
		}
	case Command::negative:
		{
 			if(_isViewInNegative)
 				_isViewInNegative = false;
 			else 
 				_isViewInNegative = true;
			break;
		}

	case Command::reset:
		{
			resetParameters();
			break;
		}
	
		

	}	
}

void ViewParameters::setDicomData( DicomData * dicomData )
{
	bool isFirstData = false;
	if(!_dicomData)
		isFirstData = true;
	_dicomData = dicomData; 
	if(isFirstData)
		setWLVector();
}
