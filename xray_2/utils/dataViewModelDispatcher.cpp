#include "stdafx.h"
#include "dataviewmodeldispatcher.h"
#include "TreeDataViewModel.h"
#include "treedatamodel.h"


DataViewModelDispatcher::DataViewModelDispatcher( TreeDataViewModel * _model, QObject *parent )
	: model (_model)
{	
	connect (	this,		SIGNAL	( changeDataModelIndexSignal	(QModelIndex, QModelIndex ) ),
				model,		SLOT	( changeDataModelAndFillIndex	(QModelIndex, QModelIndex ) )
		);

	connect (	this,		SIGNAL	( selectNextChildrenSignal		(QModelIndex ) ),
				model,		SLOT	( selectNextChildren			(QModelIndex ) )
		);

	connect (	this,		SIGNAL	( selectPrevChildrenSignal		(QModelIndex ) ),
				model,		SLOT	( selectPrevChildren			(QModelIndex ) )
		);
	connect (	this,	SIGNAL	( addNewBranchToParentSignal	(QModelIndex, QModelIndex ) ),
				model,	SLOT	( addNewBranchToParent			(QModelIndex, QModelIndex ) )
		);

	connect (	this,	SIGNAL	( setDivisionForItemSignal		(QModelIndex, QVector2D ) ),
				model,	SLOT	( setDivisionForItem			(QModelIndex, QVector2D ) )
		);

	connect (	this,	SIGNAL	( selectNextPrevToLowLevelSignal		(TreeDataViewModel::Direction, QModelIndex ) ),
				model,	SLOT	( selectNextPrevToLowLevel				(TreeDataViewModel::Direction, QModelIndex ) )
		);
	

	TreeDataModel * dataModel = model->getDataModel();

	connect (	this,		SIGNAL	( removeBranchByIndexFromDataModelSignal(QModelIndex ) ),
				dataModel,	SLOT	( removeBranchByIndex					(QModelIndex ) )
		);
	model->setDispatcher(this);
	
}

DataViewModelDispatcher::~DataViewModelDispatcher()
{

}

void DataViewModelDispatcher::changeDataModelIndex( QModelIndex index1, QModelIndex index2 )
{
	emit changeDataModelIndexSignal(index1, index2);
}

void DataViewModelDispatcher::removeBranchByIndexFromDataModel( QModelIndex index )
{


	//dataModel->removeBranchByIndex(index);

	emit removeBranchByIndexFromDataModelSignal(index);
}


void DataViewModelDispatcher::setDivisionForItem( QModelIndex index1, QVector2D div )
{
	emit setDivisionForItemSignal(index1, div);
}

void DataViewModelDispatcher::selectNextChildren( QModelIndex index )
{
	//emit selectNextChildrenSignal(index);
	
	emit selectNextPrevToLowLevelSignal(TreeDataViewModel::Forward, QModelIndex() );	
	//emit selectNextPrevToLowLevelSignal(TreeDataViewModel::Forward, QModelIndex() );	
	//emit selectNextPrevToLowLevelSignal(TreeDataViewModel::Forward, QModelIndex() );	
	//emit selectNextPrevToLowLevelSignal(TreeDataViewModel::Forward, QModelIndex() );	
}

void DataViewModelDispatcher::selectPrevChildren( QModelIndex index )
{
	//emit selectPrevChildrenSignal(index);

	emit selectNextPrevToLowLevelSignal(TreeDataViewModel::Back, QModelIndex() );	
}

void DataViewModelDispatcher::addNewBranchToParent( QModelIndex index1, QModelIndex index2 )
{
	emit addNewBranchToParentSignal (index1, index2);

	//emit selectNextPrevToLowLevelSignal(TreeDataViewModel::Back, QModelIndex() );	
}
