#ifndef PLANEINFO_H
#define PLANEINFO_H

/////////////////////////////////////////////////////////////////////
// TPlaneInfo ///////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
struct TPlaneInfo
{
	TPlaneInfo(): height(0) {}				//������� ������������� ���������
	//
	QVector3D pos, orientRow, orientCol, normal;
	float thickness
	,	  spacing
	,	  width
	,	  height;
};

/////////////////////////////////////////////////////////////////////

//��������� �����(������ �����) �� 2D-��������� ��������� p � ���������������� ����������
QVector3D vector3DFromPoint( const QVector2D &point, const TPlaneInfo &p );
QList<QVector3D> vector3DFromPoint( const QList<QVector2D> &ptList, const TPlaneInfo &p );

//��������� �����(������ �����) �� ���������������� ��������� � 2D-���������� ��������� p
QVector2D pointFromVector3D( const QVector3D &vec, const TPlaneInfo &p );
QList<QVector2D> pointFromVector3D( const QList<QVector3D> &vecList, const TPlaneInfo &p );

//����� ����� ����������� 2�� ����������, � 3D-����������� 
QList<QVector3D> planesInterseption3D( const TPlaneInfo &p1, const TPlaneInfo &p2 );

//����� ����� ����������� 2�� ����������, � 2D-����������� ������ ���������
QList<QVector2D> planesInterseption2D( const TPlaneInfo &p1, const TPlaneInfo &p2 );

//������� ������������� ��������� �� ����� ����������� �� �� ��������� p
TPlaneInfo makeAbstractPlane( const QList<QVector2D> &ptList, const TPlaneInfo &p );
TPlaneInfo makeAbstractPlane( const QVector2D &pt1, const QVector2D &pt2, const TPlaneInfo &p );

// ��������� plane
//imgPlane - ���������, �� ������� �������� ����� �����������
//pOldList, pNewList - ������ � ����� 2D ����� ����������� 
void modifyPlane( TPlaneInfo &plane, const TPlaneInfo &imgPlane
				, const QList<QVector2D> &pOldList, const QList<QVector2D> &pNewList );

//��������� ����� ������������ ���������
// > 0 - �� �������, ==0 - �� ���������, < 0 - ������ �������
// basePlane - ��������� �����, refPlane - ��������� ��� ���������
int pointPlaneRelation( const TPlaneInfo &basePlane, const TPlaneInfo &relPlane
					   , const QVector2D &point );

#endif	//PLANEINFO_H