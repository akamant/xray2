#ifndef DATAVIEWMODELDISPATCHER_H
#define DATAVIEWMODELDISPATCHER_H

#include <QObject>
#include "TreeDataViewModel.h"
class DataViewModelDispatcher : public QObject
{
	Q_OBJECT

public:
	DataViewModelDispatcher(TreeDataViewModel * model, QObject *parent = NULL);
	~DataViewModelDispatcher();


signals:
	void changeDataModelIndexSignal		(	QModelIndex, QModelIndex	);
	void addNewBranchToParentSignal		(	QModelIndex, QModelIndex	);
	void selectNextChildrenSignal		(	QModelIndex					);
	void selectPrevChildrenSignal		(	QModelIndex					);
	void setDivisionForItemSignal		(	QModelIndex, QVector2D		);
	void selectNextPrevToLowLevelSignal (  TreeDataViewModel::Direction, QModelIndex );



	void removeBranchByIndexFromDataModelSignal (QModelIndex );
public slots:
// 	void changeDataModelDivision(QVector2D division);
 	void changeDataModelIndex	(QModelIndex index1, QModelIndex index2);
	void removeBranchByIndexFromDataModel (QModelIndex index);
	void setDivisionForItem( QModelIndex index1, QVector2D div );
	void  selectNextChildren			(QModelIndex index);
	void  selectPrevChildren			(QModelIndex index);
	void  addNewBranchToParent			(QModelIndex index1, QModelIndex index2)	;	


	// 	void goNextModelIndex();
// 	void goPrevModelIndex();
// 	void selectNextChildren();
// 	void selectPrevChildren();
// 	void addNewBranchToParent();
// 	void processTag();
// 	void eraseBranch();

private:
	TreeDataViewModel * model;
};

#endif // DATAVIEWMODELDISPATCHER_H
