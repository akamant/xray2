#ifndef TOOLBUTTONWITHMENU_H
#define TOOLBUTTONWITHMENU_H

#include <QToolButton>

class ToolButtonWithMenu : public QToolButton
{
	Q_OBJECT

public:
	ToolButtonWithMenu(QActionGroup * actionGroup, QWidget *parent = NULL);
	~ToolButtonWithMenu();	

	QAction * activeAction;

	int count ();

protected:
	QMenu * splitMenu;
protected slots:
	void changeSplitMenu(QAction * act);
	
};

#endif // TOOLBUTTONWITHMENU_H
