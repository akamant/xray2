#include "stdafx.h"
#include "planeInfo.h"

// only for v > 0
uint round2( float v )
{
	float i = (uint)v;
	return v-i < 0.5 ? i : i+1;
}

//
QVector3D vector3DFromPoint( const QVector2D &point, const TPlaneInfo &p )
{
	QVector3D xVec = point.x()*p.orientRow
	,		  yVec = point.y()*p.orientCol;

	return p.pos + (xVec + yVec)*p.spacing;
}

//
QList<QVector3D> vector3DFromPoint( const QList<QVector2D> &ptList, const TPlaneInfo &p )
{
	QList<QVector3D> res;

	foreach( const QVector2D &pt, ptList )
		res << vector3DFromPoint( pt, p );

	return res;
}

//
QVector2D pointFromVector3D( const QVector3D &vec, const TPlaneInfo &p )
{
	QVector3D dist = vec - p.pos;

	float xDist = QVector3D::dotProduct( p.orientRow, dist )
	,	  yDist = QVector3D::dotProduct( p.orientCol, dist );

	return QVector2D( round2(xDist / p.spacing), round2(yDist / p.spacing) ) ;
}

//
QList<QVector2D> pointFromVector3D( const QList<QVector3D> &vecList, const TPlaneInfo &p )
{
	QList<QVector2D> res;

	foreach( const QVector3D &vec, vecList )
		res << pointFromVector3D( vec, p );

	return res;
}

//
QVector3D segmentPlaneIntersept( const QVector3D &s1, const QVector3D &s2, const TPlaneInfo &p )
{
	qreal d1 = abs( s1.distanceToPlane( p.pos, p.normal ) )
	,	  d2 = abs( s2.distanceToPlane( p.pos, p.normal ) )
	,	  d  = d1 + d2;
	QVector3D diff = s2 - s1;

	return s1 + (diff * d1/d);
}

//���������� ������ ����� ����������� ������ ������ ��������� ������, 4�� ���������� - ����� �����
QList<QVector4D> abstractPlanesInterseption3D( const TPlaneInfo &p1, const TPlaneInfo &p2 )
{
	QList<QVector4D> res;

	if( p1.normal == p2.normal || p1.normal == -p2.normal )	//�����������
		return res;

	QVector3D p11 = p1.pos
	,		  p12 = p1.pos + p1.orientRow*p1.width
	,		  p13 = p1.pos + p1.orientCol*p1.height
	,		  p14 = p13 + p1.orientRow*p1.width;
	float p11d = p11.distanceToPlane( p2.pos, p2.normal )
	,	  p12d = p12.distanceToPlane( p2.pos, p2.normal )
	,	  p13d = p13.distanceToPlane( p2.pos, p2.normal )
	,	  p14d = p14.distanceToPlane( p2.pos, p2.normal );

	if( p11d * p12d < 0 )
		res << QVector4D( segmentPlaneIntersept(p11,p12,p2), 0 );
	if( p11d * p13d < 0 )
		res << QVector4D( segmentPlaneIntersept(p11,p13,p2), 3 );
	if( p12d * p14d < 0 )
		res << QVector4D( segmentPlaneIntersept(p12,p14,p2), 1 );
	if( p13d * p14d < 0 )
		res << QVector4D( segmentPlaneIntersept(p13,p14,p2), 2 );
	
	return res;
}

//
void correctPointsOrder( QList<QVector4D> &pts, const TPlaneInfo &p1, const TPlaneInfo &p2 )
{
	int pos = QVector3D::dotProduct( p1.normal, p2.pos - p1.pos )
	,	dot = QVector3D::dotProduct( p1.normal, p2.orientCol );
	int pt1 = pts.first().w()
	,   pt2 = pts.last().w();
	
	if( pos > 0 ){
		if( dot > 0 ){
			if( pt2 == 0 ) return;
			if( pt1 == 0 ){ pts.swap(0,1); return; }
			Q_ASSERT(true);
		}
		else if ( dot < 0 ){
			if( pt1 == 3 ) return;
			if( pt2 == 3 ){ pts.swap(0,1); return; }
			if( pt1 == 2 ) return;
			if( pt2 == 2 ){ pts.swap(0,1); return; }
			Q_ASSERT(true);
		}
	}
	else if( pos < 0 ){
		if( dot > 0 ){
			if( pt2 == 3 ) return;
			if( pt1 == 3 ){ pts.swap(0,1); return; }
			if( pt1 == 1 ) return;
			if( pt2 == 1 ){ pts.swap(0,1); return; }
			Q_ASSERT(true);
		}
		else if ( dot < 0 ){
			if( pt1 == 0 ) return;
			if( pt2 == 0 ){ pts.swap(0,1); return; }
			Q_ASSERT(true);
		}
	}
}

//
void insertPoint( QList<QVector3D> &list, const QVector3D &point )
{
	QVector3D beg = list.first()
			, norm = (list[1] - beg).normalized();
	float pD = QVector3D::dotProduct( point-beg, norm )
	,	  pC;
	QList<QVector3D>::iterator it = list.begin(), end = list.end();
	while( it != end ){
		pC = QVector3D::dotProduct( *it-beg, norm );
		if( pD < pC ){
			list.insert( it, point );
			return;
		}
		++it;
	}
	list.insert( it, point );
}

//
QList<QVector3D> planesInterseption3D( const TPlaneInfo &p1, const TPlaneInfo &p2 )
{
	QList<QVector4D> pts12  = abstractPlanesInterseption3D( p1, p2 )
	,				 pts34  = abstractPlanesInterseption3D( p2, p1 );
	QList<QVector3D> pts;

	if( pts12.isEmpty() || pts34.isEmpty() ) return pts;
	
	correctPointsOrder( pts34, p1, p2 );
	pts << pts34.first().toVector3D() << pts34.last().toVector3D();
	insertPoint( pts, pts12.first().toVector3D() );
	insertPoint( pts, pts12.last().toVector3D() );

	return pts.mid( 1, 2 );
}

//
QList<QVector2D> planesInterseption2D( const TPlaneInfo &p1, const TPlaneInfo &p2 )
{
	QList<QVector3D> res3D = planesInterseption3D( p1, p2 );
	return pointFromVector3D( res3D, p1 );
}

//
TPlaneInfo makeAbstractPlane( const QList<QVector2D> &ptList, const TPlaneInfo &p )
{
	return makeAbstractPlane( ptList[0], ptList[1], p );
}

//
TPlaneInfo makeAbstractPlane( const QVector2D &pt1, const QVector2D &pt2, const TPlaneInfo &p )
{
	TPlaneInfo plane;

	QVector3D pt3D1 = vector3DFromPoint( pt1, p )
	,		  pt3D2 = vector3DFromPoint( pt2, p );
	
	plane.orientRow = pt3D2 - pt3D1;
	plane.width = plane.orientRow.length();
	plane.orientRow.normalize();
	plane.orientCol = p.normal;
	plane.normal  = QVector3D::crossProduct( plane.orientRow, plane.orientCol );

	plane.pos = pt3D1;
	plane.spacing = p.spacing;
	plane.thickness = p.thickness;
	
	return plane;
}

//////////
enum EDirIntersept
{
	ENone	= 0
	,	EXAxis
	,	EYAxis
	,	EBoth
};

//
EDirIntersept dirIntersept( const TPlaneInfo &p1, const TPlaneInfo &p2 )
{
	if( p1.normal == p2.normal || p1.normal == -p2.normal )	//�����������
		return ENone;

	QVector3D p11 = p1.pos
	,		  p12 = p1.pos + p1.orientRow*p1.width
	,		  p13 = p1.pos + p1.orientCol*p1.height
	,		  p14 = p13 + p1.orientRow*p1.width;
	qreal p11d = p11.distanceToPlane( p2.pos, p2.normal )
	,	  p12d = p12.distanceToPlane( p2.pos, p2.normal )
	,	  p13d = p13.distanceToPlane( p2.pos, p2.normal )
	,	  p14d = p14.distanceToPlane( p2.pos, p2.normal );

	if( p11d*p12d < 0  && p13d*p14d < 0 )
		return EXAxis;

	if( p11d*p13d < 0  && p12d*p14d < 0 )
		return EYAxis;

	return EBoth;
}

//
void rotateVec( QVector3D &vec, const QVector3D &proj1, const QVector3D &proj2 )
{
	float dot = QVector3D::dotProduct( vec, proj1 );
	QVector3D vecProj1 = proj1 * dot;
	QVector3D vecDiff = vec - vecProj1;
	QVector3D vecProj2 = proj2 * dot;

	vec = vecProj2 + vecDiff;
}

//
void modifyPlane( TPlaneInfo &plane, const TPlaneInfo &imgPlane
				, const QList<QVector2D> &ptOldList, const QList<QVector2D> &ptNewList )
{
	QVector2D pj1 = ptOldList.last() - ptOldList.first()
	,		  pj2 = ptNewList.last() - ptNewList.first();

	QList<QVector3D> ptOld3DList = vector3DFromPoint( ptOldList, imgPlane )
				,    ptNew3DList = vector3DFromPoint( ptNewList, imgPlane );

	QVector3D proj1 = ptOld3DList.last() - ptOld3DList.first()
	,		  proj2 = ptNew3DList.last() - ptNew3DList.first();

	QVector3D planePointVec = plane.pos - ptOld3DList.first();
	
	rotateVec( planePointVec, proj1.normalized(), proj2.normalized() );
	rotateVec( plane.orientRow, proj1.normalized(), proj2.normalized() );
	rotateVec( plane.orientCol, proj1.normalized(), proj2.normalized() );

	plane.pos = ptNew3DList.first() + planePointVec;

	EDirIntersept dir = dirIntersept( plane, imgPlane );
	float ratio = proj2.length() / proj1.length();
	
	if( dir == EXAxis )
		plane.height *= ratio;
	else if ( dir == EYAxis )
		plane.width *= ratio;

	plane.normal = QVector3D::crossProduct( plane.orientRow, plane.orientCol );
}

//
int pointPlaneRelation( const TPlaneInfo &basePlane, const TPlaneInfo &relPlane, const QVector2D &point )
{
	QVector3D p = vector3DFromPoint( point, basePlane );
	return QVector3D::dotProduct( relPlane.normal, p - relPlane.pos )*basePlane.spacing;
}
