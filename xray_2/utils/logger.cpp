#include "stdafx.h"
//#include "vlogger.h"

QFile VLogger::curDayFile;
VLogger::VLogger(QObject *parent)
	: QObject(parent)
{
	if(!curDayFile.isOpen()){
		QString dirPath(QCoreApplication::applicationDirPath() + "\\logs\\");
		QDir dir;
		dir.mkdir (dirPath);
		curDayFile.setFileName(dirPath + "\\" + QDateTime::currentDateTime().date().toString() + ".log");//"./logs/" +
		curDayFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
	}			
			
	//qFile.close();		
}

void VLogger::myMessageOutput(QtMsgType type, const char *msg)
{	
	//file = fopen("file.log", "a"); // ��������� ���� �� ������
	switch (type) {
	case QtDebugMsg:
		//fprintf(file, "Debug: %s\n", msg);
		VLogger() << QString("Debug: %1\n").arg(msg);
		break;
	case QtWarningMsg:
		//fprintf(file, "Warning: %s\n", msg);
		VLogger() << QString("Warning: %1\n").arg(msg);
		break;
	case QtCriticalMsg:
		//fprintf(file, "Critical: %s\n", msg);
		VLogger() << QString("Critical: %1\n").arg(msg);
		break;
	case QtFatalMsg:
		//fprintf(file, "Fatal: %s\n", msg);
		VLogger() << QString("Fatal: %1\n").arg(msg);
		abort();
	}
	//fclose(file); // ��������� ����
}


VLogger::~VLogger()
{

}

void VLogger::operator <<(const QString & string)
{
		//curDayFile.write(string.toAscii().constData());			
		QTextStream out(&curDayFile);			
		out << string << "\r\n";	
}
