#include "stdafx.h"
#include "command.h"

//Command::commandNames Command::commandName = translate;


Command::commandStates Command::_commandState = translateState;
Command::Command(commands _command, QObject *parent)
	: QObject(parent)
	
	//, commandName(name)
{
	commandName = _command;

}

Command::Command(const Command &other)
{
}

Command::~Command()
{

}

void Command::setCommandState( commandStates state )
{	_commandState = state; 
	if(	state == DistancePrimitiveState || 
		state == AnglePrimitiveState ||
		state == CirclePrimitiveState ||
		state == PoligonPrimitiveState ||
		state == LabelPrimitiveState ||
		state == DeletePrimitiveState)
		isPrimitiveState = true;
	else
		isPrimitiveState = false;
}

bool Command::isPrimitiveState = false;
