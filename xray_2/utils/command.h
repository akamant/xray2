#ifndef COMMAND_H
#define COMMAND_H

class Command : public QObject
{
	Q_OBJECT
	
public:

	enum commandStates
	{
		contrastState,
		translateState,	
		DistancePrimitiveState,
		AnglePrimitiveState,
		CirclePrimitiveState,
		PoligonPrimitiveState,
		LabelPrimitiveState,
		DeletePrimitiveState
	};

	enum commands
	{
		non,
		contrast,
		translate,		
		rotate,
		turn,
		updateOverlay,
		negative,
		reset
	};
	
	//Command(QObject *parent);
	Command(commands _command = non, QObject *parent = NULL);
	Command(const Command &other);
	~Command();
	commands name(){return this->commandName;};

	QVector2D vector2DPar;
	QPointF pointFPar;
	QPoint pointPar;
	float floatPar;
	int intPar;
	QTransform transform;

	commands commandName;	
	static commandStates commandState(){return _commandState;};
	static void setCommandState(commandStates state);
	static bool isPrimitiveState;
private:	
	static commandStates _commandState;


};

#endif // COMMAND_H
