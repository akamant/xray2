#include "stdafx.h"
#include "config.h"

// ������� ������ � ����, ���� ������ ����� ��� ���. 
// ���������� ��� �����, ���� �� ����������� ��� ��� ������, ��� ��� �������, ���� ������� �� �������
QString extractResource(QString resource,  QString file) {
	QFile file2(file);
	if (file2.exists()) return file;
	if (file2.open(QIODevice::ReadWrite)) {
		QFile workFile(resource);
		if(workFile.open(QIODevice::ReadOnly)) {
			file2.write(workFile.readAll());
			workFile.close();
		}
		file2.close();
		return file;
	}
	return resource;
}

QDir getVidarSettingsPath()
{
	VIDAR_APP_SETTINGS;
	if (settings.format () == QSettings::IniFormat) {
		QFileInfo pathInfo( settings.fileName() );
		return pathInfo.absoluteDir();
	}
	QString dataPath = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
	QDir dir;
	dir.mkpath(dataPath);
	return dir;
}


int getToolBarButtonIconSize() 
{
	VIDAR_APP_SETTINGS;
	if (settings.value("toolBarButtonIconSize").isNull()) settings.setValue("toolBarButtonIconSize",32);
	return settings.value("toolBarButtonIconSize").toInt();
}