#ifndef BranchFrame_H
#define BranchFrame_H

#include <QObject>
#include "TreeDataViewModel.h"
#include "TreeDataModel.h"
class BranchFrame : public QFrame
{
	Q_OBJECT

public:	
	BranchFrame(TreeDataViewModel * _model, const QModelIndex & _index, QWidget *parent = NULL );	
	~BranchFrame();
	QModelIndex getIndex(){return _index;};
	virtual bool eventFilter(QObject* obj, QEvent* e);
	void showEvent( QShowEvent * e );
	virtual QSize sizeHint() const;
public slots:
	void ActivityChangeSlot(const QModelIndex & index);
	void oneViewMode();
	void changeContentFrame ( const QModelIndex & index);
	void changeSelectedItemSlot(const QModelIndex & index);
	void changePolicyByViewOne();

	
private:
	QSizePolicy::Policy pol;
	QColor color;	
	QModelIndex _index;
	QModelIndex modelIndex;
	QModelIndex index() const { return _index; }
	void setIndex(QModelIndex val) { _index = val; }	
	bool indexTypeDifferent(const QModelIndex & index);
	TreeDataViewModel * model;
};

#endif // BranchFrame_H
