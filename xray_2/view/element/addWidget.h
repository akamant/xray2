#ifndef ADDWIDGET_H
#define ADDWIDGET_H

#include <QWidget>
#include "stackedwidget.h"
class TreeDataViewModel;
class LabelWithAutoSizeText;
//class StackedWidget;

class AddWidget : public QWidget//StackedWidget
{
	Q_OBJECT
signals:
	void clicked();
	void dblClicked();
	void released();

public:
	//AddWidget(TreeDataViewModel * _model, const QModelIndex & _parentIndex,  QWidget *parent = NULL);
	//AddWidget(const QString & label, bool isExpanding = false, QWidget *parent = NULL);
	AddWidget( const QString & label, const Qt::Alignment & _alignment, bool _isExpanding =  false, QColor _BGColor = QColor (50, 50, 50, 0), QWidget *parent = NULL );
	~AddWidget();
	void setText(QString str);
	
	virtual void mouseReleaseEvent ( QMouseEvent * event );
	virtual bool eventFilter(QObject *obj, QEvent *event);
	virtual QSize sizeHint()const; 
	virtual void  enterEvent(QEvent * event);
	virtual void  leaveEvent(QEvent * event);	
	void paintEvent(QPaintEvent * event);
	void resizeEvent(QResizeEvent * event);
	void showEvent(QShowEvent * event);
	void mouseDoubleClickEvent( QMouseEvent * event );
	void mousePressEvent ( QMouseEvent * event );
	void mouseMoveEvent ( QMouseEvent * event ); 	
	void setNewBGColor( QColor color );
private:
	void setBGColor (QColor color);
	QColor _color;
	QGridLayout * GL;
	int widthFont;
	bool isExpanding;
	QColor BGColor;
	QColor diffBGCollor;
	LabelWithAutoSizeText * autoSizeLabel;


// 	TreeDataViewModel * model;
// 	QModelIndex parentIndex;
};

#endif // ADDWIDGET_H
