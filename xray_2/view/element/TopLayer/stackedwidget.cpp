#include "StdAfx.h"
#include "stackedwidget.h"
#include "cleverbutton.h"

StackedWidget::StackedWidget(bool _isAutoHided, QWidget *parent)
	: MaskedStackedWidget(parent)
	,isAutoHided(_isAutoHided)
{
	//isAutoHided = true;
	//stackedLayout->setStackingMode(QStackedLayout::StackAll);
	//setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	//setMouseTracking(true);
//	return;
	if(isAutoHided)
		timerId = startTimer(100);	
	mouseStay = false;
	entered = false;
}

StackedWidget::~StackedWidget()
{

}

int StackedWidget::addWidget( QWidget *w )
{
	int n = MaskedStackedWidget::addWidget(w);
	return n;
}

void StackedWidget::processClick()
{
	prevPos.setX(-1);
}

void StackedWidget::enterEvent( QEvent * event )
{
//	return;
	for(int i = 1; i < stackedLayout->count(); i ++){
		QWidget * w = stackedLayout->itemAt(i)->widget();
		w->show();
	}
	entered = true;
	mouseStay = false;
	killTimer(timerId);	
	if(isAutoHided)
		timerId = startTimer(100);
}

void StackedWidget::leaveEvent( QEvent * event )
{
//	return;
	for(int i = 1; i < stackedLayout->count(); i ++){
		QWidget * w = stackedLayout->itemAt(i)->widget();
		w->hide();
	}
	entered = false;
	killTimer(timerId);	
	mouseStay = false;
}

void StackedWidget::showEvent( QShowEvent * event ){
	MaskedStackedWidget::showEvent(event);
	leaveEvent(NULL);
}

void StackedWidget::timerEvent(QTimerEvent * e){
// 	if(!mouseMoved)
// 		leaveEvent(NULL);
// 	mouseMoved = false;
	bool underAdditionalWidget = false;
	for(int i = 1; i < stackedLayout->count(); i ++){
		QWidget * w = stackedLayout->itemAt(i)->widget();	
			if(w->underMouse())
				underAdditionalWidget = true;			
	}

	if(underMouse() )
	{
		if (prevPos == QCursor::pos())
		{
			if(mouseStay)
			{
				leaveEvent(NULL);	
				killTimer(e->timerId());
				mouseStay = false;
				timerId = startTimer(100);
			}else
			{
				if(entered)
				{
					mouseStay = true;
					killTimer(e->timerId());
					if(underAdditionalWidget)
						timerId = startTimer(3000);
					else
						timerId = startTimer(1500);
				}
			}
							
		}else
		{		
			killTimer(e->timerId());
			timerId = startTimer(100);
			mouseStay = false;
			enterEvent(NULL);
		}
	}

	prevPos = QCursor::pos();
}



void StackedWidget::mouseMoveEvent( QMouseEvent * e )
{
	MaskedStackedWidget::mouseMoveEvent(e);
	mouseMoved = true;
	enterEvent(NULL);	
}


bool StackedWidget::eventFilter(QObject* obj, QEvent * event) {
	if (prevPos != QCursor::pos()){
	//	mouseMoved = true;
		leaveEvent(NULL);
	}else
	{
		if(underMouse())
			enterEvent(NULL);
	}

	prevPos = QCursor::pos();
	return QWidget::eventFilter(obj, event);
}

