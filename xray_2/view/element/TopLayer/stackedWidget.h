#ifndef STACKEDWIDGET_H
#define STACKEDWIDGET_H

#include <maskedwidget.h>

class StackedWidget : public MaskedStackedWidget
{
	Q_OBJECT

public:
	StackedWidget(bool isAutoHided, QWidget *parent = 0);
	~StackedWidget();
	int addWidget(QWidget *w);
protected:
	bool isAutoHided;
	void virtual enterEvent(QEvent * event);
	void virtual leaveEvent(QEvent * event);
	void virtual showEvent( QShowEvent * event);
	void virtual timerEvent(QTimerEvent * e);
	void mouseMoveEvent( QMouseEvent * e );
	bool eventFilter(QObject* obj, QEvent * event);
	
	bool entered;
	int timerId;
private:
	bool mouseStay;
	QPoint prevPos;
	bool mouseMoved;

protected slots:
	void processClick();

};

#endif