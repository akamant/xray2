#ifndef MASKEDWIDGET_H
#define MASKEDWIDGET_H


class MaskedWidget : public QWidget
{
	Q_OBJECT

public:
	MaskedWidget(QWidget *parent = 0);
	~MaskedWidget();
	virtual void paintEvent ( QPaintEvent * event );
	virtual void updateMask();
};
class MaskedStackedWidget : public MaskedWidget
{
	Q_OBJECT

public:
	MaskedStackedWidget(QWidget *parent = 0);
	~MaskedStackedWidget();
	int addWidget(QWidget *w);
	QStackedLayout * stackedLayout;
public slots:
	
	void setCurrentIndex(int index);

	
};

#endif // MASKEDWIDGET_H
