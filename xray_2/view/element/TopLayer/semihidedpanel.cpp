#include "StdAfx.h"
#include "semihidedpanel.h"
#include "gridsplitterwidget.h"
#include "CleverButton.h"

SemihidedPanel::SemihidedPanel(const QColor & color,  QWidget *parent)
	: MaskedStackedWidget(parent)
{
// 	hLine = new SemiHidedLine(color);
 	activeWidget = new ActiveWidget;
 	stackedLayout->setStackingMode(QStackedLayout::StackAll);
// 	setStretch(100);
// 	addWidget(hLine);	
// 	addWidget(activeWidget);	
//  	connect(hLine,			SIGNAL	(enterMouse()		),
//  			this,			SLOT	(activatePanel()	)
//  		);
	QPalette pal;	
	QLinearGradient grad;
	QColor gradColor (QColor(255, 0, 0));
	gradColor.setAlpha(20);
	grad.setColorAt(0, gradColor);	;
	gradColor.setAlpha(0);	

	grad.setColorAt(1, gradColor);
	grad.setStart(0, 1);
	grad.setStart(1, 0);
	grad.setCoordinateMode(QGradient::StretchToDeviceMode);

	
	//pal.setBrush(QPalette::Foreground, QBrush (grad) );	
	int sideLength = 50;
	QPixmap * pixmapDisActiv = new QPixmap (sideLength, sideLength);
	

// 	QPixmap * pixmapMask = new QPixmap( pixmapDisActiv->size() );			
// 	QPainter painter(pixmapDisActiv);
// 	painter.fillRect(QRect (0, 0, pixmapMask->size().width(), pixmapMask->size().height()) , QColor(0, 0, 0, 255));
// 	pixmapDisActiv->setAlphaChannel(*pixmapMask);

	{QPainter painter(pixmapDisActiv);
	painter.setBrush(QBrush (grad));
	painter.drawRect(0, 0, sideLength, sideLength);}

	QPixmap * pm = new QPixmap(":/resources/images/gray/choose object.png");

// 	QPixmap * pm = new QPixmap(sideLength, sideLength);
// 	QPainter painter(pm);
// 	{
// 		QLinearGradient gradGrey;		
// 		gradGrey.setColorAt(0, QColor(255, 255, 255));
// 		//gradColor = QColor(0, 0, 0);
// 		gradColor = QColor(255, 255, 255);
// 		gradColor.setAlpha(100);
// 		gradGrey.setColorAt(1, gradColor);
// 		//gradColor.setAlpha(0);
// 		gradGrey.setStart(0, 1);
// 		gradGrey.setStart(1, 0);
// 		gradGrey.setCoordinateMode(QGradient::StretchToDeviceMode);
// 		painter.fillRect(0,0, pm->width(), pm->height(), QBrush (gradGrey));
// 	}

	CleverButton * chooseButton = new CleverButton(150, &pm->scaled(QSize(sideLength, sideLength)), pixmapDisActiv );


	//QPixmap * pm = new QPixmap(":/resources/images/gray/choose object.png");
	//CleverButton * chooseButton = new CleverButton(180, new QPixmap());
	chooseButton->setFixedSize(QSize(sideLength, sideLength));
	chooseButton->setPalette(pal);

	addWidget(chooseButton);	
 	//addWidget(activeWidget);
	activeWidget->setWindowFlags(Qt::Popup);
	//activeWidget->setAutoFillBackground(false);
	activeWidget->setAttribute(Qt::WA_TranslucentBackground); 
	activeWidget->setAttribute(Qt::WA_NoSystemBackground); 
	
	connect(chooseButton,	SIGNAL	(clicked()		),
  			this,			SLOT	(activatePanel()	)
  		);

}

SemihidedPanel::~SemihidedPanel()
{

}

void SemihidedPanel::activatePanel(){	
	//setCurrentIndex(0);
	//setCurrentIndex(1);	
	activeWidget->setParent(NULL);
	activeWidget->move(mapToGlobal(QPoint(0,0)));
	activeWidget->show();


	updateMask();
	
}

void SemihidedPanel::disactivatePanel(){
	setCurrentIndex(0);
	//activeWidget->hide();
	setCurrentIndex(0);
	updateMask();	
}

void SemihidedPanel::leaveEvent( QEvent * event)
{
	MaskedStackedWidget::leaveEvent(event);
	disactivatePanel();
}

void SemihidedPanel::showEvent ( QShowEvent * event )
{
	MaskedStackedWidget::showEvent(event);
	disactivatePanel();
}

void SemihidedPanel::mousePressEvent ( QMouseEvent * event )
{
	//disactivatePanel();
	activatePanel();
	QWidget::mousePressEvent(event);
}



void SemihidedPanel::setStretch(int widthStretch, int lengthStretch )
{
	hLine->setMaximumHeight(widthStretch);

	activeWidget->setWidth(widthStretch);
}

void SemihidedPanel::setLayoutForAtiveWindow( QLayout * layOut )
{
	activeWidget->setLayout(layOut);
}


ActiveWidget::ActiveWidget( QWidget *parent /*= NULL*/ ):
	//StackedWidget(parent)
	MaskedWidget(parent)
{
	setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
}

void ActiveWidget::setWidth( int width )
{
	
}

ActiveWidget::~ActiveWidget()
{
	
}

void ActiveWidget::leaveEvent( QEvent * event )
{
	QWidget::leaveEvent(event);
	emit leaveMouse();
}

void ActiveWidget::paintEvent(QPaintEvent * event){
	setMask(childrenRegion());
	QWidget::paintEvent(event);
	

}




















AlignedRightWidget::AlignedRightWidget( QWidget * widget /*= NULL*/ )
	:QWidget(widget)
	,widgetNumber(0)
{

	gl1 = new QGridLayout();
	setLayout(gl1);	
	gl1->setMargin(0);
	gl1->setSpacing(0);	
	

}

void AlignedRightWidget::addWidget(QWidget * widget)
{
	
	gl1->addWidget(widget, 0, widgetNumber, 1, 1);
	widgetNumber ++;
}


void AlignedRightWidget::paintEvent(QPaintEvent * event) {

	//setFixedSize(((QWidget *)parent())->width(), ((QWidget *)parent())->height() / 8);
	//setFixedHeight(((QWidget *)parent())->height() / 8);
	QWidget::paintEvent(event);
}



SemiHidedLine::SemiHidedLine(const QColor & color, QWidget *parent ):
QLabel("", parent)
{
	QPalette pal;	
	QLinearGradient grad;
	QColor gradColor (color);
	gradColor.setAlpha(150);
	grad.setColorAt(0, gradColor);
	grad.setColorAt(0.2, QColor(255, 255, 255, 120));
	gradColor.setAlpha(0);
	grad.setColorAt(0.98, gradColor);
	gradColor.setAlpha(50);	
	grad.setStart(0, 1);
	grad.setStart(1, 0);
	grad.setCoordinateMode(QGradient::StretchToDeviceMode);

	pal.setBrush(QPalette::Background, QBrush (grad) );		
	setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Ignored);	

	QGridLayout * layout = new QGridLayout(this);
	setLayout(layout);
	//QPixmap * pm = new QPixmap(":/resources/images/gray/choose object.png");
	QPixmap * pm = new QPixmap(64, 64);
	QPainter painter(pm);
	{
		QLinearGradient gradGrey;		
		gradGrey.setColorAt(0, QColor(255, 255, 255));
		gradGrey.setColorAt(1, QColor(0, 0, 0));
		//gradColor.setAlpha(0);
		painter.fillRect(0,0, pm->width(), pm->height(), QBrush (gradGrey));
	}



	CleverButton * nextB = new CleverButton(180, &pm->scaled(QSize(64, 64)));
	//layout->addWidget(nextB, 0, 0, 1, 1, Qt::AlignLeft | Qt::AlignTop);
	layout->setMargin(0);
	layout->setSpacing(0);
	setLineWidth(0);
	setMidLineWidth(0);

}

bool SemiHidedLine::underMouse(){
	return QLabel::underMouse();
}

SemiHidedLine::~SemiHidedLine()
{
	//setMouseTracking 
}


void SemiHidedLine::enterEvent(QEvent * event) {
	QLabel::enterEvent(event);
	emit enterMouse();
}


void SemiHidedLine::paintEvent(QPaintEvent * event) {

	setFixedSize(((QWidget *)parent())->width(), ((QWidget *)parent())->height() / 8);
	QLabel::paintEvent(event);

}