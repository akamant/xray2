#include "StdAfx.h"
#include "maskedwidget.h"

MaskedStackedWidget::MaskedStackedWidget(QWidget *parent)
	: MaskedWidget(parent)
{	
	//QBoxLayout * boxLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);	
	stackedLayout = new QStackedLayout();
	stackedLayout->setStackingMode(QStackedLayout::StackAll);
	//stackedLayout->setStackingMode(QStackedLayout::StackAll);
	//boxLayout->addLayout(stackedLayout);
	//setLayout(boxLayout);
	setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	setLayout(stackedLayout);

	
}

MaskedStackedWidget::~MaskedStackedWidget()
{
	
}

void MaskedStackedWidget::setCurrentIndex(int index){
	stackedLayout->setCurrentIndex(index);
}

int MaskedStackedWidget::addWidget(QWidget *w){
	stackedLayout->addWidget(w);	
	setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	stackedLayout->setCurrentWidget(w);
	return 0;
}


void MaskedWidget::updateMask(){	
	for (int n =  0; n < layout()->count(); ++ n)
	{
		QWidget* w = layout()->itemAt(n)->widget();
		w->setMask(w->childrenRegion());
	}
}


void MaskedWidget::paintEvent( QPaintEvent * event )
{
	QWidget::paintEvent(event);	
	updateMask();
}

MaskedWidget::MaskedWidget( QWidget *parent /*= 0*/ ):
	QWidget(parent)
{

}

MaskedWidget::~MaskedWidget()
{

}
