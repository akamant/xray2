#ifndef SEMIHIDEDPANEL_H
#define SEMIHIDEDPANEL_H
#include "maskedwidget.h"
#include "stackedwidget.h"



class AlignedRightWidget : public QWidget
{
	Q_OBJECT
public:
	QGridLayout * gl1;
	QBoxLayout * BL;
	AlignedRightWidget(QWidget * widget = NULL);
	virtual void paintEvent(QPaintEvent * event);
	int widgetNumber;
	void addWidget(QWidget * widget);
};

class ActiveWidget: public MaskedWidget//StackedWidget
{
	Q_OBJECT
public:
	ActiveWidget(QWidget *parent = NULL);
	~ActiveWidget();
	virtual void leaveEvent( QEvent * event);
	void setWidth(int width);
	virtual void paintEvent(QPaintEvent * event);

signals:
	void leaveMouse();
	
private:
	QBoxLayout * BL;
	//QBoxLayout * B
};


class SemiHidedLine: public QLabel
{
Q_OBJECT
public:
	SemiHidedLine(const QColor & color, QWidget *parent = NULL);
	~SemiHidedLine();
	virtual void enterEvent( QEvent * event) ;
	virtual void paintEvent(QPaintEvent * event);
	virtual bool underMouse();
signals:
	void enterMouse();
};

class SemihidedPanel : public MaskedStackedWidget
{
	Q_OBJECT

public:
	SemihidedPanel(const QColor & color, QWidget *parent = NULL);
	~SemihidedPanel();
	void showEvent ( QShowEvent * event );
	void mousePressEvent ( QMouseEvent * event );
	void setStretch(int widthStretch, int lengthStretch = 0);
	void setLayoutForAtiveWindow(QLayout * layout);

	virtual void leaveEvent( QEvent * event);
signals:
	void curWidgetChanged();
public slots:
	void activatePanel();
	void disactivatePanel();
	
private:	
	SemiHidedLine	* hLine;
	ActiveWidget	* activeWidget;
};

#endif // SEMIHIDEDPANEL_H
