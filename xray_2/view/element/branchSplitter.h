#ifndef BRANCHSPLITTER_H
#define BRANCHSPLITTER_H

class TreeDataViewModel;
class AddWidget;
class BranchSplitter : public QWidget
{
	Q_OBJECT
	//Q_PROPERTY(QString title READ title WRITE setTitle)

public:
	BranchSplitter(TreeDataViewModel * _model, const QModelIndex & _index, QWidget *parent );
	void SetRowsCols(int rows, int cols);	
	~BranchSplitter();
	QModelIndex getIndex(){return index;};
	virtual bool eventFilter(QObject* obj, QEvent * e);
	virtual void showEvent( QShowEvent * e);
	virtual QSize sizeHint() const;

	public slots:		
		void addNewTreeItemSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd );		
		void deleteChildrenSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd );
		void setDivision ( const QModelIndex & index, int rowNumberStart, int rowNumberEnd );		
		void refillAll( const QModelIndex & index );
private:
	bool isFirstShow;
	AddWidget * addWidget;
	int rows;
	int cols;
	bool isExistChildIndex( const QModelIndex & index );
	void addIndexWidget(QWidget * widget, int rowSpan = 1, int columnSpan = 1, Qt::Alignment = 0);
	bool existEmptySell();
	void fillEmtySells();



	QGridLayout gridLayout;
	QModelIndex index;
	TreeDataViewModel * model;
};
#endif // BRANCHSPLITTER_H
