#include "StdAfx.h"
#include "gridsplitterwidget.h"


  ///////////////////////////////////////////////////////////////////////
 /////////////////////////GridSplitterWidget////////////////////////////
///////////////////////////////////////////////////////////////////////


GridSplitterWidget::GridSplitterWidget(int rows, int cols, int _maxRow, int _maxCol, const QColor & _color, QWidget *parent)
	: QWidget(parent)
	,maxRow (_maxRow - 1)
	,maxCol (_maxCol - 1)
	,color(_color)
{

	//setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	//setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	//setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	//gridLayout.setMargin(2);
	//gridLayout.setSpacing(2);
	setLayout(&gridLayout);
	gridLayout.setMargin(0);
	gridLayout.setSpacing(0);


	for(int r = 0; r < maxRow + 1; r++)
	{
		for(int c = 0; c < maxCol + 1; c++)
		{			
			CellFrame * w = new CellFrame(r, c);	
			gridLayout.addWidget(w, r, c);	
			connect	(w,		SIGNAL(mouseMoved(int, int)),
					this,	SLOT(ChangePossibleActivity(int, int))
					);			
			connect	(w,		SIGNAL(mouseClicked(int, int)),
					this,	SLOT(ChangeRowsCols(int, int))
					);			
		}
	}	

	SetRowsCols(rows, cols);

//
//	setAutoFillBackground( true );
}

GridSplitterWidget::~GridSplitterWidget()
{

}

void GridSplitterWidget::SetRowsCols(int rows, int cols)
{	
	//if(this->rows == rows && this->cols == cols)
	//	return;
// 	QQueue <QWidget *> queue;
// 	QLayoutItem *child;
// 	while( (child = gridLayout.takeAt(0)) != 0)
// 	{
// 		QWidget * widget = child->widget();		
// 		delete widget;			
// 	}	
	bool boolChangedRowsCols = false;
	if(this->rows != rows || this->cols != cols)
		boolChangedRowsCols = true;
	this->rows = rows;
	this->cols = cols;

	QPalette palSelected;	
	QColor BGColor(color);
	BGColor.setAlpha(240);
	palSelected.setBrush(QPalette::Background, QBrush (BGColor));//  (30, 55, 255, 250)) );	
	palSelected.setBrush(QPalette::Foreground, QBrush (QColor(255,255,255) ));	

	QPalette palUnSelected;	
	QColor InActiveColor(color);
	InActiveColor.setAlpha(50);
	palUnSelected.setBrush(QPalette::Background, QBrush (InActiveColor.lighter())); //QColor(60, 100 , 255, 100)) );
	palUnSelected.setBrush(QPalette::Foreground, QBrush (QColor(255,255,255) ));	

	QPalette palMousedOut;	
	QColor MousedColor(color);
	MousedColor.setAlpha(220);
	palMousedOut.setBrush(QPalette::Background, QBrush (MousedColor.lighter())); //QColor(60, 100 , 255, 100)) );
	palMousedOut.setBrush(QPalette::Foreground, QBrush (QColor(255,255,255) ));	

	QPalette palMousedIn;	
	
	palMousedIn.setBrush(QPalette::Background, QBrush (BGColor.darker(220))); //QColor(60, 100 , 255, 100)) );
	//palMousedIn.setBrush(QPalette::Background, QBrush (BGColor)); //QColor(60, 100 , 255, 100)) );
	palMousedIn.setBrush(QPalette::Foreground, QBrush (QColor(255,255,255) ));	
	
	for(int r = 0; r < maxRow + 1; r++)
	{
		for(int c = 0; c < maxCol + 1; c++)
		{			
				//QFrame * w = new QFrame;
				//CellWidget * w = new CellWidget(r, c);	
				CellFrame * w =  (CellFrame *) (gridLayout.itemAtPosition (r, c)->widget());	
				if(w){
					if(r >= rows || c >= cols){
						if(r <= mousedRow && c <= mousedCol)
							w->setPalette(palMousedOut);			
						else
							w->setPalette(palUnSelected);		
						w->setFrameStyle(QFrame::Box | QFrame::Raised);
					}else{
						w->setPalette(palSelected);
						if(r <= mousedRow && c <= mousedCol)
							w->setPalette(palMousedIn);			
						else
							w->setPalette(palSelected);
						w->setFrameStyle( QFrame::Box | QFrame::Sunken);
					}
				}
//gridLayout.addWidget(w, r, c);				
// 				connect	(w,		SIGNAL(mouseClicked(int, int)),
// 						this,	SLOT(ChangeRowsCols(int, int))
// 						);			
		}
	}	
	if(boolChangedRowsCols)
		emit changedSplitting (QVector2D (rows, cols) );

}

void GridSplitterWidget::ChangeRowsCols(int rows, int cols){
	SetRowsCols(rows + 1, cols + 1);
}


void GridSplitterWidget::ChangePossibleActivity(int posRows, int posCols){
	mousedRow = posRows;
	mousedCol = posCols;
	SetRowsCols(rows, cols);
}

void GridSplitterWidget::addIndexWidget( QWidget * widget){	
	int countWidgets = gridLayout.count();
	int r = countWidgets / cols;
	int c = r <= 0 ? countWidgets : ( countWidgets % (r * cols) );	
	gridLayout.addWidget(widget, r, c, 1, 1);	
}


  ///////////////////////////////////////////////////////////////////////
 /////////////////////////////CellWidget////////////////////////////////
///////////////////////////////////////////////////////////////////////


CellFrame::CellFrame( int _row, int _col, QWidget * parent /*= NULL*/ ):
	QFrame(parent)
	,row(_row)
	,col(_col)
{
	setAutoFillBackground(true);
	setMinimumSize(QSize(10, 10));
	setMaximumSize(QSize(40, 40));
	//setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);	
	setMouseTracking(true);
	setLineWidth(1);
	setMidLineWidth(1);
	


}
	
CellFrame::~CellFrame()
{
	
}

void CellFrame::mouseMoveEvent( QMouseEvent * event )
{
	//if(event->buttons() == Qt::LeftButton)
		emit mouseMoved(row, col);
		QFrame::mouseMoveEvent(event);
}

void CellFrame::mousePressEvent ( QMouseEvent * event )
{
		emit mouseClicked(row, col);
		QFrame::mousePressEvent(event);
}


QSize CellFrame::sizeHint() const
{
	return QSize(200, 200);
}