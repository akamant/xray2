#ifndef CLEVERBUTTON_H
#define CLEVERBUTTON_H

#include <QToolButton>

class CleverButton : public QToolButton
{
	Q_OBJECT

public:
	//CleverButton(QPixmap * pixmap, uint alpha, QWidget *parent = NULL );
	CleverButton(uint alpha, QPixmap * pixmapActiv, QPixmap * pixmapNormal = NULL, QWidget *parent = NULL);
	void enterEvent( QEvent * e );
	void leaveEvent( QEvent * e );
	void timerEvent ( QTimerEvent * e );
	void mouseMoveEvent( QMouseEvent * e );
	~CleverButton();
	void showEvent( QShowEvent * e );
	virtual bool underMouse( );

protected:
	QIcon * invisbleIcon;
	QIcon * icon;
	QPixmap * pixmap;
	virtual void hide	( );
	virtual void show	( );
private:
	bool entered;
	bool entered2;	
};

#endif // CLEVERBUTTON_H