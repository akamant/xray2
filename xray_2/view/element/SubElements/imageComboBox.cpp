#include "stdafx.h"
#include "imagecombobox.h"
#include "TreeDataViewModel.h"
#include "TreeDataModel.h"
#include "modelcontrols.h"

VImageComboBox::VImageComboBox(TreeDataViewModel * _model, const QModelIndex & _index, QWidget *parent)
	: QComboBox(parent)
	, model(_model)	
	, index(_index)
{
	
	QPixmap icomPixmap(":/resources/images/gray/splitSeries1x1.png");
	addItem(QIcon(":/resources/images/gray/splitSeries1x1.png"), "");
	addItem(QIcon(":/resources/images/gray/splitSeries1x2.png"), "");
	addItem(QIcon(":/resources/images/gray/splitSeries2x1.png"), "");
	addItem(QIcon(":/resources/images/gray/splitSeries2x2.png"), "");
	setCurrentIndex(1);
	//setIconSize(icomPixmap.size());
	//setFixedSize(icomPixmap.size());
	//setFixedWidth(icomPixmap.size().width() + 20);
	setFrame(true);
	setSizeAdjustPolicy (QComboBox::AdjustToMinimumContentsLengthWithIcon);
	

	QString style  =
		"QComboBox {" 
		"border: 0px solid gray;"
		"border-radius: 0px;"
		"padding: 0px 18px 0px 3px;"
		"min-width: 6em;"
		;
	setStyleSheet(style);

	changeDivisionSlot(index, 1, 1);

	connect	(	model,				SIGNAL	( changeDivision		( const QModelIndex &, int, int ) ),
				this,				SLOT	( changeDivisionSlot	( const QModelIndex &, int, int ) )
			);	

	connect (	this,		SIGNAL	(currentIndexChanged(int)),
				this,		SLOT	(processIndexChange (int))
			);

}

VImageComboBox::~VImageComboBox()
{

}

void VImageComboBox::changeDivisionSlot(const QModelIndex &_index, int, int)
{
	disconnect (	this,		SIGNAL	(currentIndexChanged(int)),
					this,		SLOT	(processIndexChange (int))
				);

	int divValue;
	if(index ==_index)
	{
		QVector2D div = model->getDivisionForItem(index);
		divValue = div.x() * 10 + div.y();

		if(divValue != curDivValue)
		{
			curDivValue = divValue;
			switch (divValue)
			{
			case 11:
				setCurrentIndex(0);
				break;
			case 12:
				setCurrentIndex(1);
				break;
			case 21:
				setCurrentIndex(2);
				break;
			case 22:
				setCurrentIndex(3);
				break;
			}
		}
	}

	connect (	this,		SIGNAL	(currentIndexChanged(int)),
		this,		SLOT	(processIndexChange (int))
		);
	
}


void VImageComboBox::paintEvent(QPaintEvent *event){
	QComboBox::paintEvent(event);
	if(width() != height() + 22)
	{
		setFixedWidth(height() + 22);
		setIconSize(QSize(height(), height()));
	}
}

void VImageComboBox::processIndexChange( int i )
{
	//return;
	ModelControls * modelControls = new ModelControls(model, this->index, this);	

	if(curi != i)
	{
		curi = i;
		switch (i)
		{
		case 0:
			modelControls->changeDataModelDivision(QVector2D(1, 1));
			break;
		case 1:
			modelControls->changeDataModelDivision(QVector2D(1, 2));
			break;
		case 2:
			modelControls->changeDataModelDivision(QVector2D(2, 1));
			break;
		case 3:
			modelControls->changeDataModelDivision(QVector2D(2, 2));
			break;
		}
	}

		
}
