#ifndef VIMAGECOMBOBOX_H
#define VIMAGECOMBOBOX_H

#include <QObject>

class QComboBox;
class TreeDataViewModel;
class LabelWithAutoSizeText;

class VImageComboBox : public QComboBox
{
	Q_OBJECT

public:
	VImageComboBox( TreeDataViewModel * _model, const QModelIndex & index, QWidget *parent = NULL);
	~VImageComboBox();

	QModelIndex  index;
	TreeDataViewModel * model;
public slots:
	//void setCurrentIndex ( int index );
	//virtual void changeEvent( QEvent * e );
	void processIndexChange( int i );
	void changeDivisionSlot(const QModelIndex &_index, int, int);
protected:
	int curDivValue;
	int curi;
	virtual void paintEvent(QPaintEvent *event);
	

};

#endif // VIMAGECOMBOBOX_H
