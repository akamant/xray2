#include <stdafx.h>
#include "cleverbutton.h"

CleverButton::CleverButton(uint alpha, QPixmap * pixmapActiv, QPixmap * _pixmapDisAciv,   QWidget *parent)
	: QToolButton(parent)
	, pixmap(pixmapActiv)
{
	icon = new QIcon();	
	
	//pixmapActiv
	QPixmap * pixmapDisAciv;
	QPixmap * pixmapMask;	
	pixmapMask = new QPixmap( pixmapActiv->size() );			
	if(true || !_pixmapDisAciv){	
		QPixmap * pixmap3 = new QPixmap(pixmapActiv->pixmapData());		
		QPainter painter(pixmapMask);
		painter.fillRect(QRect (0, 0, pixmapMask->size().width(), pixmapMask->size().height()) , QColor(alpha, alpha, alpha, 230));
		pixmap3->setAlphaChannel(*pixmapMask);
// 		pixmapMask->fill( QColor(255, 255, 255, 255));	
// 		pixmap3->setAlphaChannel(*pixmapMask);		
		setAutoRaise(true);	
		pixmapDisAciv = pixmap3;
	}else{
		pixmapDisAciv = _pixmapDisAciv;
	}
	
	icon->addPixmap(*pixmapDisAciv, QIcon::Normal);		
	icon->addPixmap(*pixmapActiv, QIcon::Selected);
	//icon->addPixmap(*pixmapDisAciv, QIcon::Disabled);
	
	icon->addPixmap(*pixmapActiv, QIcon::Active);

	//icon->addPixmap(*pixmap3, QIcon::Active);

	pixmapMask->fill( QColor(1, 1, 1, 255));	
	QPixmap *pixmapInvis = new QPixmap(pixmapActiv->size());
	pixmapInvis->setAlphaChannel(*pixmapMask);		
		
	invisbleIcon = new QIcon();
	invisbleIcon->addPixmap(*pixmapInvis, QIcon::Normal);		
	invisbleIcon->addPixmap(*pixmapInvis, QIcon::Selected);
	invisbleIcon->addPixmap(*pixmapInvis, QIcon::Disabled);

	setFixedSize(QSize (pixmap->width(), pixmap->height()));		
	setIcon(*icon);
	setIconSize(pixmap->size());	
	setMouseTracking(true);
	startTimer(1000);
	entered2 = false;
	entered = false;
}

void CleverButton::hide(){
	setIcon(*invisbleIcon);
	setEnabled(false);
	
}
void CleverButton::show(){
	setIcon(*icon);
	setEnabled(true);

}


bool CleverButton::underMouse(){
	if(!entered2)
		return false;
	return QToolButton::underMouse();
}
CleverButton::~CleverButton()
{

}

void CleverButton::enterEvent( QEvent * e )
{
	QToolButton::enterEvent(e);	
	if(!entered)
		startTimer(1600);
}

void CleverButton::leaveEvent( QEvent * e )
{
	QToolButton::leaveEvent(e);
	
}

void CleverButton::showEvent( QShowEvent * e )
{
	entered2 = false;
	entered = false;
	QEvent event(QEvent::Leave);
	setIcon(*icon);
	setEnabled(true);
	QToolButton::showEvent(e);	
	QApplication::sendEvent(this, &event);
	//startTimer(1000);
}

void CleverButton::mouseMoveEvent( QMouseEvent * e )
{
	QToolButton::mouseMoveEvent(e);	
	if(entered)
		entered2 = true;
	//if(!entered){
		entered = true;
	//	mouseMoveEvent(e);	
	//}
}


void CleverButton::timerEvent(QTimerEvent * e){
	if(entered && !entered2){		
		QEvent event(QEvent::Leave);
		QApplication::sendEvent(this, &event);

		{QEvent event(QEvent::Enter);
		QApplication::sendEvent(this, &event);}

		{QEvent event(QEvent::Leave);
		QApplication::sendEvent(this, &event);}		
	}
	killTimer(e->timerId());

}
