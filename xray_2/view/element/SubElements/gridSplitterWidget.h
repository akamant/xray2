#ifndef GRIDSPLITTERWIDGET_H
#define GRIDSPLITTERWIDGET_H


class CellFrame : public QFrame
{
	Q_OBJECT

public:
	CellFrame(int row, int col, QWidget *parent = NULL);
	~CellFrame();
	virtual void  mouseMoveEvent ( QMouseEvent * event );
	virtual void mousePressEvent ( QMouseEvent * event );	
	virtual QSize sizeHint()const; 
signals:
	void mouseMoved(int row, int col);	
	void mouseClicked(int row, int col);
private:
	int row;
	int col;	
};


class GridSplitterWidget : public QWidget
{
	Q_OBJECT

public:
	GridSplitterWidget(int rows, int cols, int _maxRow, int _maxCol, const QColor & _color, QWidget *parent = NULL);
	~GridSplitterWidget();
	void addIndexWidget( QWidget * widget);	
	int rows;
	int cols;	
	void SetRowsCols(int rows, int cols);
	public slots:
	void ChangeRowsCols(int rows, int cols);
	void ChangePossibleActivity(int rows, int cols);
signals:
	void changedSplitting(QVector2D);
private:
	QColor  color;
	QGridLayout gridLayout;
	int maxRow;
	int maxCol;
	int mousedRow;
	int mousedCol;
};

#endif // GRIDSPLITTERWIDGET_H
