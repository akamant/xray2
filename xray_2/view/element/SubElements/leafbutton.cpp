#include "stdafx.h"
#include "leafbutton.h"
#include "treedatamodel.h"

LeafButton::LeafButton(QPixmap * pixmap, uint alpha, TreeDataViewModel * _model, const QModelIndex & _indexParent, positions _position, QWidget *parent)
	: CleverButton(alpha, pixmap, NULL, parent)
	,model(_model)
	,indexParent(_indexParent)
	, position (_position)

{
	//return;
 	connect	(	model,		SIGNAL	( dataChanged	(const QModelIndex & , const QModelIndex & ) ),
 				this,		SLOT	( checkIsOnSide	(const QModelIndex & , const QModelIndex &) )
 		);

	connect	(	model,				SIGNAL	( rowsAboutToBeRemoved 	( const QModelIndex &, int, int ) ),
				this,				SLOT	( checkIsOnSide			( const QModelIndex &, int, int ) )
		);

			
	//if(onSide())
	//	hide();
}

LeafButton::~LeafButton()
{

}

void LeafButton::checkIsOnSide	(const QModelIndex & topLeft, const QModelIndex & bottomRight){	
	if(model->parent(topLeft) == indexParent)
		if(onSide())
			hide();
		else			
			show();			
}

void LeafButton::checkIsOnSide	(const QModelIndex & index, int first, int last ){
	int a = 5;
	if(index == indexParent)
		if(onSide())			
			hide();
		else			
			CleverButton::show();	
}



bool LeafButton::onSide()
{
	//return true;
	if(position == LeafButton::FirstPosition)
	{
		QModelIndex emptyIndex = model->getDataModel()->emptyDicomDataIndex();
		QModelIndex firstIndex = model->index(0,0, indexParent);
		if( firstIndex != QModelIndex()
		//	&& model->getDataModelIndex(firstIndex) != emptyIndex
		//	&& indexParent != emptyIndex
		)
		{
			QModelIndex dataModelOfFirstChild = model->getDataModelIndex(firstIndex);


			QModelIndex dataModelParentOfFirst = model->getDataModel()->parent(dataModelOfFirstChild);
			if(model->getDataModel()->index(0, 0, dataModelParentOfFirst) == dataModelOfFirstChild)
				return true;
			else
				return false;
		}
	}else{
		//return false;
		int rowCount = model->rowCount(indexParent);		

		QModelIndex lastIndex = model->index(rowCount - 1, 0, indexParent);
		if(model->getDataModelIndex(lastIndex) == model->getDataModel()->emptyDicomDataIndex())
			return true;
		if( lastIndex != QModelIndex()){
			QModelIndex dataModelOfLastChild = model->getDataModelIndex(lastIndex);
			QModelIndex dataModelParentOfLast = model->getDataModel()->parent(dataModelOfLastChild);

			int rowCountDatamodel = model->getDataModel()->rowCount(dataModelParentOfLast);
			if(model->getDataModel()->index(rowCountDatamodel - 1, 0, dataModelParentOfLast) == dataModelOfLastChild)
				return true;
			else
				return false;
		}
	}
	return false;
}



void LeafButton::showEvent(QShowEvent * e)
{
	CleverButton::showEvent(e);
	if(onSide())
		hide();
}