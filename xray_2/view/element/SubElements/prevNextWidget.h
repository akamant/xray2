#ifndef PREVNEXTWIDGET_H
#define PREVNEXTWIDGET_H

#include <QObject>


class TreeDataViewModel;
class QModelIndex;
class PrevNextWidget : public QWidget
{
	Q_OBJECT

signals:
	void clicked();
public:	
	PrevNextWidget(TreeDataViewModel * model, const QModelIndex & indexParent, QWidget *parent = NULL);
	~PrevNextWidget();
	bool eventFilter(QObject* obj, QEvent * e);	
private:
	
};

#endif // PREVNEXTWIDGET_H
