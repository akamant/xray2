 #include "STDafx.h"
#include "labelwithautosizetext.h"


LabelWithAutoSizeText::LabelWithAutoSizeText( const QString & text, const Qt::Alignment & _alignment, QWidget * parent /*= NULL */ )
	: QWidget(parent)
	, alignment (_alignment)
{	
 	setFont (QFont("", 50 ) );		
	//QGridLayout * Gl = new QGridLayout (this);
	//setLayout(Gl);
	label.setText(text);
	//label.setMargin(0);
	//label.setLineWidth(0);
	//label.setMidLineWidth(0);
	//label.setIndent(0);
	//label.setMouseTracking(true);
	//label.setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);	
	//Gl->addWidget(&label, 0, 0, 1, 1, Qt::AlignLeft);
	//setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);	
	//Gl->setMargin(0);
	//Gl->setSpacing(0);
	
}




LabelWithAutoSizeText::~LabelWithAutoSizeText()
{

}

void LabelWithAutoSizeText::paintEvent( QPaintEvent *e )
{
	if(prevHeight !=  height() || prevWidth !=  width()){
		int h = height();
		if(height() > 50)
			h = 50;		
		//QFont font = getFontText(label.text(), h, width());
	
		//setFont(font);

	}
	prevHeight = height();
	prevWidth =  width();	
	//QWidget::paintEvent(e);

	QPainter painter (this);

	painter.setClipping (true);
	painter.setBackgroundMode(Qt::TransparentMode);
	//painter.setFont(QFont("", 100));
	painter.setPen(QPen(QColor(0, 0, 0)));
	QRect rect = this->rect();

	if(alignment == Qt::AlignRight)
		rect.setRight(rect.right() - 2);

	painter.drawText(rect, label.text(), alignment);

	painter.setPen(QPen(QColor(255, 255, 255)));
	rect.setX(rect.x() + 0); rect.setY(rect.y() + 0);
	rect.setWidth(rect.width() + 0); rect.setHeight(rect.height() + 0);
	painter.drawText(rect, label.text(), alignment);
	//painter.drawLine(QPoint(0,0), QPoint(1000,0));
	//painter.drawLine(QPoint(0,0), QPoint(0, 1000));
	painter.end();
}


void LabelWithAutoSizeText::keyReleaseEvent( QPaintEvent *e )
{


}


QFont LabelWithAutoSizeText::getFontText(const QString  & text, int heightMax, int side)
{	
	QFont  fontW ("", heightMax);
	fontW.setPixelSize(heightMax);
	QFontMetrics metric(fontW);
	int widthFont = metric.width(text);
	//int heightFont = metric.height();	
	int heightFont = metric.height();	
	heightFont = heightMax;
	int prevWidthFont = -1;
	int prevHeightFont = -1;
	while((widthFont > side  || heightFont >= heightMax) && widthFont > 1 && heightFont > 1) // ) && !(heightFont > prevHeightFont && widthFont >= prevWidthFont)
	{
		prevHeightFont = heightFont;
		prevWidthFont= widthFont;
		fontW.setPixelSize(fontW.pixelSize() - 1);
		QFontMetrics metric(fontW);
		widthFont = metric.width(text);		
		heightFont = metric.height();
		heightFont = fontW.pixelSize();
	}	
	
	return fontW;
}

void LabelWithAutoSizeText::resizeEvent( QResizeEvent * e )
{	
	setSize();
	QWidget::resizeEvent(e);
	update();
	//setSize();	
	//repaint();
}


void LabelWithAutoSizeText::showEvent( QShowEvent * e )
{	
	setSize();
	QWidget::showEvent(e);
}

void LabelWithAutoSizeText::setSize()
{
	setFont (QFont("", 50 ) );	
	int h = height();
	if(height() > 50)
		h = 50;
	QFont font = getFontText(label.text(), h, width());						
	setFont(font);
}

void LabelWithAutoSizeText::setText( QString str )
{	
	label.setText(str); 
	update();	
 	if(prevText != str)
 		prevWidth = -1;
	prevText = str;
}

void LabelWithAutoSizeText::mouseMoveEvent( QMouseEvent * event )
{
	QWidget::mouseMoveEvent(event);
}

void LabelWithAutoSizeText::setBGColor( QColor color )
{
	QPalette palette;
	palette.setColor( QPalette::Text,			QColor( 255, 255, 255 ) );	
	palette.setColor( QPalette::Foreground,		QColor( 255, 255, 255 ) );	
	palette.setColor( QPalette::Background,		color );	
	setPalette( palette );
	label.setPalette( palette );
}