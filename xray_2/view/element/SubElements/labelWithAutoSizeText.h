#ifndef TEXTEDITWITHAUTOSIZETEXT_H
#define TEXTEDITWITHAUTOSIZETEXT_H

#include <QObject>

class LabelWithAutoSizeText : public QWidget
{
	Q_OBJECT

public:
	LabelWithAutoSizeText(QWidget *parent = NULL);
	LabelWithAutoSizeText ( const QString & text, const Qt::Alignment & _alignment, QWidget * parent = NULL );
	~LabelWithAutoSizeText();
	void setText(QString str);
	QString getText(){ return label.text();};
	static QFont getFontText(const QString & text, int heightMax, int side);
	void setBGColor (QColor color);
protected:
	QString prevText;
	int prevHeight;
	int prevWidth ;
	virtual void paintEvent(QPaintEvent *e);
	virtual void resizeEvent( QResizeEvent * e );
	void showEvent( QShowEvent * e );
	void keyReleaseEvent( QPaintEvent *e );
	void mouseMoveEvent(QMouseEvent * event);
	void setSize();
private:
	Qt::Alignment alignment;
	QLabel label;
};

#endif // TEXTEDITWITHAUTOSIZETEXT_H
