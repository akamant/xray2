#ifndef LEAFBUTTON_H
#define LEAFBUTTON_H

#include "cleverbutton.h"
#include "TreeDataViewModel.h"

class LeafButton : public CleverButton
{
	Q_OBJECT

public:
	enum positions {FirstPosition, LastPosition};
	LeafButton(QPixmap * pixmap, uint alpha, TreeDataViewModel * model, const QModelIndex & indexParent, positions _position, QWidget *parent = NULL);
	~LeafButton();
	

protected:
	bool onSide();
	virtual void LeafButton::showEvent(QShowEvent * e);
private:
	TreeDataViewModel * model; 
	QModelIndex indexParent;
	positions position;
protected slots:
	//void checkIsOnSide (const QModelIndex & index);
	void checkIsOnSide (const QModelIndex & topLeft, const QModelIndex & bottomRight);
	void checkIsOnSide (const QModelIndex & index, int first, int last );
};
#endif // LEAFBUTTON_H
