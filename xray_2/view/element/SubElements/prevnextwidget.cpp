#include "stdafx.h"
#include "prevnextwidget.h"
#include "TreeDataViewModel.h"
#include "leafbutton.h"
#include "modelcontrols.h"


PrevNextWidget::PrevNextWidget(TreeDataViewModel * model, const QModelIndex & indexParent, QWidget *parent)
	: QWidget(parent)
{
	QGridLayout * prevNextLayout = new QGridLayout();
	setLayout(prevNextLayout);		

	ModelControls * controls = new ModelControls( model, indexParent, this);
	LeafButton * prevB = new LeafButton(new QPixmap(":/resources/images/gray/prev.png"), 100, model, indexParent, LeafButton::FirstPosition);	
	LeafButton * nextB = new LeafButton(new QPixmap(":/resources/images/gray/next.png"), 100, model, indexParent, LeafButton::LastPosition);

	connect(prevB,					SIGNAL	( clicked				() ),
			controls,				SLOT	( selectPrevChildren	() )
		);

	connect(nextB,					SIGNAL	( clicked				() ),
			controls,				SLOT	( selectNextChildren	() )
		);


	QWidget * prevNextWidget = new QWidget();
	QWidget * emptyWidget = new QWidget();
	QGridLayout * gridLayout = new QGridLayout ();
	prevNextWidget->setLayout(gridLayout);
	gridLayout->addWidget(prevB, 0, 0, 1, 1, Qt::AlignBottom);
	gridLayout->addWidget(nextB, 1, 0, 1, 1, Qt::AlignTop);
	//prevNextWidget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

	//prevNextLayout->addWidget(prevB, 0, 0, 1, 1, Qt::AlignLeft);
	//prevNextLayout->addWidget(nextB, 0, 1, 1, 1, Qt::AlignRight);
	prevNextLayout->addWidget(emptyWidget,		0, 0, 1, 1, Qt::AlignRight);
	prevNextLayout->addWidget(prevNextWidget,	1, 0, 1, 1, Qt::AlignRight);
	prevNextLayout->setMargin(0);
	prevNextLayout->setSpacing(0);
	prevB->installEventFilter(this);
	nextB->installEventFilter(this);
}

PrevNextWidget::~PrevNextWidget()
{

}

bool PrevNextWidget::eventFilter(QObject* obj, QEvent * e) {
	if (e->type() == QEvent::MouseButtonPress) 	{
		emit clicked();
		//mousePressEvent((QMouseEvent *)e);
		QApplication::sendEvent(this, e);
	}
	return false;
	return QWidget::eventFilter(obj, e);

}