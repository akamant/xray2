#ifndef DICOMBITMAPPRESENTER_H
#define DICOMBITMAPPRESENTER_H

#include <QObject>
#include "normalizedImage.h"
#include "dicomdata.h"

class dicomBitMapPresenter : public QObject
{
	Q_OBJECT

public:
	dicomBitMapPresenter(QObject *parent);
	bool representDicomData(DicomData * dicomData);
	~dicomBitMapPresenter();
private:
	void convertYBRFULLtoRGB(void * destImg, void * sourseImg, int width, int height);
	void turnVertStruct(DicomData * dicomData);
	//VNormalizedImage * present;
};

#endif // DICOMBITMAPPRESENTER_H
