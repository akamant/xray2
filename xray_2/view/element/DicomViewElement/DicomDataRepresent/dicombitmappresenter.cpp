#include "StdAfx.h"
#include "dicombitmappresenter.h"

dicomBitMapPresenter::dicomBitMapPresenter(QObject *parent)
	: QObject(parent)
{
	
}

dicomBitMapPresenter::~dicomBitMapPresenter()
{

}

void dicomBitMapPresenter::turnVertStruct(DicomData * dicomData){	
	int width = dicomData->imageParameters.width * dicomData->imageParameters.samplesPerPixel;
	int height = dicomData->imageParameters.height;
	char *  buffer;	
	buffer = new char [width * getSizePixel(dicomData)];	
	for(int i = 0; i < height / 2 ; i++){			
		memcpy(buffer,(((char *)dicomData->getNormImageBody() + i * width * getSizePixel(dicomData))), width * getSizePixel(dicomData));
		memcpy((((char *)dicomData->getNormImageBody() + i * width * getSizePixel(dicomData))), (((char *)dicomData->getNormImageBody() + (height - i - 1) * width * getSizePixel(dicomData))), width * getSizePixel(dicomData));
		memcpy((((char *)dicomData->getNormImageBody() + (height - i - 1) * width * getSizePixel(dicomData))), buffer, width * getSizePixel(dicomData));
	}			
	if(dicomData->numberOfRotations == 0 || dicomData->numberOfRotations == 2 
		|| dicomData->numberOfRotations == 4){
			if(dicomData->numberOfVerTurns == 0)
				dicomData->numberOfVerTurns = 1;
			else
				dicomData->numberOfVerTurns = 0;
	}else{
		if(dicomData->numberOfHorTurns == 0)
			dicomData->numberOfHorTurns = 1;
		else
			dicomData->numberOfHorTurns = 0;		
	}	
	delete [] buffer;
	return;	
}
void dicomBitMapPresenter::convertYBRFULLtoRGB(void * destImg, void * sourseImg, int width, int height)
{
	unsigned int buffer1;
	unsigned int buffer2;
	unsigned int buffer3;
	for(int i = 0; i < width * height; i ++)
	{
		int offset =  i * 3;
		buffer1 = (float)(*((unsigned char *)sourseImg + offset + 0)) + 1.402f * ((float)(*((unsigned char *)sourseImg + offset + 2)) - 128.0f);		
		buffer2 = (float)(*((unsigned char *)sourseImg + offset + 0)) - 0.344136f * ((float)(*((unsigned char *)sourseImg + offset + 1)) - 128.0f) - 0.714136 * ((float)(*((unsigned char *)sourseImg + offset + 2)) - 128.0);				
		buffer3 = (float)(*((unsigned char *)sourseImg + offset + 0)) + 1.772f * ((float)(*((unsigned char *)sourseImg + offset + 1)) - 128.0f);							
		*((unsigned char *)destImg + offset + 0) = buffer1;
		*((unsigned char *)destImg + offset + 1) = buffer2;
		*((unsigned char *)destImg + offset + 2) = buffer3;		
	}	
	return;
}

bool dicomBitMapPresenter::representDicomData( DicomData * dicomData )
{
	if(dicomData &&  !dicomData->getisNormalized() && dicomData->getBody()&& dicomData->getNormalizedImage() == NULL)
	{				
		if(dicomData->isVDRFile){
			if(dicomData->imageParameters.width % 2 != 0)
			{
				dicomData->imageParameters.width++;				
				dicomData->imageParameters.height -= 
					((int)(dicomData->imageParameters.height / dicomData->imageParameters.width) + 1);
			}
		}
		int width = dicomData->imageParameters.width * dicomData->imageParameters.samplesPerPixel;
		int height = dicomData->imageParameters.height;		
		int bitsAllocated = 32;
		int bitsStored = dicomData->imageParameters.bitsStored;
		int highBit = dicomData->imageParameters.highBit;
		int numberOfPixel;
		BOOLEAN pixelRepresentation;
		pixelRepresentation = false;
		if(dicomData->imageParameters.pixelRepresentation == 1)
			pixelRepresentation = true;
		else
			if(dicomData->imageParameters.pixelRepresentation == 0)
				pixelRepresentation = false;				
		unsigned int complement;
		unsigned int valueOfHighBit;
		int sizePixel = getSizePixel(dicomData);
		int normSize = width * height * sizePixel;
		unsigned char * pImageNew = new unsigned char[normSize];		
		void * pixel;
		unsigned int value;
		int minValue = 99999;
		int maxValue = -99999;	
		int aa = width * height / 3;		
		void * destPointer = NULL;					
		void * imageBuffer = dicomData->getBody();
		int quotient = dicomData->imageParameters.bitsAllocated / 8;
		int difference = bitsAllocated - bitsStored;
		complement = 0xFFFFFFFF;	
		int complementOffset = complement >> difference;;
		if(dicomData->imageParameters.planarConfiguration == 0)
		{
			dicomData->imageParameters.planarConfiguration = 0;
		}else{
			dicomData->imageParameters.planarConfiguration = 1;
		}


		for(int i = 0; i < height; i++){
			int offseti = width * i;
			for(int j = 0; j < width; j++){
				numberOfPixel = offseti + j;				
				if(dicomData->imageParameters.samplesPerPixel == 1)
				{
					int numberOfByte = numberOfPixel * quotient;				
					pixel = (unsigned char *) imageBuffer + numberOfByte;					
					//value = *(unsigned int *)pixel;	
					memcpy(&value, pixel, quotient);
					value = value >> (numberOfPixel * dicomData->imageParameters.bitsAllocated) % 8;						
					if(pixelRepresentation){												
						valueOfHighBit = value;	
						valueOfHighBit = valueOfHighBit << difference;					
						value = value & complementOffset;
						valueOfHighBit = valueOfHighBit & 0x80000000;						
						complement = 0;					
						if(valueOfHighBit != 0){
							for(int d = 0; d < difference; d++){
								complement += valueOfHighBit;
								valueOfHighBit = valueOfHighBit >> 1;
							}
							value = value | complement;	
						}					
					}else{						
						value = value & complementOffset;	
					}						
					if(dicomData->maxValue < (int)value)
						dicomData->maxValue = (int)value;
					if((int)value < minValue)
						minValue = (int)value;
					if((int)value > maxValue)
						maxValue = (int)value;
					dicomData->minValue = (int)minValue;						
					destPointer = pImageNew + (numberOfPixel * sizePixel);
					int aa = width * height / 2;						
				}else{					
					if(dicomData->imageParameters.planarConfiguration == 0						
						){			
							value = *((unsigned char *)imageBuffer + numberOfPixel);
							destPointer = pImageNew + (numberOfPixel * sizePixel);
					}else{
						value = *((unsigned char *)imageBuffer + numberOfPixel);
						destPointer = pImageNew + ((numberOfPixel % aa * 3 + numberOfPixel / aa - 0) * sizePixel);
					}
					if(dicomData->maxValue < (int)value)
						dicomData->maxValue = (int)value;
					if((int)value < minValue)
						minValue = (int)value;
					if((int)value > maxValue)
						maxValue = (int)value;
					dicomData->minValue = (int)minValue;
				}
				if(sizePixel == 1){
					*(unsigned char *)destPointer = (unsigned char)value;
				}
				if(sizePixel == 2){					
					*(unsigned short *)destPointer = (unsigned short)value;				
				}
				if(sizePixel == 4)
					*(unsigned int *)destPointer = (unsigned int)value;			
			}
		}
				
		if(dicomData->representationParameters.windowValue == 0)
		{
			dicomData->representationParameters.windowValue = maxValue - minValue;
			dicomData->representationParameters.levelValue = minValue + (maxValue - minValue) / 2;
		}			

		if(dicomData->imageParameters.PhotometricInterpretation == YBR_FULL)
			convertYBRFULLtoRGB(pImageNew, pImageNew, dicomData->imageParameters.width, dicomData->imageParameters.height);				
			
		if(dicomData->representationParameters.windowValue == 0){
			dicomData->representationParameters.windowValue = maxValue - minValue;		
			dicomData->representationParameters.levelValue = (maxValue + minValue) / 2;
		}
		dicomData->representationParameters.LevelValueConst = dicomData->representationParameters.levelValue;
		dicomData->representationParameters.windowValueConst = dicomData->representationParameters.windowValue;
		pixel = NULL;
		destPointer = NULL;		
		///!!!dicomData->normalizedImage = new VNormalizedImage((char *)Panel::mEMManager->getImageBuffer(dicomData), dicomData->imageParameters.width, dicomData->imageParameters.height, dicomData->imageParameters.samplesPerPixel, getSizePixel(dicomData)
			//,dicomData->imageParameters.rescaleSlope, dicomData->imageParameters.rescaleIntercept, dicomData->imageParameters.pixelRepresentation);
		dicomData->setNormImageBody(pImageNew);
		
		//dicomData->setNormalizedImage()
		if(dicomData->orient == -1){			
			turnVertStruct(dicomData);	
			dicomData->numberOfVerTurns = 0;
		}	
		dicomData->setNormalizedImage(
			new VNormalizedImage((char *)dicomData->getNormImageBody(), dicomData->imageParameters.width, dicomData->imageParameters.height, dicomData->imageParameters.samplesPerPixel, getSizePixel(dicomData)
			,dicomData->imageParameters.rescaleSlope, dicomData->imageParameters.rescaleIntercept, dicomData->imageParameters.pixelRepresentation) 
			);

		//dicomData->ELEM_CONT().ge

		//getELEM_CONT().get(0x00080060).variant.toString();

		dicomData->setisNormalized(true);	
	}

	return true;
}
