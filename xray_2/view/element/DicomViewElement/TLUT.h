#pragma once
class DicomData;


class TWLLUT
{
public:
	TWLLUT(void);
	~TWLLUT(void);
	int getOutputValue(int inputValue);	
	void setisReflected(BOOLEAN isReflected);
	void doWLLUT(int maxValue, int w, int l);	
	bool operator == (const TWLLUT& other) const;
	bool operator != (const TWLLUT& other) const;
	void setVOILUTElement(const QVariantList & _VOILUT){VOILUT = _VOILUT;};
	void setFirstVOILUTEntry(int firstVOILUTEntry){_firstVOILUTEntry = firstVOILUTEntry;};
	int firstVOILUTEntry(){return _firstVOILUTEntry;};

private:	
	QVariantList VOILUT;
	int _firstVOILUTEntry;	
	int minInputValue;
	int maxInputValue;	
	BOOLEAN isReflected;	
	QVector <int> LUT;
};

extern void transformColors(DicomData * dicomData, TWLLUT & lut, unsigned char * pImage, bool);


