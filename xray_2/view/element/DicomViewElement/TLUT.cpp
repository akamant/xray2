#include "StdAfx.h"
#include "TLUT.h"
#include "dicomdata.h"

//#include "Panel.h"

TWLLUT::TWLLUT(void):
minInputValue(-100000),
maxInputValue(100000),
isReflected(false){
}

TWLLUT::~TWLLUT(void)
{
}

void TWLLUT::doWLLUT( int maxValue, int w, int l)
{
	LUT.resize(maxInputValue - minInputValue + 1);
	int maxValueReflect = maxValue;	
	int color;
	int left = l - w / 2;
	int right = l + w / 2;	

	QVariantList curVOILUT;
	if(VOILUT.size())
	{		
		if(w == VOILUT.size())
		{
			curVOILUT = VOILUT;
		}else{
		
			float k = (float) VOILUT.size() / (float) w;	
			for(int i = 0; i < w; i++)
			{			
				if(VOILUT.size() >= i * k)
					curVOILUT << VOILUT[i * k];
				else
					curVOILUT << VOILUT.last();
			}
		}
	}
	
	for(int i = minInputValue; i <= maxInputValue; i++)
	{
		if(curVOILUT.size() )
		{
			if( i >= left && i - left < curVOILUT.size() )
				color = maxValue * curVOILUT[i - left].toInt() / curVOILUT.last().toInt();
			else if(i < left)
				color = maxValue * curVOILUT[0].toInt() / curVOILUT.last().toInt();
			else
				color = maxValue;
		}else
		{
			if(right - left == 0)
			{
				color = 0;
			}else
			{
				if(i >= left && i <= right)
					color = maxValue * ((float)i - (float)left) / ((float)right - (float)left);
				
				if(i < left)
					color = 0;
				if(i > right)
					color = maxValue;	
			}
		}
	/*	int isReflected = this->isReflected;

		if(Panel::isViewInNegative){
			if(isReflected)
				isReflected = false;
			else
				isReflected = true;
		}*/
		bool curReflection = isReflected;
		//if(dicomData->imageParameters.PhotometricInterpretation == MONOCHROME1)
		//	curReflection= !curReflection;

		if(curReflection)
			LUT[i - minInputValue ] = maxValue - color;
		else
			LUT[i - minInputValue ] = color;	
	}
}

int TWLLUT::getOutputValue( int inputValue){
//	if(inputValue < minInputValue)
//		return LUT[0];
//	if(inputValue > maxInputValue)
//		return LUT[maxInputValue - minInputValue];
	return LUT[inputValue - minInputValue];
}

void TWLLUT::setisReflected(BOOLEAN isReflected )
{
	this->isReflected = isReflected;
}

bool TWLLUT::operator==( const TWLLUT& other ) const
{	

	if(this->LUT == other.LUT )
		return true;
	else
		return false;
}

bool TWLLUT::operator!=( const TWLLUT& other ) const
{
	return !operator==(other); 
}





extern void transformColors( DicomData * dicomData, TWLLUT & lut, unsigned char * pImage, bool isViewInNegative )
{
	//bool isViewInNegative = false;
	//bool isViewInNegative = false;

	char * ppImage;

	bool TEN_BITS_STATE = false;
	

	//////////////////////////////////////////////////

	if(!TEN_BITS_STATE)
	{		
	
		unsigned int width = dicomData->imageParameters.width;
		unsigned int height = dicomData->imageParameters.height;
		int offsetold = 0;	
		int colorValue1;
		int colorValue2;
		int colorValue3;
		unsigned int offsetDIB;
		unsigned int offset;
		unsigned int offsetSrci;
		unsigned int offsetunNorm;
		unsigned int offsetunNormi;	
		unsigned int sizeDib8bitGray = width * height;
		unsigned int sizeDib32bitGray = width * height * 4;
	
	
		unsigned int value;	
		if(dicomData->imageParameters.samplesPerPixel == 3 ){
			for(int i = 0; i < height ; i++){
				offsetunNormi = width * i;
				for(int j = 0; j < width; j++){	
					offsetunNorm = offsetunNormi  + j;
					offsetDIB = offsetunNorm * 4;
					offset = offsetunNorm * 3;	
					value = dicomData->getNormalizedImage()->getValue(offset);
					colorValue1 = lut.getOutputValue((unsigned char)value);					   
					value = dicomData->getNormalizedImage()->getValue(offset + 1);
					colorValue2 = lut.getOutputValue((unsigned char)value);	
					value = dicomData->getNormalizedImage()->getValue(offset + 2);
					colorValue3 = lut.getOutputValue((unsigned char)value);
					if(!TEN_BITS_STATE){
						*(pImage + offsetDIB + 0) = colorValue3;
						*(pImage + offsetDIB + 1) = colorValue2;
						*(pImage + offsetDIB + 2) = colorValue1;
						*(pImage + offsetDIB + 3) = 0;
					}else{
						*(ppImage + offsetDIB + 0) =colorValue3;
						*(ppImage + offsetDIB + 1) = colorValue2;
						*(ppImage + offsetDIB + 2) = colorValue1;
						*(ppImage + offsetDIB + 3) = 8191; // 256 * 32
					}			
				}		   
			}
		}else{
			width *= dicomData->imageParameters.samplesPerPixel;
			unsigned int offset;
			int value;	
			int spacing = 0;
			if(width % 4)
				spacing = 4 - width % 4;
			for( int i = 0; i < height ; i++)
			{
				offsetSrci =  width * i;
				int offsetDSTi =  (width + spacing) * i;

				for( int j = 0; j < width; j++){			   
					offset =  offsetSrci + j;	

					int offsetDST = offsetDSTi + j;
					//offsetDIB = offset * 4;	
					value = dicomData->getNormalizedImage()->getValue(offset);			
					colorValue1 = lut.getOutputValue(value);
					if(dicomData->imageParameters.PhotometricInterpretation == MONOCHROME1)
						colorValue1 = ~colorValue1;
					if(!TEN_BITS_STATE)
					{
						if(offset <= sizeDib8bitGray)												  
							*(pImage + offsetDST + 0) = (unsigned char) colorValue1;
					}else
					{	
						*(ppImage + offsetDIB + 0) = (unsigned short) colorValue1;	
						*(ppImage + offsetDIB + 1) = (unsigned short) colorValue1;
						*(ppImage + offsetDIB + 2) = (unsigned short) colorValue1;
						*(ppImage + offsetDIB + 3) = (unsigned short)  8191;				
					}		   
				}		   
			}  
		}
	}
}
