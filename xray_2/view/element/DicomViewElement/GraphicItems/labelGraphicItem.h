#ifndef VLABELGRAPHICITEM_H
#define VLABELGRAPHICITEM_H

#include <distanceGraphicItem.h>


class VQTextEdit : public QTextEdit
{
	Q_OBJECT
		void keyPressEvent(QKeyEvent *e){
			QTextEdit::keyPressEvent(e);
			if(e->key() == 0x01000004)
				emit enterSignal();
	}
signals:
	void enterSignal();
};
class VNewMark : public QDialog
{
	Q_OBJECT

public:
	VNewMark();
	//
	QString GetText(){ return text->toPlainText(); }
private:
	VQTextEdit *text;
};

class VLabelGraphicItem : public VDistanceGraphicItem
{
	//Q_OBJECT

public:
	//VLabelGraphicItem(DicomData * dicomData, QGraphicsItem *parent = NULL);
	VLabelGraphicItem(DicomData * dicomData, QPointF __secondPoint = QPointF (0,0), QString __text = "" , QGraphicsItem *parent  = NULL);
	~VLabelGraphicItem();

	virtual void proceccPosChanging( QPointF pointCur, QPointF point);

	bool proceccPress(QPointF pos);
protected:	
	//QString _text;
	virtual void drawGraphicPrimitive(QPainter *painter, QPen pen);
	virtual void drawText( QPainter * painter );

};

#endif // VLABELGRAPHICITEM_H
