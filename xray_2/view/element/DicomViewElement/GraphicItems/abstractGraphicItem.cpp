#include "stdafx.h"
#include "abstractgraphicitem.h"

VAbstractGraphicItem::VAbstractGraphicItem(DicomData * __dicomData, QGraphicsItem *parent )
	: QGraphicsItem(parent)
	, _dicomData (__dicomData)
	, state (0)
	, _isActive (false)
{
	normalColor = Qt::yellow;
	normalPen = QPen (normalColor);
	activePen = QPen (Qt::red);
	//selectPen = QPen (Qt::black);
	negativePen.setWidth(4.0 );
	negativePen.setCosmetic(true);
	
	normalPen.setWidth(2.0);
	normalPen.setCosmetic(true);
	
	activePen.setWidth(2.0);
	activePen.setCosmetic(true);

	//selectPen.setWidth(1.0);
	//selectPen.setCosmetic(true);

	normalPen.setCapStyle(Qt::RoundCap);
	activePen.setCapStyle(Qt::RoundCap);
	//selectPen.setCapStyle(Qt::RoundCap);

	curentPen = normalPen;	
	
// 	setFlag(QGraphicsItem::ItemIsSelectable, true);
// 	setFlag(QGraphicsItem::ItemIsMovable, true);

	QVector<qreal> dashes;
	dashes << 2 << 4;

	backgroundBoundPen.setStyle(Qt::CustomDashLine);
	backgroundBoundPen.setCosmetic(true);
	backgroundBoundPen.setWidth(2);
	backgroundBoundPen.setDashPattern (dashes);

	foregroundBoundPen.setStyle(Qt::CustomDashLine);
	foregroundBoundPen.setCosmetic(true);
	foregroundBoundPen.setWidth(1);
	foregroundBoundPen.setDashPattern (dashes);

}

VAbstractGraphicItem::~VAbstractGraphicItem()
{

}

void VAbstractGraphicItem::paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget /*= 0 */ )
{
	if(_isActive)
	{
		curentPen = activePen;
	}else{
		curentPen = normalPen;			
	}

	QColor negativeColor (255 - curentPen.color().red(), 255 - curentPen.color().green(), 255 - curentPen.color().blue());
	negativePen.setColor(negativeColor);	
	drawGraphicPrimitive(painter, negativePen);
	drawGraphicPrimitive(painter, curentPen);

	foregroundBoundPen.setColor(curentPen.color());
	backgroundBoundPen.setColor(negativeColor);
		
	painter->setPen(backgroundBoundPen);
	painter->drawRect(realBoundingRect());

	painter->setPen(foregroundBoundPen);
	painter->drawRect(realBoundingRect());
	
	drawText(painter);	
}

void VAbstractGraphicItem::setPos( const QPointF &pos )
{
	QGraphicsItem::setPos(pos);
}

QVariant VAbstractGraphicItem::itemChange(GraphicsItemChange change,
	const QVariant &value)
{
	//if (change == QGraphicsItem::ItemSelectedChange)
	//	emit selectedChange(this);
	this;
	return value;
}

void VAbstractGraphicItem::focusOutEvent(QFocusEvent *event)
{
	//setTextInteractionFlags(Qt::NoTextInteraction);
	//emit lostFocus(this);
	QGraphicsItem::focusOutEvent(event);
}


void VAbstractGraphicItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	scene()->clearSelection();
	QGraphicsItem::mousePressEvent(event);
	setSelected(true);
}


void VAbstractGraphicItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
// 	if (textInteractionFlags() & Qt::TextEditable) {
// 		QGraphicsItem::mouseMoveEvent(event);
// 	} else
		QGraphicsItem::mouseMoveEvent(event);
}

void VAbstractGraphicItem::drawGraphicPrimitive( QPainter * painter, QPen pen )
{
	painter->setPen(pen);		
}

float VAbstractGraphicItem::distToPoint( QPointF point )
{
	return 0xFFFF;
}

void VAbstractGraphicItem::setActive( bool isActive )
{
	QList<QGraphicsItem *> gItems = scene()->items();
	for(int i = 0; i < gItems.size(); i++)
	{
		if(dynamic_cast<VAbstractGraphicItem *> (gItems[i]))
		{		
			if(gItems[i] == this)
				((VAbstractGraphicItem*)gItems[i])->_isActive = isActive;
			else
				((VAbstractGraphicItem*)gItems[i])->_isActive = false;
		}
	}
	scene()->update();
}
