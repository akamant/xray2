#ifndef VDISTANCEGRAPHICITEM_H
#define VDISTANCEGRAPHICITEM_H

#include <abstractGraphicItem.h>
#include "dicomdata.h"

class VDistanceGraphicItem : public VAbstractGraphicItem
{
	//Q_OBJECT

public:	
	VDistanceGraphicItem(DicomData * dicomData, QPointF __secondPoint = QPointF (0, 0), QGraphicsItem *parent = NULL);
	~VDistanceGraphicItem();

	virtual void proceccPosChanging( QPointF pointCur, QPointF point);

	virtual bool proceccPress(QPointF pos);
	virtual bool proceccMousePos(QPointF pos);	
	virtual void init(QPointF pos);
	virtual QRectF boundingRect (void) const;
	virtual QRectF realBoundingRect( );
	virtual bool proceccRelease(QPointF pos);
	QPointF secondPoint (){return _secondPoint;};

protected:	
	virtual void drawText(QPainter * painter);
	virtual void drawGraphicPrimitive(QPainter *painter, QPen pen);
	virtual float distToPoint(QPointF point);

	QPointF _secondPoint;
	
};

#endif // VDISTANCEGRAPHICITEM_H
