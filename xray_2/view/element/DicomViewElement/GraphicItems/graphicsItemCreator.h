#pragma once
class VAbstractGraphicItem;
class GraphicsItemCreator
{

QMap<QString, int> graphicsItemClassNames;
public:
	GraphicsItemCreator(void);
	~GraphicsItemCreator(void);
	VAbstractGraphicItem * createCopy(VAbstractGraphicItem * source);
};

