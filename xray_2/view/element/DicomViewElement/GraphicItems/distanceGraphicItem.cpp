#include "stdafx.h"
#include "distancegraphicitem.h"


VDistanceGraphicItem::VDistanceGraphicItem(DicomData * dicomData, QPointF __secondPoint, QGraphicsItem *parent)
	: VAbstractGraphicItem(dicomData, parent)
	, _secondPoint(__secondPoint)
{

}

VDistanceGraphicItem::~VDistanceGraphicItem()
{

}

void VDistanceGraphicItem::drawGraphicPrimitive(QPainter *painter, QPen pen)
{
	VAbstractGraphicItem::drawGraphicPrimitive(painter, pen);
	painter->drawLine(QPoint(0, 0), _secondPoint);
}

void VDistanceGraphicItem::drawText( QPainter * painter )
{
		
	QVector2D vector(_secondPoint.rx() * _dicomData->ScaleX, _secondPoint.ry() * _dicomData->ScaleY);
	float radius = vector.length();
	//painter->drawText(QRectF(0, 0, 100, 100), Qt::AlignLeft , QString ("%1").arg(radius));	

	//painter->drawText(QRectF(0, 0, 100, 100), Qt::AlignLeft , "123 \n sdgs");	

	_text = QString ("%1 ��").arg(radius, 0, 'F', 1);
}

void VDistanceGraphicItem::init( QPointF pos )
{

}

QRectF VDistanceGraphicItem::boundingRect( void ) const
{
	return QRectF (-44444444, -44444444, 99999999, 99999999);
 	QGraphicsLineItem item ( 0, 0, _secondPoint.x(), _secondPoint.y());
 	return item.boundingRect();
	//return QRectF (0, 0, 99999999, 99999999);
	//return QRectF (-99999999, -99999999, 99999999, 99999999);

	//QVector2D vector(_secondPoint.x(), _secondPoint.y() );
	return QRectF	(	min(0,_secondPoint.x()), min(0, _secondPoint.y()), 
		 				abs(_secondPoint.x()),	abs(_secondPoint.y())
		 			);

	//QGraphicsLineItem GLine(0, 0, secondPoint.x(), secondPoint.y());
	//return GLine.boundingRect();

	QPointF p1 =  mapToScene(QPointF(0, 0) );
	QPointF p2 =  mapToScene(QPointF(_secondPoint.x(), _secondPoint.y()) );

	//return QRectF(p1.x(), p1.y(), p2.x(), p2.y());
	//return QRectF( 0, 0, secondPoint.x(), secondPoint.y() );

	return QRectF( 0, 0, _secondPoint.x(), _secondPoint.y() );


	;
// 	return QRectF(	min(0,secondPoint.x()), min(0, secondPoint.y()), 
// 					max(0,secondPoint.x()), max(0, secondPoint.y())
// 					);
}

QRectF VDistanceGraphicItem::realBoundingRect( )
{
	QGraphicsLineItem item ( 0, 0, _secondPoint.x(), _secondPoint.y());
	return item.boundingRect();	
}

bool VDistanceGraphicItem::proceccPress( QPointF pos )
{
	curentPen = activePen;
	state ++;
	switch(state)
	{
	case 1:
		{				
			break;
		}
	case 2:
		{
			_secondPoint = mapFromScene(pos);
			break;
		}				
	}	
	if(state == 2)
	{
		curentPen = normalPen;
		return true;
	}else{
		return false;
	}
}

bool VDistanceGraphicItem::proceccMousePos( QPointF pos )
{
	_secondPoint = mapFromScene(pos);
	return false;
}

bool VDistanceGraphicItem::proceccRelease( QPointF pos )
{
	return false;
}


float VDistanceGraphicItem::distToPoint(QPointF point)
{
	float accuracy;
	QPointF point1 = pos();
	QPointF point2 = _secondPoint;
	point2 = mapToScene(point2);


	float Dist1 = sqrt(float((point1.x() - point.x()) * (point1.x() - point.x()) + (point1.y() - point.y()) * (point1.y() - point.y())));
	float Dist2 = sqrt(float((point2.x() - point.x()) * (point2.x() - point.x()) + (point2.y() - point.y()) * (point2.y() - point.y())));
	float Dist12 = sqrt(float((point2.x() - point1.x()) * (point2.x() - point1.x()) + (point2.y() - point1.y()) * (point2.y() - point1.y())));
	accuracy = abs(Dist12 - Dist1 - Dist2);	
	return accuracy;
}



void VDistanceGraphicItem::proceccPosChanging( QPointF pointCur, QPointF point )
{
	QPointF point1 = pos();	
	QPointF & point2 = _secondPoint;
	point2 = mapToScene(point2);

	float displacementX = point.x() - pointCur.x();		
	float displacementY = point.y() - pointCur.y();
	float DistFull = sqrt(float((point1.x() - point2.x()) * (point1.x() - point2.x()) + (point1.y() - point2.y()) * (point1.y() - point2.y())));
	float DistToCenter = sqrt(float(((point1.x() + point2.x()) / 2 -  point.x()) * ((point1.x() + point2.x()) / 2 - point.x()) + ((point1.y() + point2.y()) / 2 - point.y()) * ((point1.y() + point2.y()) / 2 - point.y())));
	float Dist1 = sqrt(float((point1.x() - point.x()) * (point1.x() - point.x()) + (point1.y() - point.y()) * (point1.y() - point.y())));
	float Dist2 = sqrt(float((point2.x() - point.x()) * (point2.x() - point.x()) + (point2.y() - point.y()) * (point2.y() - point.y())));
	if(Dist1 < DistToCenter && Dist1 < Dist2)
	{
		point1.setX(point1.x() - displacementX);
		point1.setY(point1.y() - displacementY);
		setPos(point1);
		point2 = mapFromScene(point2);
		return;
	}
	if(Dist2 < DistToCenter)
	{
		point2.setX(point2.x() - displacementX);
		point2.setY(point2.y() - displacementY);
		point2 = mapFromScene(point2);
		return;
	}
	point1.setX(point1.x() - displacementX);
	point1.setY(point1.y() - displacementY);
	point2.setX(point2.x() - displacementX);
	point2.setY(point2.y() - displacementY);

	setPos(point1);
	point2 = mapFromScene(point2);
}