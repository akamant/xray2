#include "stdafx.h"
#include "labelgraphicitem.h"

VLabelGraphicItem::VLabelGraphicItem(DicomData * dicomData, QPointF __secondPoint, QString __text, QGraphicsItem *parent)
	: VDistanceGraphicItem(dicomData, __secondPoint, parent)
	//,_text(__text)
{
	_text = __text;
}

VLabelGraphicItem::~VLabelGraphicItem()
{

}

void VLabelGraphicItem::drawText( QPainter * painter )
{		
}

bool VLabelGraphicItem::proceccPress( QPointF pos )
{
	bool res = VDistanceGraphicItem::proceccPress(pos);
	if(state == 2)
	{
		VNewMark mark;
		if( QDialog::Accepted == mark.exec() )
		{
			_text = mark.GetText();
		}
	}
	return res;
}


void VLabelGraphicItem::drawGraphicPrimitive( QPainter *painter, QPen pen )
{
	VDistanceGraphicItem::drawGraphicPrimitive(painter, pen);
	float X1Line = 0;
	float Y1Line = 0;
	float X2Line = _secondPoint.x();
	float Y2Line = _secondPoint.y();
	if(Y1Line - Y2Line != 0 || X2Line - X1Line != 0){
		double pi = 3.14159265359;
		double alfa = atan((double)(Y1Line - Y2Line) / (double)(X2Line - X1Line));			
		double alfaGrad =  180.0f / pi * alfa;
		double beta;	
		beta =  alfaGrad - 30;		
		beta = beta * pi / 180;			
		double lengthArrow;

		double length = 15;
		if(scene()->views().size())
			length = scene()->views()[0]->mapToScene(QRect(0, 0, 15, 15)).boundingRect().width();
	
		if((X1Line - X2Line) > 0)
			lengthArrow = -length;
		else
			lengthArrow = length;
		int Y1Array = Y1Line - sin(beta) * lengthArrow;
		int X1Array = X1Line + cos(beta) * lengthArrow;
		painter->drawLine(QPoint(0, 0), QPoint( X1Array, Y1Array));
		//whiteAndBlackLine(hdcBufferImageandInformation, X1Line, Y1Line, X1Array, Y1Array, 1, 1, RGB(255, 255, 0), rectMainWindow);		
		beta =  alfaGrad + 30;		
		beta = beta * pi / 180;
		if((X1Line - X2Line) > 0)
			lengthArrow = -length;
		else
			lengthArrow = length;
		Y1Array = Y1Line - sin(beta) * lengthArrow;
		X1Array = X1Line + cos(beta) * lengthArrow;
		painter->drawLine(QPoint(0, 0), QPoint( X1Array, Y1Array));
		//whiteAndBlackLine(hdcBufferImageandInformation, X1Line, Y1Line, X1Array, Y1Array, 1, 1,RGB(255, 255, 0), rectMainWindow);		
	}
}

void VLabelGraphicItem::proceccPosChanging( QPointF pointCur, QPointF point )
{
	VDistanceGraphicItem::proceccPosChanging(pointCur, point);
}

VNewMark::VNewMark()
	: QDialog()
{
	text				= new VQTextEdit;
	QPushButton *ok     = new QPushButton("��");
	QPushButton *cancel = new QPushButton("������");

	text->setMaximumWidth( 200 );
	text->setMaximumHeight( 70 );

	connect( ok,	 SIGNAL( pressed()), SLOT(accept()) );
	//connect(text, SIGNAL(enterSignal()), SLOT(accept()) );
	connect( cancel, SIGNAL(pressed()), SLOT(reject()) );

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addWidget( text );

	QHBoxLayout *hl = new QHBoxLayout;
	hl->addWidget( ok );
	hl->addWidget( cancel );

	vl->addLayout( hl );

	setLayout( vl );
	setWindowTitle( "������� �����" );
}
