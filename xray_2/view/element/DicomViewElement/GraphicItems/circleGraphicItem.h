#ifndef VCIRCLEGRAPHICITEM_H
#define VCIRCLEGRAPHICITEM_H

#include <distanceGraphicItem.h>

class VNormalizedImage;
class VCircleGraphicItem : public VDistanceGraphicItem
{
	//Q_OBJECT

public:	
	VCircleGraphicItem(DicomData * dicomData, VNormalizedImage * __normImage, QPointF __secondPoint = QPointF (0, 0), QGraphicsItem *parent = NULL);
	~VCircleGraphicItem();	

	virtual void proceccPosChanging( QPointF pointCur, QPointF point);
	virtual float distToPoint(QPointF point);

	VNormalizedImage * normImage(){return _normImage;};
	virtual void setPos (const QPointF &pos);
protected:
	virtual void drawGraphicPrimitive(QPainter *painter, QPen pen);
	virtual void drawText( QPainter * painter );
	virtual QRectF realBoundingRect( );
	virtual void calculateAverageDensity();
	virtual bool proceccPress( QPointF pos );
	virtual bool proceccRelease(QPointF pos);
	float averageDensity;
	VNormalizedImage *_normImage;
};

#endif // VCIRCLEGRAPHICITEM_H
