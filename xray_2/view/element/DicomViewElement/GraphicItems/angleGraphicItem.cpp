#include "stdafx.h"
#include "anglegraphicitem.h"

VAngleGraphicItem::VAngleGraphicItem( DicomData * dicomData, QPointF __secondPoint /*= QPointF (0, 0)*/, QPointF __thirdPoint /*= QPointF (0, 0)*/, QGraphicsItem *parent /*= NULL*/ )
	: VAbstractGraphicItem(dicomData, parent)
	, _secondPoint(__secondPoint)
	, _thirdPoint(__thirdPoint)
{

}

VAngleGraphicItem::~VAngleGraphicItem()
{

}

bool VAngleGraphicItem::proceccPress( QPointF pos )
{
	state ++;
	switch(state)
	{
	case 1:
		{				
			break;
		}
	case 2:
		{
			_secondPoint = mapFromScene(pos);
			break;
		}	
	case 3:
		{
			_thirdPoint = mapFromScene(pos);
			break;
		}	
	}	
	if(state == 3){
		curentPen = normalPen;
		return true;
	}else{
		return false;
	}
}

void VAngleGraphicItem::init( QPointF pos )
{

}

void VAngleGraphicItem::drawGraphicPrimitive( QPainter * painter, QPen pen )
{
	VAbstractGraphicItem::drawGraphicPrimitive(painter, pen);
	painter->drawLine(QPoint(0, 0), _secondPoint);
	if(!_thirdPoint.isNull())
		painter->drawLine(_secondPoint, _thirdPoint);
}


void VAngleGraphicItem::drawText( QPainter * painter )
{

	if(!_thirdPoint.isNull()){
		double pi = 3.14159265359;
		double angle1 = 0;
		double angle2 = 0;
		float X1Line = 0;
		float Y1Line = 0;
		float X2Line = _secondPoint.x();
		float Y2Line = _secondPoint.y();

		float X3Line = _secondPoint.x();
		float Y3Line = _secondPoint.y();

		float X4Line = _thirdPoint.x();
		float Y4Line = _thirdPoint.y();



		if((X2Line - X1Line) != 0){
			angle1 = atan((double)(Y1Line - Y2Line) / (double)(X2Line - X1Line));				
		}else{
			angle1 = pi / 2.0f;
		}
		if(Y2Line == Y1Line)
			angle1 = 0.0f;
		if((X3Line - X4Line) != 0){
			angle2 = atan((double)(Y4Line - Y3Line) / (double)(X3Line - X4Line));	
		}else{
			angle2 = pi / 2.0f;
		}
		if(Y3Line == Y4Line)
			angle2 = 0.0f;

		double angle1Grad =  180.0f / pi * angle1;
		if((X1Line - X2Line) < 0)
			angle1Grad += 180;
		double angle2Grad =  180.0f / pi * angle2;
		if((X4Line - X3Line) < 0)
			angle2Grad += 180;

		int resultAngle = abs(angle1Grad - angle2Grad);
		if(resultAngle > 180)
			resultAngle = 360 - resultAngle;

		_text = QString("%1 ����.").arg(resultAngle);
	}
}

QRectF VAngleGraphicItem::realBoundingRect()
{

	QPolygonF polygon;
	polygon << QPointF(0,0);
	if(!_secondPoint.isNull())
		polygon << _secondPoint;
	if(!_thirdPoint.isNull())
		polygon << _thirdPoint;
	return polygon.boundingRect();
}

QRectF VAngleGraphicItem::boundingRect( void ) const
{
	return QRectF (-44444444, -44444444, 99999999, 99999999);
}

bool VAngleGraphicItem::proceccMousePos( QPointF pos )
{
	switch(state)
	{
	case 1:
		{
			_secondPoint = mapFromScene(pos);
			break;
		}	
	case 2:
		{
			_thirdPoint = mapFromScene(pos);
			break;
		}	
	}	
	return false;
}

bool VAngleGraphicItem::proceccRelease( QPointF pos )
{
	return false;
}

float VAngleGraphicItem::distToPoint( QPointF point )
{
	QPointF point1 = pos();
	QPointF point2 = _secondPoint;
	QPointF point3 = _thirdPoint;
	point2 = mapToScene(point2);
	point3 = mapToScene(point3);

	float accuracy;
	float Dist1 = sqrt(float((point1.x() - point.x()) * (point1.x() - point.x()) + (point1.y() - point.y()) * (point1.y() - point.y())));
	float Dist2 = sqrt(float((point2.x() - point.x()) * (point2.x() - point.x()) + (point2.y() - point.y()) * (point2.y() - point.y())));
	float Dist12 = sqrt(float((point2.x() - point1.x()) * (point2.x() - point1.x()) + (point2.y() - point1.y()) * (point2.y() - point1.y())));
	accuracy = abs(Dist12 - Dist1 - Dist2);
	Dist1 = sqrt(float((point3.x() - point.x()) * (point3.x() - point.x()) + (point3.y() - point.y()) * (point3.y() - point.y())));
	Dist2 = sqrt(float((point2.x() - point.x()) * (point2.x() - point.x()) + (point2.y() - point.y()) * (point2.y() - point.y())));
	Dist12 = sqrt(float((point2.x() - point3.x()) * (point2.x() - point3.x()) + (point2.y() - point3.y()) * (point2.y() - point3.y())));
	if(accuracy > abs(Dist12 - Dist1 - Dist2))
		accuracy = abs(Dist12 - Dist1 - Dist2);
	return accuracy;
}

void VAngleGraphicItem::proceccPosChanging( QPointF pointCur, QPointF point )
{
	QPointF point1 = pos();	
	QPointF & point2 = _secondPoint;
	point2 = mapToScene(point2);
	QPointF & point3 = _thirdPoint;
	point3 = mapToScene(point3);


	float displacementX = point.x() - pointCur.x();
	float displacementY = point.y() - pointCur.y();
	float DistFull1 = sqrt(float((point1.x() - point2.x()) * (point1.x() - point2.x()) + (point1.y() - point2.y()) * (point1.y() - point2.y())));
	float DistFull2 = sqrt(float((point2.x() - point3.x()) * (point2.x() - point3.x()) + (point2.y() - point3.y()) * (point2.y() - point3.y())));
	float DistFull = (DistFull1 + DistFull2) / 2;
	float DistToCenter = sqrt(float(((point1.x() + point2.x() + point3.x()) / 3 -  point.x()) * ((point1.x() + point2.x()+ point3.x()) / 3 - point.x()) + ((point1.y() + point2.y() + point3.y()) / 3 - point.y()) * ((point1.y() + point2.y() + point3.y()) / 3 - point.y())));
	float Dist1 = sqrt(float((point1.x() - point.x()) * (point1.x() - point.x()) + (point1.y() - point.y()) * (point1.y() - point.y())));
	float Dist2 = sqrt(float((point2.x() - point.x()) * (point2.x() - point.x()) + (point2.y() - point.y()) * (point2.y() - point.y())));
	float Dist3 = sqrt(float((point3.x() - point.x()) * (point3.x() - point.x()) + (point3.y() - point.y()) * (point3.y() - point.y())));
	if(Dist1 < DistFull1 / 7 && Dist1 < Dist2 && Dist1 < Dist3){
		point1.setX(point1.x() - displacementX);
		point1.setY(point1.y() - displacementY);
		setPos(point1);
		point2 = mapFromScene(point2);
		point3 = mapFromScene(point3);
		return;
	}
	//if(Dist2 < DistToCenter && Dist1 < Dist3){
	if(Dist2 < DistFull / 7 && Dist2 < Dist3){
		point2.setX(point2.x() - displacementX);
		point2.setY(point2.y() - displacementY);
		point2 = mapFromScene(point2);
		point3 = mapFromScene(point3);
		return;
	}
	//if(Dist3 < DistToCenter){
	if(Dist3 < DistFull2 / 7){
		point3.setX(point3.x() - displacementX);
		point3.setY(point3.y() - displacementY);
		point2 = mapFromScene(point2);
		point3 = mapFromScene(point3);
		return;
	}

	point1.setX(point1.x() - displacementX);
	point1.setY(point1.y() - displacementY);
	point2.setX(point2.x() - displacementX);
	point2.setY(point2.y() - displacementY);
	point3.setX(point3.x() - displacementX);
	point3.setY(point3.y() - displacementY);

	setPos(point1);
	point2 = mapFromScene(point2);
	point3 = mapFromScene(point3);
}

