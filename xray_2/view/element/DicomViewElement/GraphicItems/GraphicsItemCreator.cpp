#include "StdAfx.h"
#include "GraphicsItemCreator.h"


#include "distancegraphicitem.h"
#include "anglegraphicitem.h"
#include "circlegraphicitem.h"
#include "labelgraphicitem.h"
#include "polygongraphicitem.h"

GraphicsItemCreator::GraphicsItemCreator(void)
{
// 	graphicsItemClassNames.insert (	VDistanceGraphicItem::staticMetaObject.className(), 0);
// 	graphicsItemClassNames.insert (	VAngleGraphicItem	::staticMetaObject.className(), 1);
// 	graphicsItemClassNames.insert (	VCircleGraphicItem	::staticMetaObject.className(), 2);
// 	graphicsItemClassNames.insert (	VLabelGraphicItem	::staticMetaObject.className(), 3);
// 	graphicsItemClassNames.insert (	VPolygonGraphicItem	::staticMetaObject.className(), 4);

}

VAbstractGraphicItem * GraphicsItemCreator::createCopy(VAbstractGraphicItem * source)
{
	VAbstractGraphicItem * returnItem;

	if(dynamic_cast<VPolygonGraphicItem* >(source))
		return new VPolygonGraphicItem(source->dicomData(),((VPolygonGraphicItem*)source)->normImage(), ((VPolygonGraphicItem*)source)->polygon());
	if(dynamic_cast<VAngleGraphicItem* >(source))
		return new VAngleGraphicItem(source->dicomData(), ((VAngleGraphicItem*)source)->secondPoint(), ((VAngleGraphicItem*)source)->thirdPoint() );
	if(dynamic_cast<VCircleGraphicItem* >(source))
		return  new VCircleGraphicItem(source->dicomData(),((VCircleGraphicItem*)source)->normImage(), ((VCircleGraphicItem*)source)->secondPoint() );	
	if(dynamic_cast<VLabelGraphicItem* >(source))
		return  new VLabelGraphicItem(source->dicomData(), ((VLabelGraphicItem*)source)->secondPoint(),((VLabelGraphicItem*)source)->text() );
	if(dynamic_cast<VDistanceGraphicItem* >(source))
		return  new VDistanceGraphicItem(source->dicomData(), ((VDistanceGraphicItem*)source)->secondPoint());

	

		//return new VDistanceGraphicItem(* (VDistanceGraphicItem *)source);
//  	QString className =  source->metaObject()->className();
// 
// 	if		(className == VDistanceGraphicItem::staticMetaObject.className())
// 		return new VDistanceGraphicItem(* (VDistanceGraphicItem *)source);
// 	else if	(className == VCircleGraphicItem::staticMetaObject.className())
// 		return new VCircleGraphicItem(* (VCircleGraphicItem *)source);
	
	

//  	switch (graphicsItemClassNames[className])
//  	{
// 	case graphicsItemClassNames[VDistanceGraphicItem::staticMetaObject.className()]:
// 		{
// 			return new VDistanceGraphicItem();
// 			break;
// 		}
 		
 //	}
	return NULL;
	
}

GraphicsItemCreator::~GraphicsItemCreator(void)
{
}
