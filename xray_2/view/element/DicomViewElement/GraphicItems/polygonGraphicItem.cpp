#include "stdafx.h"
#include "polygongraphicitem.h"

VPolygonGraphicItem::VPolygonGraphicItem( DicomData * __dicomData, VNormalizedImage * normImage, QPolygonF __polygon, QGraphicsItem *parent /*= NULL*/ )
	:VCircleGraphicItem(__dicomData, normImage, QPointF(0, 0), parent)
	, _polygon(__polygon)	
{	

}

VPolygonGraphicItem::~VPolygonGraphicItem()
{

}

void VPolygonGraphicItem::drawGraphicPrimitive( QPainter *painter, QPen pen )
{
	VAbstractGraphicItem::drawGraphicPrimitive(painter, pen);
	painter->drawPolygon(_polygon);
}

bool VPolygonGraphicItem::proceccPress( QPointF pos )
{
	return VCircleGraphicItem::proceccPress(pos);	
}

bool VPolygonGraphicItem::proceccMousePos( QPointF pos )
{
	_polygon <<  mapFromScene(pos);
	_text = "";
	return false;
}

bool VPolygonGraphicItem::proceccRelease(QPointF pos)
{
	return VCircleGraphicItem::proceccRelease(pos);	
}

QRectF VPolygonGraphicItem::realBoundingRect()
{
	return _polygon.boundingRect();
}

void VPolygonGraphicItem::calculateAverageDensity()
{	
	float dx, dy;	
	float pr = 0.0;
	int p = 0;

	averageDensity = 0;	
	QVector2D vector(_secondPoint.rx(), _secondPoint.ry());
	int radius = vector.length();

	QGraphicsItem * pixmapItem = NULL;
	QGraphicsScene * scene = this->scene();
	if(!scene)
		return;
	for(int i = 0; i < scene->items().size(); i ++ ){
		if(dynamic_cast<QGraphicsPixmapItem *>(scene->items()[i])){
			pixmapItem = scene->items()[i];
			break;
		}
	}
	//===== Perimeter calculation  =====//
	for (int j = 0; j < _polygon.size() - 1; j++) {
		dx = dicomData()->ScaleX * (_polygon[j].x() - _polygon[j+1].x());
		dy = dicomData()->ScaleY * (_polygon[j].y() - _polygon[j+1].y());
		if((dx*dx + dy*dy) > 0)
			pr += sqrt((dx*dx + dy*dy));
	}
	p = (int)pr;

	if (!pixmapItem)
		return;

	//===== Square and average calculation  =====//
	QPointF point1 = mapToScene(QPointF(0, 0) ).toPoint() - pixmapItem->pos();
	int xmin   =  point1.x() + _polygon.boundingRect().x();
	int ymin   =  point1.y() + _polygon.boundingRect().y();
	int xmax   =  point1.x() + _polygon.boundingRect().width();
	int ymax   =  point1.y() + _polygon.boundingRect().height();

	int i = 0;
	long long sumDensity = 0;
	int pxcnt = 0;
	for (dy = ymin; dy < ymax; dy++) 
	{
		for (dx = xmin; dx < xmax; dx++)
		{
			//int curRadius = sqrt(float((point1.x() - dx) * (point1.x() - dx) + (point1.y() - dy) * (point1.y() - dy)));
			QPointF curPoint(dx - point1.x(), dy -  point1.y());
			if (_polygon.containsPoint(curPoint, Qt::OddEvenFill))
			{
				if	(	dy * _normImage->getWidth() + dx < _normImage->getWidth() * _normImage->getHeight() 
					&&	dy * _normImage->getWidth() + dx >= 0
					)
				{											
					int value = _normImage->getValue((int)dy * _normImage->getWidth() + (int)dx);					
					sumDensity += value;
					i++;
				}		
				pxcnt++;
			}
		}
	}		
	if(i != 0){
		averageDensity = sumDensity / i;	
	}else{
		int value = _normImage->getValue((int)(int)point1.y() * _normImage->getWidth() + (int)point1.x());		
		averageDensity = value;
	}
	//if(averageDensity)	
		_text = QString ("D = %1 \nS = %2\nP = %3").arg(averageDensity).arg((int)(pxcnt *  dicomData()->ScaleX * dicomData()->ScaleY)).arg(p);
}


void VPolygonGraphicItem::proceccPosChanging( QPointF pointCur, QPointF point )
{
	QPointF point1 = pos();	
	float displacementX = point.x() - pointCur.x();		
	float displacementY = point.y() - pointCur.y();	
	point1.setX(point1.x() - displacementX);
	point1.setY(point1.y() - displacementY);
	setPos(point1);
	calculateAverageDensity();
}

float VPolygonGraphicItem::distToPoint( QPointF point )
{	
	float accuracy = 9999;	
	QPointF point2;
	for(unsigned j = 0; j < _polygon.size(); j++)
	{
		QPointF point1 = _polygon[j];	
		point1 = mapToScene(point1);
		if(j > 0)
		{
			float Dist1 = sqrt(float((point1.x() - point.x()) * (point1.x() - point.x()) + (point1.y() - point.y()) * (point1.y() - point.y())));
			float Dist2 = sqrt(float((point2.x() - point.x()) * (point2.x() - point.x()) + (point2.y() - point.y()) * (point2.y() - point.y())));
			float Dist12 = sqrt(float((point2.x() - point1.x()) * (point2.x() - point1.x()) + (point2.y() - point1.y()) * (point2.y() - point1.y())));	
			if(accuracy > abs(Dist12 - Dist1 - Dist2))
				accuracy = abs(Dist12 - Dist1 - Dist2);
		}
		point2 = point1;
	}	
	return accuracy;
}