#ifndef VABSTRACTGRAPHICITEM_H
#define VABSTRACTGRAPHICITEM_H

#include <QGraphicsItem>

class DicomData;
class VAbstractGraphicItem : public QGraphicsItem //, public QObject
{
//	Q_OBJECT

public:
	VAbstractGraphicItem(DicomData * _dicomData, QGraphicsItem *parent);
	virtual bool proceccPress(QPointF pos) = 0;
	virtual bool proceccRelease(QPointF pos) = 0;
	virtual bool proceccMousePos(QPointF pos) = 0;

	virtual void proceccPosChanging( QPointF pointCur, QPointF point) = 0;

	virtual void init(QPointF pos) = 0;
	virtual void drawGraphicPrimitive(QPainter * painter, QPen pen);
	virtual void drawText(QPainter * painter) = 0;
	virtual QRectF realBoundingRect () = 0;
	virtual float distToPoint(QPointF point);
	~VAbstractGraphicItem();
	virtual void paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget /*= 0 */ );
	virtual QString text(){return _text;};
	virtual void setPos (const QPointF &pos);
	DicomData * dicomData(){return _dicomData;};

	QVariant itemChange(GraphicsItemChange change, const QVariant &value);
	void focusOutEvent(QFocusEvent *event);
	void mousePressEvent(QGraphicsSceneMouseEvent * event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent * event);	
	void setActive (bool isActive);

	//friend void VAbstractGraphicItem::setActive (bool isActive);

protected:
	bool _isActive;
	QColor normalColor;
	QPen normalPen;
	QPen activePen;
	//QPen selectPen;
	QPen curentPen;
	QBrush normalBrush;
	QBrush activeBrush;
	QBrush selectBrush;
	QBrush curentBrush;
	QPen negativePen;
	DicomData * _dicomData;

	QPen backgroundBoundPen;
	QPen foregroundBoundPen;
	QString _text;
	int state;
	
};

#endif // VABSTRACTGRAPHICITEM_H