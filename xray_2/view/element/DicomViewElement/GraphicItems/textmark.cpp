#include "stdafx.h"
#include "textmark.h"

TextMark::TextMark(QGraphicsItem * parent)
	: QGraphicsLineItem(parent)
{
	setFlag(QGraphicsItem::ItemIsSelectable, true);
	QColor myColor;
	myColor = Qt::white;
	setPen(QPen(myColor, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	setVisible(true);
}



TextMark::~TextMark()
{

}
