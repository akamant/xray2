#ifndef VPOLYGONGRAPHICITEM_H
#define VPOLYGONGRAPHICITEM_H

#include <circlegraphicitem.h>

class VPolygonGraphicItem : public VCircleGraphicItem
{
	//Q_OBJECT

public:
	VPolygonGraphicItem(DicomData * dicomData, VNormalizedImage * normImage, QPolygonF _polygon = QPolygonF(), QGraphicsItem *parent = NULL);
	~VPolygonGraphicItem();

	QPolygonF _polygon;
	QPolygonF polygon (){return _polygon;};

	virtual bool proceccPress(QPointF pos);
	virtual bool proceccMousePos(QPointF pos);
	virtual QRectF realBoundingRect( );
	void proceccPosChanging( QPointF pointCur, QPointF point );
	float distToPoint( QPointF point );

protected:	
	virtual void drawGraphicPrimitive(QPainter *painter, QPen pen);
	virtual bool proceccRelease(QPointF pos);
	virtual void calculateAverageDensity();

private:
	
};

#endif // VPOLYGONGRAPHICITEM_H
