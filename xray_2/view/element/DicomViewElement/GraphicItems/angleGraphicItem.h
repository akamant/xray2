#ifndef VANGLEGRAPHICITEM_H
#define VANGLEGRAPHICITEM_H

#include <abstractGraphicItem.h>

class VAngleGraphicItem : public VAbstractGraphicItem
{
	//Q_OBJECT

public:		
	VAngleGraphicItem(DicomData * dicomData, QPointF __secondPoint = QPointF (0, 0), QPointF __thirdPoint  = QPointF (0, 0), QGraphicsItem *parent = NULL);
	~VAngleGraphicItem();

	virtual void proceccPosChanging( QPointF pointCur, QPointF point);

	virtual bool proceccPress(QPointF pos);
	virtual bool proceccMousePos(QPointF pos);
	virtual void init(QPointF pos);
	virtual void drawText(QPainter * painter);
	virtual QRectF realBoundingRect ();
	virtual QRectF boundingRect (void) const;
	virtual float distToPoint(QPointF point);

	QPointF secondPoint(){return _secondPoint;};
	QPointF thirdPoint(){return _thirdPoint;};
	bool proceccRelease(QPointF pos);
protected:		
	virtual void drawGraphicPrimitive(QPainter *painter, QPen pen);

private:

	QPointF _secondPoint;
	QPointF _thirdPoint;
	
};

#endif // VANGLEGRAPHICITEM_H
