#include "stdafx.h"
#include "circlegraphicitem.h"
#include "normalizedImage.h"
#include "view.h"

VCircleGraphicItem::VCircleGraphicItem( DicomData * dicomData, VNormalizedImage * __normImage, QPointF __secondPoint /*= QPointF (0, 0)*/, QGraphicsItem *parent /*= NULL*/ )
	: VDistanceGraphicItem(dicomData, __secondPoint, parent)
	, _normImage (__normImage)
	, averageDensity(0)
{
	calculateAverageDensity();
}



VCircleGraphicItem::~VCircleGraphicItem()
{

}


void VCircleGraphicItem::drawGraphicPrimitive(QPainter *painter, QPen pen)
{
	VAbstractGraphicItem::drawGraphicPrimitive(painter, pen);
	QVector2D vector(_secondPoint.rx(), _secondPoint.ry());
	int radius = vector.length();
	painter->drawEllipse(QPoint(0, 0),radius , radius);	
}

void VCircleGraphicItem::drawText( QPainter * painter )
{
		
}


void VCircleGraphicItem::calculateAverageDensity()
{
	float dx, dy;	
	averageDensity = 0;	
	QVector2D vector(_secondPoint.rx(), _secondPoint.ry());
	int radius = vector.length();

	QGraphicsItem * pixmapItem = NULL;
	QGraphicsScene * scene = this->scene();
	if(!scene)
		return;
	for(int i = 0; i < scene->items().size(); i ++ )
	{
		if(dynamic_cast<QGraphicsPixmapItem *>(scene->items()[i]))
		{
			pixmapItem = scene->items()[i];
			break;
		}
	}
	if(!pixmapItem)
		return;

	QPointF point1 = mapToScene(QPointF(0, 0) ).toPoint() - pixmapItem->pos();
	int xmin   =  point1.x() - radius;
	int ymin   =  point1.y() - radius;
	int xmax   =  point1.x() + radius;
	int ymax   =  point1.y() + radius;

	int i = 0;
	long long sumDensity = 0;	
	for (dy = ymin; dy < ymax; dy++)
	{
		for (dx = xmin; dx < xmax; dx++)
		{
			int curRadius = sqrt(float((point1.x() - dx) * (point1.x() - dx) + (point1.y() - dy) * (point1.y() - dy)));
			if (curRadius <= radius)
			{
				if( dy * _normImage->getWidth() + dx < _normImage->getWidth() * _normImage->getHeight() && dy * _normImage->getWidth() + dx >= 0)
				{											
					int value = _normImage->getValue((int)dy * _normImage->getWidth() + (int)dx);					
					sumDensity += value;
					i++;
				}			
	
			}
		}
	}		
	if(i != 0){
		averageDensity = sumDensity / i;	
	}else{
		int value = _normImage->getValue((int)(int)point1.y() * _normImage->getWidth() + (int)point1.x());		
		averageDensity = value;
	}

	if(averageDensity)	
		_text = QString ("D = %1").arg(averageDensity);

}

QRectF VCircleGraphicItem::realBoundingRect( )
{

	QVector2D vector(_secondPoint.rx(), _secondPoint.ry());
	int radius = vector.length();
	QGraphicsEllipseItem item( - radius, - radius, radius * 2 , radius * 2);
	return item.boundingRect();
	return QRectF( -100, -100, 1, 1 );
	return QRectF( 0, 0, _secondPoint.x(), _secondPoint.y() );
	// 		return QRectF	(	min(0,secondPoint.x()), min(0, secondPoint.y()), 
	// 							max(0,secondPoint.x()), max(0, secondPoint.y())
	// 						);	
}


bool VCircleGraphicItem::proceccPress( QPointF pos )
{	
	curentPen = activePen;
	//bool res =  VDistanceGraphicItem::proceccPress(pos);	
	switch(state)
	{
	case 1:
		{				
			break;
			return false;
		}
	case 2:
		{
			calculateAverageDensity();
			break;
		}	
	}
	return false;
	//return res;
}

bool VCircleGraphicItem::proceccRelease(QPointF pos)
{
	calculateAverageDensity();
	curentPen = normalPen;
	return true;
}

void VCircleGraphicItem::setPos( const QPointF &pos )
{
	VAbstractGraphicItem::setPos(pos);
	calculateAverageDensity();
}

void VCircleGraphicItem::proceccPosChanging( QPointF pointCur, QPointF point )
{
	QPointF point1 = pos();	
	float displacementX = point.x() - pointCur.x();		
	float displacementY = point.y() - pointCur.y();	
	point1.setX(point1.x() - displacementX);
	point1.setY(point1.y() - displacementY);
	setPos(point1);
	calculateAverageDensity();
}

float VCircleGraphicItem::distToPoint( QPointF point )
{	
	QPointF point1 = pos();
	QVector2D vector(_secondPoint.rx(), _secondPoint.ry());
	int radius = vector.length();
	float Dist1 = sqrt(float((point1.x() - point.x()) * (point1.x() - point.x()) + (point1.y() - point.y()) * (point1.y() - point.y())));
	return abs(Dist1 - radius);	
}
