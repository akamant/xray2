#include "StdAfx.h"
#include "dicomimageviewwidget.h"
#include "TreeDataViewModel.h"
#include "view.h"
#include "labelwithautosizetext.h"
#include "exprBuilder.h"
#include "exprDict.h"
#include "dicomDict.h"
#include "elemCont.h"
#include "elementParser.h"
#include "overlayDict.h"
#include "TreeDataViewModel.h"
#include "dicomData.h"
#include "dicombitmappresenter.h"
#include "branchFrame.h"
#include "command.h"
#include "ModelControls.h"
 #include <QTransform>
#include "addWidget.h"
#include "distancegraphicitem.h"
#include "circlegraphicitem.h"
#include "anglegraphicitem.h"
#include "polygongraphicitem.h"
#include "labelgraphicitem.h"
#include "GraphicsItemCreator.h"
#include "printtreedataviewmodel.h"

bool DicomImageViewWidget::showOverlay = true;
bool DicomImageViewWidget::showMarks = true;

DicomImageViewWidget::DicomImageViewWidget(TreeDataViewModel * _model, const QModelIndex & _index,  QWidget *parent )
	: model(_model)	
	, index(_index)
	, QFrame(parent)
	, pixmap (NULL)
	, firstShow(true)
	, pImageSize(0)
	, pImage(NULL)
	, fastImageBitmapSize(0)
	, pfastImage(NULL)
	, pol (QSizePolicy::Ignored)
	, isFirst(true)
	, currentGraphicsItem (NULL)
	, normImage(NULL)
	, view(NULL)
	, WLVector(0, 0)
	, isViewInNegative (false)
	, needCreatingWL(false)
	, dicomData(NULL)
	, overlayLayerWidget(NULL)
	, activePrimitive(NULL)
	, minDistDicomDataItem(NULL)
	, pixelAccuracy(2)
	, isForPrint(false)
{		

	if(dynamic_cast<PrintTreeDataViewModel *> (model))
		isForPrint = true;

	pixmap = new QPixmap();
		
	setLayout(&Gl);

	Gl.setSpacing(0);
	Gl.setMargin(0);
	
	LabelWithAutoSizeText * label = new LabelWithAutoSizeText("��������...", Qt::AlignCenter);
	QPalette palette;
	palette.setColor( QPalette::Background, QColor( 0, 0, 0 ) );
	palette.setColor( QPalette::Text, QColor( 255, 255, 255 ) );
	palette.setColor( QPalette::Foreground,  QColor( 255, 255, 255 ) );	
	setPalette( palette );
	label->setPalette( palette );

	setSizePolicy(QSizePolicy::Ignored , QSizePolicy::Ignored);

	scene = new QGraphicsScene;	
 	scene->addWidget(label);	
	view = new View(model, index, scene);
	view->setFastPixmap(NULL, 0, 0);
	view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	view->setVerticalScrollBarPolicy	(Qt::ScrollBarAlwaysOff);
	view->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	view->setFrameStyle(QFrame::Plain | QFrame::Box);
	view->setLineWidth(0);
	view->setMidLineWidth(0);	
	QGraphicsItem * GItem = view->items()[0];
	view->fitInView (GItem, Qt::KeepAspectRatio);
	view->setInteractive(false);
	view->setDragMode(QGraphicsView::NoDrag);
	
	processData(index);
}

void DicomImageViewWidget::createOverlay()
{
	if(!dicomData)
		return;

	createViewDependentElements();

	QStringList exprList;
	QString modality = dicomData->getELEM_CONT().get(0x00080060).toString();		
	TOvrMod mod;
	QStringList qlist = TOverlayDict::getDict()->getModalityList();
	if(qlist.contains(modality))
		mod = TOverlayDict::getDict()->getMod(modality);
	else
		mod = TOverlayDict::getDict()->getMod("Unknown");	

	TElemContList list( &dicomData->getELEM_CONT() );		
	TExprBuilder builder( TExpressionDict::getDict(), list );	
	bool printPanel = isForPrint;
	
	MaskedStackedWidget * stackedWidget = new MaskedStackedWidget ( this);
	stackedWidget->addWidget(view);

	Gl.setSpacing(0);
	Gl.setMargin(0);

	Gl.addWidget(stackedWidget, 0, 0, 1, 1);


	overlayLayerWidget = new QWidget();

	overlayLayerWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

	QGridLayout * overlayGridLayout = new QGridLayout;	
	overlayGridLayout->setMargin(0);
	overlayGridLayout->setSpacing(0);
	
	overlayLayerWidget->setLayout(overlayGridLayout);
	
	//interactiveFields << mod.expressions(TOvrMod::ELeftBottom, true, printPanel);
	//interactiveFields << mod.expressions(TOvrMod::ERightBottom, true, printPanel);
	int maxLines = max(	max(mod.expressions(TOvrMod::ELeftTop, true, printPanel).size(), mod.expressions(TOvrMod::ERightTop, true, printPanel).size()), 
						max(mod.expressions(TOvrMod::ELeftBottom, true, printPanel).size(), mod.expressions(TOvrMod::ERightBottom, true, printPanel).size())
						);
	int numberOfRows = 10;
	float heightOfRow = (width() + height()) / 2.0f / (float)numberOfRows *  20.0f / 10.0f;
	if(maxLines > 0)
		heightOfRow = (this->height() / 2) / maxLines * 0.7;

	QList<TOvrMod::EPos> exprPoses;
	exprPoses << TOvrMod::ELeftTop << TOvrMod::ERightTop << TOvrMod::ELeftBottom << TOvrMod::ERightBottom;

	for(int i = 0; i < exprPoses.size(); i++)
	{
		QWidget * spacerWidget = new QWidget ();
		spacerWidget->setMouseTracking(true);
		spacerWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

		QWidget * cellWidget = new QWidget();
		cellWidget->setMouseTracking(true);
		cellWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
		QBoxLayout * boxLayout;
		if(exprPoses[i] == TOvrMod::ELeftTop || exprPoses[i] == TOvrMod::ERightTop)
			boxLayout = new QBoxLayout(QBoxLayout::TopToBottom);
		else
			boxLayout = new QBoxLayout(QBoxLayout::BottomToTop);

		cellWidget->setLayout(boxLayout);
		boxLayout->setMargin(0);
		boxLayout->setSpacing(0);

		QStringList exprList = mod.expressions( exprPoses[i], true, printPanel);
		QStringList execList = builder.execExprList(exprList);
		Qt::Alignment alignment;

		if(exprPoses[i] == TOvrMod::ELeftTop || exprPoses[i] == TOvrMod::ELeftBottom)
			alignment = Qt::AlignLeft;
		else
			alignment = Qt::AlignRight;
		
		for(int j = 0; j < execList.size(); j ++)
		{
			execList[j].replace(QString("<not found>"), "");
			LabelWithAutoSizeText * addWidget = new LabelWithAutoSizeText(execList[j], alignment, this);	
			addWidget->setMaximumHeight(25);
			addWidget->setMouseTracking(true);
			addWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
			addWidgetHash.insert(exprList[j], addWidget);
			boxLayout->addWidget(addWidget, 0);
		}
		
		boxLayout->addWidget(spacerWidget, 0);

		int pos = i + 1;
		//overlayGridLayout->addWidget(cellWidget, pos / 2, pos % 2, 0, 0);
		if(i == 0)
			overlayGridLayout->addWidget(cellWidget, 0, 0, 1, 1);
		if(i == 1)
			overlayGridLayout->addWidget(cellWidget, 0, 1, 1, 1);
		if(i == 2)
			overlayGridLayout->addWidget(cellWidget, 1, 0, 1, 1);
		if(i == 3)
			overlayGridLayout->addWidget(cellWidget, 1, 1, 1, 1);		

	}
	
	stackedWidget->addWidget(overlayLayerWidget);
	overlayLayerWidget->setMouseTracking(true);
	overlayLayerWidget->installEventFilter(this);
	this->installEventFilter(view);
}

void DicomImageViewWidget::switchOverlay()
{
	showOverlay = !showOverlay;
}

void DicomImageViewWidget::updateOverlayShowing()
{
	//if(overlayLayerWidget)
	//	overlayLayerWidget->setVisible(showOverlay);
		//overlayLayerWidget->setFixedHeight(0);
	refreshOverlay();
}

void DicomImageViewWidget::switchMarkShowing()
{
	showMarks = !showMarks;
}

void DicomImageViewWidget::updateMarkShowing()
{
	view->clearAllGraphicsItem();	
	if(showMarks)
		addGraphicPrimitives();
}

void DicomImageViewWidget::createViewDependentElements()
{
	QModelIndex parent = model->getDataModel()->parent(model->getDataModelIndex(index));
	QModelIndex parentParent = model->getDataModel()->parent(parent);

	dicomData->getELEM_CONT().makeHardTag(0x00010005, model->getDataModel()->rowCount(parentParent));

	int nImg = model->getDataModel()->numberChildren(model->getDataModelIndex(index));
	int totalImg = model->rowCount(parent);
	dicomData->getELEM_CONT().makeHardTag(0x0001000C, nImg);
	dicomData->getELEM_CONT().makeHardTag(0x0001000D, totalImg);

	dicomData->getELEM_CONT().makeHardTag(0x00010004, QString("img : %1/%2").arg(nImg).arg(totalImg));
	dicomData->getELEM_CONT().makeHardTag(0x00010008, (int)WLVector.x());
	dicomData->getELEM_CONT().makeHardTag(0x00010009, (int)WLVector.y());
}

void DicomImageViewWidget::refreshOverlay()
{
	createViewDependentElements();
	TElemContList list( &dicomData->getELEM_CONT() );		
	TExprBuilder builder( TExpressionDict::getDict(), list );	

	QStringList keys = addWidgetHash.keys();
	for(int i = 0; i < keys.size(); i ++)
	{
		LabelWithAutoSizeText * addWidget = static_cast<LabelWithAutoSizeText *> (addWidgetHash.value(keys[i]));
		QString exprValue = builder.execExpr(keys[i]);
		exprValue.replace(QString("<not found>"), "");
		if(showOverlay)
			addWidget->setText(exprValue);
		else
			addWidget->setText("");
	}	
}



void DicomImageViewWidget::showEvent( QShowEvent * e )
{
	QFrame::showEvent(e);
	//processData(index);
	if(firstShow)
	{
		
		connect	(	model,		SIGNAL	( dataChanged				( const QModelIndex &, const QModelIndex &  ) ),
					this,		SLOT	( processChanging			( const QModelIndex &, const QModelIndex &  ) )
			);
		connect	(	model,		SIGNAL	( rowsInserted				( const QModelIndex &, int, int ) ),
					this,		SLOT	( updateTreeItemSlot		( const QModelIndex &, int, int ) )
			);

		connect	(	model,		SIGNAL	( changingParameter			( const QModelIndex &) ),
					this,		SLOT	( processChangingParameter	( const QModelIndex & ))
			);
		connect	(	model,		SIGNAL	( brothersCreated			(const QModelIndex & ) ),
					this,		SLOT	( processBrothersCreated	(const QModelIndex & ) )
			);

		connect	(	model,		SIGNAL	( overlaySwitched			() ),
					this,		SLOT	( updateOverlayShowing		() )
			);

		connect	(	model,		SIGNAL	( overlaySwitched			() ),
					this,		SLOT	( updateMarkShowing			() )
			);

		
		createOverlay();		
	}
	firstShow = false;
	
}


 void DicomImageViewWidget::processChanging( const QModelIndex & startIndex, const QModelIndex & endIndex)
 {	
	 processData(startIndex);
 }

  
void DicomImageViewWidget::presentImageWithWL(QVector2D WLVector, bool isViewInNegative)
{	
	/*dicomData = model->getDicomData(index);	*/
	if(!dicomData || !dicomData->getisNormalized() || !view)
		return;		
	normImage = dicomData->getNormalizedImage();

	if(WLVector == this->WLVector 
		&& isViewInNegative == this->isViewInNegative && !needCreatingWL)
		return;
	needCreatingWL = false;
	this->WLVector = WLVector;
	this->isViewInNegative = isViewInNegative;
	
		
	if(pImageSize < dicomData->imageParameters.width * dicomData->imageParameters.height * 4
		&& dicomData->imageParameters.width * dicomData->imageParameters.height > 0
		){
		pImageSize = dicomData->imageParameters.width * dicomData->imageParameters.height * 4;
		delete pImage;
		pImage = new unsigned char[pImageSize];
	}
	wlLUT.setisReflected(isViewInNegative);
	wlLUT.doWLLUT(255, WLVector.x(), WLVector.y());		

	QImage * img;
	int spacing = 0;
	if(dicomData->imageParameters.width % 4)
		spacing = 4 - dicomData->imageParameters.width % 4;
	if(dicomData->imageParameters.samplesPerPixel == 3 )
	{
		if(pImageSize < dicomData->imageParameters.width * dicomData->imageParameters.height * 4
			&& dicomData->imageParameters.width * dicomData->imageParameters.height > 0
			){
				pImageSize = (dicomData->imageParameters.width + spacing) * dicomData->imageParameters.height * 4;
				delete pImage;
				pImage = new unsigned char[pImageSize];
		}
		transformColors(dicomData, wlLUT, pImage, isViewInNegative);	
		img = new QImage( (uchar*)pImage, dicomData->imageParameters.width, dicomData->imageParameters.height, QImage::Format_RGB32 );			
	}else{
		if(pImageSize < dicomData->imageParameters.width * dicomData->imageParameters.height
			&& dicomData->imageParameters.width * dicomData->imageParameters.height > 0
			){				
				pImageSize = (dicomData->imageParameters.width + spacing) * dicomData->imageParameters.height;
				delete pImage;
				pImage = new unsigned char[pImageSize];
		}
		transformColors(dicomData, wlLUT, pImage, isViewInNegative);
		img = new QImage( (uchar*)pImage, dicomData->imageParameters.width, dicomData->imageParameters.height, QImage::Format_Indexed8 );
		QVector<QRgb> my_table;
		for(int i = 0; i < 256; i++) my_table.push_back(qRgb(i, i, i));
		img->setColorTable(my_table);
	}
	pixmap->convertFromImage(*img);
	delete img;
	QGraphicsPixmapItem * item = dynamic_cast<QGraphicsPixmapItem *> (view->pixmapItem());

	if(!item)
	{
		scene->clear();
		item = scene->addPixmap(*pixmap);
	}else
	{
		item->setPixmap(*pixmap);
	}

	item->setTransformationMode(Qt::SmoothTransformation);
	item->setPos(pixmap->width() * 49.5, pixmap->height() * 49.5);		
	scene->setSceneRect(QRect(0, 0, pixmap->width() * 100, pixmap->height() * 100));
}

 void DicomImageViewWidget::processData(const QModelIndex & index)
 {	
	//return;
	if(index == this->index)
	{	
		dicomData = model->getDicomData(index);	
		if(!dicomData || !dicomData->getNormalizedImage() || !dicomData->getNormImageBody() || !view)
			return;	

		wlLUT = dicomData->wlLUT;
				
		view->setNeedUpdateOverlay();
		bool needSetProperties = false;
		if(pImageSize == 0)
			needSetProperties = true;

		needCreatingWL = true;

		view->clearAllGraphicsItem();

		processChangingParameter(index);
				
		presentImageWithWL(WLVector, isViewInNegative);

		updateMarkShowing();			

		if(needSetProperties)
		{				
			view->setInteractive(true);
			QGraphicsItem * GItem = view->pixmapItem();	
			view->setRenderHints(QPainter::Antialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform | QPainter::TextAntialiasing );	
			view->fitInView (GItem, Qt::KeepAspectRatio);// 			
		}
		
		view->setNeedUpdateOverlay();
	 }
 }


 void DicomImageViewWidget::addGraphicPrimitives()
 {
	 GraphicsItemCreator creator;
	 for(int i = 0; i < dicomData->graphicsItems.size(); i ++)
	 {	
		 QPointF pos = dicomData->graphicsItems[i]->pos();
		 VAbstractGraphicItem * currentGraphicsItem = creator.createCopy( dicomData->graphicsItems[i] );			
		 scene->addItem(currentGraphicsItem);		
		 currentGraphicsItem->setPos(pos);		
	 }
 }


DicomImageViewWidget::~DicomImageViewWidget()
{
	scene->clear();
	delete pImage;
	delete pixmap;
	delete view;
	delete scene;
	delete pfastImage;
	//qDeleteAll(addWidgetHash);
	addWidgetHash.clear();	
}


bool DicomImageViewWidget::eventFilter(QObject* obj, QEvent * e) 
{	
	
	if (e->type() == QEvent::MouseButtonDblClick)
	{	
		//processEventByState((QMouseEvent *) e);
		if(isForPrint)
		{
			ModelControls * controls = new ModelControls( model, index, this);
			controls->eraseBranch();
		}

		QMouseEvent * mouseEvent =  (QMouseEvent *) e;
		if(mouseEvent->button() == Qt::LeftButton)		
			oneViewMode();		
		return true;
	}
	posInWidget = mapFromGlobal(((QMouseEvent *)e)->globalPos());

	QPoint relPoint = posInWidget;
	if(isMouseEvent(e))
		processEventByCursor(e);
	if (e->type() == QEvent::Wheel)
	{		
		wheelEvent((QWheelEvent *)e);
		return true;		
	}	
	
	if (e->type() == QEvent::MouseButtonPress)
	{		
		if(view)
			view->setInteractive(false);	
		if(isMouseEvent(e))
			processEventByState((QMouseEvent *) e);
		
		pressPoint = ((QMouseEvent *)e)->pos();		

		//if(Command::isPrimitiveState)
		//	return true;
		//else
			return QWidget::eventFilter(obj, e);

	}

	if (e->type() == QEvent::MouseButtonRelease)
	{ 		
		if(isMouseEvent(e))
			processEventByState((QMouseEvent *) e);				
	}		

	if (e->type() == QEvent::MouseMove)
	{
		processEventByState((QMouseEvent *) e);	
		if(Command::isPrimitiveState)
			view->setNeedUpdateOverlay();
		refreshOverlay();
	}

	if (e->type() == QEvent::Enter)
	{
		if(Command::isPrimitiveState)
			return true;
		else
			return QWidget::eventFilter(obj, e);	
	}

	if (e->type() == QEvent::KeyPress)
	{
		keyPressEvent((QKeyEvent *)e);
		return true;		
	}	
	
	return QWidget::eventFilter(obj, e);	
}


void DicomImageViewWidget::updateTreeItemSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd )
{
	if(model->index(rowNumberStart, 0, index) == this->index){
		//view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
		//view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	}
	if (index == this->index){
		//view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
		//view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	}
}


void DicomImageViewWidget::processChangingParameter(const QModelIndex & index )
{
	if(index == this->index)
	{
		ViewParameters params = model->getViewParameters(index);
		presentImageWithWL(params.WLVector(), params.isViewNegative());	
 		view->setNeedUpdateOverlay();
 		view->setRelativeDisplacement(params.relativeDisplacement()); 		
		view->setTransform(params.transform());
		view->setScale(params.scale());	
		refreshOverlay();
	}
}


void DicomImageViewWidget::mouseMoveEvent( QMouseEvent * event )
{		
	return;
}


bool DicomImageViewWidget::processEventByState( QMouseEvent * event )
{
	if(!view)
		return false;

	if(!Command::isPrimitiveState)
	{
		cancelGraphicsItem();
	}

	posInWidget = mapFromGlobal(((QMouseEvent *)event)->globalPos());
	scenePos = view->mapToScene(view->mapFromGlobal(event->globalPos()));

	checkInactivePrimitive(event);
	QPointF posInWidgetF = mapFromGlobal(((QMouseEvent *)event)->globalPos());
	
	switch(event->type())
	{

		case QEvent::MouseButtonPress:
		{
			if(event->buttons() != Qt::LeftButton && Command::isPrimitiveState)
			{
				cancelGraphicsItem();
				break;
			}
			movingWLVector = WLVector;
			bool itemCreated = false;
			prevState = Command::commandState();

			switch(Command::commandState())
			{
			case Command::translateState:
				{					
					view->setInteractive(true);
					break;
				}
			case Command::contrastState:
				{
					LastPanPointF = posInWidgetF;
					break;
				}		
			case Command::DistancePrimitiveState:
				{					
					LastPanPointF = posInWidgetF;
					if(!currentGraphicsItem && !getActivePrimitive(event))
					{
						currentGraphicsItem = new VDistanceGraphicItem (dicomData);
						itemCreated = true;
					}					
					break;
				}
			case Command::CirclePrimitiveState:
				{					
					LastPanPointF = posInWidgetF;
					if(!currentGraphicsItem && !getActivePrimitive(event))
					{
						currentGraphicsItem = new VCircleGraphicItem (dicomData, normImage);
						itemCreated = true;
					}			
					break;
				}
			case Command::LabelPrimitiveState:
				{					
					LastPanPointF = posInWidgetF;
					if(!currentGraphicsItem && !getActivePrimitive(event))
					{
						currentGraphicsItem = new VLabelGraphicItem (dicomData);
						itemCreated = true;
					}
					break;
				}

			case Command::AnglePrimitiveState:
				{					
					LastPanPointF = posInWidgetF;
					if(!currentGraphicsItem && !getActivePrimitive(event))
					{
						currentGraphicsItem = new VAngleGraphicItem (dicomData);
						itemCreated = true;
					}					
					break;
				}			

			case Command::PoligonPrimitiveState:
				{					
					LastPanPointF = posInWidgetF;
					if(!currentGraphicsItem && !getActivePrimitive(event))
					{
						currentGraphicsItem = new VPolygonGraphicItem (dicomData, normImage);
						itemCreated = true;
					}					
					break;
				}
			case Command::DeletePrimitiveState:
				{
					if(activePrimitive)
					{
						VAbstractGraphicItem * prevSelectItem = activePrimitive;
						setActivePrimitiveToChange(event);
						if(activePrimitive == prevSelectItem)
						{
							scene->removeItem(activePrimitive);
							delete activePrimitive;
							activePrimitive = NULL;
						}
					}else
					{
						setActivePrimitiveToChange(event);
					}
					break;
				}
			}

			if( Command::isPrimitiveState)
			{
				if(currentGraphicsItem)
				{				
					if(itemCreated)
					{					
						scene->addItem(currentGraphicsItem);			
						currentGraphicsItem->setPos( view->mapToScene( posInWidget ) );	
						activePrimitive = currentGraphicsItem;
						currentGraphicsItem->setActive(true);
					}		

					if(currentGraphicsItem->proceccPress( view->mapToScene( posInWidget )))
					{
						GraphicsItemCreator creator;					
						VAbstractGraphicItem * gItem = creator.createCopy( currentGraphicsItem );								
						gItem->setPos(currentGraphicsItem->pos());
						dicomData->graphicsItems << gItem;
						setDisActivePrimitiveToChange();
						currentGraphicsItem = NULL;						
					}
					view->setNeedUpdateOverlay();
				}else
				{
					setActivePrimitiveToChange(event);
				}
			}
			break;

		}
		case QEvent::MouseButtonRelease:
		{
			if( Command::isPrimitiveState)
			{
				if(currentGraphicsItem)
				{

					if(currentGraphicsItem->proceccRelease( view->mapToScene( posInWidget )))
					{
						GraphicsItemCreator creator;					
						VAbstractGraphicItem * gItem = creator.createCopy( currentGraphicsItem );								
						gItem->setPos(currentGraphicsItem->pos());
						dicomData->graphicsItems << gItem;
						currentGraphicsItem->setActive(false);	
						currentGraphicsItem = NULL;
					}
					view->setNeedUpdateOverlay();
				}else
				{					
					
				}
								
			}

			if(pressPoint != posInWidget)
			{
				switch(Command::commandState())
				{
				case Command::translateState:
					{	
						if(view)
						{					
							Command command(Command::translate);
							command.vector2DPar = view->getRelativeDisplacement();
							command.floatPar = view->getScale();
							command.transform = view->transform();	
							model->emitCommand(command);
						}
						break;
					}
				case Command::contrastState:
					{
						if(view)
						{	
							Command command(Command::contrast);
							command.vector2DPar = movingWLVector;	
							view->setFastPixmap(NULL, 0, 0);
							model->emitCommand(command);					
						}
						break;
					}
				}			
			}	
			break;
		}
		case QEvent::MouseMove:
		{
			if(event->buttons() == Qt::LeftButton) 
			{	
				switch(Command::commandState())
				{

				case Command::translateState:
					{					
						break;
					}
				case Command::contrastState:
					{				
						QVector2D deltaVector	(	LastPanPointF.x() - posInWidgetF.x(),
													LastPanPointF.y() - posInWidgetF.y()
												);
						LastPanPoint = event->pos();
						int difference = (dicomData->maxValue - dicomData->minValue) / 6;
						deltaVector *= difference;
						deltaVector.setX(deltaVector.x() / width());
						deltaVector.setY(deltaVector.y() / height());

						movingWLVector = movingWLVector - deltaVector;	

						if(dicomData->imageParameters.width * dicomData->imageParameters.height > width() * height())
						{	
							//// ����� ���� ����� ��������� ��� ���������� � 
							QPolygonF windowPol = view->mapToScene( 0, 0, width(), height() );
							QRectF windowRect = windowPol.boundingRect();
							QRectF pixmapRect = view->pixmapItem()->sceneBoundingRect();
							QRectF intersectRect = windowPol.boundingRect().intersect(pixmapRect);

							QPolygonF rectOut = view->mapFromScene(intersectRect);

							int outWidth =  rectOut.boundingRect().width();
							int outHeight =  rectOut.boundingRect().height();

							if(  intersectRect.width() >  intersectRect.height() && outWidth < outHeight ||
								intersectRect.width() <  intersectRect.height() && outWidth > outHeight					
														
								)
							{
								int saveWidth = outWidth;
								outWidth = outHeight;
								outHeight = saveWidth;
							}
														
							int imageX =intersectRect.x() - view->pixmapItem()->pos().x();
							int imageY =intersectRect.y() - view->pixmapItem()->pos().y();

							fastNormImageRegion.grabRegion(dicomData->getNormalizedImage(), 
								imageX, imageY, 
								intersectRect.width(), intersectRect.height(), 
								outWidth , outHeight, true);
						
							wlLUT.doWLLUT(255, movingWLVector.x(), movingWLVector.y());									

							int outPutWidth = fastNormImageRegion.significantRect.width();
							int outPutHeight = fastNormImageRegion.significantRect.height();							

							DicomData * fastDicomData = new DicomData(*dicomData);
							fastDicomData->imageParameters.width = fastNormImageRegion.significantRect.width();
							fastDicomData->imageParameters.height = fastNormImageRegion.significantRect.height();
							fastDicomData->setNormalizedImage(&fastNormImageRegion);
							fastDicomData->setNormImageBody(fastNormImageRegion.getBitmap());

							QImage * img;
							if(dicomData->imageParameters.samplesPerPixel == 3)
							{
								if(fastImageBitmapSize < outPutWidth * outPutHeight * 4)
								{
										fastImageBitmapSize = outPutWidth * outPutHeight * 4;
										delete pfastImage;
										pfastImage = new unsigned char[fastImageBitmapSize];
								}
								transformColors(fastDicomData, wlLUT, pfastImage, isViewInNegative);	
								img = new QImage( (uchar*)pfastImage, outPutWidth, outPutHeight, QImage::Format_RGB32 );			
							}else{
								int spacing = 0;
								if(outPutWidth % 4)
									spacing = 4 - outPutWidth % 4;
								if(fastImageBitmapSize < outPutWidth * outPutHeight)
								{
										fastImageBitmapSize = (outPutWidth + spacing) * outPutHeight ;
										delete pfastImage;
										pfastImage = new unsigned char[fastImageBitmapSize];
								}

								transformColors(fastDicomData, wlLUT, pfastImage, isViewInNegative);	

								img = new QImage( (uchar*)pfastImage, outPutWidth, outPutHeight, QImage::Format_Indexed8 );
								QVector<QRgb> my_table;
								for(int i = 0; i < 256; i++) 
									my_table.push_back(qRgb(i, i, i));
									
								img->setColorTable(my_table);		
							}
							
							QPixmap * fastPixmap = new QPixmap();
							fastPixmap->convertFromImage(*img);	
							view->setFastPixmap(fastPixmap, rectOut.boundingRect().x() , rectOut.boundingRect().y());
							refreshOverlay();

						}else
						{		
							ViewParameters params = model->getViewParameters(index);
							presentImageWithWL(movingWLVector, params.isViewNegative());	
						}
						QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
												
					}
				}	
				if( Command::isPrimitiveState)
				{
					if(!currentGraphicsItem)						
						if(activePrimitive)	
						{
							activePrimitive->proceccPosChanging( scenePos, prevScenePos);							
							if(minDistDicomDataItem)
								minDistDicomDataItem->proceccPosChanging( scenePos, prevScenePos);
						}	
				}
			}
			if(prevState == Command::commandState())
			{			
				if(currentGraphicsItem)
					currentGraphicsItem->proceccMousePos( view->mapToScene( posInWidget ) );					
			}
			break;			
		}
	
	}	
	
	LastPanPointF = posInWidgetF;
	LastPanPoint = posInWidget;
	prevScenePos = scenePos;
	prevState = Command::commandState();
	return false;
}


void DicomImageViewWidget::cancelGraphicsItem()
{
	if(currentGraphicsItem)
	{		
		scene->removeItem(currentGraphicsItem);
		delete currentGraphicsItem;
		currentGraphicsItem = NULL;	
	}
}


void DicomImageViewWidget::processBrothersCreated( const QModelIndex & index)
{
	return;
}

void DicomImageViewWidget::paintEvent ( QPaintEvent * event ) {
	QFrame::paintEvent(event);
	return;
	
}

void DicomImageViewWidget::oneViewMode()
{
if(!Command::isPrimitiveState)	
	model->setIsViewOne(index);	
}

void DicomImageViewWidget::resizeEvent( QResizeEvent * e )
{
	if(view)
		view->resize(view->width(), view->height());
	QFrame::resizeEvent(e);
	
}

QSize DicomImageViewWidget::sizeHint() const
{
	return QSize (99999, 99999);
}

void DicomImageViewWidget::processEventByCursor( QEvent * event )
{

	QMouseEvent * mouseEvent = (QMouseEvent *) event;

	if(QApplication::mouseButtons() == Qt::NoButton && !Command::isPrimitiveState)
		unsetCursor();

	switch(event->type())
	{	
	case QEvent::MouseButtonPress:
		{
			switch(Command::commandState())
			{
			case Command::translateState:
				{				
					if(mouseEvent->button() == Qt::LeftButton)
						setCursor(QCursor(QPixmap(":/resources/cursors/move.png"), 5, 5));
					else
						setCursor(QCursor(QPixmap(":/resources/cursors/scale.png"), 5, 5));
					break;
				}
			case Command::contrastState:
				{
					setCursor(QCursor(QPixmap(":/resources/cursors/contrast.png"), 5, 5));					
					break;
				}		
			}
			break;
		}
	case QEvent::MouseButtonRelease:
		{
			if(!Command::isPrimitiveState)
				 unsetCursor();
			break;
		}
	case QEvent::Leave:
		{		
			unsetCursor();
			break;
		}
	}
	if(Command::isPrimitiveState)
	{		
		if(!checkCursorForActivePrimitive(mouseEvent))
		{		
			switch(Command::commandState())
			{				
			case Command::DistancePrimitiveState:
				{		
					//Command::setCommandState(Command::DistancePrimitiveState);
					setCursor(QCursor(QPixmap(":/resources/cursors/dist.png"), 5, 5));
					break;
				}
			case Command::CirclePrimitiveState:
				{		
					setCursor(QCursor(QPixmap(":/resources/cursors/oval.png"), 5, 5));
					break;
				}
			case Command::LabelPrimitiveState:
				{					
					setCursor(QCursor(QPixmap(":/resources/cursors/mark.png"), 5, 5));
					break;
				}

			case Command::AnglePrimitiveState:
				{					
					setCursor(QCursor(QPixmap(":/resources/cursors/angle.png"), 5, 5));
					break;
				}			

			case Command::PoligonPrimitiveState:
				{		
					setCursor(QCursor(QPixmap(":/resources/cursors/curve.png"), 5, 5));
					break;
				}
			case Command::DeletePrimitiveState:

				{
					setCursor(QCursor(QPixmap(":/resources/cursors/del.png"), 5, 5));
					break;
				}
			}		
		}		
	}	
}


bool DicomImageViewWidget::checkCursorForActivePrimitive(QMouseEvent * event)
{ 
	
	if(getActivePrimitive(event))
	{
		setCursor(QCursor(QPixmap(":/resources/cursors/movemark.png"), 5, 5));
		return true;
	}
	return false;
}


VAbstractGraphicItem *  DicomImageViewWidget::minDistToPointPrimitive(QPointF point)
{ 	
	VAbstractGraphicItem * minDistItem = NULL;
	float minDist = 9999;

	for(int i = 0; i < view->items().size(); i ++)
	{
		VAbstractGraphicItem * vItem;
		if(vItem = dynamic_cast<VAbstractGraphicItem *> (view->items()[i]))
		{
			if(vItem->distToPoint(point) < minDist)
			{
				minDist = vItem->distToPoint(point);
				minDistItem = vItem;
			}
		}
	}
	return minDistItem;	
}


VAbstractGraphicItem *  DicomImageViewWidget::minDistToPointDicomDataPrimitive(QPointF point)
{ 	
	VAbstractGraphicItem * minDistItem = NULL;
	float minDist = 9999;
	for(int i = 0; i < dicomData->graphicsItems.size(); i ++)
	{
		VAbstractGraphicItem * gItem = dicomData->graphicsItems[i];
		if(gItem->distToPoint(point) < minDist)
		{
			minDist = gItem->distToPoint(point);
			minDistItem = gItem;
		}
	}
	return minDistItem;
}

VAbstractGraphicItem * DicomImageViewWidget::getActivePrimitive(QMouseEvent * event)
{ 
	qreal accuracy = view->mapToScene(QRect(0, 0, pixelAccuracy, pixelAccuracy)).boundingRect().width();

	QPointF scenePoint = view->mapToScene(view->mapFromGlobal(event->globalPos()));

	VAbstractGraphicItem * primitive = minDistToPointPrimitive(scenePoint);

	if(primitive && primitive->distToPoint(scenePoint) <= accuracy)
	{
		return primitive;
	}
	return NULL;
}

VAbstractGraphicItem * DicomImageViewWidget::setActivePrimitiveToChange(QMouseEvent * event)
{ 
	activePrimitive = NULL;
	minDistDicomDataItem = NULL;
	qreal accuracy = view->mapToScene(QRect(0, 0, pixelAccuracy, pixelAccuracy)).boundingRect().width();

	QPointF scenePoint = view->mapToScene(view->mapFromGlobal(event->globalPos()));

	VAbstractGraphicItem * primitive = minDistToPointPrimitive(scenePoint);

	if(primitive && primitive->distToPoint(scenePoint) <= accuracy)
	{
		activePrimitive = primitive;
		activePrimitive->setActive(true);
		minDistDicomDataItem = minDistToPointDicomDataPrimitive(scenePos);
	}

	return NULL;
}

void DicomImageViewWidget::checkInactivePrimitive(QMouseEvent * event)
{
	if(event->buttons() == Qt::LeftButton && Command::isPrimitiveState
		|| Command::commandState() == Command::DeletePrimitiveState)
	return;
	setDisActivePrimitiveToChange();
}


void DicomImageViewWidget::setDisActivePrimitiveToChange()
{	
	if(activePrimitive)
		activePrimitive->setActive(false);
	activePrimitive = NULL;
	minDistDicomDataItem = NULL;
}



bool  DicomImageViewWidget::isMouseEvent(QEvent * e)
{

	if (	e->type() == QEvent::MouseButtonPress 
		||	e->type() == QEvent::MouseButtonRelease
		||	e->type() == QEvent::MouseMove
		)
		return true;
	return false;

}