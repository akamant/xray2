#ifndef DICOMIMAGEVIEWWIDGET_H
#define DICOMIMAGEVIEWWIDGET_H

#include <QObject>
#include "TLUT.h"
#include "MaskedWidget.h"
#include "command.h"
#include "NormalizedImage.h"
#include <QGraphicsItem>

class View;
//class Command;
class TreeDataViewModel;
//class MaskedWidget;
class AddWidget;
class QFrame;
class VAbstractGraphicItem;
class DicomData;

class DicomImageViewWidget : public QFrame  //QFrame //MaskedWidget 
{
	Q_OBJECT

public:
	DicomImageViewWidget(TreeDataViewModel * _model, const QModelIndex & _index, QWidget *parent = NULL);
	~DicomImageViewWidget();


protected:

	static bool showOverlay;
	static bool showMarks;

	bool isForPrint;

	QWidget * overlayLayerWidget;
	QPoint posInWidget;
	VAbstractGraphicItem * currentGraphicsItem;
	AddWidget * bt;
	bool isFirst;	
	QSizePolicy::Policy pol;

	//DicomImageViewWidget * other;
	View * view;
	virtual bool eventFilter(QObject* obj, QEvent * e);
	
	bool processEventByState(QMouseEvent * event);
	void processEventByCursor(QEvent * event);
	virtual void showEvent( QShowEvent * e );
	virtual void resizeEvent( QResizeEvent * e );

	virtual void paintEvent( QPaintEvent * e );
	virtual void mouseMoveEvent( QMouseEvent * e );	

	QHash<QString, QWidget *> addWidgetHash;
	
	VNormalizedImage fastNormImageRegion;

//public slots:
//		void processDataChanging( const QModelIndex & index);

public slots:	
	void updateTreeItemSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd );		
	void processChanging(const QModelIndex & topLeft, const QModelIndex & bottomRight );
	void processBrothersCreated(const QModelIndex &);
	//void processCommand(const Command  & command);
	//void presentImageWithWL(QVector2D WLVector);
	void presentImageWithWL(QVector2D WLVector, bool isViewInNegative);
	//void processCommand(const QModelIndex & index, const Command & command );
	QVector2D getWLVector(){return WLVector;};
	void processChangingParameter(const QModelIndex & index );
	void updateOverlayShowing();
	void updateMarkShowing();

private:

	VNormalizedImage * normImage;
	Command::commandStates prevState; 
	QPoint LastPanPoint;
	QPointF LastPanPointF;
	QPointF prevScenePos;
	QPointF scenePos;
	QVector2D WLVector;
	QVector2D movingWLVector;
	QPoint pressPoint;
	DicomData * dicomData;
	QGraphicsScene * scene;
	void processData(const QModelIndex & index);	
	QGridLayout Gl;
	QPixmap * pixmap;
	TreeDataViewModel * model;
	QModelIndex index;	
	bool firstShow;

	int numberInTopLeft;
	int numberInTopRight;
	int numberInBottomLeft;
	int numberInBottomRight;
	bool isViewInNegative;	
	
	unsigned char * pImage;
	unsigned char * pfastImage;
	int pImageSize;
	int fastImageBitmapSize;
	TWLLUT wlLUT;
	void oneViewMode();
	bool needCreatingWL;
	void cancelGraphicsItem();
	VAbstractGraphicItem * activePrimitive;
	VAbstractGraphicItem * minDistDicomDataItem;
	int pixelAccuracy;
	

public:
	virtual QSize sizeHint() const;
	static void switchOverlay();
	static void switchMarkShowing();
	void addGraphicPrimitives();
	void createOverlay();
	void createViewDependentElements();
	void refreshOverlay();	
	bool checkCursorForActivePrimitive(QMouseEvent * event);
	VAbstractGraphicItem * minDistToPointPrimitive(QPointF point);
	bool isMouseEvent(QEvent * e);
	VAbstractGraphicItem * getActivePrimitive(QMouseEvent * event);
	VAbstractGraphicItem * minDistToPointDicomDataPrimitive(QPointF point);
	VAbstractGraphicItem * setActivePrimitiveToChange(QMouseEvent * event);
	void setDisActivePrimitiveToChange();
	void checkInactivePrimitive(QMouseEvent * event);
	static bool overlay;	
};

#endif // DICOMIMAGEVIEWWIDGET_H
