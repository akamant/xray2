#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsView>
class TreeDataViewModel;
class View : public QGraphicsView
{
	Q_OBJECT
signals:
	void drawElement (QString, QString, QRect );
public:	
	void View::setTransform(const QTransform & transform, bool combine = false);
	QVector2D getRelativeDisplacement();
	View( TreeDataViewModel * _model, const QModelIndex & _index, QGraphicsScene * scene, QWidget * parent = 0);
	~View();
	void setRelativeDisplacement(QVector2D displacement);
	float getScale();
	void setScale(float scaleNew);
	void setInteractiveFilds(QStringList interactiveFields);	
	QRectF saveBoundingRect;
	bool needUpdateFastPixmap;
	bool needUpdateOverlay;
	void setNeedUpdateOverlay(){needUpdateOverlay = true;};
	QGraphicsItem * pixmapItem();
	void clearAllGraphicsItem();
	void setFastPixmap (QPixmap * pixmap, int _fastPixmapX, int _fastPixmapY);
protected slots:
	void processChangeSelectedItem (const QModelIndex & index);
protected:
	bool canMoving;
	QTransform t;

	int fastPixmapX;
	int fastPixmapY;
	//foregroundBoundPen.setStyle(Qt::CustomDashLine);
	QPen foregroundPen;
	QPen backgroundPen;

	QPen refLineForegroundPen;
	QPen refLineBackgroundPen;

	bool graphicsPrimitiveMode;
	bool firstShow;

	//QPixmap * pixmap;
	QPixmap * maskPixmap;
	QGridLayout leftTopLayout;
	QGridLayout rightTopLayout;
	QGridLayout leftBottomLayout;
	QGridLayout rightBottomLayout;
	QGridLayout gridLayout;
	virtual void  mousePressEvent ( QMouseEvent * event );
	virtual void  showEvent ( QShowEvent * event );
	virtual void  resizeEvent ( QResizeEvent * event );
	virtual void  mouseDoubleClickEvent ( QMouseEvent * event ) ;
	virtual void  mouseMoveEvent ( QMouseEvent * event ) ;
	virtual void  mouseReleaseEvent ( QMouseEvent * event ) ;
	virtual void wheelEvent(QWheelEvent* event);
	virtual void paintEvent ( QPaintEvent * event );
	virtual void drawForeground ( QPainter * painter, const QRectF & rect );	
	bool eventFilter(QObject* obj, QEvent * e);

	
	bool needUpdateFontSizes;
	//Holds the current centerpoint for the view, used for panning and zooming
	QPointF CurrentCenterPoint;

	//From panning the view
	QPoint LastPanPoint;

	QStringList interactiveFields;

	//Set the current centerpoint in the
	void SetCenter(const QPointF& centerPoint);
	QPointF GetCenter() { return CurrentCenterPoint; };
	QPointF centerInViewScenePoint;
	QModelIndex _index;
	TreeDataViewModel * model;
	virtual QSize sizeHint() const;
private:
	QPixmap * fastPixmap;
	void setViewScale();
	QVector2D saveRelativeDisplacement;
	bool dblClickEvenOccur;
	bool isMousePressed;

	float curScale;
	double viewScale;
	float positionYprev;
	QPoint mousePressPoint;

	int numberInTopLeft;
	int numberInTopRight;
	int numberInBottomLeft;
	int numberInBottomRight;	
	void keyPressEvent(QKeyEvent * event);
	void drawScaleRule(QPainter * painter, QPen pen);
	void drawRefLines(QPainter *painter, QPen pen);
};

#endif // VIEW_H
