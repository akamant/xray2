#include "StdAfx.h"

#include "view.h"

#include "exprBuilder.h"
#include "exprDict.h"
#include "dicomDict.h"
#include "elemCont.h"
#include "elementParser.h"
#include "overlayDict.h"

#include "TreeDataViewModel.h"
#include "treedatamodel.h"

#include "dicomData.h"
#include "addwidget.h"
#include "ModelControls.h"
#include "LabelWithAutoSizeText.h"
#include "abstractgraphicitem.h"

#include "qslog.h"

#include "modelsinformation.h"


View::View( TreeDataViewModel * _model, const QModelIndex & __index, QGraphicsScene * scene, QWidget *parent )
	: QGraphicsView(scene, parent)
	,model(_model)
	,_index(__index)
	,curScale(1.0f)
	,LastPanPoint(0, 0)
	, needUpdateOverlay(true)
	//,pixmap(NULL)
	,maskPixmap(NULL)
	//,scaleNew(0)
	,viewScale(1)
	,firstShow(false)
	,graphicsPrimitiveMode(false)
	,dblClickEvenOccur(false)
	,isMousePressed(false)
	,fastPixmap(NULL)

{
	fastPixmap = new QPixmap (1, 1);
	backgroundPen.setCosmetic(true);
	backgroundPen.setWidth(2);
	backgroundPen.setColor(Qt::black);


	foregroundPen.setCosmetic(true);
	foregroundPen.setWidth(1);
	foregroundPen.setColor(Qt::white);


	refLineBackgroundPen.setCosmetic(true);
	refLineBackgroundPen.setWidth(3);
	refLineBackgroundPen.setColor(QColor(20, 200, 20));


	refLineForegroundPen.setCosmetic(true);
	refLineForegroundPen.setWidth(1);
	refLineForegroundPen.setColor(Qt::black);


	//setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	QPalette palette;
	//setTransformationAnchor(QGraphicsView::NoAnchor);

	//setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

	setResizeAnchor(QGraphicsView::AnchorViewCenter);
	setBackgroundBrush(QBrush(QColor(0, 0, 0 )));

   // setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
  //  setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
//	setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);

	QGraphicsItem * GItem =pixmapItem();	
	fitInView(this->scene()->itemsBoundingRect(), Qt::KeepAspectRatio);
	//setViewScale();
	SetCenter(sceneRect().center());	
	setRelativeDisplacement(QVector2D(1,1));

	connect (	model,		SIGNAL (changeSelectedItemSignal (const QModelIndex &)),
				this,		SLOT (processChangeSelectedItem (const QModelIndex &)));

	connect (	model,		SIGNAL (changedDataModelIndex (const QModelIndex &)),
		this,		SLOT (processChangeSelectedItem (const QModelIndex &)));
	
}

void View::setInteractiveFilds(QStringList interactiveFields)
{
	this->interactiveFields = interactiveFields;
}

View::~View()
{
	//delete pixmap;
	delete maskPixmap;
}

void  View::mousePressEvent ( QMouseEvent * event )
{		
	if(graphicsPrimitiveMode)
	{
		QGraphicsView::mousePressEvent(event);
		return;
	}
	//setCursor(Qt::ClosedHandCursor);
	if( LastPanPoint.isNull() )
		SetCenter(sceneRect().center());

	isMousePressed = true;
	LastPanPoint = event->pos();
	if(event->button() == Qt::RightButton ){
		positionYprev = event->y();
		mousePressPoint = event->pos();
	}
	
}


void  View::showEvent ( QShowEvent * event ) 
{
// 	QGraphicsView::showEvent(event);
// 	return;
// 	if(!firstShow)
// 	{
// 		QGraphicsItem * GItem =pixmapItem();	
// 		fitInView(scene()->itemsBoundingRect(), Qt::KeepAspectRatio);
// 		//setViewScale();
// 		SetCenter(sceneRect().center());	
// 		setRelativeDisplacement(QVector2D(1,1));
// 		firstShow = true;
// 	}
	
	
//��� ����� �����
// 	QGraphicsView::showEvent(event);	
// 	QPointF p0 =  mapToScene(100, 100);
// 	QPointF p1 =  mapToScene(1000, 1000);
// 
// 	QGraphicsEllipseItem * line1 = new QGraphicsEllipseItem(p0.x(), p0.y(), 200, 500);
// 
// 	QPen activePen;
// 	activePen.setWidth(2.0);
// 	line1->setPen(activePen);
// 	this->scene()->addItem(line1);
// 	p0 =  mapToScene(0, 0);
// 	p1 =  mapToScene(2000, 2000);
// 	QGraphicsLineItem * line2 = new QGraphicsLineItem(p0.x(), p0.y(), p1.x(), p1.y());
// 	this->scene()->addItem(line2);
// 	line1->setFlag(QGraphicsItem::ItemIsMovable, true);
// 	line1->setFlag(QGraphicsItem::ItemIsSelectable, true);
// 	activePen.setWidth(5.0);
// 	line2->setPen(activePen);
// 	line2->setFlag(QGraphicsItem::ItemIsMovable, true);
// 	line2->setFlag(QGraphicsItem::ItemIsSelectable, true);
}



void View::paintEvent ( QPaintEvent * event ) 
{

	if(fastPixmap)
	{			
		QPainter painterFPX (viewport());
		painterFPX.fillRect(painterFPX.window(), Qt::black);
		painterFPX.drawPixmap(fastPixmapX, fastPixmapY, fastPixmap->transformed(t));	
		painterFPX.end();
		if(needUpdateFastPixmap)
		{
			viewport()->update();
			needUpdateFastPixmap = false;
		}		
	}else
	{
		QGraphicsView::paintEvent(event);	
	}
	
	if(needUpdateOverlay)
	{
		viewport()->update();	
		//needUpdateOverlay = false;
	}

	QPainter painter (viewport());
	drawScaleRule(&painter, backgroundPen);
	drawScaleRule(&painter, foregroundPen);

	drawRefLines(&painter, refLineBackgroundPen);
	drawRefLines(&painter, refLineForegroundPen);	

	int numberOfRows = 30;
	float heightOfRow = ( min(width(), height())) / 2.0f / (float)numberOfRows *  20.0f / 10.0f;

	for(int i = 0; i < items().size(); i ++)
	{
		VAbstractGraphicItem * vItem;
		if(vItem = dynamic_cast<VAbstractGraphicItem *> (items()[i])){
			// 					QPointF connerBoundItemPoint(vItem->realBoundingRect().x(),
			// 						vItem->realBoundingRect().h());

			QPointF connerBoundItemPoint(vItem->realBoundingRect().bottomLeft());
			QPointF connerBoundScenePoint = vItem->mapToScene(connerBoundItemPoint);
			QPointF connerBoundViewPoint = mapFromScene(connerBoundScenePoint);

			QRect drawRect	(connerBoundViewPoint.x(),connerBoundViewPoint.y()  
				, 999999, 999999
				);

			painter.setClipping (true);
			painter.setBackgroundMode(Qt::TransparentMode);
			painter.setFont(QFont("", max(heightOfRow, 7)));
			painter.setPen(QPen(QColor(0, 0, 0)));

			painter.drawText(drawRect,vItem->text() , Qt::AlignLeft | Qt::AlignTop);

			painter.setPen(QPen(QColor(255, 255, 255)));
			drawRect.setTopLeft( QPoint(drawRect.x() + 1, drawRect.y() + 1));	
			painter.drawText(drawRect,vItem->text() , Qt::AlignLeft | Qt::AlignTop);
		}			
	}
	return;
}


void View::drawForeground ( QPainter * painter, const QRectF & rect ){		
	QGraphicsView::drawForeground(painter, rect );
	
}

void View::mouseDoubleClickEvent( QMouseEvent * event )
{
	//QGraphicsView::mouseDoubleClickEvent(event );
	//mousePressPoint = event->pos();
	dblClickEvenOccur = true;
	isMousePressed = false;

}

void View::setViewScale()
{
	QGraphicsItem * GItem =pixmapItem();
	if(width() > 2 && height() > 2)
	viewScale =  max(	(double)mapFromScene(GItem->boundingRect()).boundingRect().width() / ((double)width() - 2),
						(double)mapFromScene(GItem->boundingRect()).boundingRect().height() / ((double)height() - 2)
					);	
};

void  View::resizeEvent ( QResizeEvent * event ) 
{
	if( abs(event->size().height() - event->oldSize().height()) == 1  ){
		//VLogger() << "difference 1 in height";
		QGraphicsView::resizeEvent(event);	
 		return;		
 	}
	needUpdateOverlay = true; 
	needUpdateFontSizes = true;
	QGraphicsItem * GItem =pixmapItem();	
	float a = 0;

	if(!GItem->boundingRect().width() || !GItem->boundingRect().height()
		|| (event->size().width() + a) <= 0 || (event->size().height() + a) <= 0)
		return;
	double xAxeRatioOld = (double)event->oldSize().width() / (double)GItem->boundingRect().width();
	double yAxeRatioOld = (double)event->oldSize().height() / (double)GItem->boundingRect().height();
 	double xAxeRatioNew = ((double)mapFromScene(GItem->boundingRect()).boundingRect().width() + a) / ((double)event->size().width() + a) ;
 	double yAxeRatioNew = ((double)mapFromScene(GItem->boundingRect()).boundingRect().height() + a) / ((double)event->size().height() + a);	
	
	double viewScaleOldMax = max(xAxeRatioOld, yAxeRatioOld);
	double viewScaleNewMax = max(xAxeRatioNew, yAxeRatioNew);

	//viewScale = getScale();
	if(viewScaleNewMax <= 0)
		return;
	double kScale = viewScale / viewScaleNewMax;	
		
	//setViewScale();

	if(kScale > 0)
		scale(kScale, kScale);	 
	
	
	//setViewScale();
	if(width() <= 4)
		QLOG_ERROR() << QString("width - %1").arg(width());
	if(height() <= 4)
		QLOG_ERROR() << QString("height - %1").arg(height());
	centerInViewScenePoint = mapToScene(width() / 2.0f, height() / 2.0f);		
	SetCenter(centerInViewScenePoint);
	saveBoundingRect = GItem->boundingRect();

	setRelativeDisplacement(saveRelativeDisplacement);
	//scale(1, 1);
}

float View::getScale()
{
	//setViewScale();
	return viewScale;		
}

void View::setScale(float scaleNew)
{
	//setViewScale();
	if(viewScale > 0 && scaleNew > 0){
		float kScale = scaleNew / viewScale;
		if(kScale > 0)
			scale(kScale, kScale);	 		
	}
	setViewScale();
	return;
	
	
	if(!scene()->width() || !scene()->height() || !curScale )
		return;
	float k = max ((float)scene()->height() / (float)scene()->width(),
					(float)scene()->width() / (float)scene()->height()
					);
	float s = mapToScene(QRect(0, 0, 1.0f, 1.0f)).boundingRect().width() / k;
		float kScale = scaleNew / curScale * s;
		if(kScale > 0)
			scale(kScale, kScale);	
	
	setViewScale();
}

void View::mouseMoveEvent( QMouseEvent * event )
{		
	if(graphicsPrimitiveMode){
		QGraphicsView::mouseMoveEvent(event);
		return;
	}
	if(event->buttons() == Qt::RightButton && isInteractive() && height() && curScale)
	{
		float scaleNew = curScale + ((float)positionYprev - (float)event->y()) * 100.0f / height() * 0.02 * curScale;
		positionYprev = event->y();
		float kScale = scaleNew / curScale;

		if(kScale > 0)
			scale(kScale, kScale);	
		//curScale = scaleNew;
		setViewScale();
	}	

	if(event->buttons() == Qt::LeftButton   && isInteractive() && !LastPanPoint.isNull()
		&& !dblClickEvenOccur && isMousePressed) {
		//Get how much we panned		
		QPointF delta = mapToScene(LastPanPoint) - mapToScene(event->pos());
		LastPanPoint = event->pos();
		//Update the center ie. do the pan
		QVector2D vector (delta);
		//if(vector.length() < 100)
		SetCenter(GetCenter() + delta);			
	}

	dblClickEvenOccur = false;
	//QGraphicsView::mouseMoveEvent(event);
}

QVector2D View::getRelativeDisplacement()
{
	if(!scene()->width() || !scene()->height())
		return QVector2D(0, 0);

	centerInViewScenePoint = mapToScene(width() / 2.0f, height() / 2.0f); 	
	QVector2D QVrelativeDisp(	centerInViewScenePoint.rx() / (scene()->width() / 2.0f), 
								centerInViewScenePoint.ry() / (scene()->height() / 2.0f) );
	return QVrelativeDisp;	

}

void  View::setRelativeDisplacement(QVector2D displacement)
{
	centerInViewScenePoint = mapToScene(width() / 2.0f, height() / 2.0f); 
	SetCenter( QPointF (scene()->width() * displacement.x() / 2.0f  , scene()->height() * displacement.y() / 2.0f ));
	saveRelativeDisplacement = displacement;
	//setViewScale();
}

void View::wheelEvent(QWheelEvent* event) {
	QGraphicsView::wheelEvent(event);
	return;	
}

void View::SetCenter(const QPointF& centerPoint) {
    //Get the rectangle of the visible area in scene coords
    QRectF visibleArea = mapToScene(rect()).boundingRect();
 
    //Get the scene area
    QRectF sceneBounds = sceneRect();
 
    double boundX = visibleArea.width() / 2.0f;
    double boundY = visibleArea.height() / 2.0f;
    double boundWidth = sceneBounds.width() - 2.0f * boundX;
    double boundHeight = sceneBounds.height() - 2.0f * boundY;
 
    //The max boundary that the centerPoint can be to
    QRectF bounds(boundX, boundY, boundWidth, boundHeight);
 
    if(bounds.contains(centerPoint)) {
        //We are within the bounds
        CurrentCenterPoint = centerPoint;
    } else {
        //We need to clamp or use the center of the screen
        if(visibleArea.contains(sceneBounds)) {
            //Use the center of scene ie. we can see the whole scene
            CurrentCenterPoint = sceneBounds.center();
        } else {
 
            CurrentCenterPoint = centerPoint;
 
            //We need to clamp the center. The centerPoint is too large
            if(centerPoint.x() > bounds.x() + bounds.width()) {
                CurrentCenterPoint.setX(bounds.x() + bounds.width());
            } else if(centerPoint.x() < bounds.x()) {
                CurrentCenterPoint.setX(bounds.x());
            }
 
            if(centerPoint.y() > bounds.y() + bounds.height()) {
                CurrentCenterPoint.setY(bounds.y() + bounds.height());
            } else if(centerPoint.y() < bounds.y()) {
                CurrentCenterPoint.setY(bounds.y());
            }
        }
    }
 
    //Update the scrollbars
	centerOn(CurrentCenterPoint);
	centerInViewScenePoint = mapToScene(width() / 2.0f, height() / 2.0f);	
} 

void View::mouseReleaseEvent(QMouseEvent* event) 
{	
	if(graphicsPrimitiveMode)
	{
		QGraphicsView::mouseReleaseEvent(event);
		return;
	}
	isMousePressed = false;
    //setCursor(Qt::OpenHandCursor);
    //LastPanPoint = QPoint();

	centerInViewScenePoint = mapToScene(width() / 2.0f, height() / 2.0f);
	QGraphicsView::mouseReleaseEvent(event);
}

QGraphicsItem *  View::pixmapItem()
{	
	for(int i = 0; i < items().size(); i ++ )
		if(dynamic_cast<QGraphicsPixmapItem *>(items()[i]))
			return items()[i];
	
	if(items().size())
		return items()[0];
	return NULL;
}

void View::keyPressEvent(QKeyEvent * event)
{
	QGraphicsView::keyPressEvent(event);
}

void View::clearAllGraphicsItem()
{
	QQueue <VAbstractGraphicItem *> itemsForRemove;
	for(int i = 0; i < scene()->items().size(); i ++ ){
		if(dynamic_cast<VAbstractGraphicItem *>(items()[i]))
			itemsForRemove << (VAbstractGraphicItem *)scene()->items()[i];
			//return items()[i];
	}
	
// 	for(int i = 0; i < itemsForRemove.size(); i ++ ){
// 		if(dynamic_cast<VAbstractGraphicItem *>(items()[i]))
			//itemsForRemove << items()[i];
	while(itemsForRemove.size())
	{
		VAbstractGraphicItem * item = itemsForRemove.dequeue() ;
			scene()->removeItem(item);
	}
//	}
	
}

void View::drawScaleRule(QPainter *painter, QPen pen)
{
	painter->setPen(pen);

	DicomData * dicomData = model->getDicomData(_index);
	if(!dicomData || dicomData->ScaleY == 0 || dicomData->ScaleX == 0)
		return;

	QString modality = dicomData->getELEM_CONT().get(0x00080060).toString();			
	float k = 0.9f;
	if(modality == QString("MG") || modality.contains("MG", Qt::CaseInsensitive))
		k = 0.97f;

	QRect rect = this->rect();
	float X1Line = (float)rect.right() * k;
	float Y1Line = (float)rect.bottom() * 0.2;
	float X2Line = (float)rect.right() * k;
	QPolygonF mapPol = mapFromScene(QRectF(0, 0, 1, 10.0f / dicomData->ScaleY));
	float tenMM =  mapPol.boundingRect().height();
	float Y2Line = (float)rect.bottom() * 0.2 + ((float)rect.bottom() * 0.6 / tenMM) * tenMM;
	painter->drawLine(QPoint(X1Line, Y1Line), QPoint(X2Line, Y2Line));
	//pen.setWidth()
	//painter->setPen(pen);
	for(float i = 0;; i++){
		if((int)i % 10 == 0){
			X1Line = (float)rect.right() * k - 12;
			Y1Line = (float)rect.bottom() * 0.2 + tenMM * i;// + i;
			X2Line = (float)rect.right() * k ;		
			Y2Line = (float)rect.bottom() * 0.2 + tenMM * i;// + i;
		}
		else if ((int)i % 5 == 0)
		{
			X1Line = (float)rect.right() * k - 5;
			Y1Line = (float)rect.bottom() * 0.2 + tenMM * i;// + i;
			X2Line = (float)rect.right() * k ;		
			Y2Line = (float)rect.bottom() * 0.2 + tenMM * i;// + i;
		}
		else{
			X1Line = (float)rect.right() * k - 5;
			Y1Line = (float)rect.bottom() * 0.2 + tenMM * i;// + i;
			X2Line = (float)rect.right() * k;		
			Y2Line = (float)rect.bottom() * 0.2 + tenMM * i;// + i;
		}
		if(Y2Line > rect.bottom() * 0.2 + (float)((float)rect.bottom() * 0.6 / tenMM) * tenMM || Y2Line <= 0 || i > 10000)
			break;
		if((int)i % 10 == 0)
			//whiteAndBlackLine(hdc, X1Line, Y1Line, X2Line, Y2Line, 1, 1, RGB(255, 255, 255), rectMainWindow);	
			painter->drawLine(QPoint(X1Line, Y1Line), QPoint(X2Line, Y2Line));
		else
			//whiteAndBlackLine(hdc, X1Line, Y1Line, X2Line, Y2Line, 1, 1, RGB(255, 255, 255), rectMainWindow);	
			painter->drawLine(QPoint(X1Line, Y1Line), QPoint(X2Line, Y2Line));
	}
};

void View::drawRefLines(QPainter *painter, QPen pen)
{
	//return;
//	if(!getDisplayMode().isShowRefLine)
//		return;
	painter->setPen(pen);
	QGraphicsItem * GItem =pixmapItem();	
	DicomData * dicomDataCur = model->getDicomData(_index);

	//QString studyId = dicomStruct->ELEM_CONT.get(0x0020000D).toString();
	//QString seriesId = dicomStruct->ELEM_CONT.get(0x0020000E).toString();		
	//QString seriesIdActivePanel = ((ImagePanel *) clickedImagePanel)->dicomStruct->ELEM_CONT.get(0x0020000E).toString();	
	ModelsInformation mI;

	DicomData * dicomData = model->getDicomData(model->selectedItem());

	if(!dicomDataCur || !dicomData)
		return;
	
	QList<QVector2D> vector2DList = planesInterseption2D(dicomDataCur->planeInfo, dicomData->planeInfo);	

	if(vector2DList.size())
	{
		QPointF imagePoint1(vector2DList[0].x() + GItem->pos().x(), vector2DList[0].y() + GItem->pos().y());
		QPointF imagePoint2(vector2DList[1].x() + GItem->pos().x(), vector2DList[1].y() + GItem->pos().y());

		QPointF pos = GItem->pos();

		QPointF viewPoint1 = mapFromScene(imagePoint1);
		QPointF viewPoint2 = mapFromScene(imagePoint2);

			painter->drawLine(viewPoint1, viewPoint2);
	}
	QList<DicomData *> listSeriesDicomDatas = mI.seriesByDataViewModel(model, model->selectedItem());
	

	for(int i = 0; i < listSeriesDicomDatas.size(); i ++)
	{			
		DicomData * dicomData = listSeriesDicomDatas[i];
		QList<QVector2D> vector2DList = planesInterseption2D(dicomDataCur->planeInfo, dicomData->planeInfo);	

		

		if(vector2DList.size())
		{
 			QPointF imagePoint1(vector2DList[0].x() + GItem->pos().x(), vector2DList[0].y() + GItem->pos().y());
 			QPointF imagePoint2(vector2DList[1].x() + GItem->pos().x(), vector2DList[1].y() + GItem->pos().y());

			QPointF pos = GItem->pos();

			QPointF viewPoint1 = mapFromScene(imagePoint1);
			QPointF viewPoint2 = mapFromScene(imagePoint2);

			if(dicomData == model->getDicomData(model->selectedItem()))
				painter->drawLine(viewPoint1, viewPoint2);
		}

			//point1 = convertPointFromImageToPanel(tryUnConvertOnNotAlignedImage(turnHorPoint(rotateLeftPoint(point1))));
			//point2 = convertPointFromImageToPanel(tryUnConvertOnNotAlignedImage(turnHorPoint(rotateLeftPoint(point2))));
			//if(dicomStruct == ((ImagePanel *)clickedImagePanel)->dicomStruct)
			//{			
// 					blackAndWhiteLine(hdcBufferImageandInformation, 					
// 					point1.x,point1.y, point2.x, point2.y
// 					, 1, 1, RGB(0, 255, 0), rectMainWindow);
			//}else{
// 				if(getDisplayMode().isShowRefLineAll)
// 					whiteAndBlackLine(hdcBufferImageandInformation, 					
// 					point1.x,point1.y, point2.x, point2.y
// 					, 1, 0, RGB(255, 255, 0),rectMainWindow, PS_DOT);
// 			}

			//painter->drawLine(QPoint(X1Line, Y1Line), QPoint(X2Line, Y2Line));
		//}
	}
}

QSize View::sizeHint() const
{
	return QSize (99999, 99999);
}

void View::setTransform( const QTransform & matrix, bool combine /*= false*/ )
{
	QGraphicsView::setTransform(matrix, combine);
	t = matrix;
	setViewScale();
}

void View::setFastPixmap( QPixmap * pixmap, int _fastPixmapX, int _fastPixmapY )
{
	delete fastPixmap; 
	this->fastPixmap = pixmap; 
	fastPixmapX = _fastPixmapX;
	fastPixmapY = _fastPixmapY;
	if(fastPixmap)
	{
		needUpdateFastPixmap = true;		
		QPointF p0 =  mapToScene(0, 0);
		QPointF p1 =  mapToScene(1, 1);
		scene()->addLine(p0.x(), p0.y(), p1.x(), p1.y());
	}
}

bool View::eventFilter( QObject* obj, QEvent * e )
{
	if (e->type() == QEvent::MouseButtonPress)
		mousePressEvent((QMouseEvent *) e);

	if (e->type() == QEvent::MouseMove)
		mouseMoveEvent((QMouseEvent *) e);

	if (e->type() == QEvent::MouseButtonRelease)
		mouseReleaseEvent((QMouseEvent *) e);

	if (e->type() == QEvent::MouseButtonDblClick)
		mouseDoubleClickEvent((QMouseEvent *) e);

	return false;
}

void View::processChangeSelectedItem( const QModelIndex & index )
{
	//return;
	//if(_index == index)
	if(model->getDicomData(model->selectedItem()) &&
		model->getDicomData(_index) &&
		
		model->getDicomData(model->selectedItem())->getELEM_CONT().get(StudyUID).toString()!=
		model->getDicomData(_index)->getELEM_CONT().get(StudyUID).toString()
		)
		return;
	needUpdateOverlay = true;
	QPointF p0 =  mapToScene(0, 0);
	QPointF p1 =  mapToScene(1, 1);
	QGraphicsLineItem * line2 = new QGraphicsLineItem(p0.x(), p0.y(), p1.x(), p1.y());
	//this->scene()->addItem(line2);
	//this;
	//update();
}
