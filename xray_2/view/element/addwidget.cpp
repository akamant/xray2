#include "StdAfx.h"
#include "addwidget.h"
#include "view.h"
#include "modelsinformation.h"
#include "treedatamodel.h"

#include "labelwithautosizetext.h"
#include "command.h"


AddWidget::AddWidget( const QString & label, const Qt::Alignment & _alignment, bool _isExpanding /*= false*/, QColor _BGColor /*= QColor (50, 50, 50, 0)*/, QWidget *parent /*= NULL */ )
	:QWidget(parent)
	 ,isExpanding(_isExpanding)
	 , widthFont(0)
	 , BGColor (_BGColor)
	
{	
	GL = new QGridLayout ();
	setLayout(GL);

	autoSizeLabel = new LabelWithAutoSizeText(label,  _alignment);	
	
	GL->setSpacing(0);
	GL->setMargin(0);		
	
	
	
	setAutoFillBackground(true);
	autoSizeLabel->setMouseTracking(true);
	setMouseTracking(true);
	_color = QColor(0, 0, 0);

	setBGColor(BGColor);
}



AddWidget::~AddWidget()
{
	
}

void AddWidget::setText(QString str)
{
	autoSizeLabel->setText(str);	
	QFontMetrics metric(autoSizeLabel->font());
	widthFont = metric.width(autoSizeLabel->getText());	
};

bool AddWidget::eventFilter(QObject *obj, QEvent *event)
{
	
	if (event->type() == QEvent::MouseButtonPress) 
	{
		QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);	
		if(keyEvent->buttons() == Qt::LeftButton)
			mousePressEvent(keyEvent);
		if(keyEvent->button() == Qt::LeftButton)
			mousePressEvent(keyEvent);
		return true;
	} else 
	{
		if(event->type() == QEvent::MouseMove)
		{
			QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);	
			if(keyEvent->buttons() == Qt::RightButton)
				int a = 1;
			return true;
		}else{
			// standard event processing
			if(event->type() == QEvent::Paint || event->type() == QEvent::Show || event->type() == QEvent::Resize)
				return QObject::eventFilter(obj, event);
		}
	}
	return false;
}

void  AddWidget::mousePressEvent ( QMouseEvent * event )
{
	emit clicked();	

}

void  AddWidget::mouseReleaseEvent ( QMouseEvent * event )
{
	QWidget::mouseReleaseEvent(event);
	//emit released();		
}

void  AddWidget::mouseDoubleClickEvent( QMouseEvent * event )
{
	emit dblClicked();	
	QWidget::mouseDoubleClickEvent(event);
}

QSize AddWidget::sizeHint() const
{
	return QSize(200, 100);
}

void AddWidget::enterEvent( QEvent * event )
{
	QFontMetrics metric(autoSizeLabel->font());
	widthFont = metric.width(autoSizeLabel->getText());		

	QColor activeBGColor =  BGColor.lighter(150);
	activeBGColor.setAlpha(255);

	setBGColor(activeBGColor);

	if(isExpanding)
	{
		QFontMetrics metric(autoSizeLabel->font());
		int widthFont = metric.width(autoSizeLabel->getText());	
	}	

	
}

void AddWidget::leaveEvent( QEvent * event )
{
	QFontMetrics metric(autoSizeLabel->font());
	widthFont = metric.width(autoSizeLabel->getText());	
	if(isExpanding)
	{		
		//setFixedWidth(99999999);
		//layout()->setSizeConstraint(QLayout::SetDefaultConstraint);
	}		

	setBGColor(BGColor);
}

void AddWidget::paintEvent(QPaintEvent * event){	
	//setMask(QRegion(0, 0, widthFont, height()));
	QWidget::paintEvent(event);
}

void AddWidget::resizeEvent(QResizeEvent * event){		
	GL->addWidget(autoSizeLabel);	
	QWidget::resizeEvent(event);
	QFontMetrics metric(autoSizeLabel->font());
	widthFont = metric.width(autoSizeLabel->getText());	
}

void AddWidget::showEvent(QShowEvent * event){		
	QWidget::showEvent(event);
	QFontMetrics metric(autoSizeLabel->font());
	widthFont = metric.width(autoSizeLabel->getText());	
}

void AddWidget::mouseMoveEvent( QMouseEvent * event )
{
	QWidget::mouseMoveEvent(event);
}

void AddWidget::setBGColor( QColor color )
{
	QPalette palette;
	palette.setColor( QPalette::Text,			QColor( 255, 255, 255 ) );	
	palette.setColor( QPalette::Foreground,		QColor( 255, 255, 255 ) );	
	palette.setColor( QPalette::Background,		color );	
	setPalette( palette );
	autoSizeLabel->setPalette( palette );
}


void AddWidget::setNewBGColor( QColor color )
{
	BGColor = color;
	setBGColor(BGColor);
}