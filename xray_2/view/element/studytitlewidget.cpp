#include "stdafx.h"
#include "TreeDataViewModel.h"
#include "TreeDataModel.h"
#include "modelcontrols.h"
#include "studytitlewidget.h"
#include "dicomdata.h"
#include "labelwithautosizetext.h"
#include "addwidget.h"


StudyTitleWidget::StudyTitleWidget( TreeDataViewModel * _model, const QModelIndex & _index, QWidget *parent )
	: model(_model)
	, QWidget(parent)
	, index(_index)
	, timerId(0)
	, allreadyDblClicked(false)
{	
	//return;
	DicomData * dicomData =  model->getDicomData(index); 
	QString nameClass = this->metaObject()->className();
	if(!dicomData){
		VLogger() << "NULL dicomData in" +  nameClass;	
		VLogger() << QString("%1").arg(model->rowCount(index.child(0,0).child(0,0)));
		VLogger() << QString("%1").arg(model->rowCount(index.child(0,0)));
		VLogger() << QString("%1").arg(model->rowCount(index));
		VLogger() << QString("%1").arg(model->rowCount(index.parent()));
		VLogger() << QString("%1").arg(model->rowCount(index.parent().parent()));
		VLogger() << "";
		return;
	}

	QGridLayout * Gl = new QGridLayout(this);
	QString str =		  dicomData->getELEM_CONT().get(0x00080060).toString() + " " 
 						+ dicomData->getELEM_CONT().get(0x00100010).toString() + " "
						+ dicomData->getELEM_CONT().get(0x00080020).toString() + " "
						+ dicomData->getELEM_CONT().get(0x00080030).toString(); 
	
	addWidget = new AddWidget( str, Qt::AlignLeft, false);

	addWidget->installEventFilter(this);	
	setLayout(Gl);
	Gl->addWidget(addWidget, 0, 0, 1, 1);
	Gl->setColumnStretch(0, 1);

	QIcon * maximizeIcon = new	QIcon();
	maximizeIcon->addPixmap(QPixmap(":/resources/images/gray/maximize.png"), QIcon::Normal, QIcon::Off);		
	maximizeIcon->addPixmap(QPixmap(":/resources/images/gray/restore.png"),  QIcon::Normal, QIcon::On);		

	windowMaximizeBtn = new QPushButton();
	windowMaximizeBtn->setIcon(*maximizeIcon);
	windowMaximizeBtn->setCheckable(true);
	windowMaximizeBtn->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);

	windowCloseBtn = new QPushButton();
	windowCloseBtn->setIcon(QIcon(":/resources/images/gray/windowsclose.png"));
	windowCloseBtn->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);

	Gl->addWidget(windowMaximizeBtn,	0, 1, 1, 1);
	Gl->addWidget(windowCloseBtn,	0, 2, 1, 1);

	Gl->setMargin(0);
	Gl->setSpacing(0);

	controls = new ModelControls( model, index, this);	

	connect	(	model,		SIGNAL	( dataChanged				( const QModelIndex &, const QModelIndex &  ) ),
				this,		SLOT	( processChanging			( const QModelIndex &, const QModelIndex &  ) )
		);	

	connect	(	windowCloseBtn,		SIGNAL	( clicked				()	),
				controls,			SLOT	( eraseBranch			()	) 
		);	

	connect	(	windowMaximizeBtn,	SIGNAL	( clicked					()	),
				this,				SLOT	( windowMaximizeBtnClicked	()	) 
		);	

	QPalette palette;
	palette.setColor( QPalette::Foreground, QColor( 255, 255, 255 ) );
	palette.setColor( QPalette::Text,		QColor( 255, 255, 255 ) );
	setPalette( palette );

	addWidget->setPalette( palette );

	connect	(	model,		SIGNAL	( changeActivitySignal	(const QModelIndex & ) ),
				this,		SLOT	( ActivityChangeSlot	(const QModelIndex & ) )
		);

	connect	(	model,				SIGNAL	( changedDataModelIndex	( const QModelIndex &) ),
				this,				SLOT	( changeContentFrame	( const QModelIndex & ) )
		);

	ActivityChangeSlot(index);
}

void StudyTitleWidget::ActivityChangeSlot(const QModelIndex & index)
{
		uint tag = model->getIndexTag(this->index);

		if(model->getActivityItem(this->index))
			addWidget->setNewBGColor( QColor( 150, 10, 10, 255) );
		else
			addWidget->setNewBGColor( QColor( 50, 50, 50, 0) );
}
		



void StudyTitleWidget::processChanging( const QModelIndex & startIndex, const QModelIndex & endIndex)
{
		DicomData * dicomData =  model->getDicomData(index); 
		QString nameClass = this->metaObject()->className();
		if(dicomData)
		{
			QString str =		dicomData->getELEM_CONT().get(0x00080060).toString() + " " 
							+	dicomData->getELEM_CONT().get(0x00100010).toString() + " "
							+	dicomData->getELEM_CONT().get(0x00080020).toString() + " "
							+	dicomData->getELEM_CONT().get(0x00080030).toString();
			addWidget->setText(str);	
		}
}



StudyTitleWidget::~StudyTitleWidget()
{
	killTimer(timerId);
	timerId = 0;
}

void StudyTitleWidget::showEvent( QShowEvent * event )
{
 	QWidget::showEvent(event);
}


void  StudyTitleWidget::mouseReleaseEvent ( QMouseEvent * event )
{
	QWidget::mouseReleaseEvent(event);
}

void  StudyTitleWidget::resizeEvent(QResizeEvent * event )
{
	QWidget::resizeEvent(event);
	windowMaximizeBtn->setIconSize	(	QSize(height(), height()));
	windowCloseBtn->setIconSize		(	QSize(height(), height()));

}

void StudyTitleWidget::windowCloseBtnClicked( )
{	
	//emit dblClicked();
}

void StudyTitleWidget::windowMaximizeBtnClicked( )
{
	//emit dblClicked();
	model->setIsViewOne(index);
}

void StudyTitleWidget::mousePressEvent( QMouseEvent * event )
{
	QWidget::mousePressEvent(event);
	if( !timerId && QApplication::keyboardModifiers () != Qt::ShiftModifier )
		timerId = startTimer(1);
		//timerId = startTimer(QApplication::doubleClickInterval() + 1);
	//
}


void StudyTitleWidget::mouseDoubleClickEvent( QMouseEvent * event )
{	
	return;

	killTimer(timerId);
	timerId = 0;
	allreadyDblClicked = true;
	if(windowMaximizeBtn->isChecked())
		windowMaximizeBtn->setChecked(false);
	else
		windowMaximizeBtn->setChecked(true);
	windowMaximizeBtnClicked();
}

QSize StudyTitleWidget::sizeHint() const
{
	return QSize(300, 50);
}

bool StudyTitleWidget::eventFilter(QObject* obj, QEvent * e) 
{

	if (e->type() == QEvent::MouseButtonDblClick)
	{
		mouseDoubleClickEvent((QMouseEvent *) e );
		return false;
	}

	if (e->type() == QEvent::MouseButtonPress)
	{
		mousePressEvent((QMouseEvent *) e );
		return false;
	}
	return QWidget::eventFilter(obj, e);
}


void StudyTitleWidget::timerEvent(QTimerEvent * e)
{	
	killTimer(e->timerId());
	timerId = 0;
	allreadyDblClicked = false;
	if(!allreadyDblClicked)
		controls->changeDataModelIndex();	
}