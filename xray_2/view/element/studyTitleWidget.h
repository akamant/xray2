#ifndef STUDYTITLEWIDGET_H
#define STUDYTITLEWIDGET_H

//#include <QWidget>
class QWidget;
class TreeDataViewModel;
class LabelWithAutoSizeText;
class ModelControls;

class AddWidget;
class StudyTitleWidget : public QWidget
{
	Q_OBJECT
signals:
	void dblClicked();
public:
	//StudyTitleWidget(TreeDataViewModel * model, QWidget *parent);
	StudyTitleWidget( TreeDataViewModel * _model, const QModelIndex & index, QWidget *parent );
	~StudyTitleWidget();
	virtual void showEvent(QShowEvent * event);
	virtual QSize sizeHint() const;
private:
	bool allreadyDblClicked;
	AddWidget * addWidget;
	QModelIndex  index;
	TreeDataViewModel * model;
	LabelWithAutoSizeText * textEdit;
	QPushButton * windowMaximizeBtn;
	QPushButton * windowCloseBtn;
	uint timerId;
	ModelControls * controls;
public slots:
	void processChanging( const QModelIndex & startIndex, const QModelIndex & endIndex);
	void windowCloseBtnClicked( );
	void windowMaximizeBtnClicked( );
	void ActivityChangeSlot(const QModelIndex & index);
protected:
	void mouseReleaseEvent ( QMouseEvent * event );
	void mouseDoubleClickEvent( QMouseEvent * event );
	void resizeEvent(QResizeEvent * event );
	bool eventFilter(QObject* obj, QEvent * e);
	void timerEvent(QTimerEvent * e);
	void mousePressEvent( QMouseEvent * event );
	

};




#endif // STUDYTITLEWIDGET_H
