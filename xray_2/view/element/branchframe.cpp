#include "StdAfx.h"
#include "BranchFrame.h"
#include "branchsplitter.h"
#include "modulewidgetsmanager.h"
#include "modelcontrols.h"
#include "command.h"



BranchFrame::BranchFrame( TreeDataViewModel * _model, const QModelIndex & __index, QWidget *parent /*= 0*/ ):
QFrame(parent)
,model(_model)
,_index(__index)
, pol (QSizePolicy::Ignored)
{	
	uint tag = model->getIndexTag(_index);	

	connect	(	model,		SIGNAL	( changeActivitySignal	(const QModelIndex & ) ),
				this,		SLOT	( ActivityChangeSlot	(const QModelIndex & ) )
			);

	connect	(	model,				SIGNAL	( changedDataModelIndex	( const QModelIndex &) ),
				this,				SLOT	( changeContentFrame	( const QModelIndex & ) )
			);

	connect	(	model,				SIGNAL	( changeSelectedItemSignal	( const QModelIndex &) ),
				this,				SLOT	( changeSelectedItemSlot	( const QModelIndex & ) )
		);

	connect	(	model,				SIGNAL	( changedViewOne		( ) ),
				this,				SLOT	( changePolicyByViewOne	( ) )
		);		

	modelIndex = model->getDataModelIndex(_index);
	setSizePolicy(QSizePolicy::Ignored , QSizePolicy::Ignored);	
	
	ModuleWidgetsManager::shapeFrame(this, tag, model, _index);
	ActivityChangeSlot(QModelIndex());	
}

void BranchFrame::changeSelectedItemSlot(const QModelIndex & index)
{
	QPalette palette = this->palette();
	if(_index == index)
	{
		palette.setColor( QPalette::Foreground, QColor( Qt::yellow) );	
		setPalette(palette);
	}
	else
	{
		palette.setColor( QPalette::Foreground, color );	
		setPalette(palette);
	}	
}

void BranchFrame::ActivityChangeSlot(const QModelIndex & index)
{
//	if(index == this->index){
	uint tag = model->getIndexTag(this->_index);
	QColor color;
	QPalette palette;
	if(model->getActivityItem(this->_index))
	{			
		switch( tag )
		{
		case 0x00080018:
			{ 		
				color = QColor( 100, 100, 100 ) ;
				break;
			}
		case SeriesUID:
			{ 					

				color = QColor( 100, 255, 100 ) ;
				break;
			}	
		case StudyUID:
			{ 
				color = QColor( 255, 100, 100 ) ;
				break;
			}	
		default: 
			{
				color = QColor( 100, 100, 100 ) ;
			};
		}
	}else
	{
		color = QColor( 100, 100, 100 ) ;
	}

	if( color != this->color )
	{
		palette.setColor( QPalette::Foreground, color );			
		setPalette( palette );
		this->color = color;
	}	
}

BranchFrame::~BranchFrame()
{

}

bool BranchFrame::eventFilter(QObject* obj, QEvent * e) 
{
	if (e->type() == QEvent::Wheel)
	{
		return QWidget::eventFilter(obj, e);
		return true;

		if(model->getIndexTag(_index) == SeriesUID)
		{
			QWheelEvent * wheelEvent = dynamic_cast<QWheelEvent *>(e);
			if(wheelEvent)
			{
				int numDegrees = wheelEvent->delta() / 8;
				int numSteps = numDegrees / 15;
				ModelControls * controls = new ModelControls( model, _index, this);	
				if(numDegrees < 0)
					controls->selectNextChildren();		
				else
					controls->selectPrevChildren();									
			}
		}
	}
	
	if (e->type() == QEvent::MouseButtonPress) 
	{
		QMouseEvent * mouseEvent = static_cast<QMouseEvent *>(e);
		//if(mouseEvent->button() == Qt::LeftButton)
			if(QApplication::keyboardModifiers () == Qt::ShiftModifier)		
				model->selectTreeItem(_index, true);
			else
				model->selectTreeItem(_index, false);	
		return QWidget::eventFilter(obj, e);
			
	} 	

	if (e->type() == QEvent::MouseButtonDblClick)
	{
		if(QApplication::keyboardModifiers () == Qt::ShiftModifier)
		{			
			return true;
			return QWidget::eventFilter(obj, e);
		}
	}
	return QFrame::eventFilter(obj, e);

}


void BranchFrame::showEvent( QShowEvent * e )
{
	QFrame::showEvent(e);
}


void BranchFrame::oneViewMode()
{
	return;
	if(Command::isPrimitiveState)
		return;
	QSizePolicy::Policy pol = QSizePolicy::Ignored;	
	QSizePolicy::Policy pol2 = QSizePolicy::Minimum;	
	if(this->pol == pol){
		setSizePolicy(pol2 , pol2);
		this->pol = pol2;
	}else{
		setSizePolicy(pol , pol);
		this->pol = pol;
	}		
}

void BranchFrame::changeContentFrame ( const QModelIndex & index)
{
	if(index == _index)
	{
		if(indexTypeDifferent(model->getDataModelIndex(index)))
		{

			QLayoutItem *item;
			while ((item=layout()->takeAt(0)) !=0)
			{
				QWidget  *w = item->widget();
				delete item;

				layout()->removeWidget(w);
				delete w;
			}
			delete layout();
			uint tag = model->getIndexTag(index);	
			ModuleWidgetsManager::shapeFrame(this, tag, model, index);		
		}
		modelIndex = model->getDataModelIndex(_index);
	}
}

bool BranchFrame::indexTypeDifferent(const QModelIndex & index)
{
	if	(	modelIndex != model->getDataModel()->emptyDicomDataIndex() &&
			index == model->getDataModel()->emptyDicomDataIndex()
			||
			modelIndex == model->getDataModel()->emptyDicomDataIndex() &&
			index != model->getDataModel()->emptyDicomDataIndex()
		)
		return true;
	else
		return false;
}
QSize BranchFrame::sizeHint() const
{
	return QSize(99999, 99999);
}

void BranchFrame::changePolicyByViewOne()
{	
	//return;
	QSizePolicy::Policy pol;
	if(model->isIndexViewOne(_index))	
	{		
		setFrameStyle(QFrame::NoFrame);		
		pol = QSizePolicy::Minimum;		
	}else
	{		
		setFrameStyle(QFrame::Plain | QFrame::Box);
		pol = QSizePolicy::Ignored;
	}

	//return;
	if(this->pol != pol)
	{
		QWidget * w = this;
		w->setSizePolicy(pol , pol);	
		
		while(w != NULL)
		{		
			w->setSizePolicy(pol , pol);	
			
			w = dynamic_cast<QWidget *>(w->parent());

			BranchFrame * branchFrame =  dynamic_cast<BranchFrame *>(w);
			if(branchFrame)
				break;		
		}
	}
	this->pol = pol;
}
