#include "StdAfx.h"
#include "branchsplitter.h"
#include "BranchFrame.h"
#include "dicomimageviewwidget.h"
#include "addwidget.h"
#include "modulewidgetsmanager.h"
#include "ModelControls.h"

BranchSplitter::BranchSplitter( TreeDataViewModel * _model, const QModelIndex & _index, QWidget *parent /*= 0*/ ):
	 QWidget(parent)
	,model(_model)
	,index(_index)
	,addWidget(NULL)
	,rows(1)
	,cols(1)
	, isFirstShow(true)
{
	QVector2D div = model->getDivisionForItem(index);
	SetRowsCols(div.x(), div.y());
	setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	gridLayout.setMargin(0);
	gridLayout.setSpacing(1);
	setLayout(&gridLayout);	
	setMinimumSize(QSize(50, 50));
	connect	(	model,				SIGNAL	( rowsInserted			( const QModelIndex &, int, int ) ),
				this,				SLOT	( addNewTreeItemSlot	( const QModelIndex &, int, int ) )
		);	

	connect	(	model,				SIGNAL	( changedDataModelIndex	( const QModelIndex &) ),
				this,				SLOT	( refillAll				( const QModelIndex & ) )
		);

	connect	(	model,				SIGNAL	( changeDivision		( const QModelIndex &, int, int ) ),
				this,				SLOT	( setDivision			( const QModelIndex &, int, int ) )
		);	

	fillEmtySells();
}

BranchSplitter::~BranchSplitter()
{
}

//////��� ������������� ���� ��� �������� ������� ���������� �� ������� � ���������� ������� ��� � ������������
void BranchSplitter::SetRowsCols(int rows, int cols)
{	
	QQueue <QWidget *> queue;
	QLayoutItem *child;
	while( (child = gridLayout.takeAt(0)) != 0)
	{
		QWidget * widget = child->widget();
		queue.enqueue(widget);			
	}	
	this->rows = rows;
	this->cols = cols;
	for(int r = 0; r < rows; r++)
	{
		for(int c = 0; c < cols; c++)
		{
			if(!queue.isEmpty())
			{
				QWidget * w = queue.dequeue();
				gridLayout.addWidget(w, r, c);	
			}
		}
	}	
	fillEmtySells();	
	while( !queue.isEmpty())
		delete queue.dequeue();
}

void BranchSplitter::fillEmtySells()
{	
	int rowCount = model->rowCount(index);
	for(int r = 0; r < rowCount; r ++)
	{		
		QModelIndex childIndex =  model->index(r, 0, index);	
		if(!isExistChildIndex(childIndex))
		{
			if(!existEmptySell())
				break;
			BranchFrame * branchFrame = new BranchFrame(model, childIndex, this);
		
			addIndexWidget(branchFrame);
		}
	}		
}

bool BranchSplitter::existEmptySell(){
	int countWidgets = gridLayout.count();
	if(countWidgets >= cols * rows)	
		return false;
	else
		return true;
}

bool BranchSplitter::isExistChildIndex( const QModelIndex & index){
	for(int i = 0; i < gridLayout.count(); i ++)
		if( ((BranchFrame *)gridLayout.itemAt(i)->widget())->getIndex() == index){
			return true;
		}
	return false;
}

void BranchSplitter::addIndexWidget( QWidget * widget,  int rowSpan, int columnSpan, Qt::Alignment){	
	int countWidgets = gridLayout.count();
	int r = countWidgets / cols;
	int c = r <= 0 ? countWidgets : ( countWidgets % (r * cols) );	
	gridLayout.addWidget(widget, r, c, 1, 1);	
}

void BranchSplitter::addNewTreeItemSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd )
{
	if(this->index == index)
	{	
		SetRowsCols(rows, cols);		
	}
}


void BranchSplitter::deleteChildrenSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd )
{
return;

	if(this->index == index)
	{	
		QQueue <QWidget *> queue;
		QLayoutItem *child;
		for(int i = 0; i < rowNumberEnd - rowNumberStart; i ++)
		{
			child = gridLayout.takeAt(gridLayout.count() - 1);
			QWidget * widget = child->widget();				
			queue.enqueue(widget);				
		}	

		while( !queue.isEmpty())
			delete queue.dequeue();
	}
}


bool BranchSplitter::eventFilter(QObject* obj, QEvent * event) 
{	
return true;

	if (event->type() == QEvent::MouseButtonPress )
	{
		QMouseEvent * mouseEvent = static_cast<QMouseEvent *>(event);
		int a = mouseEvent->buttons() ;		
		if(QApplication::keyboardModifiers () == Qt::ShiftModifier)					
			return QWidget::eventFilter(obj, event);			
	} 	
	return QWidget::eventFilter(obj, event);
}

void BranchSplitter::showEvent( QShowEvent * event )
{
	
	QWidget::showEvent(event);
	if(isFirstShow)
	{
		isFirstShow = false;

		connect	(	model,				SIGNAL	( rowsRemoved 			( const QModelIndex &, int, int ) ),
					this,				SLOT	( deleteChildrenSlot	( const QModelIndex &, int, int ) )
			);
			
	}
	
}

void BranchSplitter::setDivision( const QModelIndex & index, int rows, int cols )
{
	if(this->index == index)
 		SetRowsCols(rows, cols);
}


void BranchSplitter::refillAll( const QModelIndex & index )
{
	if(this->index == index)
	{
		QQueue <QWidget *> queue;
		QLayoutItem *item;
		while ((item=gridLayout.takeAt(0)) !=0)
		{
			QWidget  *w = item->widget();
			delete item;

			gridLayout.removeWidget(w);
			delete w;
		}

		addWidget = NULL;
		fillEmtySells();
	}
}


QSize BranchSplitter::sizeHint() const
{
	return QSize(999999, 999999);
}
