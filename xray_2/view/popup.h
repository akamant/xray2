#ifndef VPOPUP_H
#define VPOPUP_H 

#include <QtGui> 
#include <QStandardItemModel>

#include "dicomDirItem.h"

 
/////////////////////////////////////////////////////////////////
// VWidget //////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//
class VWidget : public QFrame
{
	Q_OBJECT
public:
	VWidget(QWidget* Parent = NULL);
	void setCentralWidget(QWidget *widget);
	virtual void showPopup(QPoint tlPos);

protected:
	virtual QRect rectHintForPos(QPoint tlPos, QSize area){ return QRect(tlPos, sizeHint()); };

protected:
	void focusOutEvent( QFocusEvent * event ){hide();}

private:		
	QVBoxLayout* vBox;	
};

/////////////////////////////////////////////////////////////////
// TTypedItemModel //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//
class TTypedItemModel : public QStandardItemModel
{
public:
	void setHeader( const THeaderInfoList &header );
	const THeaderInfoList &header(){ return headerInfo; } 

private:
	THeaderInfoList headerInfo;
};

////////////////////////////////////////////////////////////////////
// TTypedItemProxyModel /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//
class TTypedItemProxyModel : public QSortFilterProxyModel
{
public:
	virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const;
};


/////////////////////////////////////////////////////////////////
// VPopupTable //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//
class VPopupTable : public VWidget
{
	Q_OBJECT
public:
	VPopupTable(QWidget *Parent = NULL);
	VPopupTable( QStringList &horzHeaders, QWidget *Parent = NULL );
	//
	void showPopup(QPoint tlPos);
	//void addRow( QStringList &values, bool enabled = true );
	void addRow(QStringList &values, bool enabled= true, int selectedItem = 0);
	void setHeader( const THeaderInfoList &header );
	void clear();
	//
	void setChosen(int row);
	int  getChosen() { return chosed; }

protected slots:
	void slotChosed();

protected:
	QRect rectHintForPos(QPoint tlPos, QSize area);
	void init();

private:	
	QTableView		*view;
	TTypedItemModel		 *model;
	TTypedItemProxyModel *proxyModel;

	int chosed;
};


















#endif