#ifndef STANDARDSHAPER_H
#define STANDARDSHAPER_H

class TreeDataViewModel;

class TreeLayout : public QObject
{
	Q_OBJECT

public:
	TreeLayout(QObject *parent);
	~TreeLayout();
	static void shapeFrame( QFrame *frame, uint tag, TreeDataViewModel * model, const QModelIndex & index);
	static void shapeLowLevel(QGridLayout * Gl, QFrame * frame, QWidget * mainWidget, uint tag, TreeDataViewModel * model, const QModelIndex & index);
private:
	
};

#endif // STANDARDSHAPER_H
