#include "StdAfx.h"
#include "popup.h" 

 
//////////////////////////////////////////////////
// VWidget //////////////////////////////////////// 
///////////////////////////////////////////////////
//
VWidget::VWidget(QWidget* Parent)
:QFrame(Parent)
{
	vBox = new QVBoxLayout;
	vBox->setContentsMargins(0,0,0,0);
	setLayout(vBox);
	
	setFrameShape( QFrame::Panel );
	setFrameShadow( QFrame::Raised );
}

//
void VWidget::setCentralWidget(QWidget *widget)
{
	vBox->insertWidget(1,widget);
	setFocusProxy(widget);
}

//          
void VWidget::showPopup(QPoint tlPos)
{
	setParent(NULL);
	
	setWindowFlags(Qt::Popup);
	
	QRect r = rectHintForPos( tlPos, QApplication::desktop()->size() );
	
	r.setWidth ( r.width()  + 2 ); //��-�� setFrameShape( QFrame::Panel );
	r.setHeight( r.height() + 2 );
	
	setGeometry(r);
	
	show();
	setFocus();
	
	while (isVisible()) {
		qApp->processEvents();
	}
}


///////////////////////////////////////////////////
// VPopupTable ////////////////////////////////////
///////////////////////////////////////////////////
//
VPopupTable::VPopupTable(QWidget* Parent)
: VWidget(Parent)
{
	init();
}

//
VPopupTable::VPopupTable(QStringList &horzHeaders, QWidget* Parent)
:VWidget(Parent)
{
	init();
	
//	widget->setColumnCount( horzHeaders.count() );
//	widget->setHorizontalHeaderLabels( horzHeaders );
}

//
void VPopupTable::init()
{
	chosed = -1;

	model	   = new TTypedItemModel;
	proxyModel = new TTypedItemProxyModel;
	view	   = new QTableView;

	proxyModel->setSourceModel( model );
	view->setModel( proxyModel );

	connect( view, SIGNAL(clicked(const QModelIndex&)), SLOT(slotChosed()) );
	connect( view, SIGNAL(entered(const QModelIndex&)), SLOT(slotChosed()) );

	view->verticalHeader()->hide();
	view->horizontalHeader()->setHighlightSections(false);

	view->setSelectionBehavior(QAbstractItemView::SelectRows);
	view->setSelectionMode(QAbstractItemView::SingleSelection);

	view->setSortingEnabled( true );

	setCentralWidget( view );

	QHeaderView * verticalHeaders = view->horizontalHeader() ;
	QFont font = verticalHeaders->font();
	font.setBold(true);
	verticalHeaders->setFont(font);
}

//
void VPopupTable::clear()
{
	chosed = -1;
	model->clear();
}

//
void VPopupTable::addRow(QStringList &values, bool enabled, int selectedItem)
{
	int rows = model->rowCount()
	  , cols = model->columnCount();
	
	if( values.count() < cols )
		return;

	QStandardItem *item;
	for( int i = 0; i < values.count(); i++ ){
		item = new QStandardItem( values[i] );

		if( enabled ){
			item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );
		}else{
			item->setFlags( Qt::NoItemFlags );

			QFont font = item->font();
			font.setBold(true);
			item->setFont(font);
			}
		
		model->setItem( rows, i, item );
	}
}

//
void VPopupTable::showPopup(QPoint tlPos)
{
	chosed = -1;
	view->resizeColumnsToContents();
	view->resizeRowsToContents();
	
	VWidget::showPopup(tlPos);
}

//
void VPopupTable::setChosen(int row)
{
	int rows = model->rowCount()
	  , cols = model->columnCount();
	
	if( row < 0 || row >= rows )
		row = 0;
	
	QModelIndex sourceIndex = model->index( row, 0 );
	QModelIndex proxyIndex  = proxyModel->mapFromSource( sourceIndex );

	view->setCurrentIndex( proxyIndex );

	chosed = row;
}

//
QRect VPopupTable::rectHintForPos(QPoint tlPos, QSize area)
{
	int rows = model->rowCount()
	  , cols = model->columnCount();
	
	int w = 0, h = 0;
	for(int i =0; i < cols; i++){
		w += view->columnWidth(i);
	}
	h = view->rowHeight(0)*rows + view->horizontalHeader()->height() ;
	
	w += 0;
	h += 5;
	
	if( tlPos.y() + h + 10 > area.height() ){
		if( tlPos.y() > h + 5 )
			tlPos.setY( tlPos.y() - h );
		else{
			tlPos.setY( 10 );
			if( area.height() < h + 10 ){
				h = area.height() - 10;
				w += view->verticalScrollBar()->width();
			}
		}
	}
	if( tlPos.x() + w + 10 > area.width() )
		tlPos.setX( tlPos.x() - w );
	
	return QRect( tlPos.x(), tlPos.y(), w, h );
}

//
void VPopupTable::slotChosed()
{
	QModelIndex index = view->currentIndex();
	
	if( proxyModel->flags(index) && Qt::ItemIsSelectable  ){
		int sourceIndexRow = proxyModel->mapToSource( index ).row();

		if( chosed != sourceIndexRow ){
			chosed = sourceIndexRow;
			hide();
		}
	}
//	else{
//		widget->setRangeSelected( QTableWidgetSelectionRange(chosed,0,chosed,widget->columnCount()-1), true );
//	}
}

//
void VPopupTable::setHeader( const THeaderInfoList &header )
{
	model->setHeader(header);
}

/////////////////////////////////////////////////////////////////
// TTypedItemModel //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//
void TTypedItemModel::setHeader( const THeaderInfoList &header )
{
	headerInfo = header;

	QStringList headerList;
	foreach( const THeaderSectionInfo &info, header )
		headerList << info.label;

	setHorizontalHeaderLabels( headerList );
}


/////////////////////////////////////////////////////////////////
// TTypedItemProxyModel /////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//
bool TTypedItemProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
	if( left.parent().isValid() )
		return true;

	TTypedItemModel *typedModel = dynamic_cast<TTypedItemModel*>( sourceModel() );
	QVariant::Type type = typedModel->header()[ left.column() ].type;

	QVariant leftData  = typedModel->data(left);
	QVariant rightData = typedModel->data(right);

	if( leftData.canConvert(type) && rightData.canConvert(type) ){
		switch(type){
			case QVariant::Int:
				return leftData.toInt() < rightData.toInt();
			case QVariant::Double:
				return leftData.toDouble() < rightData.toDouble();
			case QVariant::Time:
				return leftData.toTime() < rightData.toTime();
			case QVariant::Date:{
				
				QDate leftDate = QDate::fromString( leftData.toString(), "d.M.yyyy" )
				,	  rightDate = QDate::fromString( rightData.toString(), "d.M.yyyy" );
				return leftDate < rightDate;
			}
			case QVariant::DateTime:
				::MessageBox( NULL, "������� ��������� QDateTime! � TTypedItemProxyModel", "", MB_OK );
		}
	}

	return leftData.toString() < rightData.toString();
}