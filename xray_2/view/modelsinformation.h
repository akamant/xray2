#ifndef MODELSINFORMATION_H
#define MODELSINFORMATION_H

#include "popup.h"

class TreeDataViewModel;
class DicomData;
class ModelsInformation : public QObject
{
	Q_OBJECT

public:
	ModelsInformation(QObject *parent = NULL);
	~ModelsInformation();

QVector<THeaderSectionInfo> getChildrenHeadersByViewDataModelIndex(TreeDataViewModel * model, const QModelIndex & viewDataModelIndex);


QList <QStringList>  getChildrenDataListByViewDataModelParentIndex(TreeDataViewModel * model, const QModelIndex & viewDataModelIndex);
QList <DicomData *> seriesByDataViewModel(TreeDataViewModel * model, const QModelIndex & viewDataModelIndex );

private:
	
};

#endif // MODELSINFORMATION_H
