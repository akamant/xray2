#include "StdAfx.h"
#include "modelsinformation.h"
#include "TreeDataViewModel.h"
#include "treedatamodel.h"
#include "dicomdata.h"
#include "exprBuilder.h"
#include "exprDict.h"
//#include "vpopup.h"
ModelsInformation::ModelsInformation(QObject *parent)
	: QObject(parent)
{

}

ModelsInformation::~ModelsInformation()
{

}
/*
QVector<THeaderSectionInfo> ModelsInformation::getChildrenHeadersByViewDataModelIndex(TreeDataViewModel * model,
																					  const QModelIndex & viewDataModelIndex )
{	
	QModelIndex dataModelIndex = model->getDataModelIndex(viewDataModelIndex);

	QModelIndex parentIndex = dataModelIndex.parent();
	TreeDataModel * dataModel = model->getDataModel();
	int rowCount = dataModel->rowCount(parentIndex);
	for(int r = 0; r < rowCount; r ++){
		addNewTreeItem(modelIndexParent, r);
		QModelIndex modelIndex = dataModel->index(r, 0, modelIndexParent);
	}
	
	//model->getDataModel()->getDicomDataByIndex( );
	QVector<THeaderSectionInfo> topHeadersList;    	
	return topHeadersList;
}*/


QVector<THeaderSectionInfo> ModelsInformation::getChildrenHeadersByViewDataModelIndex(TreeDataViewModel * model,
																					  const QModelIndex & viewDataModelIndex ){
	QVector<THeaderSectionInfo> topHeadersList; 
	//qDebug() << model->getIndexTag(viewDataModelIndex);
	//uint tag = model->getIndexTag(model->index(0,0,viewDataModelIndex));

	uint tag = model->getIndexTag(viewDataModelIndex);
	//uint tag = StudyUID;
	if(tag == SeriesUID){		
		topHeadersList << THeaderSectionInfo( QVariant::Int, "�����");
		topHeadersList << THeaderSectionInfo( QVariant::Char, "��������");
		topHeadersList << THeaderSectionInfo( QVariant::Char, "����� ����");
	}
	if(tag == StudyUID){				
		topHeadersList 
					<< THeaderSectionInfo( QVariant::String,   "�����������" )
					<< THeaderSectionInfo( QVariant::String, "��� ��������" )
					<< THeaderSectionInfo( QVariant::Date,   "���� ����." )
					<< THeaderSectionInfo( QVariant::Time,   "����� ����." )
					<< THeaderSectionInfo( QVariant::Date,   "���� ����." );				
					
	}

	if(tag == 0x00080018){				
		topHeadersList 
						<< THeaderSectionInfo( QVariant::String, "Number" )
						<< THeaderSectionInfo( QVariant::Time,   "Time" );
	}

	
	return topHeadersList;
}




QList <QStringList> ModelsInformation::getChildrenDataListByViewDataModelParentIndex(TreeDataViewModel * model,
																			 const QModelIndex & viewDataModelIndex )
{	

	QVector<THeaderSectionInfo> topHeadersList; 
	//qDebug() << model->getIndexTag(viewDataModelIndex);
	
	
	QStringList values;	
	QList <QStringList> retList ;
	QModelIndex parentIndex = model->getDataModelIndex(model->parent(viewDataModelIndex));	
	TreeDataModel * dataModel = model->getDataModel();
	int rowCount = dataModel->rowCount(parentIndex);

	//uint tag = model->getIndexTag(model->index(0, 0, viewDataModelParentIndex));
	//tag = StudyUID;
	uint tag = model->getIndexTag(viewDataModelIndex);
	if(tag == 0x00080018)
	{			
		for(int r = 0; r < rowCount; r ++)
		{
			QModelIndex modelIndex = dataModel->index(r, 0, parentIndex);		
			DicomData * dicomData = dataModel->getDicomDataByIndex(modelIndex);
			TElemContList list(&dicomData->getELEM_CONT());
			TExprBuilder builder( TExpressionDict::getDict(), list );
			values 		
				//<< dicomData->getELEM_CONT().get(0x0001000C).toString()
				<< QString("%1").arg(r + 1)
				<< dicomData->getELEM_CONT().get(0x00080032).toString();
				//<< builder.execExpr("BODYPARTEXAMINED_LATERALITY");
				retList << values;
			values.clear();
		}
	}


	if(tag == SeriesUID)
	{			
		for(int r = 0; r < rowCount; r ++){
			QModelIndex modelIndex = dataModel->index(r, 0, parentIndex);		
			DicomData * dicomData = dataModel->getDicomDataByIndex(modelIndex);
			TElemContList list(&dicomData->getELEM_CONT());
			TExprBuilder builder( TExpressionDict::getDict(), list );
			values 		
				<< dicomData->getELEM_CONT().get(0x00200011).toString()
				<< dicomData->getELEM_CONT().get(0x0008103E).toString()
				<< builder.execExpr("BODYPARTEXAMINED_LATERALITY");
			retList << values;
			values.clear();
		}
	}

	if(tag == StudyUID)
	{                  
		for(int r = 0; r < rowCount; r ++){
			QModelIndex modelIndex = dataModel->index(r, 0, parentIndex);           
			DicomData * dicomData = dataModel->getDicomDataByIndex(modelIndex);
			TElemContList list(&dicomData->getELEM_CONT());
			TExprBuilder builder( TExpressionDict::getDict(), list );
			values     
				<< dicomData->getELEM_CONT().get(0x00080060).toString()
				<< dicomData->getELEM_CONT().get(0x00100010).toString() //NAME_PATIENT_PARAMETER				                                               
				<< dicomData->getELEM_CONT().get(0x00080020).toString()
				<< dicomData->getELEM_CONT().get(0x00080030).toString()
				<< dicomData->getELEM_CONT().get(0x00100030).toString()  
				;
			retList << values;
			values.clear();
		}
		values  << "������� � �����.."
				<< ""			                                               
				<< ""
				<< ""
				<< ""
				;
		retList << values;
	}
	return retList;
}

QList <DicomData *> ModelsInformation::seriesByDataViewModel(TreeDataViewModel * model,	const QModelIndex & viewDataModelIndex )
{	

	QVector<THeaderSectionInfo> topHeadersList; 
	//qDebug() << model->getIndexTag(viewDataModelIndex);


	QStringList values;	
	QList <QStringList> retList ;
	QModelIndex parentIndex = model->getDataModelIndex(model->parent(viewDataModelIndex));	
	TreeDataModel * dataModel = model->getDataModel();
	int rowCount = dataModel->rowCount(parentIndex);
	
	QList <DicomData *> dicomDataList;               
	for(int r = 0; r < rowCount; r ++)
	{
		QModelIndex modelIndex = dataModel->index(r, 0, parentIndex);           
		DicomData * dicomData = dataModel->getDicomDataByIndex(modelIndex);
		dicomDataList << dicomData;
	}
	
	return dicomDataList;
}