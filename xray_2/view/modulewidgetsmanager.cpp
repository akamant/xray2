#include "StdAfx.h"
#include "modulewidgetsmanager.h"
#include "treelayout.h"

ModuleWidgetsManager::ModuleWidgetsManager(QObject *parent)
	: QObject(parent)
{

}

ModuleWidgetsManager::~ModuleWidgetsManager()
{

}

void ModuleWidgetsManager::shapeFrame( QFrame * frame, uint tag, TreeDataViewModel * model, const QModelIndex & index )
{
		
	QString modality = model->getModality(index);
	TreeLayout::shapeFrame(frame, tag, model, index);

}