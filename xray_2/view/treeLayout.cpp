#include "StdAfx.h"
#include "treedatamodel.h"
#include "treelayout.h"
#include "branchsplitter.h"
#include "dicomimageviewwidget.h"
#include "stackedwidget.h"
#include "semihidedpanel.h"
#include "gridsplitterwidget.h"
#include "modelcontrols.h"
#include "studytitlewidget.h"
#include "treedataviewmodel.h"
#include "cleverbutton.h"
#include "LeafButton.h"
#include "addwidget.h"

#include "prevnextwidget.h"
#include "ImageComboBox.h"

#include "semihidedpanel.h"
#include "PrintTreeDataViewModel.h"


TreeLayout::TreeLayout(QObject *parent)
	: QObject(parent)
{

}

TreeLayout::~TreeLayout()
{

}

void TreeLayout::shapeLowLevel(QGridLayout * Gl, QFrame * frame, QWidget * mainWidget, uint tag, TreeDataViewModel * model, const QModelIndex & index)
{
	if(model->getDataModelIndex(index) == model->getDataModel()->emptyDicomDataIndex())
	{				

		mainWidget = new MaskedStackedWidget();
		//((QFrame *)mainWidget)->setFrameStyle(QFrame::Plain | QFrame::Box);	
		//((QFrame *)mainWidget)->setLineWidth(0);	
		//frame->setLineWidth(0);	
		mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	}else
	{			
		QWidget * splitterW = (QWidget *)frame->parent();				
		frame->setLineWidth(1);
		mainWidget = new DicomImageViewWidget(model, index);
	}
	mainWidget->installEventFilter(frame);

	Gl->addWidget(mainWidget, 0, 0);	
	return;
}


void TreeLayout::shapeFrame( QFrame * frame, uint tag, TreeDataViewModel * model, const QModelIndex & index)
{
	QGridLayout * Gl = new QGridLayout(frame);
	
	frame->setLayout(Gl);
	frame->setFrameStyle(QFrame::Plain | QFrame::Box);			
	QString modality = model->getModality(index);
	QWidget * mainWidget;

	switch( tag )
	{
		case 0x00200052:
		{
			shapeLowLevel(Gl, frame, mainWidget , tag, model, index);
			return;
			break;
		}	
		case 0x00080018:
		{ 			
			Gl->setMargin(0);
			Gl->setSpacing(0);	

			if(modality.contains("US"))
			{			
				mainWidget = new BranchSplitter( model, index, frame);
				frame->setLineWidth(2);				
				break;
			}else
			{
				shapeLowLevel(Gl, frame, mainWidget,  tag, model, index);				
				return;
			}			
		}
		case SeriesUID:
		{ 
			Gl->setMargin(0);
			Gl->setSpacing(0);		
			break;
		}	
		case StudyUID:
		{ 		
			Gl->setMargin(1);
			Gl->setSpacing(0);	
			break;
			mainWidget = new BranchSplitter( model, index, frame);
			frame->setLineWidth(2);
			break;
		}	
		default: 
		{
			mainWidget = new BranchSplitter( model, index, frame);
			frame->setLineWidth(0);
			break;
		}
	}

	StackedWidget * stackedWidget = new StackedWidget(true);

	if(tag == SeriesUID)
	{
		if(model->getDataModelIndex(index) == model->getDataModel()->emptyDicomDataIndex())
		{

			mainWidget = new AddWidget("������� �����..", Qt::AlignCenter);
			mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

			frame->setLineWidth(0);

			stackedWidget->addWidget(mainWidget);

			ModelControls * controls = new ModelControls( model, index, NULL);
			connect	(mainWidget,			SIGNAL	( clicked				() ),
						controls,			SLOT	( changeDataModelIndex	() )
				);			
		}else
		{
			mainWidget = new BranchSplitter( model, index, frame);
			mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

			frame->setLineWidth(2);

			QWidget * prevNextWidget = new PrevNextWidget(model, index);			
			prevNextWidget->installEventFilter(frame);

			QColor color (220, 220, 220);		
			SemihidedPanel * panel = new SemihidedPanel(color);
		
			stackedWidget->addWidget(mainWidget);		
			//stackedWidget->addWidget(prevNextWidget);	

			if(!modality.contains("MG"))
				stackedWidget->addWidget(panel);	

			QBoxLayout * bLayout = new QBoxLayout(QBoxLayout::LeftToRight);		

			panel->setLayoutForAtiveWindow(bLayout);

			CleverButton * prevSeriesB = new CleverButton(220, new QPixmap(":/resources/images/gray/prevSeries.png"));
			prevSeriesB->installEventFilter(frame);
			prevSeriesB->setShortcut(Qt::Key_Home);
			CleverButton * nextSeriesB = new CleverButton(220, new QPixmap(":/resources/images/gray/nextSeries.png"));		
			nextSeriesB->installEventFilter(frame);
			nextSeriesB->setShortcut(Qt::Key_End);
			QPushButton * chooseButton = new QPushButton("������� �����..");

			QPalette palette;
			palette.setColor( QPalette::Text,			QColor( 255, 255, 255 ) );	
			palette.setColor( QPalette::Foreground,		QColor( 255, 255, 255 ) );	
			palette.setColor( QPalette::Background,		QColor (0, 255, 0, 255) );	

			chooseButton->setPalette( palette );
			chooseButton->setFixedHeight(prevSeriesB->height());
			chooseButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);	
			GridSplitterWidget * gridSplitterWidget = new GridSplitterWidget(model->getDivisionForItem(index).x(), model->getDivisionForItem(index).y(), 3 , 3, color);
			gridSplitterWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);	
	
			QWidget * ww = new QWidget();
			bLayout->setMargin(0);
			bLayout->setSpacing(0);
		

			if(!modality.contains("MG"))
			{
				if(!modality.contains("XA"))
					bLayout->addWidget(gridSplitterWidget, 1, Qt::AlignTop | Qt::AlignLeft);	
				bLayout->addWidget(chooseButton, 1, Qt::AlignTop | Qt::AlignLeft);
			}
			

			ModelControls * controls = new ModelControls( model, index, frame);
			connect(gridSplitterWidget,		SIGNAL	(changedSplitting			(QVector2D) ),
					controls,				SLOT	(changeDataModelDivision	(QVector2D) )
					);

			connect(prevSeriesB,			SIGNAL	( clicked				() ),
					controls,				SLOT	( goPrevModelIndex		() )
					);

			connect(chooseButton,			SIGNAL	( clicked				() ),
					controls,				SLOT	( changeDataModelIndex	() )
					);

			connect(nextSeriesB,			SIGNAL	( clicked				() ),
					controls,				SLOT	( goNextModelIndex		() )
					);
		}
	
		stackedWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);		
		mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
		Gl->addWidget(stackedWidget, 0, 0);	


	}else 
	if(tag == StudyUID)
	{	

		if(model->getDataModelIndex(index)  == model->getDataModel()->emptyDicomDataIndex() 
		)
		{			
			if(!dynamic_cast<PrintTreeDataViewModel *> (model))
			{
				mainWidget = new AddWidget( "������� ������������...", Qt::AlignCenter);	
				mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

				stackedWidget->addWidget(mainWidget);	

				ModelControls * controls = new ModelControls( model, index, NULL);	

				connect	(	mainWidget,			SIGNAL	( clicked				() ),
							controls,			SLOT	( changeDataModelIndex	() )
						);
			}else
			{
				mainWidget = new MaskedStackedWidget();
				mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
			}			
			
		}else
		{
			mainWidget = new BranchSplitter( model, index, frame);
			mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
			frame->setLineWidth(2);	

			QColor color (10, 255, 10);
			SemihidedPanel * panel = new SemihidedPanel(color);

			QWidget * w = new QWidget;

			QBoxLayout * boxLayout = new QBoxLayout(QBoxLayout::TopToBottom, frame);		
			boxLayout->setSpacing(0);
			boxLayout->setMargin(0);

			QWidget * topWidget = new QWidget();	
			//topWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
			QGridLayout * topGridLayout = new QGridLayout();
			topGridLayout->setSpacing(0);
			topGridLayout->setMargin(0);
			topWidget->setLayout(topGridLayout);
			topWidget->setMaximumHeight(30);

			VImageComboBox * comboBox = new VImageComboBox(model, index);	

			StudyTitleWidget * studyTitleWidget = new StudyTitleWidget (model, index, NULL);
			studyTitleWidget->installEventFilter(frame);


			//studyTitleWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
			//studyTitleWidget->setFixedHeight(icomPixmap.height());
			//studyTitleWidget->setFixedHeight(comboBox->height());

			QWidget * emtyTopWiget = new QWidget();
			//emtyTopWiget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
			comboBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
			topGridLayout->addWidget(comboBox, 0, 0, 0, 1); // , Qt::AlignTop | Qt::AlignLeft
			topGridLayout->addWidget(studyTitleWidget, 0, 1, 1, 1 ); //, Qt::AlignTop | Qt::AlignLeft
		
			boxLayout->addWidget(topWidget);
			boxLayout->addWidget(mainWidget);
			w->setLayout(boxLayout);

			stackedWidget->addWidget(w);
			//stackedWidget->addWidget(mainWidget);
			//stackedWidget->addWidget(panel);	
			Gl->addWidget(stackedWidget, 0, 0);

			QGridLayout * gridLayout = new QGridLayout();
			gridLayout->setSpacing(0);
			gridLayout->setMargin(0);
			panel->setLayoutForAtiveWindow(gridLayout);	
		
		
			CleverButton * prevStudyB =  new CleverButton(110, new QPixmap(":/resources/images/gray/prevStudy.png") );
			prevStudyB->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);	
			CleverButton * nextStudyB = new CleverButton(110, new QPixmap(":/resources/images/gray/nextStudy.png"));
			nextStudyB->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);	
			QPushButton * chooseButton = new QPushButton("������� ������������");
			//chooseButton->setFixedHeight(prevStudyB->height());
			chooseButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

			QWidget * rightWidget = new QWidget();
			QBoxLayout * boxrightLayout = new QBoxLayout(QBoxLayout::LeftToRight);
			rightWidget->setLayout(boxrightLayout);
			QWidget * emtyWidget = new QWidget();
			emtyWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
			boxrightLayout->addWidget(emtyWidget, 1, Qt::AlignTop | Qt::AlignRight);
			boxrightLayout->addWidget(prevStudyB, 0, Qt::AlignTop | Qt::AlignRight);
			boxrightLayout->addWidget(nextStudyB, 0, Qt::AlignTop | Qt::AlignRight);
			boxrightLayout->setMargin(0);
			boxrightLayout->setSpacing(0);

			//gridLayout->addWidget(leftWidget, 0, 0, 1, 1, Qt::AlignTop | Qt::AlignLeft);
			//gridLayout->addWidget(rightWidget, 0, 0, 1, 1, Qt::AlignTop | Qt::AlignRight);
			//stackedWidget->addWidget(rightWidget);	

			ModelControls * controls = new ModelControls( model, index, frame);
	// 		connect(gridSplitterWidget,		SIGNAL	(changedSplitting			(QVector2D) ),
	// 				controls,				SLOT	(changeDataModelDivision	(QVector2D) )
	// 			);
			connect(prevStudyB,				SIGNAL	( clicked				() ),
					controls,				SLOT	( goPrevModelIndex		() )
					);

			connect(chooseButton,			SIGNAL	( clicked				() ),
					controls,				SLOT	( changeDataModelIndex	() )
					);

			connect(nextStudyB,				SIGNAL	( clicked				() ),
					controls,				SLOT	( goNextModelIndex		() )
					);
			connect(studyTitleWidget,		SIGNAL	( dblClicked			() ),
					frame,					SLOT	( oneViewMode			() )
				);
		}
		
	}else{		
		stackedWidget->addWidget(mainWidget);	
		Gl->setMargin(1);
		Gl->setSpacing(0);
	
	}
	stackedWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);		
		mainWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
		Gl->addWidget(stackedWidget, 0, 0);	
	


}

