﻿#!/usr/bin/env python 
# -*- coding: utf-8 -*- 

#import ConfigParser
from xml.dom.minidom import parse

class FakeSecHead(object):
	def __init__(self, fp):
		self.fp = fp
		self.sechead = '[dummy]\n'
	def readline(self):
		if self.sechead:
			try: return self.sechead
			finally: self.sechead = None
		else: return self.fp.readline()

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)		
		
version_resource_path = 'resources/version/version_resource.h'
#git_properties_path = '../target/git.properties'
pom_xml_path = '..\pom.xml'

#config=ConfigParser.RawConfigParser()
#config.readfp( FakeSecHead(open(git_properties_path)) )

#commit = config.get( "dummy", "git.commit.id.abbrev" )[:4]
#branch = config.get( "dummy", "git.branch" )

dom = parse( pom_xml_path )
number_version_node = dom.getElementsByTagName("project")[0].getElementsByTagName("version")[0]
number_version = number_version_node.firstChild.nodeValue

number_version = number_version.split('-')[0]

version_string = u'#define STRPRODUCTNAME "xRay Dicom Viewer 2, версия ' + number_version

#if branch != 'develop':
#	version_string += ', ' + 'b: ' + branch + 'c:' + commit
	
f = open( version_resource_path, 'w')
f.write( version_string.encode('cp1251') + '"\n' )