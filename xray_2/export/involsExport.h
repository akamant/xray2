#pragma once

struct TImgInfo
{
	QVector3D pos;
	QVector3D orientX, orientY;
	float thickness;
};

struct TImgGroup
{
	int begin, length;
	float refBias;
	TImgInfo info;
};

///////////////////////////////////////////////////////////
// TInvolsExport //////////////////////////////////////////
///////////////////////////////////////////////////////////
//
class DicomData;

class TInvolsExport : public QObject
{
	Q_OBJECT
public:
	TInvolsExport(const QList<DicomData*> &imgList, int window, int level );
	~TInvolsExport(){}
	//
	bool groupSelected();
	bool exportTo( const QString &filename, const QString &path );
	QString error(){ return m_error; }
	//
	static bool isInvolsAvailable();
	static QString involsPath();

signals:
	void setBounds( int curr, int total );
	void imageExported( int index );

private:
	bool isValid();
	TImgInfo &getImageInfo( DicomData *img );
	bool exportGroup();
	bool ifBelongsToGroup( DicomData *img, DicomData *prevImg, TImgGroup &group );
	bool chooseGroupToExport( const QList<TImgGroup> &groups );
	QList<TImgGroup> getGroups();
	//
	int m_window, m_level;
	QString m_filename, m_path, m_error;
	//Panel	*m_panel;
	QList<DicomData*>	m_imgList;
	TImgGroup	m_chosenGroup;
	QMap<DicomData*,TImgInfo> m_infos;
};

