#pragma once

#include "involsExport.h"

///////////////////////////////////////////////////////////
// TGroupExportDialog /////////////////////////////////////
///////////////////////////////////////////////////////////
//
class TGroupExportDialog : public QDialog
{
	Q_OBJECT
public:
	TGroupExportDialog( const QList<TImgGroup> &groups, QWidget *parent = NULL );
	~TGroupExportDialog(){}
	//
	int getSelectedGroup(){ return m_selectedIndex; }

private slots:
	void slotAccept();
	void slotItemDblClicked( QTreeWidgetItem * item, int column );

private:
	void fillGroupsTree( const QList<TImgGroup> &groups );
	//
	QTreeWidget *m_groupsTree;
	int m_selectedIndex;
};