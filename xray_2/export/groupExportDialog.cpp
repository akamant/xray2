#include "stdafx.h"

#include "groupExportDialog.h"

///////////////////////////////////////////////////////////
// TGroupExportDialog /////////////////////////////////////
///////////////////////////////////////////////////////////
//
TGroupExportDialog::TGroupExportDialog( const QList<TImgGroup> &groups, QWidget *parent /* = NULL */ )
: m_selectedIndex(-1)
{
	setWindowTitle( "����� ����������� ��� ��������" );

	QLabel *infoLabel = new QLabel("��������� ����������� � ������ ����� �� ���������\n"
									"�������� �������� ������ ����������� ��� ��������" );

	m_groupsTree = new QTreeWidget;
	connect( m_groupsTree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int))
						, SLOT(slotItemDblClicked(QTreeWidgetItem*,int)) );
	fillGroupsTree( groups );

	QPushButton *okButton = new QPushButton( "��������������" );
	connect( okButton, SIGNAL(clicked()), SLOT(slotAccept()) );

	QPushButton *cancerButton = new QPushButton( "������" );
	connect( cancerButton, SIGNAL(clicked()), SLOT(reject()) );

	QHBoxLayout *hl = new QHBoxLayout;
	hl->addWidget( okButton );
	hl->addStretch();
	hl->addWidget( cancerButton );
	
	QVBoxLayout *vl = new QVBoxLayout;
	vl->addWidget( infoLabel );
	vl->addWidget( m_groupsTree );
	vl->addLayout( hl );

	setLayout( vl );

	resize( 700, 320 );
}

//
void TGroupExportDialog::fillGroupsTree( const QList<TImgGroup> &groups )
{
	QStringList headers = QStringList() << "������" << "����������" << "������� �����" << "���������� ����� �������";
	m_groupsTree->setHeaderLabels( headers );
	m_groupsTree->header()->setResizeMode( QHeaderView::ResizeToContents );

	foreach( const TImgGroup &group, groups ){
		QString imgsRange = QString("%1-%2").arg(group.begin+1).arg(group.begin + group.length);

		QTreeWidgetItem *item = new QTreeWidgetItem;
		item->setText( 0, imgsRange );
		item->setText( 1, QString("%1").arg(group.length) );
		item->setText( 2, QString("%1").arg(group.info.thickness) );
		item->setText( 3, QString("%1").arg(group.refBias) );

		m_groupsTree->addTopLevelItem( item );
	}
}

//
void TGroupExportDialog::slotAccept()
{
	QList<QTreeWidgetItem*> selected = m_groupsTree->selectedItems();
	
	if( selected.count() ){
		QTreeWidgetItem *item = selected.first();
		m_selectedIndex = m_groupsTree->indexOfTopLevelItem( item );
		
		done( QDialog::Accepted );
	}
}

//
void TGroupExportDialog::slotItemDblClicked( QTreeWidgetItem * item, int column )
{
	m_selectedIndex = m_groupsTree->indexOfTopLevelItem( item );

	done( QDialog::Accepted );
}