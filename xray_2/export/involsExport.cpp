#include "stdafx.h"

#include "involsExport.h"
//#include "Panel.h"
#include "dicomData.h"
#include "normalizedImage.h"
#include "groupExportDialog.h"
#include "config.h"
#include "dicomBitmapPresenter.h"

void bitmapFromNormImg( char *data, VNormalizedImage *normImg, uint size )
{
	size /= 2;
	short *ddata = (short*)data;
	for( uint i = 0; i < size; ++i )
		ddata[i] = (short)normImg->getValue(i);
}

///////////////////////////////////////////////////////////
// TInvolsExport //////////////////////////////////////////
///////////////////////////////////////////////////////////
//
TInvolsExport::TInvolsExport(const QList<DicomData*> &imgList, int window, int level )
:	//m_panel(panel),
	m_imgList(imgList),
	m_window(window), 
	m_level(level)
{
}

//
bool TInvolsExport::isInvolsAvailable()
{
	return !involsPath().isEmpty();
}

//
QString TInvolsExport::involsPath()
{
	VIDAR_APP_SETTINGS;

	QString path = settings.value( "involsDirectory", "/invols" ).toString();
	if( path == "" ) path = "/invols";
	if( path[0] == QChar('/') || path[0] == QChar('\\'))
		path = QCoreApplication::applicationDirPath() + path;

	QDir dir;
	if( dir.exists(path + "/invols.exe") )
		return path;
	else{
		QString defPath = QCoreApplication::applicationDirPath() + "/invols";
		if( dir.exists(defPath + "/invols.exe") ){
			dir.setPath( defPath );
			return dir.absolutePath();
		}
		
		QString oldPath = QCoreApplication::applicationDirPath() + "/../invols";
		if( dir.exists(oldPath + "/invols.exe") ){
			dir.setPath( oldPath );
			return dir.absolutePath();
		}
	}
	return QString();
}

//
bool TInvolsExport::isValid()
{
	//if( !m_panel ){
	//	m_error = "m_panel == 0";
	//	return false;
	//}

	if( m_imgList.size() <= 2){
		m_error = "������� ���� �����������.";
		return false;
	}
	
	DicomData *firstImg = m_imgList.first();
	if(    !firstImg->ELEM_CONT.hasElement(0x00200032)
		|| !firstImg->ELEM_CONT.hasElement(0x00200037)
		|| !firstImg->ELEM_CONT.hasElement(0x00180050) ){
			m_error = "����������� ����������� DICOM-����.";
			return false;
	}

	return true;
}

//
TImgInfo &TInvolsExport::getImageInfo( DicomData *img )
{
	TImgInfo info;
	
	QVariantList posList = img->ELEM_CONT.get( 0x00200032 ).toList()
	,			 orientList = img->ELEM_CONT.get( 0x00200037 ).toList();
	
	info.pos = QVector3D( posList[0].toFloat(), posList[1].toFloat(), posList[2].toFloat() );
	info.orientX = QVector3D( orientList[0].toFloat(), orientList[1].toFloat(), orientList[2].toFloat() );
	info.orientY = QVector3D( orientList[3].toFloat(), orientList[4].toFloat(), orientList[5].toFloat() );
	info.thickness = img->ELEM_CONT.get( 0x00180050 ).toFloat();

	m_infos[img] = info;

	return m_infos[img];
}

//
bool TInvolsExport::exportTo( const QString &filename, const QString &path )
{
	m_filename = filename;
	m_path = path;

	return exportGroup();
}

//
bool TInvolsExport::groupSelected()
{
	if( !isValid() )
		return false;

	return chooseGroupToExport( getGroups() );
}

//
QList<TImgGroup> TInvolsExport::getGroups()
{
	DicomData *dcmFirst = m_imgList.first();

	//if( !dcmFirst->getNormalizedImage() ) m_panel->doNormalizedImage( dcmFirst );
	VNormalizedImage *nImg = dcmFirst->getNormalizedImage();

	int begIndex = 0, length = 1;
	DicomData *prev = dcmFirst;

	QList<TImgGroup> groups;

	TImgGroup curGroup;
	curGroup.begin = 0;
	curGroup.length = 1;
	curGroup.info = getImageInfo( dcmFirst );

	foreach( DicomData *img, m_imgList.mid(1, m_imgList.count()-1) ){
		begIndex++;
		if( ifBelongsToGroup(img, prev, curGroup) )
			curGroup.length++;
		else{
			if( curGroup.length > 2 )
				groups << curGroup;

			curGroup.begin = begIndex;
			curGroup.length = 1;
			curGroup.info = m_infos[img];
		}
		prev = img;
	}
	if( curGroup.length > 2 )
		groups << curGroup;

	return groups;
}

//
bool TInvolsExport::chooseGroupToExport( const QList<TImgGroup> &groups )
{
	if( !groups.count() ){
		m_error = "�� ������� ���������� ����� �����������!";
		return false;
	}

	if( groups.count() == 1 ){
		m_chosenGroup = groups.first();
		return true;
	}

	TGroupExportDialog dlg( groups );
	if( dlg.exec() ){
		m_chosenGroup = groups.at( dlg.getSelectedGroup() );
		return true;
	}

	return false;
}

//
bool TInvolsExport::exportGroup()
{
	QList<DicomData*> tmpList = m_imgList.mid( m_chosenGroup.begin, m_chosenGroup.length );
	
	DicomData *dcmFirst = tmpList.first();

	dicomBitMapPresenter presenter(NULL);
	presenter.representDicomData(dcmFirst);	

	VNormalizedImage *nImg = dcmFirst->getNormalizedImage();	

	uint sliceWidth = nImg->getWidth()
		, sliceHeight = nImg->getHeight();
	uint bytesPerColor = nImg->getBytePerColor()
		, colorsPerPixel = nImg->getColorsPerPixel()
		, bytesPerPixel = bytesPerColor * colorsPerPixel;
	float pixelSpacing = dcmFirst->planeInfo.spacing
		, sliceThickness = dcmFirst->planeInfo.thickness;

	float sliceSpacing;
//	if( dcmFirst->ELEM_CONT.hasElement(0x00180088) )
//		sliceSpacing = dcmFirst->ELEM_CONT.get( 0x00180088 ).toFloat();
//	else{
		TImgInfo img1 = getImageInfo(tmpList[0])
		,		 img2 = getImageInfo(tmpList[1]);
		sliceSpacing = (img2.pos-img1.pos).length();
//	}
	
	QString infoString = QString("size: %1 %2 %3\n"
								"spacing: %4 %5 %6\n"
								"value_format: %7\n"
								"window_level: %8\n"
								"window_width: %9\n")
		.arg(sliceWidth).arg(sliceHeight).arg(m_chosenGroup.length)
		.arg(pixelSpacing).arg(pixelSpacing).arg(sliceSpacing)
		.arg( bytesPerPixel == 1 ? "UBYTE" : "USHORT" )
		.arg(m_level).arg(m_window);

	QDir pathdir;
	pathdir.mkpath( m_path );
	pathdir.cd( m_path );

	QFile rawinfo( pathdir.absolutePath() + "/" + m_filename + ".txt" );
	rawinfo.open( QIODevice::WriteOnly );
	rawinfo.write( infoString.toLatin1() );

	uint size = sliceWidth * sliceHeight * bytesPerPixel;

	QFile rawimgs( pathdir.absolutePath() + "/" + m_filename );
	rawimgs.open( QIODevice::WriteOnly );

	emit setBounds( 0, m_chosenGroup.length );
	uint cur = 0;

	DicomData *dcmLast = tmpList.last();
	float imgPosFirstZ = dcmFirst->ELEM_CONT.get(0x00200032).toList().last().toFloat()
		, imgPosLastZ = dcmLast->ELEM_CONT.get(0x00200032).toList().last().toFloat();
	bool direction = imgPosFirstZ > imgPosLastZ;

	char *data = new char[size];

	QListIterator <DicomData*> iter(tmpList);
	direction ? iter.toBack() : iter.toFront();
	
	while( direction ? iter.hasPrevious() : iter.hasNext() ){
		DicomData *dcmStruct = direction ? iter.previous() : iter.next();

	//	if( !dcmStruct->getNormalizedImage() ) m_panel->doNormalizedImage( dcmStruct );
		dicomBitMapPresenter presenter(NULL);
		presenter.representDicomData(dcmStruct);	


		VNormalizedImage *normImg = dcmStruct->getNormalizedImage();

		emit imageExported( cur++ );

		bitmapFromNormImg( data, normImg, size );
		rawimgs.write( (const char*)data, size );
	}
	delete data;

	return true;
}

//
bool TInvolsExport::ifBelongsToGroup( DicomData *img, DicomData *prevImg, TImgGroup &group )
{
	TImgInfo imgInfo = getImageInfo( img )
		,	 prevInfo = getImageInfo( prevImg );
	
	if( imgInfo.thickness != group.info.thickness )
		return false;

	qreal crossX = QVector3D::crossProduct( imgInfo.orientX,group.info.orientX ).length()
		, crossY = QVector3D::crossProduct( imgInfo.orientY,group.info.orientY ).length();

	if( crossX > 0.1 || crossY > 0.1 )
		return false;

	if( group.length > 2 ){
		float bias = (prevInfo.pos - imgInfo.pos).length();
		if( abs(bias - group.refBias) > 0.01 )
			return false;
	}
	else
		group.refBias = (prevInfo.pos - imgInfo.pos).length();
	
	return true;
}