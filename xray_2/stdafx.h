#ifndef STDAFX_H
#define STDAFX_H
#define ID_SETSCALE						33825
#define ID_SETDPI						33826

////tags////
#define DicomDataPtr QSharedPointer <DicomData >


enum DicomTags{StudyUID = 0x0020000D, SeriesUID = 0x0020000E};
#define TAG_STUDYUID 
#include "logger.h"
#include <QtGui>
#include <QtCore>
#include <QtNetwork>
#include <QtXml>


#include  <vector>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <math.h>
#include "Shlobj.h"
// 
#include "JpegDecoderxx.h"
#include "JpegDecoder08.h"
#include "JpegDecoder12.h"
#include "JpegDecoder16.h"


extern "C" {
//#include "jpeglib.h"
}
//#include "jpeglib.h"

/*
#pragma comment (lib, "dcmimgle.lib")
#pragma comment (lib, "ofstd.lib")
#pragma comment (lib, "dcmdata.lib")
#pragma comment (lib, "dcmimage.lib") 
#pragma comment (lib, "dcmjpeg.lib")
#pragma comment (lib, "ijg8.lib")
#pragma comment (lib, "ijg12.lib")
#pragma comment (lib, "ijg16.lib")
*/
//#pragma comment (lib, "zlib.lib")
//#include "jpeglib16.h"

//#include <stdafx2.h>
//#include <jpeglib.h>


#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#endif

#include <GdiPlus.h>
using namespace Gdiplus;
#pragma comment(lib, "GdiPlus.lib")

//CRT_SECURE_NO_WARNINGS

