#include "stdafx.h"

#include "xray_2.h"
#include "elemCont.h"
TElemCont getELEM_CONT();
#include "exprDict.h"
#include "dicomDict.h"
#include "overlayDict.h"
#include "config.h"
#include "qslog.h"
#include "QsLogDest.h"

void createDicts()
{
	TDicomDict::getDict()->loadDict( ":/expr/dictionary.xml",  extractResource(":/expr/local.xml",getVidarSettingsPath().absoluteFilePath("local.xml")));
	TExpressionDict::getDict()->loadDict(extractResource(":/expr/expressions.xml", getVidarSettingsPath().absoluteFilePath("expressions.xml")));
	TOverlayDict::getDict()->loadDict(extractResource(":/expr/overlay.xml",getVidarSettingsPath().absoluteFilePath("overlay.xml")));
}

//
void saveDicts()
{
	TDicomDict::getDict()->saveDict(getVidarSettingsPath().absoluteFilePath("local.xml"));
	TExpressionDict::getDict()->saveDict(getVidarSettingsPath().absoluteFilePath("expressions.xml"));
	TOverlayDict::getDict()->saveDict(getVidarSettingsPath().absoluteFilePath("overlay.xml"));
}

//
void SetCodec()
{
	QTextCodec * russian = QTextCodec::codecForName("CP1251"); 
	QTextCodec::setCodecForTr(russian);
	QTextCodec::setCodecForCStrings(russian);
}

//
void PrepareLogger()
{
	VIDAR_APP_SETTINGS;
	
	QsLogging::Logger& logger = QsLogging::Logger::instance();
	logger.setLoggingLevel( (QsLogging::Level)settings.value("logLevel").toInt());

	QString logName = QDate::currentDate().toString("dd") + ".txt";

	QDir homeDir = getVidarSettingsPath();
	homeDir.mkdir("log");
	homeDir.cd("log");

	const QString sLogPath( homeDir.absoluteFilePath(logName) );
	static QsLogging::DestinationPtr fileDestination(
		QsLogging::DestinationFactory::MakeFileDestination(sLogPath) );
	static QsLogging::DestinationPtr debugDestination(
		QsLogging::DestinationFactory::MakeDebugOutputDestination() );
	logger.addDestination( debugDestination.get() );
	logger.addDestination( fileDestination.get() );

	QLOG_INFO() << "Started";
}

//
void PreparePlugins()
{
	QStringList list_path;
	QDir dir = QDir( qApp->applicationDirPath() + "/plugins/" );
	list_path << dir.absolutePath () << qApp->libraryPaths();
	qApp->setLibraryPaths( list_path  );	
}

//
int main(int argc, char *argv[])
{
	{ // ���� ��� ���� ������� � ������ ��� ��������� - �������� ��������� � ���������� ��������� � ��������� ������
		HWND hwnd = FindWindowA((LPCSTR)NULL, "Vidar-XRAY");
		if(argc > 1 && hwnd){
			COPYDATASTRUCT cd;
			char * buffer = new char [255];				
			unsigned short Cmd = 5004;
			memcpy(buffer, &Cmd, 2);
			unsigned char size = 250;
			memcpy(buffer + 2, &size, 1);			
			strncpy_s( buffer + 3, 252, argv[1], size );
			cd.lpData = buffer;
			cd.cbData = size;
			::SendMessage(hwnd, WM_COPYDATA, 0, (LPARAM) &cd);
			return 0;
		}
	}
	
//	qInstallMsgHandler(VLogger::myMessageOutput);
	
	char * s = new char [1];	
	sprintf_s( s, 1, "", "%d" );
	delete[] s;	
	
	QApplication a(argc, argv);

	a.setOrganizationDomain("povidar.ru");
	a.setOrganizationName("PO VIDAR");

	QSettings::setDefaultFormat(QSettings::IniFormat);

#ifndef XRAY_LT
	a.setApplicationName("xRay");
	// ������� ������ ��������� ����� � ����������� ������
	QSettings::setPath(QSettings::IniFormat, QSettings::UserScope, a.applicationDirPath());
#else
	a.setApplicationName("xRay Lite");
#endif
	a.setApplicationVersion("2.0.0");

	VIDAR_APP_SETTINGS;
	if (settings.value("logLevel").isNull()) settings.setValue("logLevel", QsLogging::InfoLevel);

	QDir::setCurrent( QCoreApplication::applicationDirPath() );

	PrepareLogger();

	QString iniFile = getVidarSettingsPath().absoluteFilePath("xray.sample.ini");
	extractResource(":/resources/xRay.sample.ini", iniFile );

	createDicts();

	SetCodec();

	xray_2 * w;
	if(argc > 1)
		w = new xray_2(argv[1]);
	else
		w = new xray_2();
	a.setWindowIcon( QIcon(":/resources/img/main.png") );

	PreparePlugins();
	
	w->show();

	int value =  a.exec();	

	saveDicts();

	QLOG_INFO() << "Closed.\n\n";

	delete w;
	return value;
}
