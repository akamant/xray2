#ifndef PRINTWIDGET_H
#define PRINTWIDGET_H

#include <QWidget>

class PrintWidget : public QFrame
{
	Q_OBJECT

public:
	PrintWidget(QWidget *parent);
	~PrintWidget();

	QPrinter::Orientation disposition; //���������� + �������. ������� ���������� � printPane.disposition �� ���������� - ������ :(
	QWidget * printPanel;
	void deleteAll(); // ������� ��� ����������� �� �������
	QVector2D format;
	void doRealSize ();
	bool isPaper;
	void setPrintPanel(QWidget * _printPanel);

protected:

	void seitPrintWidgetFont();
	virtual void paintEvent(QPaintEvent * event);
	virtual void showEvent( QShowEvent *event );	
	//virtual bool winEvent( MSG * message, long * result );
	float ratio();
	QSizeF destinationSize();
	

};

	


#endif // PRINTWIDGET_H
