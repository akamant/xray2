#ifndef PRINTSIZESMENU_H
#define PRINTSIZESMENU_H

#include "toolbuttonwithmenu.h"

//class ToolButtonWithMenu;

class PrintFormatMenu : QObject
{
	Q_OBJECT

public:
	PrintFormatMenu(QObject *parent);
	QVector2D format();
	ToolButtonWithMenu * menu();
	~PrintFormatMenu();
	bool isPaper();
private:
	QActionGroup * group;
	QVector2D _format;
	ToolButtonWithMenu * _menu;
};

#endif // PRINTSIZESMENU_H
