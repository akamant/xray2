#include "StdAfx.h"
#include "printformatsmenu.h"
#include "config.h"

PrintFormatMenu::PrintFormatMenu(QObject *parent)	
	:QObject(parent)
{
	group = new QActionGroup(this);

	VIDAR_APP_SETTINGS;
	settings.beginGroup("Print");
	settings.setIniCodec("Windows-1251");

	int j = 1;		
	QString strItem ;	
	QStringList items;
	while((strItem = settings.value(tr("Name%1").arg(j), "").toString()).compare ("") )
	{
		QAction * act = new QAction(strItem, this) ;
		group->addAction(act);
		j++;
	}
		 
	group->addAction(new QAction("�������� �������", this)); 
	_menu = new ToolButtonWithMenu (group);
}

PrintFormatMenu::~PrintFormatMenu()
{

}

ToolButtonWithMenu * PrintFormatMenu::menu()
{
	return _menu;
}

QVector2D PrintFormatMenu::format()
{

	if(isPaper())
		return QVector2D (8.26771, 11.6929);

	VIDAR_APP_SETTINGS;
	settings.beginGroup("Print");
	int index = menu()->count() + 1;
	QString stringFormat = settings.value(tr("Format%1").arg(index)).toString();
	if(stringFormat.isEmpty())
		QMessageBox::information( NULL, "XRay", "� ������ [Print] �� ���������� ��������� Format ��� �������� ��������\n" );
	settings.endGroup();
	QVector2D format(35, 43);

// 	QRegExp rx("(\\d+|\\d+.\\d+)");
// 	QStringList list;
// 	int pos = 0;	
// 	for(int i = 0; i < 2 && (pos = rx.indexIn(stringFormat, pos)) != -1; i++)
// 	{		
// 		QString sre = rx.cap(1);
// 		if(i == 0)
// 		{
// 			format.setX( rx.cap(1).toFloat() );
// 		}else
// 		{
// 			format.setY( rx.cap(1).toFloat() );
// 			break;
// 		}
// 		pos += rx.matchedLength();
// 	}	
// 	format = QVector2D(format.x(), format.y()); 

	QStringList splitingList = stringFormat.split("x", QString::SkipEmptyParts, Qt::CaseInsensitive);
	if(splitingList.size() != 2)	
		QMessageBox::information( NULL, "XRay", 
		QString("� ������ [Print] �������� ������ ��������� Format ��� �������� �������� (%1)\n").arg(stringFormat) );
	else
		format = QVector2D(splitingList[0].toFloat(), splitingList[1].toFloat());

	return format;
}

bool PrintFormatMenu::isPaper()
{
	if(_menu->count() + 1 == group->actions().size())
		return true;
	else
		 return false;

}