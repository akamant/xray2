#include "StdAfx.h"
#include "printdialog.h"
#include "printmainwindow.h"
#include "TreeDataModel.h"

PrintDialog::PrintDialog(TreeDataViewModel * _printModel, QWidget *parent)
	: QDialog(parent)
	, printModel (_printModel)
{
	setWindowTitle("������� ������: ��� �������"); //������� ��������� ������� � ������
	setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);	

	QGridLayout *mainLayout = new QGridLayout(this);
	setLayout(mainLayout);
	mainLayout->setMargin(0);

	printMainWindow = new PrintMainWindow(printModel, this);
	QSettings settings;
	printMainWindow->restoreState(settings.value("printDialogState").toByteArray());	
	printMainWindow->setWindowFlags(printMainWindow->windowFlags() & ~Qt::Window);
	mainLayout->addWidget(printMainWindow);

	
	restoreGeometry(settings.value("printDialogGeometry").toByteArray());	
	
	connect(	printModel,		SIGNAL	( modelChanged() ),
				this,			SLOT	( reStructSlot() ));

	connect	(	printModel->getDataModel(),		SIGNAL	( modelChanged() ),
				this,							SLOT	( reStructSlot() )
		);
}

PrintDialog::~PrintDialog()
{

}

void PrintDialog::setPrintWidget( QWidget * widget )
{
	printMainWindow->printWidget->setPrintPanel(widget);
}


void PrintDialog::saveSettings() 
{
	QSettings settings;
	settings.setValue("printDialogGeometry", saveGeometry());
	settings.setValue("printDialogState", printMainWindow->saveState());
}


void PrintDialog::reStructSlot()
{	
	int totalPages = printMainWindow->allElementsSize() / printMainWindow->viewElementsSize();
	//int currentPage = totalPages + (printMainWindow->viewElementsSize() - (printMainWindow->allElementsSize() - printMainWindow->firstElementNumber())) / printMainWindow->viewElementsSize();

	int currentPage = 1 + totalPages -  (printMainWindow->allElementsSize() - printMainWindow->firstElementNumber()) 
										/ printMainWindow->viewElementsSize();

	
	if(printMainWindow->allElementsSize() % printMainWindow->viewElementsSize())
	{
		//currentPage ++;
		totalPages ++;
	}else{
	}
	
	//if (currentPage)  
		setWindowTitle(QString("������� ������: %1 �� %2 ������").arg(currentPage).arg(totalPages));
	//else
	//	setWindowTitle("������� ������: ��� �������");
}



