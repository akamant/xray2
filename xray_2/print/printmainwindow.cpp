#include "StdAfx.h"
#include "printmainwindow.h"
#include "modelcontrols.h"
#include "config.h"
#include "printformatsmenu.h"
#include "TreeDataModel.h"
//TreeDataModel


PrintMainWindow::PrintMainWindow( TreeDataViewModel * _printModel, QWidget* Parent )
	:QMainWindow(Parent)
	, printModel(_printModel)
	,printFormatMenu(NULL)
{

	printWidget = new PrintWidget(this);
	setCentralWidget(printWidget);
	//panelForPrint = NULL;
	makeToolbars();	
	
}

PrintMainWindow::~PrintMainWindow() {
// 	if(panelForPrint != NULL)
// 		::DestroyWindow(panelForPrint->MainhWnd);	
// 	delete panelForPrint;	
}


//!!! ��������� �� VImageViewerWidget. ������� �������� � �����-������ ��� ������� �������-�����-��������� QMainWindow
QAction* PrintMainWindow::createAction(QString name, QString iconName, const char* member, QActionGroup* actionGroup, QMenu* menu, QToolBar* toolBar) 
{
	QAction * rez = new QAction (QIcon(QString("%1").arg(iconName)), name, this);
	connect(rez, SIGNAL(triggered()), this, member);
	if (actionGroup) actionGroup->addAction(rez);
	if (menu) menu->addAction(rez);
	if (toolBar) toolBar->addAction(rez);
	return rez;
}

void PrintMainWindow::makeToolbars()
{
	QToolBar * tb1 = new QToolBar("��������������");		
	tb1->setObjectName("printFormat");	

	QActionGroup * pageOrientGroup = new QActionGroup(this);  
	createAction ("�������������� ������������ ����� ������", ":/resources/images/print/horisontal_page.png", SLOT(onPRINTHOR()), pageOrientGroup,NULL, tb1)->setCheckable(true);	
	createAction ("������������ ������������ ����� ������", ":/resources/images/print/vertical_page.png", SLOT(onPRINTVER()), pageOrientGroup,NULL, tb1)->setCheckable(true);		
	pageOrientGroup->actions()[1]->setChecked(true);	

	printFormatMenu = new PrintFormatMenu(this);
	connect(printFormatMenu->menu(), SIGNAL(triggered ( QAction * )), this, SLOT(changeFormat( QAction * )));
	printWidget->format = printFormatMenu->format();
	tb1->addWidget (printFormatMenu->menu());
	
	checkTypeMaterial();


	QToolBar * tb2 = new QToolBar("��������� �����");
	tb2->setObjectName("splitPrint");	

	pageLayoutGroup = new QActionGroup(this);	
	createAction ("��������� 1x1", ":/resources/images/print/1x1.png", SLOT(onCHANGEPOSITIONFRAME_1ON1()), pageLayoutGroup)->setCheckable(true);	
	createAction ("��������� 1x2", ":/resources/images/print/1x2.png", SLOT(onCHANGEPOSITIONFRAME_1ON2()), pageLayoutGroup)->setCheckable(true);	
	createAction ("��������� 2x1", ":/resources/images/print/2x1.png", SLOT(onCHANGEPOSITIONFRAME_2ON1()), pageLayoutGroup)->setCheckable(true);	
	createAction ("��������� 2x2", ":/resources/images/print/2x2.png", SLOT(onCHANGEPOSITIONFRAME_2ON2()), pageLayoutGroup)->setCheckable(true);	
	createAction ("��������� 3x2", ":/resources/images/print/3x3.png", SLOT(onCHANGEPOSITIONFRAME_3ON2()), pageLayoutGroup)->setCheckable(true);	
	//	createAction ("��������� 3x3", ":/resources/images/print/3x3pr.png", SLOT(onCHANGEPOSITIONFRAME_3ON3()), pageLayoutGroup)->setCheckable(true);	
	//	createAction ("��������� 4x3", "print_4on3.png", SLOT(onCHANGEPOSITIONFRAME_4ON3()), pageLayoutGroup)->setCheckable(true);

	

	///////////createAction ("������ �������", ":/resources/images/print/printlist.png", SLOT(onPRINT()), NULL,NULL,tb2);	
	

	pageLayoutGroup->actions()[0]->setChecked(true);

	tb2->addActions(pageLayoutGroup->actions());	
	//createAction ("���������", "CustomSplitPrint.png", SLOT(onCHANGEPOSITIONFRAMECUSTOM()), NULL,NULL,tb2);//->setCheckable(true);	


	QToolBar * tb3 = new QToolBar("��������");	
	tb3->setObjectName("listPrint");	
	createAction ("��������� ����", ":/resources/images/print/next_list.png",	SLOT(onNEXTPAGE()), NULL, NULL, tb3);	
	createAction ("���������� ����", ":/resources/images/print/prev_list.png",	SLOT(onPREVPAGE()), NULL, NULL, tb3);	
	
	
	QToolBar * tb4 = new QToolBar("��������������");			

	realSizeAction =  createAction ("�������� ������", "real.png", SLOT(doRealSize()), NULL, NULL, tb4);
	extTb = tb4;	
	tb4->setObjectName("realSizePrint");	
	
	// TOOLBAR 5 ///////////////////////////////////////////////////////////
	

	QToolBar * tb5 = new QToolBar("������ �������");			

	createAction ("������ �������", ":/resources/images/print/toPrint.png", SLOT(onPRINT()), NULL, NULL, tb5);	
	tb5->setObjectName("toPrintPrintTb");	

	QToolBar * tb6 = new QToolBar("�������� �����");			
	tb6->setObjectName("printClearTb");	
	createAction ("�������� �����", ":/resources/images/print/clearSheets.png", SLOT(onCLEARSHEETS()), NULL, NULL, tb6);	
	
	int sizeIconTB = getToolBarButtonIconSize();
	QSize sizeIcon(sizeIconTB, sizeIconTB);
	tb1->setIconSize(sizeIcon);
	tb2->setIconSize(sizeIcon);	
	tb3->setIconSize(sizeIcon);	
	tb4->setIconSize(sizeIcon);	
	tb5->setIconSize(QSize(sizeIconTB * 3 , sizeIconTB));	
	tb6->setIconSize(sizeIcon);	
	
	

	addToolBar (Qt::LeftToolBarArea, tb3);
	addToolBar (Qt::LeftToolBarArea, tb1);
	addToolBar (Qt::RightToolBarArea, tb2);	
	addToolBar (Qt::RightToolBarArea, tb4);	
	addToolBar (Qt::RightToolBarArea, tb5);	
	addToolBar (Qt::RightToolBarArea, tb6);	


}

void PrintMainWindow::doRealSize()
{
	printWidget->doRealSize();
}

void PrintMainWindow::onCLEARSHEETS()
{
	ModelControls * controls = new ModelControls( printModel, QModelIndex(), this);
	controls->eraseBranch();
}

void PrintMainWindow::changeFormat(QAction * action )
{	
	printWidget->format = printFormatMenu->format();
	checkTypeMaterial();
	resize(size().width() + 1, size().height());
	resize(size().width() - 1, size().height());
	repaint();
}

void PrintMainWindow::checkTypeMaterial()
{	
	printWidget->isPaper = printFormatMenu->isPaper();
	
}

void PrintMainWindow::onPRINTHOR()
{
	printWidget->disposition = QPrinter::Landscape;	
	resize(size().width() + 1, size().height());
	resize(size().width() - 1, size().height());
	repaint();

}

void PrintMainWindow::onPRINTVER(){
	printWidget->disposition = QPrinter::Portrait;
	resize(size().width() + 1, size().height());
	resize(size().width() - 1, size().height());
	repaint();
}

QString randFileName(){
	QTime curTime = QDateTime::currentDateTime().time();
	QDate curDate = QDateTime::currentDateTime().date();	
	return QString("print") + QString(curTime.hour())+ QString(".dpf");
}

void PrintMainWindow::onPRINT()
{

	if(printWidget->isPaper)
		paperPrinting();
	else
		filmPrinting();
}

void PrintMainWindow::paperPrinting()
{
	QPrinter printer;			
	printer.setOrientation((QPrinter::Orientation)printWidget->disposition);	
	printer.setResolution((QPrinter::Orientation)QPrinter::HighResolution);
	printer.setFullPage(false);
	QRectF target2(0, 0, printer.width(), printer.height());
	QPrintDialog printDialog(&printer, this);
	if(printDialog.exec() == QDialog::Accepted)
	{	
// 		int dpi = printer.resolution();			
// 		QRectF target(0, 0, printer.width(), printer.height());			
// 		float width = target.width() * 2;
// 		float height = target.height() * 2;

		printAllPages(&printer);

		
	}
}

void PrintMainWindow::printAllPages (QPaintDevice * device)
{
	parentWidget()->hide();

	while(firstElementNumber() + viewElementsSize() < allElementsSize())	
	{			
		renderPrintList(device);	
		onNEXTPAGE();
	}
	renderPrintList(device);	
}

void PrintMainWindow::renderPrintList (QPaintDevice * dest)
{
	QPainter p;
	p.begin(dest);
	QWidget * sourseWidget = printWidget->printPanel;
	double xscale = dest->width() / double(sourseWidget->width());
	double yscale = dest->height() / double(sourseWidget->height());
	double scale = qMin(xscale, yscale);
	p.translate(dest->width() / 2.0f, + dest->height() / 2.0f);
	p.scale(scale, scale);
	p.translate(-sourseWidget->width() / 2.0f, -sourseWidget->height() / 2.0f);	
	sourseWidget->render(&p);
	p.end();
}

void PrintMainWindow::filmPrinting()
{
	QSettings settings;
	settings.beginGroup("Print");

	int j = 1;		
	QString strItem = settings.value(tr("Name%1").arg(j), "").toString();	
	j++;
	QStringList items;
	while(strItem.compare ("")){
		items << strItem;
		strItem = settings.value(tr("Name%1").arg(j), "").toString();	
		j++;
	}

	QVector<QRgb> colorTable;
	for (int i = 0; i < 256; i++)
		colorTable.push_back(QColor(i, i, i).rgb());	
	
	QWidget * printWidget = this->printWidget->printPanel;
	QPainter p;

	int printerDPI = settings.value("dpi", QVariant(320)).toInt();

	settings.setValue("dpi", printerDPI);

	goFirstPage();

	QApplication::processEvents();

	QWidget w;	
	QGridLayout GL;
	GL.addWidget(new QLabel ("��������� ����������.\n���� �������� � ������."));
	w.setLayout(&GL);	
	w.setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	w.move(parentWidget()->pos());

	//
	w.show();
	QCoreApplication::processEvents();
	QPixmap pixmap(printFormatMenu->format().x() * printerDPI, printFormatMenu->format().y() * printerDPI);
	//
	QList <QImage> images;	

	while(firstElementNumber() + viewElementsSize() < allElementsSize())	
	{			
		renderPrintList(&pixmap);
		QImage image = pixmap.toImage();
		images << image;
		onNEXTPAGE();
	}
	renderPrintList(&pixmap);
	QImage image = pixmap.toImage();
	if(image.width() == 0)
		QMessageBox::information( NULL, "XRay", QString("�������� ���� ������ �� ������!\n"
		"�������� ������� �������� printerDPI � ����� ��������� (%1) \n").arg(printerDPI) );
	images << image;	

	for(int i = 0; i < images.size(); i++ )
	{
		int w = images[i].width();
		int h = images[i].height();
		//QLOG_INFO() <<"printing on film w: " << w <<  ", h: " << h ;			

		if(images[i].width() == 0 || images[i].height() == 0)
			return;		
		int index = printFormatMenu->menu()->count() + 1;

		QString printerPathStr = settings.value(tr("Path%1").arg(index), getVidarSettingsPath().absolutePath() + "\\dcmPrint").toString();				

		QDir printPath( printerPathStr );
		if( !printPath.exists() )
		{
			printPath.mkpath( printerPathStr );
			QMessageBox::information( NULL, "XRay", "������� ����� ��� ������� \n" 
				+ printerPathStr );
		}

		DIAGN_DpfFile dpffile;
		dpffile.Width     = images[i].width();
		dpffile.Height    = images[i].height();
		dpffile.Bpp       = 1;
		if (this->printWidget->disposition == QPrinter::Landscape) 
			dpffile.Landscape = 0x0001;
		else
			dpffile.Landscape = 0x0000;
		QString nameF(printerPathStr + QString("\\print_") + QDateTime::currentDateTime().toString("ddMMyyyy_hhmmsszzz") + tr("_%1").arg(qrand()) + QString(".dpf"));
		QFile qFile;
		qFile.setFileName(nameF); 		
		qFile.open(QIODevice::WriteOnly);		
		qFile.write("VDPF", 4);
		qFile.write((const char *)&dpffile, sizeof(DIAGN_DpfFile));
		int val = *images[i].bits();
		for(int j = 0; j < dpffile.Width * dpffile.Height; j ++)
		{
			int off = j * 4;
			int val = (*(images[i].bits() + off) + *(images[i].bits() + off + 1) + *(images[i].bits() + off + 2)) / 3.0f;
			//int val = *(images[i].bits() + j);
			qFile.write((const char *)&val, 1);		
		}			
		qFile.close();		
	}
	ModelControls controls ( printModel, QModelIndex(), this);
	controls.eraseBranch();

	w.close();
	parentWidget()->hide();

}


void PrintMainWindow::goFirstPage()
{
	while(firstElementNumber() > 0)	
	{	
		onPREVPAGE();
	}	
}

QList<QImage> PrintMainWindow::getScaledImage(float k)
{
	float width = printWidget->printPanel->width();
	float height = printWidget->printPanel->height();
	while(width * height < 1024 * 1024 * k){
		width *= 1.05f;
		height *= 1.05f;
	}

	while(width * height > 1024 * 1024 * k){
		width /= 1.05f;
		height /= 1.05f;
	}
/*
	while(width * height > 1024 * 1024 * 8 - 1){
		width /= 1.1f;
		height /= 1.1f;
	}*/
	QList<QImage> imageList;
	//while(image.width() == NULL && width > 1){
		imageList = getScaledImage(width, height);
		//width /= 1.2f;
		//height /= 1.2f;
	//}
	return imageList;		
}

QList<QImage> PrintMainWindow::getScaledImage(int width, int height)
{	
		return QList<QImage>();
}


//void PrintMainWindow::resizeEvent( QResizeEvent * event){	
//}

void PrintMainWindow::onSPLITMAINWINDOW(int x, int y) 
{
	return;
	//printWidget->printPanel->SetColsRows(x, y);
	
}

//
void PrintMainWindow::onCHANGEPOSITIONFRAMECUSTOM()
{
// 	TSetCustomSplit setSplit;
// 	setSplit.createGroup( 0, 1, 16, QString("������ ��������� �������� �������:") );
// 	setSplit.createGroup( 1, 1, 4, QString("������ ��������� ����� �� �������:"), true );
// 
// 	connect( &setSplit, SIGNAL(signalSplitApply(int,int,int)), SLOT(onSPLITGROUPACTIVATED(int,int,int)) );
// 	setSplit.exec();
}

//
void PrintMainWindow::onSPLITGROUPACTIVATED( int group, int x, int y )
{
	if( group == 0 )		onSPLITactiveWINDOWS( y, x );
	else if ( group == 1 )	onSPLITMAINWINDOW( y, x );
}


void PrintMainWindow::onNEXTPAGE()
{ 
	printModel->selectNextChildren(QModelIndex());
}
void PrintMainWindow::onPREVPAGE()
{
	printModel->selectPrevChildren(QModelIndex());
}

void PrintMainWindow::showEvent( QShowEvent *event )
{
	QMainWindow::showEvent(event);
}


void PrintMainWindow::addExternalActions( QList <QAction *> list )
{
	if(extTb)
		extTb->addActions(list);
}




int PrintMainWindow::allElementsSize()
{
	TreeDataModel * dataModel = printModel->getDataModel();
	return dataModel->rowCount(QModelIndex());	
}

int PrintMainWindow::firstElementNumber()
{
	TreeDataModel * dataModel = printModel->getDataModel();
	int rowCount = dataModel->rowCount(QModelIndex());	

	QModelIndex dataModelFirstElementIndex = printModel->getDataModelIndex(printModel->index(0, 0, QModelIndex()));
	int dataModelNumberInFirstElement = 0;

	for(int i = 0; i < rowCount; i++)
	{
		if(dataModel->index(i, 0, QModelIndex ()) == dataModelFirstElementIndex)
		{
			dataModelNumberInFirstElement = i;
			break;
		}		
	}
	return dataModelNumberInFirstElement;
}

int PrintMainWindow::viewElementsSize()
{
	TreeDataModel * dataModel = printModel->getDataModel();
	int rowCount = dataModel->rowCount(QModelIndex());	
	QVector2D div = printModel->getDivisionForItem(QModelIndex());		
	return div.x() * div.y();
}


