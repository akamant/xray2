#include "StdAfx.h"
#include "printwidget.h"

PrintWidget::PrintWidget( QWidget* Parent /*= NULL*/ )
	:QFrame(Parent)
	, printPanel(NULL) 
	, format(0, 0)
{
		//� printPanel ���������� �������������, ������ ��� ����� �� ������ ����������� � ��� ����
		disposition = QPrinter::Portrait;

		QPalette palette;
		palette.setColor( QPalette::Background, QColor( 50, 50, 50 ) );
		palette.setColor( QPalette::Text,		QColor( 255, 0, 255 ) );
		palette.setColor( QPalette::Foreground,  QColor( 255, 255, 0 ) );	
		setPalette( palette );
		setAutoFillBackground(true);
}

PrintWidget::~PrintWidget(){
}

void PrintWidget::showEvent( QShowEvent *event )
{
	QWidget::showEvent(event);
	setMouseTracking(true);
	setFocusPolicy(Qt::StrongFocus);	
	
}


void PrintWidget::deleteAll()
{
	
}

void PrintWidget::paintEvent(QPaintEvent * event){
	//if(printPanel != NULL)
	//	printPanel->isPaper = isPaper;

	float k = ratio();

	float addHeight = 0;//numPages.fontMetrics().height() + 2;
	float k1 = (float)width() / ((float)height() - addHeight );		
	
	float sizeWidth;
	float sizeHeight;
	if(k1 < k){
		sizeWidth = width();
		sizeHeight = (float) sizeWidth / k - addHeight;
	}else{
		sizeWidth = (float) height() * k;
		sizeHeight = height() - addHeight * 2.0f;
	}
	printPanel->setParent(this);	
	if(!printPanel->isVisible())
		printPanel->show();
	seitPrintWidgetFont();
	//printPanel->setDestHeight(destinationSize().height());
	printPanel->move( (width() - sizeWidth) / 2.0f, (height() - sizeHeight) / 2.0f + addHeight );
	printPanel->setFixedSize( sizeWidth, sizeHeight );
	//::MoveWindow(hwndContainerPanel1, (width() - sizeWidth) / 2.0f, (height() - sizeHeight) / 2.0f + addHeight, sizeWidth, sizeHeight, true);
}

void PrintWidget::doRealSize()
{	
	//printPanel->doRealSizeForPrint(destinationSize().height());
}

void PrintWidget::setPrintPanel(QWidget * _printPanel)
{
	printPanel = _printPanel;
	//printPanel->setParent(this);
}

float PrintWidget::ratio() 
{
	float ratio = 1189.0f / 841.0f;

	//return ratio;	

	if(!format.isNull())
	{
		ratio =	(max(	(float) format.x() / (float) format.y(), 
			(float) format.y() / (float) format.x())
			);
	}

	if(disposition == QPrinter::Landscape)
		return ratio;
	else
		return 1.0f / ratio;

	
}

void PrintWidget::seitPrintWidgetFont()
{
	QPalette palette;
	palette.setColor( QPalette::Background, QColor( 0, 0, 0 ) );
	palette.setColor( QPalette::Text, QColor( 255, 0, 255 ) );
	palette.setColor( QPalette::Foreground,  QColor( 255, 255, 0 ) );	
	printPanel->setPalette( palette );
	printPanel->setAutoFillBackground(true);
}
