#ifndef PRINTMAINWINDOW_H
#define PRINTMAINWINDOW_H

#include <QMainWindow>
#include "printwidget.h"
#include "TreeDataViewModel.h"

class PrintFormatMenu;


class PrintMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	PrintMainWindow(TreeDataViewModel * _printModel, QWidget *parent);
	~PrintMainWindow();

	int viewElementsSize();
	int allElementsSize();
	int firstElementNumber();


	QList<QImage> getScaledImage(int width, int height);
	QList<QImage> getScaledImage(float k);
	//PrintPanel * panelForPrint;
	PrintWidget * printWidget;
	void addExternalActions(QList <QAction *> list);
	protected slots:
	//void changeFormat();
	void doRealSize();
	void onCLEARSHEETS();
	void changeFormat(QAction * action );
	void onSPLITMAINWINDOW(int x, int y);
	void onNEXTPAGE();
	void onPREVPAGE();
	void onPRINT();
	void onPRINTHOR();
	void onPRINTVER();
	void onSPLITactiveWINDOWS(int x, int y){
		//ContainerPanel::SetColsRowsInSeriesPrint(x, y);
	};
	void onCHANGEPOSITIONFRAME_1ON1()
	{
		realSizeAction->setEnabled(true);			
		printModel->setDivisionForItem(QModelIndex(), QVector2D(1, 1));
	};		
	void onCHANGEPOSITIONFRAME_1ON2()
	{
		realSizeAction->setDisabled(true);
		printModel->setDivisionForItem(QModelIndex(), QVector2D(1, 2));
	};
	void onCHANGEPOSITIONFRAME_2ON1()
	{
		realSizeAction->setDisabled(true);
		printModel->setDivisionForItem(QModelIndex(), QVector2D(2, 1));
	};
	void onCHANGEPOSITIONFRAME_2ON2()
	{
		realSizeAction->setDisabled(true);
		printModel->setDivisionForItem(QModelIndex(), QVector2D(2, 2));
	};
	void onCHANGEPOSITIONFRAME_3ON2()
	{
		realSizeAction->setDisabled(true);
		printModel->setDivisionForItem(QModelIndex(), QVector2D(10, 12));
	};
	void onCHANGEPOSITIONFRAME_3ON3()
	{
		realSizeAction->setDisabled(true);
		printModel->setDivisionForItem(QModelIndex(), QVector2D(3, 3));
	};
	void onCHANGEPOSITIONFRAME_4ON3()
	{
		realSizeAction->setDisabled(true);
		printModel->setDivisionForItem(QModelIndex(), QVector2D(4, 3));
	};

	void onCONTRASTIMAGE()
	{
		//ContainerPanel::setWindowLevelMode();	
	};

	void onMOVEIMAGE()
	{	
		//ContainerPanel::setMoveMode();	
	};

	void onCHANGEPOSITIONFRAMECUSTOM();
	void onSPLITGROUPACTIVATED( int group, int x, int y );


protected:

	QToolBar * extTb;
	virtual void showEvent( QShowEvent *event );
private:
	TreeDataViewModel * printModel;
	QAction * contrastAction;
	QAction * moveAction;
	QLabel numPages;
	QActionGroup * pageLayoutGroup;
	QAction * createAction(QString name, QString iconName, const char* member, QActionGroup* actionGroup=NULL, QMenu* menu=NULL, QToolBar* toolBar=NULL);
	void makeToolbars();
	void checkTypeMaterial();	
	void filmPrinting();
	void paperPrinting();
	void renderPrintList (QPaintDevice * dest);
	void printAllPages (QPaintDevice * device);
	void goFirstPage();
	PrintFormatMenu * printFormatMenu;
	QAction * realSizeAction;

};

struct DIAGN_DpfFile 
{
public:
	int     Width;
	int     Height;
	short   Bpp;
	short   Landscape;
};

#endif // PRINTMAINWINDOW_H
