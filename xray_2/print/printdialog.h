#ifndef PRINTDIALOG_H
#define PRINTDIALOG_H

#include <QObject>
class PrintMainWindow;

class TreeDataViewModel;
class PrintDialog : public QDialog
{
	Q_OBJECT

public slots:
	void reStructSlot();
public:
	PrintDialog(TreeDataViewModel * printModel, QWidget *parent);
	~PrintDialog();
	void setPrintWidget (QWidget * widget);
	void saveSettings();		
	PrintMainWindow * printMainWindow; 
private:
	TreeDataViewModel * printModel;
};

#endif // PRINTDIALOG_H
