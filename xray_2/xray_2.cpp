#include "stdafx.h"
#include "xray_2.h"
#include "command.h"
#include "modelcontrols.h"
#include "TreeDataViewModel.h"
#include "branchFrame.h"
#include "parsingdispatcherthread.h"
#include "localLoader.h"
#include "Loader.h"
#include "elemCont.h"
#include "exprEditor.h"
#include "overlayEditor.h"
#include "dicomTagDialog.h"
#include "explorer/addFilmDialog.h"
#include "explorer/dicomDirItem.h"
#include "ModalityShemeElements.h"
#include "dicomImageViewWidget.h"
#include "HttpLoader.h"
#include "branchFrame.h"
#include "TreeDataViewModel.h"
#include "LoadIndicationWindow.h"
#include "DataViewModelDispatcher.h"
#include "printdialog.h"
#include "printtreedatamodel.h"
#include "printtreedataviewmodel.h"
#include "involsExport.h"
#include "modelsinformation.h"


xray_2::xray_2(QString _VrefFilePath, QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
	, addFilmDialog(NULL)
	, VrefFilePath (_VrefFilePath)
	, isFirstShow(true)
{
	setWindowTitle("Vidar-XRAY");	
	QPalette palette;
	palette.setColor( QPalette::Background, QColor( 0, 0, 0 ) );
	palette.setColor( QPalette::Window,		QColor( 0, 0, 0 ) );
	palette.setColor( QPalette::Base,		QColor( 0, 0, 0 ) );
	setPalette( palette );

	QDesktopWidget desktopWidget;

	qRegisterMetaType< DicomFileList >( "DicomFileList" );
	qRegisterMetaType< QModelIndex >( "QModelIndex" );		

	makeToolbars();	
	
	QDir::setCurrent(QCoreApplication::applicationDirPath());	

	//VIDAR_APP_SETTINGS;
	QSettings settings;
	restoreState(settings.value("mainWindowState").toByteArray());	
	if (!settings.value("mainWindowGeometry").isNull()) 
	{
		restoreGeometry(settings.value("mainWindowGeometry").toByteArray());
		this->show();
	} else {
		this->showMaximized();
	}
	
	loadStyles();

	//setPositionOnMonitors();
}

#define withPrint
void xray_2::initVewAndModels()
{	
		isFirstShow = false;
		QThread * thread = new QThread();
		thread->setPriority(QThread::LowestPriority);
		thread->start();

		

		TreeDataModel * model = new TreeDataModel();	

#ifdef withPrint
		printDataModel = new PrintTreeDataModel();
#endif

		treeDataViewModel = new TreeDataViewModel(model);	

		DataViewModelDispatcher * dispatcher = new DataViewModelDispatcher(treeDataViewModel);
		dispatcher->moveToThread(thread);	

		branchFrame = new BranchFrame( treeDataViewModel, QModelIndex(), this);	
		localLoader = new LocalLoader(this);
		httpLoader = new HttpLoader(this);
		ParsingDispatcherThread * dicomParserThread = new ParsingDispatcherThread(NULL);

		connect(	localLoader,						SIGNAL	(objectsRecieved				(DicomFileList)),
					dicomParserThread->dicomParser,		SLOT	(processStartedRecieveObjects	(DicomFileList))
			);

		connect(	httpLoader,							SIGNAL	(objectsRecieved				(DicomFileList)),
					dicomParserThread->dicomParser,		SLOT	(processStartedRecieveObjects	(DicomFileList))
			);

		connect(	dicomParserThread->dicomParser,		SIGNAL	(	newDicomDataPtr		(const DicomDataPtr & )	),
					model,								SLOT	(	processDicomDataPtr	(const DicomDataPtr & )	)
			);
#ifdef withPrint
		connect(	dicomParserThread->dicomParser,		SIGNAL	(	newDicomData		(DicomData * )	),
					printDataModel,						SLOT	(	processDicomData	(DicomData * )	)
			);
#endif
		connect(	dicomParserThread->dicomParser,		SIGNAL	(startLoading		()),
					this,								SLOT	(startLoadingSlot	())
			);

		connect(	dicomParserThread->dicomParser,		SIGNAL	(endLoading			()),
					this,								SLOT	(endLoadingSlot		())
			);

		setCentralWidget(branchFrame);

#ifdef withPrint		
		treeDataViewModel;
		PrintTreeDataViewModel * printDataViewModel = new PrintTreeDataViewModel(printDataModel);	
		DataViewModelDispatcher * printDispatcher = new DataViewModelDispatcher(printDataViewModel);
		printDispatcher->moveToThread(thread);		

		BranchFrame * printBranchFrame = new BranchFrame( printDataViewModel, QModelIndex());	

		QPalette palette;
		palette.setColor( QPalette::Background, QColor( 0, 255, 0 ) );
		palette.setColor( QPalette::Text, QColor( 255, 0, 255 ) );
		palette.setColor( QPalette::Foreground,  QColor( 255, 255, 0 ) );	
		printBranchFrame->setPalette( palette );
		printBranchFrame->setAutoFillBackground(true);

		printBranchFrame->setFrameStyle(QFrame::Plain | QFrame::Box);	

		printDialog = new PrintDialog(printDataViewModel, this);
		printDialog->setPrintWidget(printBranchFrame);

#endif
}

void xray_2::startLoadingSlot(){
	//setWindowTitle("xray_2 loading files...");
}


void xray_2::endLoadingSlot(){
	//setWindowTitle("xray_2");
}

int __stdcall BrowseCallbackProc(HWND hDlg, UINT uMsg, LPARAM lParam, LPARAM lpData) 
{ 
	if( uMsg == BFFM_INITIALIZED ) 
	{ 
		QWidget *widget = reinterpret_cast<QWidget*>(lpData); 
		//QSize sz = widget->parentWidget()-> size();
		QSize sz = widget->size();
		RECT r = {0}; 
		::GetWindowRect(hDlg, &r); 
		int x = (sz.width() - (r.right-r.left)) /2;
		int y = (sz.height() - (r.bottom-r.top)) /2;

		//!!! ���� �� ���������, ����������
		::SetWindowPos( 
			hDlg, 
			NULL, 
			100,
			100, 
			0, 
			0, 
			SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER); 
	} 
	return 0; 
} 

void xray_2::onDICOMDIRECTORY()
{
	QSettings settings;
	QString initPath = settings.value( "lastLoadPath"
		, QDesktopServices::storageLocation(QDesktopServices::DesktopLocation)
		).toString();

	QString selected = 
		QFileDialog::getExistingDirectory( this
		, "�������� ������� � ������� �����������"
		, initPath
		, QFileDialog::ShowDirsOnly
		| QFileDialog::DontResolveSymlinks
		| QFileDialog::HideNameFilterDetails
		| QFileDialog::ReadOnly );
	if( !selected.isEmpty() )
	{
		//QLOG_INFO() << QString("Load files from %1").arg(selected);
		VLogger() << QString("Load files from %1").arg(selected);
		QStringList files;
		//		files << "c:\\users\\anton\\desktop\\se2_small\\im1";
		files << selected;
		// �������� �� �������� � �������� ���� ������ ������
		
		onADDFILES(files);

		QDir dir(selected);
		dir.cdUp();
		settings.setValue( "lastLoadPath", dir.absolutePath() );		
	}	 
}

void xray_2::onDOWNLOADFILES(QStringList files)
{
// 	QUrl url = getUrlFromVrefFile(files[0]);
// 	TDownloadFilesDialog downloadFilesDialog(url, this);	
// 	downloadFilesDialog.show();
// 	onAUTOFILLSTUDIES();
	return;
}

void xray_2::onADDFILES(QStringList files)
{
	for(int i =0; i < files.size(); i ++)
		localLoader->load(QStringList(files[i]));	
}

void xray_2::hideEvent(QHideEvent * event)
{
	if(addFilmDialog)
	{
		if(!addFilmDialog->isHidden())
			addFilmDialog->hide();
		delete addFilmDialog;
		addFilmDialog = NULL;
	}
}

void xray_2::onCREATETREE(QString dirName)
{

}

xray_2::~xray_2()
{	
	//printDialog->close();
}

void xray_2::makeToolbars()
{
	QSize iconSize(48,48);
	QMenuBar * mainMenu = new QMenuBar;
	QAction * curAct;	
	QMenu * menu1 = new QMenu("����");	
	QToolBar * tb1 = new QToolBar ("������");

	menu1->addAction("�������...", this, SLOT(onDICOMDIRECTORY()), Qt::Key_F2 );    	

	curAct = new QAction (QIcon(":/resources/images/print/print.png"), "������", this);
	connect( curAct, SIGNAL(triggered()), SLOT(onPRINT()) );
	curAct->setShortcut(Qt::Key_P);
	menu1->addAction(curAct);
	tb1->addAction(curAct);

	menu1->addSeparator();
	menu1->addAction( "�������� ���������",	this, SLOT(onExpressionEditor()),  Qt::SHIFT + Qt::Key_F5);
	menu1->addAction( "�������� �������",	this, SLOT(onOverlayEditor()),	   Qt::SHIFT + Qt::Key_F6);
	menu1->addAction( "������ �����",	this, SLOT(onDicomTagDialog()));
	menu1->addSeparator();
	menu1->addAction ("��������� ��������� ����", this, SLOT(onSaveWindowState()));

	if( TInvolsExport::isInvolsAvailable()
		//&& (isModalitySupported("CT") || isModalitySupported("MR")) 
		)
	{
			if( !tb1 ){
				tb1 = new QToolBar("��������");		
				tb1->setObjectName("mainToolbar");
			}

			tb1->addSeparator();
			menu1->addSeparator();
			curAct = new QAction (QIcon(":/resources/images/3D/mpr_3d.png"), "MPR + 3D", this);
			connect( curAct, SIGNAL(triggered()), SLOT(onEXPORTTOINVOLS()) );
			curAct->setShortcut(Qt::Key_R);
			menu1->addAction(curAct);
			tb1->addAction(curAct);
	}

	QToolBar * tb2 =  new QToolBar("�������� ��������� ������");
	tb2->setObjectName("tb2");		
	menuAction = new QToolButton();	
	tb2->addWidget(menuAction);
	splitMenu = new QMenu();
	splitMenu->setIcon( QIcon(":/resources/images/gray/resetALL.png") );

	QAction * act1 = new QAction(QIcon(":/resources/images/layout/1x1Study.png"), "1x1", this);	
	connect( act1, SIGNAL(triggered()), SLOT(onCHANGEPOSITIONMAIN1()) );
	QAction * act2 = new QAction(QIcon(":/resources/images/layout/1x2Study.png"), "1x2", this);
	connect( act2, SIGNAL(triggered()), SLOT(onCHANGEPOSITIONMAIN2()) );
	QAction * act3 = new QAction(QIcon(":/resources/images/layout/2x1Study.png"), "2x1", this);
	connect( act3, SIGNAL(triggered()), SLOT(onCHANGEPOSITIONMAIN3()) );
	QAction * act4 = new QAction(QIcon(":/resources/images/layout/2x2Study.png"), "2x2", this);
	connect( act4, SIGNAL(triggered()), SLOT(onCHANGEPOSITIONMAIN4()) );		
	menuAction->setIcon(act1->icon());
	splitMenu->addAction(act1);
	splitMenu->addAction(act2);
	splitMenu->addAction(act3);
	splitMenu->addAction(act4);
	connect(	splitMenu,	SIGNAL	( triggered			( QAction * )	),
				this,		SLOT	( changeSplitMenu	( QAction * )	)
		);
			
	menuAction->setPopupMode(QToolButton::InstantPopup);
	menuAction->setMenu(splitMenu);


	//TOOLBAR 3 ///////////////////////////////////////////////////////////

	QToolBar * tb3 =  new QToolBar("�������� �������");
	tb3->setObjectName("tb3");		

	QMenu * menu3 = new QMenu("��������");		
	curAct = new QAction (QIcon(":/resources/images/navigate/next_image.png"), "��������� ������", this);
	curAct->setShortcut(Qt::Key_PageDown);
	connect(curAct, SIGNAL(triggered()), SLOT(onGONEXTIMAGE()) );	
	tb3->addAction(curAct);

	curAct = new QAction (QIcon(":/resources/images/navigate/prev_image.png"), "���������� ������", this);
	curAct->setShortcut(Qt::Key_PageUp);
	tb3->addAction(curAct);
	connect(curAct, SIGNAL(triggered()), SLOT(onGOPREVIMAGE()) );	
	
	
	// TOOLBAR 4 ///////////////////////////////////////////////////////////
	

	///xray_2/resources/images/gray/splitStudy1x2.png
	 	QToolBar * tb4 = new QToolBar("��������� �����������");
	 	tb4->setObjectName("tb4");	
	 	QActionGroup * actGroup4 = new QActionGroup(this);	
		//actGroup4->setUsesDropDown(true);
	 	QAction * resetAct = new QAction( QIcon(":/resources/images/equalizer/reset.png"), "������������", this);
	 	resetAct->setShortcut(Qt::Key_F12);
	 	tb4->addAction( resetAct );
	 	connect( resetAct, SIGNAL(triggered()), SLOT(onRESETALL()) );	
	 	QAction * contrastAct = new QAction( QIcon(":/resources/images/equalizer/brightness_contrast.png"), "�������/�������������", actGroup4);
	 	contrastAct->setShortcut(Qt::Key_W);
	 	connect(contrastAct, SIGNAL(triggered()), SLOT(onCONTRASTIMAGE()));
	 	contrastAct->setCheckable(true);
	 	tb4->addAction(contrastAct);
	 	QAction * LWAct = new QAction( QIcon(":/resources/images/equalizer/WL.png"), "����/�����", this);
	 	LWAct->setShortcut(Qt::Key_F4);
	 	tb4->addAction( LWAct );
	 	connect( LWAct, SIGNAL(triggered()), SLOT(onSetWL()) );
	 	QAction * posNegAct = new QAction(QIcon(":/resources/images/equalizer/positive_negative.png"), "������� - �������",	  this);
	 	tb4->addAction(posNegAct);
	 	connect( posNegAct, SIGNAL(triggered()), SLOT(onREFLECT()));
	 	QAction * actMoveImage = new QAction( QIcon(":/resources/images/displace/move.png"), "�����/�������", actGroup4);
	 	actMoveImage->setShortcut(Qt::Key_M);
	 	connect( actMoveImage, SIGNAL(triggered()), SLOT(onMOVEIMAGE()));
	 	actMoveImage->setCheckable(true);
	 	actMoveImage->setChecked(true);		
	 	QAction * hideOverlayAct = new QAction( QIcon(":/resources/images/equalizer/overlay.png"), "������ �������", this);
	 	hideOverlayAct->setShortcut(Qt::Key_F10);
	 	connect( hideOverlayAct, SIGNAL(triggered()), SLOT(onHIDEOVERLAY()) );
	 	hideOverlayAct->setCheckable(true);
	 	tb4->addAction( hideOverlayAct );
	 	connect( actGroup4, SIGNAL(triggered(QAction*)), SLOT(checkTB4Group(QAction*)) );	

		QMenu * menu4 = new QMenu("���������");
	 	menu4 = new QMenu("���������");
	 	menu4->addAction(contrastAct);
	 	menu4->addAction(LWAct);
	 	menu4->addAction(posNegAct);
	 	menu4->addAction(actMoveImage);	
	// 	// TOOLBAR 5 ///////////////////////////////////////////////////////////
	 	QToolBar * tb5 = new QToolBar("����� ����������� � ��������");
	 	tb5->setObjectName("tb5");	
	 	tb5->addAction(actMoveImage);
	 	

	 	tb5->addAction  (QIcon(":/resources/images/displace/turnVertical.png"), "��������� ���� - ���",  this, SLOT(onTURNVERT()) );
	 	menu4->addAction(QIcon(":/resources/images/displace/turnVertical.png"), "��������� ���� - ���", this, SLOT(onTURNVERT()));
	 	tb5->addAction  (QIcon(":/resources/images/displace/turnHorizontal.png"), "��������� ����� - ����",	  this, SLOT(onTURNHOR()) );
	 	menu4->addAction(QIcon(":/resources/images/displace/turnHorizontal.png"), "��������� ����� - ����", this, SLOT(onTURNHOR()));
	 	tb5->addAction  (QIcon(":/resources/images/displace/rotateRight.png"), "������� 90", this, SLOT(onROTATERIGHT()) );
	 	menu4->addAction(QIcon(":/resources/images/displace/rotateRight.png"), "������� 90", this, SLOT(onROTATERIGHT()));
	 	//tb5->addAction  (QIcon(":/resources/images/gray/rotateRight.png"), "������� -90",  this, SLOT(onROTATELEFT()));
	 	tb5->addAction  (QIcon(":/resources/images/displace/rotateLeft.png"), "������� -90",  this, SLOT(onROTATELEFT()));
	 	menu4->addAction(QIcon(":/resources/images/displace/rotateLeft.png"), "������� -90 ", this, SLOT(onROTATELEFT()));	 
	 	menu4->addAction(resetAct);		
	 	QMenu * menu41 = new QMenu("������");
	
	// 	// TOOLBAR 6 ///////////////////////////////////////////////////////////
	// 
		QMenu * menu5 = new QMenu("��������� � �������");
		QToolBar * tb6 = new QToolBar("��������� � �������");
		tb6->setObjectName("tb6");
		QActionGroup * actGroup6 = new QActionGroup(this);	
		curAct = new QAction( QIcon(":/resources/images/gray/distance.png"), "����������", actGroup6 );
		connect( curAct, SIGNAL(triggered()), SLOT(onDISTANCE()) );
		curAct->setCheckable(true);	

		//actGroup6 = new QActionGroup(this);		
		curAct = new QAction( QIcon(":/resources/images/measure/angle.png"), "����", actGroup6 );
		connect( curAct, SIGNAL(triggered()), SLOT(onANGLE()) );
		curAct->setCheckable(true);
		curAct = new QAction( QIcon(":/resources/images/measure/polygon.png"), "�������", actGroup6 );
		connect( curAct, SIGNAL(triggered()), SLOT(onCURVE()) );
		curAct->setCheckable(true);
		curAct = new QAction( QIcon(":/resources/images/measure/circle.png"), "����", actGroup6 );
		connect( curAct, SIGNAL(triggered()), SLOT(onSPOT()) );
		curAct->setCheckable(true);	
		curAct = new QAction( QIcon(":/resources/images/measure/label.png"), "�����", actGroup6 );
		connect( curAct, SIGNAL(triggered()), SLOT(onTEXT()) );
		curAct->setCheckable(true);
	// 
	// 	curAct = new QAction( QIcon(":/" + dir + "setScale.png"), "������ �������", actGroup6 );
	// 	connect( curAct, SIGNAL(triggered()), SLOT(onSETSCALE()) );
	// 	curAct->setCheckable(true);	
	// 
	   	tb6->addActions( actGroup6->actions() );
	 	menu5->addActions( actGroup6->actions());
	   	QAction * DeleMarkAct = new QAction( QIcon(":/resources/images/measure/delete_measure.png"), "������� �������", actGroup6 );
	 	connect( DeleMarkAct, SIGNAL(triggered()), SLOT(onDELETEMARKS()) );
	   	DeleMarkAct->setCheckable(true);
	 	tb6->addAction( DeleMarkAct );	
	   	 QAction * onHideMarkAct = new QAction( QIcon(":/resources/images/measure/hide_measures.png"), "��������/�������� �������", this );
	 	connect( onHideMarkAct, SIGNAL(triggered()), SLOT(onHIDEMARKS()) );
	   	connect( onHideMarkAct, SIGNAL(triggered()), SLOT(checkTB6HideMark()) );
	 	onHideMarkAct->setCheckable(true);
	   	tb6->addAction( onHideMarkAct );	
	 	connect( actGroup6, SIGNAL(triggered(QAction*)), SLOT(checkTB6Group(QAction*)));	
	   	QMenu * menu51 = new QMenu("�����");
	 	menu5->addMenu(menu51);
	   	menu51->addAction(DeleMarkAct);
	 	menu51->addAction(onHideMarkAct);	
	 	QMenu * menu6 = new QMenu("������");
	 	menu6->addAction("������", this, SLOT(onHELP()), Qt::Key_F1);
	 	menu6->addAction("� ���������", this, SLOT(onABOUT()));


	tb1->setIconSize(iconSize);
	tb2->setIconSize(iconSize);
	tb3->setIconSize(iconSize);
	tb4->setIconSize(iconSize);
	tb5->setIconSize(iconSize);
	tb6->setIconSize(iconSize);	
	addToolBar(Qt::RightToolBarArea,	tb1);
	addToolBar(Qt::LeftToolBarArea,		tb2);
	addToolBar(Qt::LeftToolBarArea,		tb3);
	addToolBar(Qt::RightToolBarArea,	tb4);
	addToolBar(Qt::RightToolBarArea,	tb5);
	addToolBar(Qt::RightToolBarArea,	tb6);	

	mainMenu->addMenu(menu1);
 	mainMenu->addMenu(menu4);
	mainMenu->addMenu(menu5);
	mainMenu->addMenu(menu6);

	setMenuBar( mainMenu );	
}


void xray_2::loadStyles()
{
	QString styleStr, sourceStr;
	QSettings settings;
	if (settings.value("style").isNull())
	{
		QFile workFile(":/resources/style.css");
		if(workFile.open(QIODevice::ReadOnly))
		{
			sourceStr = QString::fromUtf8(workFile.readAll().constData());
			workFile.close();
		}		

		foreach( const QString &str, sourceStr.split(QRegExp("(\\r\\n)|(\\n\\r)|\\r|\\n"),QString::SkipEmptyParts) )
			styleStr += str;

		settings.setValue( "style", styleStr );
	}
	else
		styleStr = settings.value( "style" ).toStringList().join(" ");

	QString curStyle = ((QApplication*)parent())->styleSheet();

	((QApplication*)parent())->setStyleSheet( styleStr );
	//((QApplication*)parent())->setStyleSheet( ((QApplication*)parent())->styleSheet() + styleStr );
	//setStyleSheet("QPushButton {color: white}");
}


void xray_2::onCHANGEPOSITIONMAIN1()
{
	ModelControls modelControls (treeDataViewModel, QModelIndex(), this);
	modelControls.changeDataModelDivision(QVector2D(1, 1));

}

void xray_2::onCHANGEPOSITIONMAIN2()
{
	ModelControls modelControls (treeDataViewModel, QModelIndex(), this);
	modelControls.changeDataModelDivision(QVector2D(1, 2));

}

void  xray_2::onCHANGEPOSITIONMAIN3()
{
	ModelControls modelControls (treeDataViewModel, QModelIndex(), this);
	modelControls.changeDataModelDivision(QVector2D(2, 1));
}

void  xray_2::onCHANGEPOSITIONMAIN4()
{
	ModelControls modelControls (treeDataViewModel, QModelIndex(), this);
	modelControls.changeDataModelDivision(QVector2D(2, 2));
}

void xray_2::changeSplitMenu(QAction * act)
{
	if(menuAction)
		menuAction->setIcon(act->icon());
}

void xray_2::onRESETALL()
{
	Command command(Command::reset);	
	TreeDataViewModel * model = treeDataViewModel;
	if(model)
		model->emitCommand(command);	
}


void  xray_2::onCONTRASTIMAGE()
{
	Command::setCommandState(Command::contrastState);	
}

void  xray_2::onANGLE()
{
	Command::setCommandState(Command::AnglePrimitiveState);	
}

void  xray_2::onCURVE()
{
	Command::setCommandState(Command::PoligonPrimitiveState);
}

void  xray_2::onSPOT()
{
	Command::setCommandState(Command::CirclePrimitiveState);	
}

void  xray_2::onTEXT(){
	Command::setCommandState(Command::LabelPrimitiveState);	
}

void  xray_2::onDISTANCE()
{
	Command::setCommandState(Command::DistancePrimitiveState);	
}

void xray_2::onDELETEMARKS()
{
	Command::setCommandState(Command::DeletePrimitiveState);	
}

void xray_2::onHIDEMARKS()
{
	DicomImageViewWidget::switchMarkShowing();
	TreeDataViewModel * model = treeDataViewModel;
	if(model)
		model->switchOverlay();
}

void  xray_2::onSetWL()
{
	TSetWL setWL;
		setWL.setModal(true);
		//setWL.move( QPoint(QCursor::pos().x(), QCursor::pos().y() - setWL.height() ));
		//setWL.move( QPoint(QCursor::pos().x(), QCursor::pos().y() - 100));
		//setWL.show();
		if( QDialog::Accepted == setWL.exec() )	
		{
			Command command(Command::contrast);
			command.vector2DPar = QVector2D(setWL.getY(), setWL.getX());
			TreeDataViewModel * model = treeDataViewModel;
			if(model)
				model->emitCommand(command);					
		}
}

void  xray_2::onREFLECT()
{
	Command command (Command::negative);
	if(treeDataViewModel)
		treeDataViewModel->emitCommand(command);
}

void  xray_2::onMOVEIMAGE()
{
	Command::setCommandState(Command::translateState);	
}

void  xray_2::onHIDEOVERLAY()
{	
	DicomImageViewWidget::switchOverlay();
	TreeDataViewModel * model = treeDataViewModel;
	if(model)
		model->switchOverlay();
}

void  xray_2::onTURNVERT()
{
	Command command (Command::turn);
	command.vector2DPar = QVector2D(1, -1);
	TreeDataViewModel * model = treeDataViewModel;
	if(model)
		model->emitCommand(command);
	
}

void  xray_2::onTURNHOR()
{	
	Command command (Command::turn);
	command.vector2DPar = QVector2D(-1, 1);
	TreeDataViewModel * model = treeDataViewModel;
	if(model)
		model->emitCommand(command);
}

void  xray_2::onROTATERIGHT()
{
	Command command (Command::rotate);
	command.intPar = 90;
	TreeDataViewModel * model = treeDataViewModel;
	if(model)
		model->emitCommand(command);
}	 

void  xray_2::onROTATELEFT()
{
	Command command (Command::rotate);
	command.intPar = -90;
	TreeDataViewModel * model = treeDataViewModel;
	if(model)
		model->emitCommand(command);
}

void xray_2::setPositionOnMonitors()
{
	QDesktopWidget desktopWidget;
	int numberOfMonitors = desktopWidget.numScreens();	
	primaryScreen = desktopWidget.primaryScreen();	
	secondScreen = primaryScreen;
	QRect rectPrimary = desktopWidget.screenGeometry(primaryScreen);
	QRect rect;
	QRect rectSecond;
	QRect rectThird;
	int isAutoDisplay = 1;

	if(desktopWidget.isVirtualDesktop() && isAutoDisplay == 1)
	{				
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() - 50, rectPrimary.top() - 50);
				secondScreen = desktopWidget.screenNumber(point);			
			}
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() + 50, rectPrimary.top() - 50);
				secondScreen = desktopWidget.screenNumber(point);			
			}		
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() + rectPrimary.width() + 50, rectPrimary.top() - 50);
				secondScreen = desktopWidget.screenNumber(point);				
			}
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() + rectPrimary.width() + 50, rectPrimary.top() + 50);
				secondScreen = desktopWidget.screenNumber(point);				
			}
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() + rectPrimary.width() + 50, rectPrimary.top() + rectPrimary.height() + 50);
				secondScreen = desktopWidget.screenNumber(point);			
			}
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() + 50, rectPrimary.top() + rectPrimary.height() + 50);
				secondScreen = desktopWidget.screenNumber(point);
			}
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() - 50, rectPrimary.top() + rectPrimary.height() + 50);
				secondScreen = desktopWidget.screenNumber(point);	
			}
			if(primaryScreen == secondScreen || secondScreen == -1){
				QPoint point(rectPrimary.left() - 50, rectPrimary.top() + 50);
				secondScreen = desktopWidget.screenNumber(point);	
			}			
	}
	thirdScreen = secondScreen;
	rectSecond = desktopWidget.screenGeometry(secondScreen);
	if(desktopWidget.isVirtualDesktop() && isAutoDisplay == 1 && secondScreen != primaryScreen){
		//if(Panel::LOADFROMXRAYFOLDER){				
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() - 50, rectSecond.top() - 50);
			thirdScreen = desktopWidget.screenNumber(point);
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() + 50, rectSecond.top() - 50);
			thirdScreen = desktopWidget.screenNumber(point);	
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}		
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() + rectSecond.width() + 50, rectSecond.top() - 50);
			thirdScreen = desktopWidget.screenNumber(point);	
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() + rectSecond.width() + 50, rectSecond.top() + 50);
			thirdScreen = desktopWidget.screenNumber(point);	
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() + rectSecond.width() + 50, rectSecond.top() + rectSecond.height() + 50);
			thirdScreen = desktopWidget.screenNumber(point);	
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() + 50, rectSecond.top() + rectSecond.height() + 50);
			thirdScreen = desktopWidget.screenNumber(point);
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() - 50, rectSecond.top() + rectSecond.height() + 50);
			thirdScreen = desktopWidget.screenNumber(point);	
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}
		if(thirdScreen == secondScreen || thirdScreen == -1){
			QPoint point(rectSecond.left() - 50, rectSecond.top() + 50);
			thirdScreen = desktopWidget.screenNumber(point);	
			if(thirdScreen == primaryScreen)
				thirdScreen = secondScreen;
		}			
	}
	
	rectSecond = desktopWidget.screenGeometry(secondScreen);
	rectThird = desktopWidget.screenGeometry(thirdScreen);			
	move(rectSecond.left(), rectSecond.top());	
	resize(rectSecond.width() - 7, rectSecond.height() - 50);
	showMaximized();		


}


void xray_2::onExpressionEditor()
{
	TElemCont *elemCont = NULL;
	//ImagePanel *imgPanel = (ImagePanel*)Panel::clickedImagePanel;
// 	if( imgPanel && imgPanel->dicomStruct )
// 		elemCont = &imgPanel->dicomStruct->getELEM_CONT();

	TElemContList list( elemCont );
	TExprEditor exprEditor( TExpressionDict::getDict()
						  , list
//						  , TElemContList(&getELEM_CONT())
						  , TDicomDict::getDict() );
	QDesktopWidget desktopWidget;
	//exprEditor.setGeometry(desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).x() + 50, desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).y() + 50, desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).width() * 0.9 - 50 , desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).height() * 0.9 - 50);
	exprEditor.exec();
}



void xray_2::onOverlayEditor()
{
	TElemCont *elemCont = NULL;
// 	ImagePanel *imgPanel = (ImagePanel*)Panel::clickedImagePanel;
// 	if( imgPanel && imgPanel->dicomStruct )
// 		elemCont = &imgPanel->dicomStruct->getELEM_CONT();

	QString mod;
// 	if( list.hasElement(0x00080060) )
// 		mod = list.get(0x00080060).toString();

	TElemContList list( elemCont );
	TOverlayEditor overlayEditor( TExpressionDict::getDict()
		, TOverlayDict::getDict()
		//								, TElemContList(&getELEM_CONT()) );
		, list, mod );
	QDesktopWidget desktopWidget;
//	overlayEditor.setGeometry(desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).x() + 50, desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).y() + 50, desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).width() * 0.9 - 50 , desktopWidget.screenGeometry(desktopWidget.screenNumber(mainWindowParent->pos())).height() * 0.9 - 50);
	overlayEditor.exec();
}


void xray_2::onHELP(){
	QString dir = QApplication::applicationDirPath();
	QString hlp = dir + "\\rukp_z.chm";
	::ShellExecute( NULL, "open", hlp.toAscii().constData(), NULL, dir.toAscii().constData(), SW_SHOWNORMAL );
}


void xray_2::onABOUT()
{
	AboutDialog  aboutDialog;		
	QPixmap pixmap(":/resources/images/zastviewer.png");	
	aboutDialog.setMinimumWidth(pixmap.width());
	aboutDialog.setMinimumHeight(pixmap.height());
	aboutDialog.setMaximumWidth(pixmap.width());
	aboutDialog.setMaximumHeight(pixmap.height());
	aboutDialog.exec();	 
}


AboutDialog::AboutDialog()
	: QDialog()
{
}

void AboutDialog::mousePressEvent(QMouseEvent * event){ 
	hide();
}

void AboutDialog::paintEvent(QPaintEvent * event){	
	QWidget::paintEvent(event);
	QPainter painter;
	painter.begin(this);		
	QPixmap pixmap(":/resources/images/zastviewer.png");
	painter.drawPixmap( 0, 0, pixmap);	
	painter.end();	
}

void xray_2::wheelEvent(QWheelEvent * event){
	//return QWidget::eventFilter(obj, e);
	int numDegrees = event->delta() / 8;
	int numSteps = numDegrees / 15;
	TreeDataViewModel * model = treeDataViewModel;
	if(model){
		ModelControls * controls = new ModelControls( model, QModelIndex(), this);	
		if(numDegrees < 0){
			controls->selectNextChildren();		
		}else{
			controls->selectPrevChildren();		
		}			
	}
}

void xray_2::keyPressEvent(QKeyEvent * event){
	switch(event->key())
	{
		case Qt::Key_PageDown:
		{	
			//TreeDataViewModel * model = treeDataViewModel;
			//if(model)
			//	model->selectNextPrevToLowLevel(TreeDataViewModel::Forward);
			break;
		}
		case Qt::Key_PageUp:
		{
			//TreeDataViewModel * model = treeDataViewModel;
			//if(model)
			//	model->selectNextPrevToLowLevel(TreeDataViewModel::Back);				
			break;
		}
	}
}


PrintTreeDataModel * xray_2::printDataModel;

void xray_2::showEvent( QShowEvent * event )
{
	QMainWindow::showEvent(event);
	if(isFirstShow)
	{
		initVewAndModels();

		if(VrefFilePath != "")
			httpLoader->loadFromVrefFilePath(VrefFilePath);

		Loader loader;
		QStringList fileNames = loader.getListFiles(QString("images.txt")); 

		for(int i =0; i < fileNames.size(); i ++)
			localLoader->load(QStringList(fileNames[i]));

		#ifdef TEST_FILE_LOAD
				QStringList list;
				list << "C:\\dicom files\\����������\\CT_BIG-2k_MESHANIHIN_IGOR__VIKTOROVICH_2013-10-02";
				//list << "C:\\dicom files\\dvd.img\\1.2.840.113619.2.22.288.4576.9567.20090617.194210";
				
				localLoader->load(list);
		#endif
	}
}

bool xray_2::winEvent(MSG *message, long *result)
{	
	if( message->message == WM_COPYDATA ) 
		processWM_COPYDATA(message, result);
	return false;
}

void xray_2::processWM_COPYDATA(MSG *message, long *result) 
{
	COPYDATASTRUCT* pCD = (COPYDATASTRUCT*) message->lParam;
	DIAGN_VidarMSG data;		
	memcpy(&data, pCD->lpData, pCD->cbData);
	// ������� ������ �� 1 ���� (������� ������) � ������ ����������� 0
	int size = (unsigned char)data.Str1[0];
	memmove (data.Str1, data.Str1 + 1, size);
	data.Str1[size] = 0;
	size = (unsigned char)data.Str2[0];		
	memmove (data.Str2, data.Str2 + 1, size);
	data.Str2[size] = 0;

	//logMessage(data);


	switch(data.Cmd) 
	{
	case 50:
		{			
			close();
			break;
		}
	case 5000: 
		{			
			break;
		}		
	case 5001: 
		{
		break;
		}
	case 5002: 
		{			
		break;
		}
	case 5003: 
		{ 		
		break;
		}		
	case 5004:
		{		
		//showAlways();
		QString fileName = data.Str1;
		//QString tag = getUrlFromVrefFile(fileName).toString();	
		httpLoader->loadFromVrefFilePath(fileName);
		break;			  
		}
	}
}

void xray_2::onSaveWindowState()
{
	printDialog->saveSettings();
	QSettings settings;
	QMainWindow * mw = this;
	settings.setValue("mainWindowGeometry", mw->saveGeometry());
	settings.setValue("mainWindowState", mw->saveState());
}


void xray_2::onGONEXTIMAGE()
{
	ModelControls modelControls (treeDataViewModel, QModelIndex(), this);
	modelControls.selectNextChildren();
}

void xray_2::onGOPREVIMAGE()
{
	ModelControls modelControls (treeDataViewModel, QModelIndex(), this);
	modelControls.selectPrevChildren();
}


void xray_2::onPRINT()
{
	QDesktopWidget desktopWidget;
	TreeDataModel * dataModel = treeDataViewModel->getDataModel();
	
	if(treeDataViewModel->selectedItem() != dataModel->emptyDicomDataIndex())
	{
		QModelIndex index = treeDataViewModel->getDataModelIndex(treeDataViewModel->selectedItem());		
		printDataModel->addDicomDataPtr(dataModel->getDicomDataPtrByIndex(index), treeDataViewModel->getViewParameters(treeDataViewModel->selectedItem()));
	}

	printDialog->show();
	printDialog->setFocus();
	return;
}

void xray_2::onEXPORTTOINVOLS()
{
	if( !TInvolsExport::isInvolsAvailable() ){
		QMessageBox::critical( NULL, "������", "�� ������ ���� ��� ��������� ���������� �������������!" );
		return;
	}
	//QString studyId = dicomStruct->ELEM_CONT.get(0x0020000D).toString();
	//QString seriesId = dicomStruct->ELEM_CONT.get(0x0020000E).toString();	

	QList<DicomData*> dicomStrList;

	DicomData * dicomData =  treeDataViewModel->getDicomData( treeDataViewModel->selectedItem() );

	int window = dicomData->getELEM_CONT().get(0x00010008).toInt()
		,	level = dicomData->getELEM_CONT().get(0x00010009).toInt();
	ModelsInformation mI;

	TInvolsExport exporter( mI.seriesByDataViewModel(treeDataViewModel, treeDataViewModel->selectedItem()) , window, level );

	if( exporter.groupSelected() ){
		QProgressDialog pd( "������� �����..", "������", 0, 0 );
		pd.setWindowModality( Qt::WindowModal );
		pd.show();

		connect( &exporter, SIGNAL(setBounds(int,int)), &pd, SLOT(setRange(int,int)) );
		connect( &exporter, SIGNAL(imageExported(int)), &pd, SLOT(setValue(int)) );

		QString involsDir = TInvolsExport::involsPath();
		QString involsPath = involsDir + "/invols.exe";

		QString exportFileName = "xray_invols_export.raw";
		QString exportFilePath = QDir::tempPath() + "/" + exportFileName;
		QString cmdString = QString("0 0 \"%1\"").arg(exportFilePath);

		if( exporter.exportTo(exportFileName, QDir::tempPath()) )
			::ShellExecute( NULL, "open", involsPath.toLatin1()
			, cmdString.toLatin1(), involsDir.toLatin1(), SW_SHOW );
		else if( !exporter.error().isEmpty() )
			QMessageBox::critical( NULL, "�������", exporter.error() );
	}
	else if( !exporter.error().isEmpty() )
		QMessageBox::critical( NULL, "�������", exporter.error() );
}




