#include "stdafx.h"
#include "modelcontrols.h"
#include "TreeDataViewModel.h"
#include "treedatamodel.h"
#include "modelsinformation.h"
#include "command.h"
#include "dataviewmodeldispatcher.h"
#include "xray_2.h"

ModelControls::ModelControls( TreeDataViewModel * _model, const QModelIndex & _index, QObject *parent)
	: QObject(parent)
	,model(_model)
	,index(_index)
{
// 	connect (	this,		SIGNAL	( changeDataModelIndexSignal	(QModelIndex, QModelIndex ) ),
// 		model,		SLOT	( changeDataModelIndex			(QModelIndex, QModelIndex ) )
// 		);

	DataViewModelDispatcher * dispatcher = model->modelDispatcher();

	connect (	this,			SIGNAL	( removeBranchByIndexFromDataModelSignal	(QModelIndex ) ),
				dispatcher,		SLOT	( removeBranchByIndexFromDataModel			(QModelIndex ) )
			);

	connect (	this,			SIGNAL	( changeDataModelIndexSignal	(QModelIndex, QModelIndex ) ),
				dispatcher,		SLOT	( changeDataModelIndex			(QModelIndex, QModelIndex ) )
			);

// 	connect (	this,			SIGNAL	( selectNextChildrenSignal		(QModelIndex ) ),
// 				model,			SLOT	( selectNextChildren			(QModelIndex ) )
// 		);
// 
// 	connect (	this,		SIGNAL	( selectPrevChildrenSignal		(QModelIndex ) ),
// 				model,		SLOT	( selectPrevChildren			(QModelIndex ) )
// 		);
// 	connect (	this,	SIGNAL	( addNewBranchToParentSignal	(QModelIndex, QModelIndex ) ),
// 				model,	SLOT	( addNewBranchToParent			(QModelIndex, QModelIndex ) )
// 		);


	connect (	this,			SIGNAL	( selectNextChildrenSignal		(QModelIndex ) ),
				dispatcher,			SLOT	( selectNextChildren			(QModelIndex ) )
		);

	connect (	this,		SIGNAL	( selectPrevChildrenSignal		(QModelIndex ) ),
				dispatcher,		SLOT	( selectPrevChildren			(QModelIndex ) )
		);
	connect (	this,	SIGNAL	( addNewBranchToParentSignal	(QModelIndex, QModelIndex ) ),
				dispatcher,	SLOT	( addNewBranchToParent			(QModelIndex, QModelIndex ) )
		);


	connect (	this,		SIGNAL	( setDivisionForItemSignal		(QModelIndex, QVector2D ) ),
				dispatcher,	SLOT	( setDivisionForItem			(QModelIndex, QVector2D ) )
		);
// 	connect (	this,	SIGNAL	( setDivisionForItemSignal		(QModelIndex, QVector2D ) ),
// 		model,	SLOT	( setDivisionForItem			(QModelIndex, QVector2D ) )
// 		);
	
}


ModelControls::~ModelControls()
{

}


void ModelControls::changeDataModelDivision( QVector2D division )
{
	//model->setDivisionForItem(index, division);
	emit setDivisionForItemSignal(index, division);

}


void ModelControls::changeDataModelIndex()
{
	ModelsInformation modelsInformation;
	VPopupTable * popup = new VPopupTable();
	QModelIndex  parentIndex = model->parent(index);

	QModelIndex  curModelIndex = model->getDataModelIndex(index);

	TreeDataModel * dataModel = model->getDataModel();
	QModelIndex modelParentIndex = model->getDataModelIndex(parentIndex);
	uint tag = model->getIndexTag(index);

	QList <QStringList> list =  modelsInformation.getChildrenDataListByViewDataModelParentIndex(model, index);
	popup->setHeader(modelsInformation.getChildrenHeadersByViewDataModelIndex(model, index));	

// 	if(tag == StudyUID && list.size() > 1)
// 	{		
// 	}
	
	for(int i = 0; i < list.size(); i++)
	{
		bool enables = true;
				
		QModelIndex curdataModelIndex = dataModel->index(i, 0,  modelParentIndex);
 		if(model->findTreeItem(curdataModelIndex)  &&  tag == StudyUID && i != list.size() - 1)
 			enables = false;
		QStringList values = list[i];
		popup->addRow( values , enables );

		if(dataModel->index(i, 0, modelParentIndex) == curModelIndex)
			popup->setChosen(i);		
	}
	QWidget * widget = new QWidget();
	widget->setWindowOpacity(0.1);
	widget->setFixedSize(5000, 5000);
	widget->show();
	popup->showPopup( QCursor::pos());	

	widget->hide();
	delete widget;

	int row = popup->getChosen();
	if(row != -1)	
// 		if(model->rowCount(index))
// 		{
			if(tag == StudyUID && row == list.size() - 1)
			{
				//xray_2::onDICOMDIRECTORY();
				xray_2 * app = (xray_2 *) QApplication::activeWindow();
				app->onDICOMDIRECTORY();
			}else
			{
				emit changeDataModelIndexSignal	(index, dataModel->index(row, 0,  modelParentIndex));
			}

// 		}else	
// 		{	
// 			VLogger() << QString("changeChildrenModelIndexesToParent row - %1").arg(row);
// 			model->changeChildrenModelIndexesToParent(model->parent(index), dataModel->index(row - index.row(), 0,  modelParentIndex));
// 		}			
	
	delete popup;	
}

void ModelControls::eraseBranch()
{
	TreeDataModel * dataModel = model->getDataModel();

	if(index == QModelIndex())
	{

		QModelIndex modelParentIndex = index;

		int rowCount = dataModel->rowCount(modelParentIndex);
		for(int r = rowCount - 1; r >=0; r--)
		{			
			QModelIndex modelIndex = dataModel->index(r, 0, modelParentIndex);
			emit removeBranchByIndexFromDataModelSignal(modelIndex);

		}
	}else
	{
		emit removeBranchByIndexFromDataModelSignal(model->getDataModelIndex(index));
	}
	
}
	

void ModelControls::goNextModelIndex( )
{	
	QModelIndex  curModelIndex = model->getDataModelIndex(index);	
	TreeDataModel * dataModel = model->getDataModel();
	QModelIndex modelParentIndex = dataModel->parent(curModelIndex);

	int rowCount = dataModel->rowCount(modelParentIndex);
		for(int r = 0; r < rowCount; r ++){			
			QModelIndex modelIndex =  dataModel->index(r, 0, modelParentIndex);
			if(curModelIndex == modelIndex && r + 1 < rowCount)
				//emit changeDataModelIndexSignal	(index, dataModel->index(r + 1, 0, modelParentIndex));
				model->changeDataModelIndex( index, dataModel->index(r + 1, 0, modelParentIndex) );
			
		}
}

void ModelControls::goPrevModelIndex( )
{
	QModelIndex  curModelIndex = model->getDataModelIndex(index);	
	TreeDataModel * dataModel = model->getDataModel();
	QModelIndex modelParentIndex = dataModel->parent(curModelIndex);

	int rowCount = dataModel->rowCount(modelParentIndex);
		for(int r = rowCount; r >= 0; r --){			
			QModelIndex modelIndex =  dataModel->index(r, 0, modelParentIndex);
			if(curModelIndex == modelIndex && r - 1 >= 0)
				//emit changeDataModelIndexSignal	(index, dataModel->index(r - 1, 0, modelParentIndex));
				model->changeDataModelIndex	(index, dataModel->index(r - 1, 0, modelParentIndex));
			
		}
}

void ModelControls::addNewBranchToParent()
{
	ModelsInformation modelsInformation;
	VPopupTable * popup = new VPopupTable();	
	QModelIndex parentIndex = index;
	popup->setHeader(modelsInformation.getChildrenHeadersByViewDataModelIndex(model, parentIndex));
	QList <QStringList> list =  modelsInformation.getChildrenDataListByViewDataModelParentIndex(model, parentIndex);
	for(int i = 0; i < list.size(); i++)
	{
		QStringList values = list[i];
		popup->addRow( values );
	}
	popup->showPopup( QCursor::pos());		
	int row = popup->getChosen();
	if(row != -1){
		TreeDataModel * dataModel = model->getDataModel();
		QModelIndex modelParentIndex = model->getDataModelIndex(parentIndex);			
		//model->addNewBranchToParent(parentIndex, dataModel->index(row, 0,  modelParentIndex));
		emit addNewBranchToParentSignal	(parentIndex, dataModel->index(row, 0,  modelParentIndex));
	}
	delete popup;	
}


void ModelControls::selectNextChildren()
{
	emit selectNextChildrenSignal	(index);
	//model->selectNextChildren(index);	

	//model->selectNextPrevToLowLevel(TreeDataViewModel::Forward);	
// 	model->selectNextPrevToLowLevel(TreeDataViewModel::Forward, QModelIndex());	
// 	model->selectNextPrevToLowLevel(TreeDataViewModel::Forward, QModelIndex());	
// 	model->selectNextPrevToLowLevel(TreeDataViewModel::Forward, QModelIndex());	
// 	model->selectNextPrevToLowLevel(TreeDataViewModel::Forward, QModelIndex());	

}


void ModelControls::selectPrevChildren()
{
	emit selectPrevChildrenSignal	(index);


	//model->selectNextPrevToLowLevel(TreeDataViewModel::Back);	


	//model->selectPrevChildren(index);	
	//selectNextChildren();
}

void ModelControls::processTag()
{
	if(tag == "SERIES_NUMBER" || tag == "NUMBER")
		changeDataModelIndex();
	
	if(tag == "WINDOWLEVEL")
	{
		TSetWL setWL;
		setWL.setModal(true);
		//setWL.move( QPoint(QCursor::pos().x(), QCursor::pos().y() - setWL.height() ));
		setWL.move( QPoint(QCursor::pos().x(), QCursor::pos().y() - 100));
		if( QDialog::Accepted == setWL.exec() )	{
			Command command(Command::contrast);
			command.vector2DPar = QVector2D(setWL.getY(), setWL.getX());				
			model->emitCommand(command);					
		}
					
	}
}



TSetWL::TSetWL()
	: QDialog()
{
	spinX = new QSpinBox;
	spinY = new QSpinBox;
	spinX->setMinimum(-9999);
	spinX->setMaximum(9999);
	spinX->setSingleStep(5);
	spinX->setValue(40);
	spinY->setMinimum(1);
	spinY->setMaximum(9999);
	spinY->setValue(80);
	spinY->setSingleStep(5);
	//connect( spinX,     SIGNAL(pressed()), SLOT(accept()));
	QPushButton *ok     = new QPushButton("��");
	QPushButton *cancel = new QPushButton("������");
	connect(spinX, SIGNAL(valueChanged (int)), SLOT(ChangeWL(int)));
	connect(spinY, SIGNAL(valueChanged (int)), SLOT(ChangeWL(int)));
	connect( ok,     SIGNAL(pressed()), SLOT(accept()) );
	connect( cancel, SIGNAL(pressed()), SLOT(reject()) );    
	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addWidget( new QLabel("Window:") );
	hl1->addWidget( spinY );
	hl1->addWidget( new QLabel("Level:") );
	hl1->addWidget(spinX);
	cb = new QComboBox;
	connect( cb, SIGNAL(activated(int)), SLOT(itemActiveated(int)));
	QHBoxLayout *hl2 = new QHBoxLayout;
	hl2->addWidget( new QLabel("Defaults:") );
	hl2->addWidget( cb );
	QHBoxLayout *hl3 = new QHBoxLayout;
	hl3->addWidget(ok);
	hl3->addWidget(cancel);
	QVBoxLayout *vl = new QVBoxLayout;
	vl->addLayout( hl1 );
	vl->addLayout( hl2 );
	vl->addLayout( hl3 );
	setLayout( vl );
	setWindowTitle( "������� �������� Window � Level" );	
	cb->addItem("������ 80 40");
	cb->addItem("��� 350 80");
	cb->addItem("�������., ��, ��� 350 40");
	cb->addItem("˸���� 1500 -600");
	cb->addItem("����� 2500 500");	
}

//
void TSetWL::itemActiveated( int i )
{
	switch (i){
	case 0:{
		spinY->setValue(80);
		spinX->setValue(40);
		break;
		   }
	case 1:{
		spinY->setValue(350);
		spinX->setValue(80);
		break;
		   }
	case 2:{
		spinY->setValue(350);
		spinX->setValue(40);
		break;
		   }
	case 3:{
		spinY->setValue(1600);
		spinX->setValue(-600);
		break;
		   }
	case 4:{
		spinY->setValue(2500);
		spinX->setValue(500);
		break;
		   }
	}
}

void TSetWL::ChangeWL(int i){
	emit signalWLChanged(spinY->value(), spinX->value());
	//ContainerPanel::repaintImageAll(spinY->value(),spinX->value());
}