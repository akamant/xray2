#ifndef STUDYDATAVIEWMODELCONTROLS_H
#define STUDYDATAVIEWMODELCONTROLS_H

class TreeDataViewModel;
class DataViewModelDispatcher;

class ModelControls : public QObject
{
	Q_OBJECT

signals:
	void removeBranchByIndexFromDataModelSignal (QModelIndex);
	void changeDataModelIndexSignal		(	QModelIndex, QModelIndex	);
	void addNewBranchToParentSignal		(	QModelIndex, QModelIndex	);
	void selectNextChildrenSignal		(	QModelIndex					);
	void selectPrevChildrenSignal		(	QModelIndex					);
	void setDivisionForItemSignal		(	QModelIndex, QVector2D		);
public:	
	void setTag(QString _tag){tag = _tag;};

	QString tag;
	ModelControls( TreeDataViewModel * _model, const QModelIndex & _index, QObject *parent);
	~ModelControls();

public slots:
	void changeDataModelDivision(QVector2D division);
	void changeDataModelIndex();
	void goNextModelIndex();
	void goPrevModelIndex();
	void selectNextChildren();
	void selectPrevChildren();
	void addNewBranchToParent();
	void processTag();
	void eraseBranch();
private:
	QModelIndex index;
	TreeDataViewModel * model;	
};



class TSetWL : public QDialog
{
    Q_OBJECT
public:
    TSetWL();
    //
    int getX(){ return spinX->value(); }
    int getY(){ return spinY->value(); }
private slots:
    void itemActiveated( int i );
	void ChangeWL(int i );
private:
    QSpinBox *spinX, *spinY;
    QComboBox *cb;
signals:
	void signalWLChanged(int w, int l);
};


#endif // STUDYDATAVIEWMODELCONTROLS_H
