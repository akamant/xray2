#include "StdAfx.h"
#include "dicomdatadecoder.h"
//#include "dicomparsingtask.h"

DicomDataDecoder::DicomDataDecoder(QObject *parent)
	: QObject(parent)
{

}

DicomDataDecoder::~DicomDataDecoder()
{

}

void DicomDataDecoder::parseData(DicomData * dicomData, void * inputData, uint inputSize , void * outputData, uint & outputSize)
{
	int transferSyntax = dicomData->getDataTransfer();
	this->dicomData = dicomData;		
	if(transferSyntax >= 4 && transferSyntax <= 12)
	{
		switch(transferSyntax)
		{
			case JPEGBaseline:
			{	
				decompressJPEG(inputData, inputSize, outputData);															
				break;
			 }
			case RLELossless:
			{	
				decompressRLELossless(inputData, inputSize, outputData, outputSize);										
				break;
			}												
			case JPEG2000LosslessAndLossy:
			{	
				break;
				decompressJPEG(inputData, inputSize, outputData);										
			}	
			case JPEG2000Lossless:
			{	
				break;
				decompressJPEG(inputData, inputSize, outputData);		
			}
			case JPEGLossless:
			{	
				break;
				decompressJPEG(inputData, inputSize, outputData);	
			}
			case JPEGLosslessFirstOrderPrediction:
			{										
				decompressJPEG(inputData, inputSize, outputData);	
				break;	
			}
		}	
	}

}


void DicomDataDecoder::decompressJPEG(void * valueOfItem, int length, void * outputItem){
#ifndef _X64

	if(jpegDecoder == NULL){
		jpegDecoder = new JpegDecoder(valueOfItem, length, outputItem, dicomData->imageParameters.bitsAllocated);
		jpegDecoder->decompress();
	}else{
		jpegDecoder->initData(valueOfItem, length, outputItem,dicomData->imageParameters.bitsAllocated);
		jpegDecoder->decompress();
	}	
	return;
	#endif
}

void DicomDataDecoder::decompressRLELossless( void * valueOfItem, int length, void * outputItem, int outputSize )
{
	QList<unsigned int> sizeOfSegment;
	QList<unsigned int> offsetOfSegment;
	QList<unsigned int> sizeOfResultSegments;
	unsigned int k = 64;							
	unsigned int numberOfRLESegments = *((char *)valueOfItem);	
	unsigned int curByteInImage = 0;	
	unsigned int sizeSum = 0;
	for (uint i = 0; i < numberOfRLESegments ; i++)
	{
		offsetOfSegment << *((unsigned int *) valueOfItem + i + 1);
	}
	for (uint i = 0; i < numberOfRLESegments - 1; i++)
	{
		sizeOfSegment << offsetOfSegment[i + 1] - offsetOfSegment[i];
		sizeSum +=  sizeOfSegment[i];
	}
	unsigned int endSize = length - sizeSum;
	sizeOfSegment << endSize;
	//sizeSum += (int) sizeOfSegment[numberOfRLESegments - 1];
	for(unsigned int p = 0; p < numberOfRLESegments; p++){
		sizeOfResultSegments << 0;
		//int l = *(valueOfItem_intPointer + p + 1); 
		unsigned int  size =  sizeOfSegment[p];
		unsigned int curByteOffset = 0;
		unsigned int offset = (int) offsetOfSegment[p];
		unsigned int rleIndex = 0;
		while (rleIndex < size){
			if(curByteInImage >= (uint)outputSize)
				break;
			char n = *((char *)valueOfItem + rleIndex + offsetOfSegment[p]);
			if(n >= 0 && n <= 127){
				for(int j = 0; j < n + 1 ; j ++){
					  rleIndex++;
					if (rleIndex >= size) 
						break;				
					if(curByteInImage >= (uint)outputSize)
						break;
					*((unsigned char *)outputItem + curByteInImage) = *((char *)valueOfItem + rleIndex + offsetOfSegment[p]);					
					curByteInImage ++;

					
					sizeOfResultSegments[p] ++;
				}										
			//	k += (n + 1 + 1);
			}
			if(n <= -1 && n >= -127){			
				rleIndex++;
				if (rleIndex >= size) 
					break;
				for(int j = 0; j < - n + 1; j ++){		
					if(curByteInImage >= (uint)outputSize)
						break;
					*((unsigned char *)outputItem + curByteInImage) = *((char *)valueOfItem + rleIndex + offsetOfSegment[p]);					
					curByteInImage ++;	
					sizeOfResultSegments[p] ++;
				}													
			}
			
			rleIndex++;
			if(n == -128){				
				break;
			}
		}
	}

	unsigned int fullSize = 0;
	unsigned int maxSize = 0;
	for( int i = 0; i < sizeOfResultSegments.size(); i++)
	{
		fullSize +=  sizeOfResultSegments[i];
		if(maxSize < sizeOfResultSegments[i])
			maxSize = sizeOfResultSegments[i];
	}
	char byteBuffer;


	if(sizeOfResultSegments.size() == 2)
	{
		void * newoutputItem = new char[maxSize * sizeOfResultSegments.size()];
		unsigned int length = 0;		
		for (int i = 0; i < sizeOfResultSegments.size(); i++)
		{
			for (uint j = 0; j < sizeOfResultSegments[i]; j++)
			{				      
				//*((char *)newoutputItem + sizeOfResultSegments.size() * j + 1 - i) = *((char *)outputItem + length  + j);
				memcpy(&byteBuffer, (char *)newoutputItem + sizeOfResultSegments.size() * j + 1 - i,  1);
				memcpy((char *)newoutputItem + sizeOfResultSegments.size() * j + 1 - i, (char *)outputItem + length  + j,  1);
				memcpy((char *)outputItem + length  + j, &byteBuffer, 1);
			}
			length += sizeOfResultSegments[i];
		}

		memcpy((char *)outputItem, (char *)newoutputItem, fullSize);
		delete [] newoutputItem;
	}
		/* ���� ��� "���������" ��� � big endian ����������� ���� ��� 2� 
		��������� ����c�������� ����� ��� ����������� � "���������"

			for (uint i = 0; i < fullSize; i++)
			{	
				memcpy(&byteBuffer, (char *)outputItem + i + 0 ,  1);
				memcpy((char *)outputItem + i + 0, (char *)outputItem + i + 1,  1);
				memcpy((char *)outputItem + i + 1, &byteBuffer, 1);
			}

		*/
	
}