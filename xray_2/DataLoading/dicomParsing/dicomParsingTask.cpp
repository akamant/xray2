#include "stdafx.h"

#include "dicomparsingtask.h"
#include "localdicomfile.h"
#include "HTTPDicomFile.h"
#include "qslog.h"


QStringList DicomParsingTask::typeOneElementTagTypes;
QStringList DicomParsingTask::allElementTagTypes;
DicomDataDecoder DicomParsingTask::dicomDataParser;
DicomParsingTask::DicomParsingTask( DicomFile * _dicomFile, QObject *parent /*= NULL*/ )
	: QObject(parent)
	, readBytes(0)
	, dicomFile(_dicomFile)
	, parser(new TElementParser(TDicomDict::getDict()))
	, parsDeeping(0)
	, DICOMFileHandle(NULL)
	, isFirstTag(true)
	, parentGroupTagSeqPrev(0)
	, parentItemTagSeqPrev(0)
	, parentGroupTagSeq(0)
	, parentItemTagSeq(0)
	//, 
	, currentByte(0)
	, TransferSyntax(-1)
	, curDirectoryRecordType(-1)
	, lengthBody(0)
	, offsetPImage(0)
	, value(NULL)
	, ItemTag (0xE000FFFE)
	, lengthOfbasicOffsetTable(0)
	, curByte(0)
	, curOffset(0)
	, dicomDataForParse(NULL)
	, curFrame(0)
	, headIsParsed(false)
	, firstData(NULL)
	, sortElementParsing (true)
	//,allTypes("AE\0AS\0AT\0CS\0DA\0DS\0DT\0FL\0FD\0IS\0LO\0LT\0OB\0OF\0OW\0PN\0SH\0SL\0SQ\0SS\0ST\0TM\0UI\0UL\0UN\0US\0UT\0\0")
	//,firstTypes("OB\0OW\0OF\0SQ\0UT\0UN\0\0")
{
		if(typeOneElementTagTypes.size() == 0)
			typeOneElementTagTypes 	<< "OB"<< "OF"<< "OW"<< "SQ"<< "UN"<< "UT"; // "<< "
		if(allElementTagTypes.size() == 0)
			allElementTagTypes << "AE"<< "AS"<< "AT"<< "CS"<< "DA"<< "DS"<< "DT"<< "FL"<< "FD"<< "IS"<< "LO"<< "LT"
			<< "OB"<< "OF"<< "OW"<< "PN"<< "SH"<< "SL"<< "SQ"<< "SS"<< "ST"<< "TM"<< "UI"<< "UL"<< "UN"<< "US "<< "UT";
		correspondenceCodecs.insert("ISO_IR_192", "UTF-8");
		correspondenceCodecs.insert("ISO_IR_100", "ISO-8859-1");
		correspondenceCodecs.insert("ISO_2022_IR_144", "ISO-8859-5");
		correspondenceCodecs.insert("ISO_2022_IR_6", "Windows-1250");  //US-ASCII
		correspondenceCodecs.insert("ISO_IR_144","ISO-8859-5");	

		firstData = new DicomData("unknown");
		dicomDataForParse = firstData;

		takeMetaInformation();

		if(dynamic_cast<LocalDicomFile *> (dicomFile))
			parseHead();

		if(sortElementParsing)
			this->SetFilePointer(DICOMFileHandle, 0, NULL, FILE_BEGIN);
		sortElementParsing = false;

		//dicomFile->
}

void DicomParsingTask::parseHead()
{
	if(!firstData)
		return;
	size = dicomFile->size();	
	firstData->getELEM_CONT().makeHardTag(0x0001000A, curFrame);
	curFrame ++;
	isCreated = true;		
	firstData->setisNormalized(false);
	firstData->filePath = filePath;	
	TransferSyntax  = -1;
	if(!parseVDRFile(filePath)){
		if(parseFileMetaInformation())
		{
			switch(TransferSyntax)
			{
				case ExplicitVRBigEndian:
				{
					parseBodyExplicitVRBigEndian();
					break;
				}
				case ImplicitVRBigEndian:
				{
					parseBodyImplicitVRBigEndian();
					break;
				}
				case ImplicitVRLittleEndian:
				{
					parseBodyImplicitVRLittleEndian();
					break;
				}
				case ExplicitVRLittleEndian:
				{
					parseBodyExplicitVRLittleEndian();
					break;
				}
				case RLELossless:
				{
					parseBodyExplicitVRLittleEndian();
					break;
				}				
				case JPEGBaseline:
				{						
					parseBodyExplicitVRLittleEndian();
					break;
				}
				case JPEGExtended:
				{						
					parseBodyExplicitVRLittleEndian();
					break;
				}
				case JPEGLossless:{						
					parseBodyExplicitVRLittleEndian();
					break;
								  }
				case JPEGLosslessFirstOrderPrediction:
				{						
					parseBodyExplicitVRLittleEndian();
					break;
				}
				case JPEGLSLossless:
				{						
					parseBodyExplicitVRLittleEndian();
					break;
				}
				case JPEGLSNearLossless:
				{						
					parseBodyExplicitVRLittleEndian();
					break;
				}
				case JPEG2000Lossless:
				{					
					parseBodyExplicitVRLittleEndian();
					break;
				}
				case JPEG2000LosslessAndLossy:
				{						 
					parseBodyExplicitVRLittleEndian();
					break;
				}
			}					
		}else{
			//VLogger() << "cant parse fileMetaInformation in " + dicomFile->fileName();
		}
	}	
	firstData->doEmptyTags();
	firstData->setID(firstData->getELEM_CONT().get(StudyUID).toString());
	studyID = firstData->getID();
	//dicomData->setID(0);
	if(firstData->MediaStorageSOPClassUID != MediaStorageDirectoryStorage){
		if(lengthBody == 0){
			QLOG_ERROR() << "lengthBody = 0";
			delete firstData;
			firstData = NULL;
			return;
		}
		//emit newDicomData(dicomData);
	}
	else{
		QLOG_ERROR() << "MediaStorageSOPClassUID != MediaStorageDirectoryStorage";
		delete firstData;
		firstData = NULL;
	}
	if(!sortElementParsing)
		headIsParsed = true;	

}

QVariant DicomParsingTask::getAttribute( int tagGroup, int tagAttr )
{
	return * (new QVariant);
}



// void DicomParsingTask::doTagsFromVidarParams(const QString & params){
// 	if(params.size() != 0){
// 		QString str(params);
// 		QStringList list1 = str.split("@");
// 		for(int i = 0; i < list1.size(); i ++){   //$(SolutionDir)$(ConfigurationName)
// 			switch(i)
// 			{
// 				case 0:
// 				{
// 					parseElement(0x08, 0x20, (char *)list1[i].toAscii().constData(), strlen(list1[i].toAscii().constData()) + 1);
// 					break;
// 				}
// 				case 1:
// 				{
// 					parseElement(0x10, 0x10, (char *)list1[i].toAscii().constData(), strlen(list1[i].toAscii().constData()) + 1);				
// 					break;
// 				}
// 				case 2:
// 				{
// 					parseElement(0x10, 0x30, (char *)list1[i].toAscii().constData(), strlen(list1[i].toAscii().constData()) + 1);				
// 					break;
// 				}
// 				case 3:
// 				{
// 					if(list1[i].toInt() == 1)
// 						parseElement(0x10, 0x40, "M", 2);	
// 					if(list1[i].toInt() == 2)
// 						parseElement(0x10, 0x40, "F", 2);		
// 					break;
// 				}
// 			}			
// 		}
// 	}
// }

void DicomParsingTask::setVIDARFileID(const QString &  filter){
	firstData->idFile = filter;	
};


BOOLEAN DicomParsingTask::parseFileMetaInformation(){	
	parseFileMetaInformationExplicitVRLittleEndian();	
	if(TransferSyntax == -1){
		parseFileMetaInformationImplicitVRLittleEndian();	
		
	}	
	parseFileMetaInformationExplicitVRLittleEndian();	
	if(TransferSyntax == -1){
		TransferSyntax = ImplicitVRLittleEndian;		
		checkEndianAndExplicity();			
	}else{		
	}	
	if(TransferSyntax >= 0)
		return true;
	else
		return false;
}

void DicomParsingTask::checkEndianAndExplicity(){
	checkPreamble();	
	char  firstByte;
	char  secondByte;
	unsigned int nb;	
	char * type  = new char[3];
	char * reserveBuffer = new char[2];
	unsigned short tagAttr;

	int bResult = this->ReadFile(DICOMFileHandle, & firstByte, 1, (unsigned long *) & nb, NULL); // ������ ��� DATA Element		
	bResult = this->ReadFile(DICOMFileHandle, & secondByte, 1, (unsigned long *) & nb, NULL); // ������ ��� DATA Element		
	if(!(firstByte == 8 || secondByte == 8)){			
		parseFileMetaInformationExplicitVRLittleEndian();			
		bResult = this->ReadFile(DICOMFileHandle, & firstByte, 1, (unsigned long *) & nb, NULL); // ������ ��� DATA Element		
		bResult = this->ReadFile(DICOMFileHandle, & secondByte, 1, (unsigned long *) & nb, NULL);		
	}
	if(!(firstByte == 8 || secondByte == 8)){			
		parseFileMetaInformationImplicitVRLittleEndian();		
		bResult = this->ReadFile(DICOMFileHandle, & firstByte, 1, (unsigned long *) & nb, NULL); // ������ ��� DATA Element		
		bResult = this->ReadFile(DICOMFileHandle, & secondByte, 1, (unsigned long *) & nb, NULL);		
	}
	if(firstByte == 8 || secondByte == 8){
		if(firstByte == 8){
			bResult = this->ReadFile(DICOMFileHandle, & tagAttr, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element
			if(bResult &&  nb == 0)
				return;
			bResult = this->ReadFile(DICOMFileHandle, type, 2, (unsigned long *) & nb, NULL);	// ������ VR (���) DATA Element
			if(bResult &&  nb == 0)
				return;
			type[2] = '\0';
			QString typeElement = QString(type);		
			if(allElementTagTypes.indexOf(typeElement) != -1){
				TransferSyntax = ExplicitVRLittleEndian;
				this->SetFilePointer(DICOMFileHandle, -6, NULL, FILE_CURRENT);		
				return;
			}else{
				TransferSyntax = ImplicitVRLittleEndian;
				this->SetFilePointer(DICOMFileHandle, -6, NULL, FILE_CURRENT);		
			}
			return;		
		}else{
			this->SetFilePointer(DICOMFileHandle, -2, NULL, FILE_CURRENT);		
			TransferSyntax = ImplicitVRBigEndian;
			return;
		}
			
	}else{
		return;
	}
	
	


}

DicomParsingTask::~DicomParsingTask(void)
{	
	delete parser;
	delete value;
}

BOOLEAN DicomParsingTask::parseBodyExplicitVRBigEndian()
{
	return true;
}

inline bool DicomParsingTask::parseElement(int tagGroup, int tagAttr, char * value, unsigned int lengthInt)
{
	DicomData * dicomData = firstData;	
	if(sortElementParsing)
		if(! (	tagGroup == 2 || tagGroup == 0 || 
				(tagGroup == 0x0008 && tagAttr == 0x0018) ||
				(tagGroup == 0x0020 && tagAttr == 0x0011) ||
				(tagGroup == 0x0020 && tagAttr == 0x0013) ||
				(tagGroup == 0x0020 && tagAttr == 0x000D) ||
				(tagGroup == 0x0020 && tagAttr == 0x000E) 
				)
			)
			return true;


	if(parentGroupTagSeq == 0x0028 && parentItemTagSeq == 0x3010)
	{

		if(tagGroup == 0x0028 && tagAttr == 0x3002)
		{
			QVariantList vals = parser->getElement(tagGroup, tagAttr, value, lengthInt).variant.toList();
			if(vals.size() > 0)
				dicomData->wlLUT.setFirstVOILUTEntry(vals.at(1).toInt());
		}

		if(tagGroup == 0x0028 && tagAttr == 0x3006)
		{
			QVariantList vals = parser->getElement(tagGroup, tagAttr, value, lengthInt).variant.toList();

			dicomData->wlLUT.setVOILUTElement(vals);			

			QVariant var;
			var.setValue(vals.length());
			TElement newElem( 0x00281051, var);
			dicomData->ELEM_CONT.add(0x0028, 0x1051, newElem);
			var.setValue(dicomData->wlLUT.firstVOILUTEntry() + vals.length() / 2);
			TElement newElem2( 0x00281050, var);
			dicomData->ELEM_CONT.add(0x0028, 0x1050, newElem2);

			dicomData->representationParameters.levelValue = dicomData->ELEM_CONT.get(0x00281050).toInt();
			dicomData->representationParameters.windowValue = dicomData->ELEM_CONT.get(0x00281051).toInt();
		}

	}

	if(dicomData->MediaStorageSOPClassUID != MediaStorageDirectoryStorage &&
		(parentGroupTagSeq != 0 || parentItemTagSeq != 0))
		return true;		
	
	
	if(tagGroup % 2 == 0)
	{		
		TElement elem = parser->getElement(tagGroup, tagAttr, value, lengthInt);
		dicomData->getELEM_CONT().add(tagGroup, tagAttr, elem);	
	}
	if(tagGroup != 0x7FE0 && tagAttr != 0x0010)
	{		
		currentByte = currentByte + lengthInt + 4;	
		if(currentByte < pos)
			currentByte = pos;
	}	
	if(tagGroup == 0x0008 && tagAttr == 0x0005)
	{
		QString dicomCodecName = dicomData->getELEM_CONT().get(0x00080005).toString();
		dicomCodecName.replace("-", "_").replace(" ", "_").replace("^", "_");		
		parser->setCodec(correspondenceCodecs.value(dicomCodecName, "Windows-1251"));
	}

	switch(tagGroup){  ///���� ��� ��� transfer syntax
		case 0x4:{
			switch(tagAttr){
				case 0x1430:{
					int prevDirectoryRecordType = curDirectoryRecordType;					
					QString typeRecord = dicomData->getELEM_CONT().get(0x00041430).toString();
					if(typeRecord.contains(QString("PATIENT"),Qt::CaseInsensitive))
						curDirectoryRecordType = PATIENT;
					if(typeRecord.contains(QString("STUDY"),Qt::CaseInsensitive))
						curDirectoryRecordType = STUDY;
					if(typeRecord.contains(QString("SERIES"),Qt::CaseInsensitive))
						curDirectoryRecordType = SERIES;
					if(typeRecord.contains(QString("IMAGE"),Qt::CaseInsensitive))
						curDirectoryRecordType = IMAGE;
					if(curDirectoryRecordType <= prevDirectoryRecordType)
					{
 						///DICOMStruct * dicomStructPtr = new DICOMStruct(*dicomData);	
						///dicomFile->addDICOMStruct(dicomStructPtr);
						isCreated = true;
					}
					break;
				}				
				default:{
				  if(lengthInt != 0xFFFFFFFF){
						//delete value;	
					}else{
						int t = 6;
					}
				   break;
				}
			}
			break;
		}		
				
		case 0x28:{
			switch(tagAttr){
				case 0x2:{	
					dicomData->imageParameters.samplesPerPixel = *(unsigned short *)value;
					break;						
				}
				case 0x4:{	
					 dicomData->imageParameters.PhotometricInterpretation = 0;
					 QString photometricInterpretation = dicomData->getELEM_CONT().get(0x00280004).toString();
					 if(photometricInterpretation.contains(QString("RGB"), Qt::CaseInsensitive))
						dicomData->imageParameters.PhotometricInterpretation = RGB_INTERPETATION;
					 if(photometricInterpretation.contains(QString("ARGB"), Qt::CaseInsensitive))						
						 dicomData->imageParameters.PhotometricInterpretation = ARGB_INTERPETATION;
					 if(photometricInterpretation.contains(QString("YBR_FULL"), Qt::CaseInsensitive))				
						dicomData->imageParameters.PhotometricInterpretation = YBR_FULL;
					 if(photometricInterpretation.contains(QString("YBR_FULL_422"), Qt::CaseInsensitive))				
						dicomData->imageParameters.PhotometricInterpretation = YBR_FULL_422;
					 if(photometricInterpretation.contains(QString("YBR_PARTIAL_422"), Qt::CaseInsensitive))							
					    dicomData->imageParameters.PhotometricInterpretation = YBR_PARTIAL_422;
					 if(photometricInterpretation.contains(QString("YBR_ICT_422"), Qt::CaseInsensitive))										
					    dicomData->imageParameters.PhotometricInterpretation = YBR_ICT_422;
					 if(photometricInterpretation.contains(QString("YBR_RCT_422"), Qt::CaseInsensitive))									
					    dicomData->imageParameters.PhotometricInterpretation = YBR_RCT_422;			
					 if(photometricInterpretation.contains(QString("MONOCHROME1"), Qt::CaseInsensitive))								
					    dicomData->imageParameters.PhotometricInterpretation = MONOCHROME1;					
					 if(photometricInterpretation.contains(QString("MONOCHROME2"), Qt::CaseInsensitive))										
					    dicomData->imageParameters.PhotometricInterpretation = MONOCHROME2;					
					 if(photometricInterpretation.contains(QString("PALETTE_COLOR"), Qt::CaseInsensitive))							
					    dicomData->imageParameters.PhotometricInterpretation = PALETTE_COLOR;					
					break;	
				}
				 case 0x6:{	
					dicomData->imageParameters.planarConfiguration = *(unsigned short *)value;
					break;						
				}			    
				case 0x10:{	
					dicomData->imageParameters.height = *(unsigned short *)value;
					break;						
				}
				case 0x11:{	
					dicomData->imageParameters.width = *(unsigned short *)value;
					break;						
				}
				case 0x30:{	
					 //sscanf((char *)value, "%f\\%f", & dicomData->ScaleX, & dicomData->ScaleY);
					 QVariantList list = dicomData->getELEM_CONT().get(0x00280030).toList();
					 if(list.size() > 0)		
						dicomData->ScaleX = list[0].toFloat();
					 if(list.size() > 1)		
						dicomData->ScaleY = list[1].toFloat();
					break;						
				}
				case 0x100:{	
					 dicomData->imageParameters.bitsAllocated = *(unsigned short *)value;
					break;						
				}
				case 0x101:{	
					 dicomData->imageParameters.bitsStored = *(unsigned short *)value;
					break;						
				}
				case 0x102:{	
				    dicomData->imageParameters.highBit = *(unsigned short *)value;
					break;						
				}
				 case 0x103:{	
				    dicomData->imageParameters.pixelRepresentation = *(unsigned short *)value;
					break;						
				}			
				case 0x1050:{					
					dicomData->representationParameters.levelValue = dicomData->getELEM_CONT().get(0x00281050).toFloat();
					break;						
				}
				case 0x1051:{	
				    dicomData->representationParameters.windowValue = dicomData->getELEM_CONT().get(0x00281051).toFloat();
					break;						
				}
				case 0x1052:{					   
					dicomData->imageParameters.rescaleIntercept = dicomData->getELEM_CONT().get(0x00281052).toFloat();
					break;						
				}
			    case 0x1053:{	
					dicomData->imageParameters.rescaleSlope = dicomData->getELEM_CONT().get(0x00281053).toFloat();
					break;						
				 }				
			   default:{
				   if(lengthInt != 0xFFFFFFFF){
						//delete value;	
					}else{
						int t = 6;
					}
				   break;
				}
			}
			break;
		}
		case 0x7FE0:
		{
			switch(tagAttr)
			{
				case 0x0010:
				{					
					return false;
					break;
				}
				default:{					
				   break;
				}
			}
			break;
		}									
		case 0x2:{
			switch(tagAttr){
				case 0x2:{

					if(dicomData->getELEM_CONT().get(0x00020002).toString().contains(QString("1.2.840.10008.1.3.10")))	{				
						dicomData->MediaStorageSOPClassUID = MediaStorageDirectoryStorage;	
						QLOG_ERROR() << "MediaStorageDirectoryStorage file";
					}else{
						//VLogger() << dicomData->getELEM_CONT().get(0x00020002).toString();
						//dicomData->MediaStorageSOPClassUID = NotMediaStorageDirectoryStorage;	
					}
					break;		
				}
				case 0x10:{				
					QString transferSyntax = dicomData->getELEM_CONT().get(0x00020010).toString();
					if(transferSyntax == QString("1.2.840.10008.1.2"))
						TransferSyntax = ImplicitVRLittleEndian;		
					if(transferSyntax == QString("1.2.840.10008.1.2.1"))	
						TransferSyntax = ExplicitVRLittleEndian;		
						
					if(transferSyntax == QString("1.2.840.10008.1.2.2"))			
						TransferSyntax = ExplicitVRBigEndian;
					///if(transferSyntax.contains(QString("1.2.840.10008.1.2.4.50", Qt::CaseInsensitive)))			
			//			TransferSyntax = ExplicitVRBigEndian;
					////////////
					if(transferSyntax == QString("1.2.840.10008.1.2.5"))				
						TransferSyntax = RLELossless;	
					////////////
					if(transferSyntax == QString("1.2.840.10008.1.2.4.50"))		
						TransferSyntax = JPEGBaseline;
					if(transferSyntax == QString("1.2.840.10008.1.2.4.57"))			
						TransferSyntax = JPEGLossless;
					if(transferSyntax == QString("1.2.840.10008.1.2.4.70"))					
						TransferSyntax = JPEGLosslessFirstOrderPrediction;
					/////////////
					if(transferSyntax == QString("1.2.840.10008.1.2.4.51"))					
						TransferSyntax = JPEGExtended;					
					if(transferSyntax == QString("1.2.840.10008.1.2.4.52"))					
						TransferSyntax = JPEGExtended;/////////////�� ���
					if(transferSyntax == QString("1.2.840.10008.1.2.4.53"))					
						TransferSyntax = JPEGExtended;/////////////�� ���
					if(transferSyntax == QString("1.2.840.10008.1.2.4.54"))				
						TransferSyntax = JPEGExtended;/////////////�� ���
					if(transferSyntax == QString("1.2.840.10008.1.2.4.55"))					
						TransferSyntax = JPEGExtended;/////////////�� ���
					if(transferSyntax == QString("1.2.840.10008.1.2.1.99"))					
						TransferSyntax = JPEGExtended;/////////////�� ���										
					if(transferSyntax == QString("1.2.840.10008.1.2.4.80"))				
						TransferSyntax = JPEGLSLossless;
					if(transferSyntax == QString("1.2.840.10008.1.2.4.81"))				
						TransferSyntax = JPEGLSNearLossless;
					if(transferSyntax == QString("1.2.840.10008.1.2.4.90"))	
						TransferSyntax = JPEG2000Lossless;
					if(transferSyntax == QString("1.2.840.10008.1.2.4.91"))
						TransferSyntax = JPEG2000LosslessAndLossy;										
					if(TransferSyntax == -1){
						TransferSyntax = -2;
						dicomData->withoutErrors = false;
						dicomData->descriptionError = "unknown transferSyntax";
					}
					dicomData->transferSyntax = TransferSyntax;
					break;						
				}
				default:{
				   if(lengthInt != 0xFFFFFFFF){
						//delete value;	
					}else{
						int t = 6;
					}
				   break;
				}
			}			
			break;
		}
		default:{			
			break;
		}
	}
	return true;
}


BOOLEAN DicomParsingTask::parseBodyExplicitVRLittleEndian()
{
	parseBodyExplicitVRLittleEndianItemTag(0xFFFFFFFF);
	return true;
}

int DicomParsingTask::parseBodyExplicitVRLittleEndianItemTag(unsigned int lengthItem)
{
	if(lengthItem == 0)
		return 0;
	unsigned int nb;	
	char type [3];	
	char reserveBuffer [2];
	unsigned short lengthShort;
	unsigned int lengthInt;
	unsigned short tagGroup;
	unsigned short tagAttr;	
	unsigned int lengthOfSeq = 0;
	unsigned int lengthOfItemSeq = 0;
	unsigned int length = 0;
	nb = 1;
	BOOLEAN bResult = true;
	int tagGroupPrev = 0;	
	int curD = 0;	
	while(true){	
		//QTime time = QTime::currentTime();
		//int timeA = time.msec();
		lengthInt = 0;
		char * value;	
		bResult = this->ReadFile(DICOMFileHandle, & tagGroup, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element	
		if(isFirstTag)
		{
			isFirstTag = false;
			if(!(tagGroup == 0x8 || (firstData->MediaStorageSOPClassUID == MediaStorageDirectoryStorage && tagGroup == 0x4)))			
				return -1;
		}		
		tagGroupPrev = tagGroup;
		if(bResult &&  nb == 0 )
			break;			
		bResult = this->ReadFile(DICOMFileHandle, & tagAttr, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element	
		if(tagAttr == 0x10 && tagGroup == 0x6000)
			int a = 4;
		bResult = this->ReadFile(DICOMFileHandle, type, 2, (unsigned long *) & nb, NULL);	// ������ VR (���) DATA Element
		if(tagGroup != 0x10 || tagAttr == 0x1070){
			int r = 4;
		}	
		length += 6;
		type[2] = '\0';			
		QString typeElement = QString(type);		
		if(typeOneElementTagTypes.indexOf(typeElement) != -1){
			bResult = this->ReadFile(DICOMFileHandle, reserveBuffer, 2, (unsigned long *) & nb, NULL);				
			bResult = this->ReadFile(DICOMFileHandle, & lengthInt, 4, (unsigned long *) & nb, NULL);					
			length += 6;		
			if(typeElement == QString("SQ"))
			{
				int parentGroupTagSeqPrev = parentGroupTagSeq;
				int parentItemTagSeqPrev = parentItemTagSeq;
				parentGroupTagSeq =  tagGroup;
				 parentItemTagSeq = tagAttr;
				parsDeeping++;
				if(lengthInt == -1)
					int a = 4;
				lengthOfSeq = lengthInt;
				if(true){
					lengthOfSeq = parseBodyExplicitVRLittleEndianReadSQ(lengthInt);		
					parsDeeping--;
					if(lengthOfSeq != lengthInt)
						int a = 3;
					parentGroupTagSeq =  parentGroupTagSeqPrev;
					parentItemTagSeq = parentItemTagSeqPrev;
					if(lengthOfSeq != 0)
						this->SetFilePointer(DICOMFileHandle, -(int)lengthOfSeq, NULL, FILE_CURRENT);
					else 
						int a = 3;
				}else{					
					parsDeeping--;					
					parentGroupTagSeq =  parentGroupTagSeqPrev;
					parentItemTagSeq = parentItemTagSeqPrev;						
				}
				lengthInt = lengthOfSeq;			
			}	
			if(lengthInt == 0xFFFFFFFF)
			{
				parsDeeping++;
				lengthOfSeq = parseBodyExplicitVRLittleEndianReadOB(lengthInt);			
				parsDeeping--;
				this->SetFilePointer(DICOMFileHandle, -(int)lengthOfSeq, NULL, FILE_CURRENT);
				lengthInt = lengthOfSeq;			
			}	
		}else
		{				
			bResult = this->ReadFile(DICOMFileHandle, & lengthShort, 2, (unsigned long *) & nb, NULL);
			length += 2;
			lengthInt = lengthShort;						
		}	
		if(tagGroup == 0xFFFE && tagAttr == 0xE00D)
			return length;		
		if(lengthInt == 0xFFFFFFFF)	
			int a = 3;

		if(size - currentByte < lengthInt)
			return lengthInt;	
		if(lengthInt != 0 && typeElement != QString("SQ") && (parsDeeping < 2 ||
			firstData->MediaStorageSOPClassUID == MediaStorageDirectoryStorage))
		{
			if(tagGroup == 0x7FE0 && tagAttr == 0x0010)	{
				lengthBody = lengthInt;
				//qDebug() << "finded 0x7FE0, 0x0010 length - " << lengthInt;
				return lengthInt;
			}
			value = new char[lengthInt];
			
			bResult = this->ReadFile(DICOMFileHandle, value, lengthInt, (unsigned long *) & nb, NULL);						
			if(bResult &&  nb == 0 && lengthInt != 0)
			{
				delete [] value;		
				break;		
			}
			if(
				//!(tagAttr == 0x10 && tagGroup == 0x6000) && 
				!(tagAttr == 0x11 && tagGroup == 0x6000)&&
				!(tagAttr == 0x3000 && tagGroup == 0x6000)
				)
			if(tagAttr == 0x10 && tagGroup == 0x6000)
				int as = 3;
			parseElement(tagGroup, tagAttr, value, lengthInt);							
			delete [] value;		
		}else{
			this->SetFilePointer(DICOMFileHandle, lengthInt, NULL, FILE_CURRENT);
		}	
		length += lengthInt;
	//	if(length > lengthItem && lengthItem != -1)
	//		int a = 4;
		if(length == lengthItem)
			return length;		
	//	int timeB = time.msecsTo(QTime::currentTime());
	//	if(curD < timeB)
	//		curD = timeB;		
		
	}
	return -1;
}

int DicomParsingTask::parseBodyExplicitVRLittleEndianReadSQ(unsigned int lengthItem)
{
	if(lengthItem == 0)
		return 0;
	unsigned int nb;	
	unsigned int lengthOfSeq = 0;
	unsigned int lengthOfItemSeq = 0;
	unsigned int itemTagSeq = 0xE000FFFE;		
	int lengthCurItem = 0;
	lengthOfSeq = 0;
	lengthOfItemSeq = 0;
	int numberOfFrames = 0;
	BOOLEAN bResult = true;
	while(itemTagSeq == 0xE000FFFE || itemTagSeq == 0xE0DDFFFE)
	{	
		bResult = this->ReadFile(DICOMFileHandle, & itemTagSeq, 4, (unsigned long*) & nb, NULL);	
		if(bResult && nb == 0)
			break;
		bResult = this->ReadFile(DICOMFileHandle, & lengthOfItemSeq, 4, (unsigned long*) & nb, NULL);	
		if(bResult && nb == 0)
			break;
		lengthOfSeq += 8;
		if(itemTagSeq == 0xE0DDFFFE)
			return lengthOfSeq;			
		lengthOfSeq += parseBodyExplicitVRLittleEndianItemTag(lengthOfItemSeq);	
		if(lengthOfSeq >= lengthItem)
			return lengthOfSeq;
	}	
	return lengthOfSeq;
}

int DicomParsingTask::parseBodyExplicitVRLittleEndianReadOB(unsigned int lengthItem)
{
	if(lengthItem == 0)
		return 0;
	unsigned int nb;	
	unsigned int lengthOfSeq = 0;
	unsigned int lengthOfItemSeq = 0;
	unsigned int itemTagSeq = 0xE000FFFE;		
	int lengthCurItem = 0;
	lengthOfSeq = 0;
	lengthOfItemSeq = 0;
	int numberOfFrames = 0;
	BOOLEAN bResult = true;
	while(itemTagSeq == 0xE000FFFE || itemTagSeq == 0xE0DDFFFE){	
		bResult = this->ReadFile(DICOMFileHandle, & itemTagSeq, 4, (unsigned long*) & nb, NULL);	
		if(bResult &&  nb == 0)
			break;
		bResult = this->ReadFile(DICOMFileHandle, & lengthOfItemSeq, 4, (unsigned long*) & nb, NULL);	
		if(bResult &&  nb == 0)
			break;
		lengthOfSeq += 8;
		if(itemTagSeq == 0xE0DDFFFE)
			return lengthOfSeq;				
		if(lengthOfItemSeq == 0xFFFFFFFF){					
			lengthOfSeq += parseBodyExplicitVRLittleEndianItemTag(0xFFFFFFFF);	
		}else{
			this->SetFilePointer(DICOMFileHandle, lengthOfItemSeq, NULL, FILE_CURRENT);	
			lengthOfSeq += lengthOfItemSeq;
		}			
		if(lengthOfSeq == lengthItem)
			return lengthOfSeq;
	}	
	return lengthOfSeq;
}

BOOLEAN DicomParsingTask::parseBodyImplicitVRBigEndian()
{
	return true;
}

BOOLEAN DicomParsingTask::parseBodyImplicitVRLittleEndian()
{
		parseBodyImplicitVRLittleEndianItemTag(0xFFFFFFFF);
		return true;
}

int DicomParsingTask::parseBodyImplicitVRLittleEndianItemTag(unsigned int lengthItem)
{
	if(lengthItem == 0)
		return 0;
	unsigned int nb;	
	unsigned int lengthInt;
	unsigned short tagGroup;
	unsigned short tagAttr;	
	unsigned int lengthOfSeq = 0;
	unsigned int lengthOfItemSeq = 0;
	nb = 1;
	int length = 0;
	BOOLEAN bResult = true;
	int tagGroupPrev = 0;
	while(true){	
		lengthInt = 0;
		char * value = NULL;	
		bResult = this->ReadFile(DICOMFileHandle, & tagGroup, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element	
		if(isFirstTag){
			isFirstTag = false;
			if(!(tagGroup == 0x8 || (firstData->MediaStorageSOPClassUID == MediaStorageDirectoryStorage && tagGroup == 0x4))){
				qDebug() << tagGroup;
				qDebug() << firstData->MediaStorageSOPClassUID;
				qDebug() << tagGroup;
				return -1;
			}				
		}	
		tagGroupPrev = tagGroup;		
		if(bResult &&  nb == 0)
			break;
		bResult = this->ReadFile(DICOMFileHandle, & tagAttr, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element			
		bResult = this->ReadFile(DICOMFileHandle, & lengthInt, 4, (unsigned long*) & nb, NULL);	
		//QString a = dicomFile->fileName();
		length += 8;
		if(tagGroup == 0xFFFE && tagAttr == 0xE00D)
			return length;
		QString typeElement("");
		if(lengthInt == 0xFFFFFFFF){
			typeElement = QString("SQ");
			int parentGroupTagSeqPrev = parentGroupTagSeq;
			int parentItemTagSeqPrev = parentItemTagSeq;	
			parentGroupTagSeq =  tagGroup;
			parentItemTagSeq = tagAttr;
			parsDeeping++;
			lengthOfSeq = parseBodyImplicitVRLittleEndianReadSQ(lengthInt);			
			parsDeeping--;
			parentGroupTagSeq =  parentGroupTagSeqPrev;
			parentItemTagSeq = parentItemTagSeqPrev;			
			this->SetFilePointer(DICOMFileHandle, -(int)lengthOfSeq, NULL, FILE_CURRENT);
			lengthInt = lengthOfSeq;			
		}	
		if(size - currentByte < lengthInt)
			return lengthInt;
		if(lengthInt != 0 && typeElement != QString("SQ")  && (parsDeeping < 2 || firstData->MediaStorageSOPClassUID == MediaStorageDirectoryStorage)){
			if(tagGroup == 0x7FE0 && tagAttr == 0x0010)	
			{
				lengthBody = lengthInt;
				return lengthInt;
			}
			value = new char[lengthInt];
			bResult = this->ReadFile(DICOMFileHandle, value, lengthInt, (unsigned long*) & nb, NULL);						
			if(bResult &&  nb == 0 && lengthInt != 0)
			{
				delete [] value;	
				break;
			}
			if(tagGroup == 40 && tagAttr == 4178)
				int a = 4;
			//QString a = dicomFile->fileName();
			parseElement(tagGroup, tagAttr, value, lengthInt);	
			delete [] value;
		}else{			
			this->SetFilePointer(DICOMFileHandle, lengthInt, NULL, FILE_CURRENT);
		}
		length += lengthInt;
		if(length == lengthItem)
			return length;		
	}
	return -1;
}

int DicomParsingTask::parseBodyImplicitVRLittleEndianReadSQ(unsigned int lengthItem){
	if(lengthItem == 0)
		return 0;
	unsigned int nb;	
	unsigned int lengthOfSeq = 0;
	unsigned int lengthOfItemSeq = 0;
	unsigned int itemTagSeq = 0xE000FFFE;					
	BOOLEAN bResult = true;
	lengthOfSeq = 0;
	lengthOfItemSeq = 0;
	int numberOfFrames = 0;
	while(itemTagSeq == 0xE000FFFE || itemTagSeq == 0xE0DDFFFE){	
		bResult = this->ReadFile(DICOMFileHandle, & itemTagSeq, 4, (unsigned long*) & nb, NULL);	
		if(bResult &&  nb == 0)
			break;
		bResult = this->ReadFile(DICOMFileHandle, & lengthOfItemSeq, 4, (unsigned long*) & nb, NULL);		
		lengthOfSeq += 8;
		if(itemTagSeq == 0xE0DDFFFE)
			return lengthOfSeq;		
		lengthOfSeq += parseBodyImplicitVRLittleEndianItemTag(lengthOfItemSeq);
		if(lengthOfSeq == lengthItem)
			return lengthOfSeq;
	}	
	return lengthOfSeq;
}

void DicomParsingTask::parseFileMetaInformationExplicitVRBigEndian(){
	return;
}

void DicomParsingTask::parseFileMetaInformationExplicitVRLittleEndian(){
	unsigned int nb;		
	char type [3];	
	char reserveBuffer [2];
	unsigned short lengthShort;
	unsigned int lengthInt;
	unsigned short tagGroup;
	unsigned short tagAttr;
	char * value = NULL;		
	checkPreamble();
	tagGroup = 2;
	BOOLEAN isNoErrors = false;
	BOOLEAN bResult = true;	
	while(tagGroup == 2 || tagGroup == 0){			
		bResult = this->ReadFile(DICOMFileHandle, & tagGroup, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element		
		if(bResult &&  nb == 0)
			break;
		if(!(tagGroup == 2 || tagGroup == 0))
			break;
		bResult = this->ReadFile(DICOMFileHandle, & tagAttr, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element
		if(bResult &&  nb == 0)
			break;
		bResult = this->ReadFile(DICOMFileHandle, type, 2, (unsigned long *) & nb, NULL);	// ������ VR (���) DATA Element
		if(bResult &&  nb == 0)
			break;
		type[2] = '\0';
		QString typeElement(type);
		if(typeOneElementTagTypes.indexOf(typeElement) != -1){
			bResult = this->ReadFile(DICOMFileHandle, reserveBuffer, 2, (unsigned long *) & nb, NULL);	
			if(bResult &&  nb == 0)
				break;	
			bResult = this->ReadFile(DICOMFileHandle, & lengthInt, 4, (unsigned long *) & nb, NULL);	
			if(bResult &&  nb == 0)
				break;							
			if(size - currentByte < lengthInt)
				return;			
			if(bResult &&  nb == 0 && lengthInt != 0)
				break;								
		}else{					
			//��� ��������� DATA Element ���������:  ����� ���� � ���� ���� ������:�����: 2 �����, ��������: ����� �����				
				bResult = this->ReadFile(DICOMFileHandle, & lengthShort, 2, (unsigned long*) & nb, NULL);
				if(bResult &&  nb == 0)
					break;
				if(size - currentByte < lengthShort)
					return;		
			lengthInt = lengthShort;
		}
		if(lengthInt == 0xFFFFFFFF){
			//int h = 4;
			return ;
		}	
		if(tagGroup == 0x7FE0 && tagAttr == 0x0010)	{
			lengthBody = lengthInt;
			return;
		}
		value = new char[lengthInt];
		bResult = this->ReadFile(DICOMFileHandle, value, lengthInt, (unsigned long*) & nb, NULL);		
		//*(value + lengthInt) = '\0';
		parseElement(tagGroup, tagAttr, value, lengthInt);
		delete [] value;
	}
	this->SetFilePointer(DICOMFileHandle, -2, NULL, FILE_CURRENT);		
	return;
}

void DicomParsingTask::parseFileMetaInformationImplicitVRBigEndian(){
	return;
}



void DicomParsingTask::parseFileMetaInformationImplicitVRLittleEndian(){
	unsigned int nb;		
	unsigned int lengthInt;
	unsigned short tagGroup;
	unsigned short tagAttr;	
	checkPreamble();
	tagGroup = 2;
	BOOLEAN bResult;	
	BOOLEAN firstCheck = true;
	while(tagGroup == 2 || tagGroup == 0)
	{			
		bResult = this->ReadFile(DICOMFileHandle, & tagGroup, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element		
		if(!(tagGroup == 2 || tagGroup == 0))
			break;		
		char * value = NULL;			
		if(bResult &&  nb == 0 )
			break;
		bResult = this->ReadFile(DICOMFileHandle, & tagAttr, 2, (unsigned long *) & nb, NULL); // ������ ��� DATA Element			
		if(bResult &&  nb == 0)
			break;	
		bResult = this->ReadFile(DICOMFileHandle, & lengthInt, 4, (unsigned long*) & nb, NULL);
		if(bResult &&  nb == 0)
			break;	
		if(firstCheck)
		{
			int lengthIntCheck = lengthInt;
			char * type = (char*)( &lengthIntCheck);	
			if(bResult &&  nb == 0)
				break;
			type[2] = '\0';		
			QString typeElement = QString(type);		
			if(allElementTagTypes.indexOf(typeElement) != -1)
				return;
			firstCheck = false;
		}		
		if(size - currentByte < lengthInt)
				return;
		if(tagGroup == 0x7FE0 && tagAttr == 0x0010)	{
			lengthBody = lengthInt;
			return;
		}
		value = new char[lengthInt];
		bResult = this->ReadFile(DICOMFileHandle, value, lengthInt, (unsigned long*) & nb, NULL);		
		if(bResult &&  nb == 0 && lengthInt != 0)
		{
			delete [] value;	
			break;		
		}
		parseElement(tagGroup, tagAttr, value, lengthInt);	
		delete [] value;
		if(lengthInt == 0xFFFFFFFF){
		//	int h = 4;
			return;
		}
	}
	this->SetFilePointer(DICOMFileHandle, -2, NULL, FILE_CURRENT);		
	return;
}

void DicomParsingTask::checkPreamble(){
	this->SetFilePointer(DICOMFileHandle, 0, NULL, FILE_BEGIN);
	unsigned int nb;	
	int offset = 132;
	char iPointer [132 + 1];		
	BOOLEAN bResult = true;
	bResult = this->ReadFile(DICOMFileHandle, iPointer, offset, (unsigned long *) & nb, NULL);
	iPointer[132] = '\0';
	QString qStr ( &iPointer[128]);
	if(qStr.contains("DICM", Qt::CaseInsensitive))
	{
		
	}else{
		this->SetFilePointer(DICOMFileHandle, 0, NULL, FILE_BEGIN);
	}
	
}

void DicomParsingTask::parseVDRExternData()
{	
	int             count;
    unsigned short  grp;
    unsigned short  elm;
    unsigned char   code;
	int             isize;
    unsigned char   csize;
	char			Type;
	int pointer = 0;
	char * value;
	code = *(fileHeader->Rezerv + pointer);
	pointer ++;
	if(code != 0xDD)
		return;	
	code = *(fileHeader->Rezerv + pointer);
	pointer ++;
	count = (int) code;
	if(code == 0xE0){
		code = *(fileHeader->Rezerv + pointer);
		pointer ++;
		count = (int) code;
	}
	grp = *(fileHeader->Rezerv + pointer);
	pointer += 2;
	 for (int index = 0; index < count;)
	 {           
            elm = *((unsigned short *)(fileHeader->Rezerv + pointer));
			pointer += 2;
            if (elm == 0xE0DD) 
			{
                grp = *(fileHeader->Rezerv + pointer);
				pointer += 2;
                continue;
            }
			Type = *(fileHeader->Rezerv + pointer);
			pointer += 1;
			switch (Type) 
			{
				case '2':
				{
					value = new char[3];	
					memcpy(value, fileHeader->Rezerv + pointer, 2);					
					pointer += 2; 
					*(value + 2) = '\0';
					break;
				}
				case '4':
				{
					value = new char[5];	
					memcpy(value, fileHeader->Rezerv + pointer, 4);					
					pointer += 4; 
					*(value + 4) = '\0';
					break;
				}
				case '8':
				{
					value = new char[9];	
					memcpy(value, fileHeader->Rezerv + pointer, 9);					
					pointer += 8; 
					*(value + 8) = '\0';
					break;
				}
				case 'L':
				{
					csize = *(fileHeader->Rezerv + pointer);
					pointer += 1;
					isize = (int)csize;
					value = new char[isize + 1];	
					memcpy(value, fileHeader->Rezerv + pointer, isize);					
					pointer += isize; 
					*(value + isize) = '\0';
					break;		
				}
			}
			parseElement(grp, elm, value, isize);		
            index++;
        }		
	return;
}

BOOLEAN DicomParsingTask::parseVDRFile (const QString & fileName)
{
	unsigned int nb;	
	char string [7];		
	int length = sizeof(FileHeader);
	fileHeader = new FileHeader;
	this->SetFilePointer(DICOMFileHandle, 0, NULL, FILE_BEGIN);
	this->ReadFile(DICOMFileHandle, fileHeader, length, (unsigned long *) & nb, NULL);
	string[6] = '\0';
	memcpy(string, fileHeader, 6);
	QString WINVDRString(string);
	if(WINVDRString.contains("WINVDR") )
	{
		{
			QVariant var(QTime(fileHeader->TimDat.tm_year, fileHeader->TimDat.tm_mon, fileHeader->TimDat.tm_mday));
			TElement newElem( 0x00080020, var); //studyDate
			firstData->getELEM_CONT().add(0x0008, 0x0020, newElem);	
		}
		{
			QVariant var(QTime(fileHeader->TimDat.tm_year, fileHeader->TimDat.tm_mon, fileHeader->TimDat.tm_mday));
			TElement newElem( 0x00080022, var); //AcquisitionDate
			firstData->getELEM_CONT().add(0x0008, 0x0022, newElem);	
		}
		{
			QVariant var(QTime(fileHeader->TimDat.tm_hour, fileHeader->TimDat.tm_min, fileHeader->TimDat.tm_sec));		
			TElement newElem( 0x00080032, var); //AcquisitionTime
			firstData->getELEM_CONT().add(0x0008, 0x0032, newElem);	
		}
		firstData->isVDRFile = true;
		parseVDRExternData();
		firstData->imageParameters.width = fileHeader->x;
		firstData->imageParameters.height = fileHeader->y;
		if(fileHeader->Orient == 0){
			firstData->orient = 1;
		}else{
			firstData->orient = -1;
		}
		int lengthBuffer = 0;
		if(fileHeader->BPerPix == 0){
			firstData->imageParameters.bitsAllocated = 8;
			firstData->imageParameters.bitsStored = 8;			
			firstData->imageParameters.samplesPerPixel = 1;
			lengthBuffer = firstData->imageParameters.width * firstData->imageParameters.height * firstData->imageParameters.samplesPerPixel;			
		}
		if(fileHeader->BPerPix == 1){
			firstData->imageParameters.bitsAllocated = 16;
			firstData->imageParameters.bitsStored = 16;			
			firstData->imageParameters.samplesPerPixel = 1;		
			firstData->imageParameters.pixelRepresentation = 1;
			lengthBuffer = firstData->imageParameters.width * firstData->imageParameters.height * firstData->imageParameters.samplesPerPixel * 2;			
		}
		if(fileHeader->BPerPix == 3 || fileHeader->BPerPix == 2){
			firstData->imageParameters.bitsAllocated = 8;
			firstData->imageParameters.bitsStored = 8;			
			firstData->imageParameters.samplesPerPixel = 3;												
			lengthBuffer = firstData->imageParameters.width * firstData->imageParameters.height * firstData->imageParameters.samplesPerPixel;			
		}
		void * imageBuffer = new char [lengthBuffer];
		lengthBody = lengthBuffer;
		this->ReadFile(DICOMFileHandle, imageBuffer, lengthBuffer, (unsigned long *) & nb, NULL);			
		if(fileHeader->BPerPix == 3 || fileHeader->BPerPix == 2)
		{
			char buffer;		
			char * pImg = (char *)imageBuffer;
			for(int i = 0; i < firstData->imageParameters.width * firstData->imageParameters.height; i ++){				
				buffer = *(pImg + i * firstData->imageParameters.samplesPerPixel);

				*((char *) pImg + i * firstData->imageParameters.samplesPerPixel) = 
				*((char *) pImg + i * firstData->imageParameters.samplesPerPixel + 2);

				*((char *) pImg + i * firstData->imageParameters.samplesPerPixel + 2) = buffer;
			}
		}
		//!firstData->setImageBuffer(imageBuffer, lengthBuffer);
		firstData->setBody(imageBuffer, lengthBuffer);
		ImageLow * imageLow  = new ImageLow;
		ImagePar * imagePar  = new ImagePar;
		this->ReadFile(DICOMFileHandle, imageLow, sizeof(ImageLow), (unsigned long *) & nb, NULL);
		this->ReadFile(DICOMFileHandle, imagePar, sizeof(ImagePar), (unsigned long *) & nb, NULL);
		firstData->ScaleX = imagePar->ScaleX;
		firstData->ScaleY = imagePar->ScaleY;
		if(fileHeader->BPerPix == 0){
			if(imageLow->Low == 0){
				firstData->representationParameters.levelValue = (imageLow->Max - imageLow->Min) / 2;
				firstData->representationParameters.windowValue = (imageLow->Max - imageLow->Min);
			}
			if(imageLow->Low == 1){
				firstData->representationParameters.levelValue = (imageLow->Right - imageLow->Left) / 2;
				firstData->representationParameters.windowValue = (imageLow->Right - imageLow->Left) / 2;
			}
		}
		if(fileHeader->BPerPix == 1){			
			firstData->representationParameters.levelValue = imagePar->WindowCenterPos;
			firstData->representationParameters.windowValue = imagePar->WindowWidth;			
		}
		QVariant var;
		var.setValue(firstData->representationParameters.levelValue);
		{
			TElement newElem( 0x00281050, var);
			firstData->getELEM_CONT().add(0x0028, 0x1050, newElem);		
		}
		var.setValue(firstData->representationParameters.windowValue);
		{		
			TElement newElem2( 0x00281051, var);
			firstData->getELEM_CONT().add(0x0028, 0x1051, newElem2);		
		}
		firstData->doEmptyTags();	
		delete imageLow ;
		delete imagePar ;
		return true;

	}
	else{
		this->SetFilePointer(DICOMFileHandle, 0, NULL, FILE_BEGIN);
		return false;
	}
}



DWORD
WINAPI DicomParsingTask::SetFilePointer(__in        HANDLE hFile,
								   __in        LONG lDistanceToMove,
								   __inout_opt PLONG lpDistanceToMoveHigh,
								   __in        DWORD dwMoveMethod){
 	   if(dwMoveMethod == FILE_BEGIN){
		   pos = 0 + lDistanceToMove;
	   }
	   if(dwMoveMethod == FILE_CURRENT){
		   pos = pos + lDistanceToMove;
	   }
	   if(dwMoveMethod == FILE_END){
		   pos = size - 1 + lDistanceToMove;
	   }
	   if(pos > size)
		   pos = size;
	   bool result = dicomFile->seek(pos);	 
	   return result;										
}

BOOL WINAPI DicomParsingTask::ReadFile( __in HANDLE hFile, LPVOID lpBuffer, __in DWORD nNumberOfBytesToRead, __out_opt LPDWORD lpNumberOfBytesRead, __inout_opt LPOVERLAPPED lpOverlapped ){
	if(pos > size)
			pos = size;
	/*	if(count == 0){
			count = 10;
			emit signalLoadedFile(pos, size);
		}
		count --;*/
		pos += nNumberOfBytesToRead;
		if(pos > size)
			pos = size;		
		*lpNumberOfBytesRead = dicomFile->read((char *)lpBuffer, nNumberOfBytesToRead);	
		int a  = * (int *)lpBuffer;
		if(*lpNumberOfBytesRead == -1){
			return false;
		}
		else{
			return true;
		}
}


DicomData * DicomParsingTask::dataForParse()
{
	if(firstData && (firstData->isVDRFile || lengthBody == 0))
		return firstData;
	if(readBytes < lengthBody )
	{
		if(!dicomDataForParse && firstData)
			dicomDataForParse = new DicomData(*firstData);
	}else{
		dicomDataForParse = NULL;
	}
	if(dicomDataForParse)
	{
		dicomDataForParse->getELEM_CONT().makeHardTag(0x0001000A, curFrame);
		curFrame ++;
	}
	return dicomDataForParse;
}


DicomData * DicomParsingTask::parseUnit()
{
	if(!headIsParsed)
		parseHead();
	if(lengthBody == 0)
		return NULL;
	uint lengthInt = this->lengthBody;

	if(firstData->isVDRFile)
	{
		readBytes = lengthBody;		
		dicomDataForParse = NULL;
		lengthBody = 0;
		return firstData;
	}

	DicomData * dicomData = dataForParse();		
	if(dicomData == NULL)
		return NULL;
	unsigned int outputItemLength =  dicomData->imageParameters.width * dicomData->imageParameters.height * dicomData->imageParameters.samplesPerPixel * getSizePixel(dicomData); 
	//qDebug() << outputItemLength;
	unsigned int nb;	

	//value = new char[230400 * 2];
	//int bResult = this->ReadFile(DICOMFileHandle, value, 230400 * 2, (unsigned long *) & nb, NULL);	

 	if(!value)
	{
// 		if(outputItemLength > lengthInt){
// 			value = new char[outputItemLength + 100];
// 			int bResult = this->ReadFile(DICOMFileHandle, value, outputItemLength + 100, (unsigned long *) & nb, NULL);				
// 		}else{
// 			value = new char[lengthInt];
// 			int bResult = this->ReadFile(DICOMFileHandle, value, lengthInt, (unsigned long *) & nb, NULL);	
// 		}

		value = new char[lengthInt];
		int bResult = this->ReadFile(DICOMFileHandle, value, lengthInt, (unsigned long *) & nb, NULL);	
 	}
			
	if(TransferSyntax >= 4 && TransferSyntax <= 12)
	{
		unsigned int SaveByte = currentByte;		
		unsigned int length = 0;						
		unsigned char * valueOfItem;
		if(curByte == 0)
		{
			lengthOfbasicOffsetTable = *(unsigned int *)(value + curByte + 4);
			char * basicOffsetTable = (value + curByte + 8);							
			curByte += 8 + length;						
			unsigned int i = 0;			
			unsigned int nextOffset = 0;
			if(lengthOfbasicOffsetTable != 0)
				curOffset = *(unsigned int*)(basicOffsetTable + i * 4);					
			else
				curOffset = 0;
			if(i * 4 + 4 < lengthOfbasicOffsetTable)
				nextOffset = *(unsigned int*)(basicOffsetTable + i * 4 + 4);
			else
				nextOffset = 0xFFFFFFFF;	

			curByte = 8 + lengthOfbasicOffsetTable;
		}

		QVariant var(curByte);
		TElement newElem( 0x00200052, var);
		dicomData->getELEM_CONT().add(0x0020, 0x0052, newElem);	
			
		ItemTag = *(int *)(value + curOffset + curByte);
		if(ItemTag != 0xE000FFFE)
			int r = 43;
		if(ItemTag == 0xE000FFFE)
		{ 	
			length = *(int *)(value + curOffset + curByte + 4);			
			valueOfItem = ((unsigned char *)value + curOffset + curByte + 8);													
			curByte += 8 + length;
			ItemTag = *(int *)(value + curOffset + curByte);
			void * outputItem = new char [outputItemLength];
			//uint l;
			readBytes = curByte;
			dicomDataParser.parseData(dicomData, valueOfItem, length, outputItem, outputItemLength);
			dicomData->setBody(outputItem, outputItemLength);
			currentByte = SaveByte + curOffset + curByte;
		}else
		{
			qDebug() << "lengthIn t- " <<lengthInt << ", readBytes - " << readBytes;		
			return NULL;
		}


		//i++;						
		currentByte = currentByte + lengthInt + 4;						
	}else{
		if(lengthInt != 0xFFFFFFFF && 
			!(TransferSyntax >= 4 && TransferSyntax <= 12))
		{		
			currentByte = currentByte + lengthInt + 4;			
			int sizeP = getSizePixel(dicomData);			
			int i = 1;
			if(offsetPImage + outputItemLength <= lengthInt)
			{						
				char * outputItem = new char[outputItemLength];
				if(offsetPImage + outputItemLength > lengthInt)
					memcpy(outputItem, value + offsetPImage, lengthInt - offsetPImage);
				else
					memcpy(outputItem, value + offsetPImage, outputItemLength);							
				readBytes += outputItemLength;	
				dicomData->setBody(outputItem, outputItemLength);					
				offsetPImage += outputItemLength;	
				i++;
			}else
			{
				return NULL;
			}
		}
		else
		{							
		}
	}
	DicomData * dicomDataRet = dicomDataForParse;
	dicomDataForParse = NULL;
	return dicomDataRet;
}


const TElemCont * DicomParsingTask::getElementCont()
{
	if(firstData)
		return &firstData->getELEM_CONT();
	return new TElemCont();
}

void DicomParsingTask::takeMetaInformation()
{
	return;
	HTTPDicomFile * file = dynamic_cast<HTTPDicomFile * > (dicomFile);
	if(file)
	{
		if(firstData){
			firstData->getELEM_CONT().makeHardTag(0x00080018, file->getObjectUID());		
		
			QDomDocument doc( "imgInfo" );

			QString errorStr;
			int row, column;
			//if( !doc.setContent( file->metaInfornationXML(), false, &errorStr, &row, &column ) ){


			if( !doc.setContent(new QFile(file->metaInfornationXML()->fileName()) , false, &errorStr, &row, &column ) ){
				//QLOG_ERROR() << QString("������ xml �� ������ %1, ������� %2:").arg(row).arg(column);
				//QLOG_ERROR() << errorStr;
				QMessageBox::warning (NULL, "������ ��������� ������", "�������� ���������� XML-�����." );
				return;
			}

			// QDomElement topElem = doc.documentElement();
			QDomElement studySet = doc.documentElement();
			QDomElement study = studySet.firstChildElement();
			QDomElement series = study.firstChildElement();

			int total = 0, index = 1;
			series = study.firstChildElement();
			while( !series.isNull() )
			{
				QDomElement image = series.firstChildElement();

				while( !image.isNull() )
				{
	// 				TAttrMap attrMap;
	// 
	// 				foreach( const QString &attrName, m_attrsPassToParser )
	// 					if( image.hasAttribute(attrName) )
	// 						attrMap[attrName] = image.attribute( attrName );

					if( image.hasAttribute("instanceUid") && image.attribute("instanceUid") == file->getObjectUID())
					{
						if( study.hasAttribute("instanceUid"))
							firstData->getELEM_CONT().makeHardTag(StudyUID, study.attribute("instanceUid"));
						if( series.hasAttribute("instanceUid"))
							firstData->getELEM_CONT().makeHardTag(SeriesUID, series.attribute("instanceUid"));
						//firstData->getELEM_CONT().makeHardTag(SeriesUID, series.attribute("instanceUid"));	
						break;
					}
						//emit localImageLoaded( index, total, image.attribute("path"), attrMap );

					//++index;
					image = image.nextSiblingElement();
				}

				series = series.nextSiblingElement();
			}
		}
		
	}
}

