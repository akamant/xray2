#ifndef STUDYPARSINGTASK_H
#define STUDYPARSINGTASK_H

#include "dicomtask.h"

class StudyParsingTask : public QObject
{
	Q_OBJECT

public:
	StudyParsingTask(QString _studyID, QObject * parent = NULL);
	void addDicomTask(DicomTask * dicomTask);
	QString getID();
	DicomData * parseUnit();
	QList <DicomData * >preParsingList(){return _preParsingList;};
	bool inPreParsingList(DicomData * DD) {return _preParsingList.contains(DD);};
	~StudyParsingTask();
	bool isSorted(){return _isSorted;};

	//void sort();

private:	

	QList <DicomData * > _preParsingList;
	QStringList series;
	QStringList instances;
	QList <QString> instances2;
	bool _isSorted;
	QQueue <DicomTask *> dicomTaskQueue;
	QString studyID;
};

#endif // STUDYPARSINGTASK_H
