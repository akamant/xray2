#ifndef DICOMTASK_H
#define DICOMTASK_H

class DicomData;
class TElemCont;

class DicomFile;
class DicomParsingTask;

class DicomTask : public QObject
{
	Q_OBJECT

public:
	DicomTask(DicomFile * _dicomFile, QObject *parent = NULL);
	~DicomTask();
	QString getID();
	const TElemCont * getElementCont();
	DicomData * getUnit();	
	DicomData * dataForParse();
private:
	DicomParsingTask * dicomParsingTask;
	DicomFile * dicomFile;
};

#endif // DICOMTASK_H
