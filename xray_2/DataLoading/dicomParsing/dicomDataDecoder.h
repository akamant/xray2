#ifndef DATAPARSER_H
#define DATAPARSER_H

//#ifndef _X64
#include "JpegDecoder.h"
//#endif
#include "dicomdata.h"
class DicomDataDecoder : public QObject
{
	Q_OBJECT

public:
	DicomDataDecoder(QObject *parent = NULL);	
	void parseData(DicomData * dicomData, void * inputData, uint inputSize , void * outputData, uint & outputSize );
	~DicomDataDecoder();

private:
	DicomData * dicomData;
	JpegDecoder * jpegDecoder;
	void decompressJPEG(void * valueOfItem, int length, void * outputItem);
//	void decompressRLELossless(void * valueOfItem, int length,void * outputItem );
	void decompressRLELossless( void * valueOfItem, int length, void * outputItem, int outputSize );

};

#endif // DATAPARSER_H
