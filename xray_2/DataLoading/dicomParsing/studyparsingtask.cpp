#include "stdafx.h"
#include "studyparsingtask.h"
#include "elemCont.h"

StudyParsingTask::StudyParsingTask( QString _studyID, QObject * parent )
:	studyID(_studyID)
	,QObject(parent)	
{

}

StudyParsingTask::~StudyParsingTask()
{
		
}

DicomData * StudyParsingTask::parseUnit()
{
	DicomData * dicomData = NULL;
	while(!dicomData)
	{
		if(!dicomTaskQueue.empty())
		{
			DicomTask * task = dicomTaskQueue.first();
			if(task->dataForParse())
			{
	// 			if(isSorted) 
	// 				emit 
			}

			if(dicomData = task->getUnit())
			{	
				return dicomData;
			}
			dicomTaskQueue.dequeue();
			delete task;
		}
		else{
			return NULL;
		}
	}
	return NULL;	
}

//StudyParsingTask::task->dataForParse()

QString StudyParsingTask::getID()
{
	return studyID;
}

void StudyParsingTask::addDicomTask( DicomTask * dicomTask )
{		
	_isSorted = true;	
	//_isSorted = false;	
	QString seriesUID =  dicomTask->getElementCont()->get(SeriesUID).toString();
	if(!series.contains(seriesUID)){
		series.append(seriesUID);
		series.sort();
	}

	QString instanceUID =  dicomTask->getElementCont()->get(0x00080018).toString();
	if(!instances.contains(instanceUID)){
		instances.append(instanceUID);
		instances.sort();
		
	}

	int seriesNumber = 0;
	int seriesFactor = 0;
 	if( dicomTask->getElementCont()->hasElement(0x00200011))
	{
 		seriesNumber = dicomTask->getElementCont()->get(0x00200011).toInt();			
		seriesFactor = seriesNumber;
 	}else{
		seriesFactor = series.indexOf(seriesUID);
	}
	
	int instanceNumber = 0;
	int instanceFactor = 0;
 	if( dicomTask->getElementCont()->hasElement(0x00200013))
	{
 		instanceNumber = dicomTask->getElementCont()->get(0x00200013).toInt();			
		instanceFactor = instanceNumber;
 	}else{
		instanceFactor = instances.indexOf(instanceUID);
	}
		
	uint addedTaskkWeight = 0;

	int maxNumberInstancesInSeries = 10000;
	addedTaskkWeight = instanceFactor;
	addedTaskkWeight = seriesFactor * maxNumberInstancesInSeries + instanceFactor;

//  	if(addedTaskkWeight == 201)
//  		return;


	bool inserted = false;
	for(int i = 0; i < dicomTaskQueue.size(); i ++)
	{
		//DicomTask * dicomTaskCur = dicomTaskQueue.at(i);
		DicomTask * dicomTaskCur = dicomTaskQueue[i];
		QString seriesUIDCur =  dicomTaskCur->getElementCont()->get(SeriesUID).toString();
		QString instanceUIDCur =  dicomTaskCur->getElementCont()->get(0x00080018).toString();

		int seriesNumberCur = 0;
		int seriesFactorCur = 0;
 		if( dicomTaskCur->getElementCont()->hasElement(0x00200011))
		{
 			seriesNumberCur = dicomTaskCur->getElementCont()->get(0x00200011).toInt();			
			seriesFactorCur = seriesNumberCur;
 		}else{
			seriesFactorCur = series.indexOf(seriesUIDCur);
		}
		
		int instanceNumberCur = 0;
		int instanceFactorCur = 0;
 		if( dicomTaskCur->getElementCont()->hasElement(0x00200013))
		{
 			instanceNumberCur = dicomTaskCur->getElementCont()->get(0x00200013).toInt();			
			instanceFactorCur = instanceNumberCur;
 		}else{
			instanceFactorCur = instances.indexOf(instanceUIDCur);
		}

		//uint currentTaskWeight = seriesFactorCur * 100 + instanceFactorCur;
		uint currentTaskWeight = 0;
		currentTaskWeight = instanceFactorCur;
		currentTaskWeight = seriesFactorCur * maxNumberInstancesInSeries + instanceFactorCur;
		if(addedTaskkWeight < currentTaskWeight ){
			dicomTaskQueue.insert(i , dicomTask);
			inserted = true;
			break;
		}		
	}
	if(!inserted)
		dicomTaskQueue << dicomTask;

//////clear and refill /////////////////////////////////////////
	_preParsingList.clear();////////////////////////////////////
	for(int i = 0; i <dicomTaskQueue.size(); i ++){/////////////
		DicomTask * dicomTask = dicomTaskQueue[i];//////////////
		DicomData * dicomData = dicomTask->dataForParse();//////
		if(dicomData)///////////////////////////////////////////
			_preParsingList << dicomData;///////////////////////
	}///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
	//_preParsingList.clear();

	_isSorted = false;
}