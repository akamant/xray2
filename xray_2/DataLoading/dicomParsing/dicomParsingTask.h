#ifndef DICOMPARSINGTASK_H
#define DICOMPARSINGTASK_H

#include "DicomFile.h"
#include "elementParser.h"
#include "dicomdatadecoder.h"



class DicomParsingTask : public QObject
{
	Q_OBJECT
signals:
	void newDicomData(DicomData *);
public:
	DicomParsingTask (DicomFile * _dicomFile, QObject * parent = NULL);
	~DicomParsingTask();
	void parseHead();
	void takeMetaInformation();
	//void parseBody();
	DicomData * parseUnit();
	QVariant getAttribute(int tagGroup, int tagAttr);
	//DicomData * getDicomData(){return dicomData;};
	QString getID(){return studyID;};
	//DicomData * getFirstData(){return firstData;};
	DicomData * dataForParse();
	const TElemCont * getElementCont();

private:
	bool sortElementParsing;
	bool headIsParsed;
	int curFrame;
	DicomData * firstData;
	DicomData * dicomDataForParse;
	static DicomDataDecoder dicomDataParser;
	QString studyID;
	unsigned int pos;	
	int count;
	unsigned int size;
	unsigned int currentByte;
	DicomFile * dicomFile;


	///////////////////*/////////////////
	unsigned int offsetPImage;
	char * value;
	unsigned int ItemTag;
	unsigned int lengthOfbasicOffsetTable;
	unsigned int curByte;
	unsigned int curOffset;
	///////////////////////////

	QMap <QString, QString> correspondenceCodecs;
	void parse(const QString &filePath, const QString & params, const QString & filter);
	void parseFile(const QString &filePath, const QString & params = QString(""), const QString & filter = QString(""));
	//	void parseFile(const QString &filePath, const QString & params, const QString & filter);
//	void parseMemory(char * pointer, const QString & params = QString(""), const QString & filter= QString(""), int size = 0);
	//void initAndParsing( const QString & params, const QString & filter);	


	TElementParser * parser;
	BOOLEAN isFirstTag;
	int parentGroupTagSeq;
	int parentItemTagSeq;
	int parentGroupTagSeqPrev;
	int parentItemTagSeqPrev;
	HANDLE DICOMFileHandle;
	QString filePath;
	int curDirectoryRecordType;
	BOOLEAN isCreated;
	void parseVDRExternData();	
	BOOLEAN parseVDRFile (const QString & fileName);
	//BOOLEAN parseVDRFile (const QString & fileName);
	inline bool parseElement(int, int, char *, unsigned int);
	int TransferSyntax;		
	void * pImageDICOM;
	void * pImageApplication;
	//DICOMStruct  * dicomStruct;
	//DicomData * dicomData; 
//	DICOMFile * dicomFile;
	FileHeader * fileHeader;	
	//char * allTypes;			
	char * errorInformation;
	//char * firstTypes;
	static QStringList typeOneElementTagTypes;
	static QStringList allElementTagTypes;
	//BOOLEAN loadFile(const char *);
	BOOLEAN loadFile(const QString & filePath);
	BOOLEAN parseFileMetaInformation();
	void parseFileMetaInformationExplicitVRBigEndian();
	void parseFileMetaInformationExplicitVRLittleEndian();
	void parseFileMetaInformationImplicitVRBigEndian();
	void parseFileMetaInformationImplicitVRLittleEndian();
	void checkPreamble();
	void checkEndianAndExplicity();
	BOOLEAN parseBodyExplicitVRBigEndian();
	BOOLEAN parseBodyExplicitVRLittleEndian();
	BOOLEAN parseBodyImplicitVRBigEndian();
	BOOLEAN parseBodyImplicitVRLittleEndian();
	int parseBodyExplicitVRLittleEndianItemTag(unsigned int);
	int parseBodyExplicitVRLittleEndianReadSQ(unsigned int);
	int parseBodyExplicitVRLittleEndianReadOB(unsigned int);
	int parseBodyImplicitVRLittleEndianItemTag(unsigned int);
	int parseBodyImplicitVRLittleEndianReadSQ(unsigned int);		
	DWORD WINAPI SetFilePointer(__in HANDLE hFile, __in LONG lDistanceToMove, __inout_opt PLONG lpDistanceToMoveHigh, __in DWORD dwMoveMethod);
	BOOL WINAPI ReadFile( __in HANDLE hFile, LPVOID lpBuffer, __in DWORD nNumberOfBytesToRead, __out_opt LPDWORD lpNumberOfBytesRead, __inout_opt LPOVERLAPPED lpOverlapped );
	void doTagsFromVidarParams(const QString & params);
	void setVIDARFileID(const QString & filter);
	
	int parsDeeping;
	//inline void doBody();

	char * valueOfBody;
	uint lengthBody;
	uint readBytes;
	
};

#endif // DICOMPARSINGTASK_H
