#include "stdafx.h"
#include "dicomtask.h"
#include "dicomFile.h"
#include "dicomParsingTask.h"


DicomTask::DicomTask(DicomFile * _dicomFile, QObject * parent)
	:dicomFile(_dicomFile) 
	,QObject(parent)
{
  dicomParsingTask = new DicomParsingTask(dicomFile);	

  //if(dicomFile)
  //if()
 
}

DicomTask::~DicomTask()
{
	delete dicomParsingTask;
}

DicomData * DicomTask::dataForParse(){
	return dicomParsingTask->dataForParse();
}


QString DicomTask::getID()
{
	return dicomParsingTask->getID();
}


DicomData * DicomTask::getUnit()
{
	return dicomParsingTask->parseUnit();
}


const TElemCont *DicomTask::getElementCont()
{
	return dicomParsingTask->getElementCont();
}

