#ifndef DICOMPARSER_H
#define DICOMPARSER_H

#include "studyparsingtask.h"
#include "DicomFile.h"
#include "dicomdata.h"

typedef QList<DicomFile *> DicomFileList;

class TreeDataModel;

class ParsingDispatcher : public QObject
{
	Q_OBJECT
signals:
	void startLoading();
	void endLoading();
	void newDicomDataPtr(const DicomDataPtr &);
	public slots:
		//void processNewDicomData(DicomData *);
		//void processRecivedObjectInformation( QList <DicomFile *> recievedObjects );
		void processStartedRecieveObjects(DicomFileList);
public:
		ParsingDispatcher(QThread * _thread, TreeDataModel * _treeDataModel = NULL, QObject *parent = NULL);
		~ParsingDispatcher();		
		//void processStudyTasks();		
		void processStudyTasks(QQueue<StudyParsingTask *> studyParsingTaskQueue );
private:
		QList <DicomData *> preParsingList;
		//QQueue<StudyParsingTask *> studyParsingTaskQueue;
		bool startedStudyProcessingQueue;
		QThread * thread;
		TreeDataModel * treeDataModel;
};

#endif // DICOMPARSER_H
