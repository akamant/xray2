#ifndef HTTPDICOMFILE_H
#define HTTPDICOMFILE_H

#include "dicomfile.h"

class HTTPDicomFile : public DicomFile
{
	Q_OBJECT
signals:
	void finished();
public:
	HTTPDicomFile(QNetworkAccessManager & _networkManager, const QUrl & _sourceURL, const QString & _ObjectUID);
	~HTTPDicomFile();
	qint64	read ( char * data, qint64 maxSize );
	bool	seek ( qint64 pos );
	uint size();
//	virtual QIODevice * source(){ return reply;};
	void setMetaInfornationXML(QFile *__metaInfornationXML) {_metaInfornationXML = __metaInfornationXML;};

	QFile * metaInfornationXML() {return _metaInfornationXML;};
	QString getObjectUID (){return objectUID;};
public slots:
	void start();

protected slots:
	void finishedSlot(){ emit finished(); };
	void downloadProgressSlot(qint64 cur, qint64 total);
	
private:
	QFile * _metaInfornationXML;
	QBuffer * buffer;
	QNetworkAccessManager & networkManager;
	QUrl sourceURL; 
	QString objectUID;
	QNetworkReply * reply;		
	
	
};

#endif // HTTPDICOMFILE_H
