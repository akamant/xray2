#pragma once

class DicomFile : public QObject	
{
	Q_OBJECT	
public:		
	DicomFile();
	~DicomFile(void);
	virtual qint64	read ( char * data, qint64 maxSize ) = 0;
	virtual bool	seek ( qint64 pos ) = 0;
	virtual uint size() = 0;
	
protected:
	//QIODevice * device;
};

