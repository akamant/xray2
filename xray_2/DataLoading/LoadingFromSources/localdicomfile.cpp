#include "stdafx.h"

#include "localdicomfile.h"
#include "qslog.h"

LocalDicomFile::LocalDicomFile(const QString & _source)
	: DicomFile()
{
 	file.setFileName(_source);			
	if(!file.open(QIODevice::ReadOnly))
	{
		QLOG_ERROR() << "cant open file - " + _source;
	}
	//device = &file;
}

LocalDicomFile::~LocalDicomFile()
{

}

qint64 LocalDicomFile::read( char * data, qint64 maxSize )
{			
	return file.read(data, maxSize );
}

bool LocalDicomFile::seek( qint64 pos )
{	
	return file.seek(pos);
}

uint LocalDicomFile::size()
{
	return file.size();
}