#pragma once

#include "abstractloader.h"

class LocalLoader :	public AbstractLoader	
{
	Q_OBJECT
public:
	LocalLoader(QObject * parent);
	void load(const QStringList & fileList);
	~LocalLoader(void);	
public slots:
	void startedLoop();
private:
	bool isLoopEventStarted;
	//QMutex mutex;
};


