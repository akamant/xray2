#ifndef HTTPLOADER_H
#define HTTPLOADER_H

#include "abstractloader.h"

class HttpLoader : public AbstractLoader
{
	Q_OBJECT

public:
	HttpLoader(QObject *parent);
	~HttpLoader();
	QString getVrefFileFromURL (QUrl url);	
	QUrl getStudyURLFromVref( const QString & fileName );
	void loadStudy( const QUrl & url );
	//void replyFinishedINI(QNetworkReply* qReply);	
	void loadFromVrefFilePath(QString url);
	QNetworkAccessManager networkManager;
	QUrl serverSourceURL;
private slots:
	//void replyFinishedINI(QNetworkReply * reply, QUrl serverSourceURL);	
	void replyFinishedINI(QNetworkReply* reply);
	void errorDownloadINI (QNetworkReply::NetworkError error);
	void replyFinishedXML(QNetworkReply* reply);
	void errorDownloadXML (QNetworkReply::NetworkError error);
	
private:

	QFile * fileXML;
	
};

#endif // HTTPLOADER_H
