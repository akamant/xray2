#include "stdafx.h"
#include "httpdicomfile.h"

HTTPDicomFile::HTTPDicomFile(QNetworkAccessManager & _networkManager, const QUrl & _sourceURL, const QString & _ObjectUID)
	: 
	DicomFile()
	, networkManager(_networkManager)
	 ,sourceURL(_sourceURL)
	 ,objectUID (_ObjectUID)
	 ,reply(NULL)
	 ,buffer(NULL)
	// ,id ()
{
// 	buffer = new QBuffer();
// 	if (!buffer->open(QIODevice::WriteOnly | QIODevice::ReadOnly)) 			
// 		return; 
}

HTTPDicomFile::~HTTPDicomFile()
{

}

void HTTPDicomFile::start( )
{
	QUrl url;		
	QList<QPair<QString, QString> > arglist;
	arglist.append( QPair<QString, QString>( "requestType", "WADO") );
	arglist.append( QPair<QString, QString>( "objectUID", objectUID));	
	url.setScheme("http");
	url.setHost(sourceURL.host());
	url.setPort(sourceURL.port());
	url.setPath(sourceURL.path());
	
	url.setQueryItems( arglist );	
	QNetworkRequest networkRequest;
	networkRequest.setUrl(url);
	networkRequest.setRawHeader("Connection", "keep-alive");
	networkRequest.setRawHeader("Keep-Alive", "300");	
	reply = networkManager.get(networkRequest);			
	buffer = new QBuffer();
	if (!buffer->open(QIODevice::WriteOnly | QIODevice::ReadOnly)) 			
			return; 	
	connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(downloadProgressSlot(qint64, qint64)));	
	connect(reply, SIGNAL(finished()), this, SLOT(finishedSlot()));	
	

	
}

void HTTPDicomFile::downloadProgressSlot(qint64 cur, qint64 total)
{
	//qDebug() << reply->bytesAvailable();
	//qDebug() << cur;
	//qDebug() << total;

	if(cur == total)
	{
		buffer->write(reply->readAll());
		if (!buffer->open(QIODevice::WriteOnly | QIODevice::ReadOnly)) { 			
			return; 
		}
		return;
		QFile file;
		file.setFileName("1.txt");  		
		if (!file.open(QIODevice::WriteOnly)) { 			
			return; 
		}	
		reply->seek(0);	
		file.write(buffer->readAll());	
		file.close();			
	}
}

qint64 HTTPDicomFile::read( char * data, qint64 maxSize )
{
	//return 0;
	//qDebug() << reply->bytesAvailable();

	int size = buffer->size();


	while(buffer == NULL || maxSize + buffer->pos() > buffer->size())
		QApplication::processEvents();

	//return reply->read(data, maxSize );
	return buffer->read(data, maxSize );

}

bool HTTPDicomFile::seek( qint64 pos )
{
	//return true;
	while(buffer == NULL || pos + buffer->pos() > buffer->size())
		QApplication::processEvents();
	return buffer->seek(pos);
	//return reply->seek(pos);
}

uint HTTPDicomFile::size()
{
	while(buffer == NULL || ! buffer->size())
		QApplication::processEvents();
	return buffer->size();
	return reply->size();
}


// const QByteArray * AbstractLoader::get( int objectID, uint size )
//{
	//while(!reply->finished()){

	//}
	//return &dicomFiles[objectID]->read(size);
//}