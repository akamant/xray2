#ifndef LOCALDICOMFILE_H
#define LOCALDICOMFILE_H

#include <DicomFile.h>

class LocalDicomFile : public DicomFile
{
	Q_OBJECT

public:
	LocalDicomFile(const QString & sourse);
	~LocalDicomFile();
	QFile file;

	virtual qint64	read ( char * data, qint64 maxSize );
	virtual bool	seek ( qint64 pos );
	virtual uint size();
//	virtual QIODevice * source(){ return device;};
private:
	
};

#endif // LOCALDICOMFILE_H
