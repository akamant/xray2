#include "stdafx.h"
#include "httploader.h"
#include "httpdicomfile.h"
#include "qslog.h"

HttpLoader::HttpLoader(QObject *parent)
	: AbstractLoader(parent)
{

}

HttpLoader::~HttpLoader()
{

}

void HttpLoader::loadFromVrefFilePath(QString frefFilePath){
	 loadStudy	(getStudyURLFromVref(frefFilePath) );
}

QString HttpLoader::getVrefFileFromURL (QUrl url) {	
	QString fileName(QCoreApplication::applicationDirPath() + QString("\\xray.href"));		
	QFile file(fileName);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);	
	out << url.toString();
	file.close();
	return fileName;

}

QUrl HttpLoader::getStudyURLFromVref( const QString & fileName )
{
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return QUrl();
	QTextStream in(&file);

	QString urlStr("");
	while (!in.atEnd()) {
		QString line = in.readLine();
		urlStr.append(line);
	}

	return QUrl(urlStr);
}

void HttpLoader::loadStudy( const QUrl & url )
{
	serverSourceURL = url;

	QNetworkRequest networkRequest;		
	networkRequest.setUrl(QUrl (serverSourceURL.toString() + "&format=xml.1"));
	networkRequest.setRawHeader("Connection", "keep-alive");
	networkRequest.setRawHeader("Keep-Alive","300");
	QNetworkReply * currentReply = networkManager.get(networkRequest); 	
	connect(currentReply,		SIGNAL	(error				(QNetworkReply::NetworkError)),
			this,				SLOT	(errorDownloadXML	(QNetworkReply::NetworkError))
			);	
	connect(&networkManager,	SIGNAL	(finished			(QNetworkReply*)),
			this,				SLOT	(replyFinishedXML	(QNetworkReply*))
			);
}


void HttpLoader::errorDownloadINI	(QNetworkReply::NetworkError error){	
	QString str = QString("INI downloading error - %1").arg(error);
	QLOG_ERROR() << str;
}


void HttpLoader::errorDownloadXML	(QNetworkReply::NetworkError error){	
	QString str = QString("XML downloading error - %1").arg(error);
	QLOG_ERROR() << str;
}


void HttpLoader::replyFinishedXML(QNetworkReply* reply){
	disconnect(&networkManager,		SIGNAL	(finished			(QNetworkReply*)),
				this,				SLOT	(replyFinishedXML	(QNetworkReply*))
		);	

	fileXML = new QFile();
	fileXML->setFileName(QCoreApplication::applicationDirPath() + "\\metainformation.xml") ;	
	if (!fileXML->open(QIODevice::WriteOnly)) { 		
		return; 
	} 		
	fileXML->write(reply->readAll());
	fileXML->close();	
	reply->deleteLater();


	QNetworkRequest networkRequest;		
	networkRequest.setUrl(serverSourceURL);
	networkRequest.setRawHeader("Connection", "keep-alive");
	networkRequest.setRawHeader("Keep-Alive","300");
	QNetworkReply * currentReply = networkManager.get(networkRequest); 	
	connect(currentReply,		SIGNAL	(error				(QNetworkReply::NetworkError)),
			this,				SLOT	(errorDownloadINI	(QNetworkReply::NetworkError))
		);	
	connect(&networkManager,	SIGNAL	(finished			(QNetworkReply*)),
			this,				SLOT	(replyFinishedINI	(QNetworkReply*))
		);
}

void HttpLoader::replyFinishedINI(QNetworkReply* reply)
{
		disconnect(&networkManager,		SIGNAL	(finished			(QNetworkReply*)),
					this,				SLOT	(replyFinishedINI	(QNetworkReply*))
		);	

	QFile fileINI(QCoreApplication::applicationDirPath() + "\\wado.ini") ;	
	if (!fileINI.open(QIODevice::WriteOnly)) { 
// 		if(!noMessage)
// 			QMessageBox::warning(NULL, tr("HTTP Get"), 
// 			tr("Cannot write file %1\n%2.") 
// 			.arg(fileINI.fileName()) 
// 			.arg(fileINI.errorString())); 
		return; 
	} 		
	fileINI.write(reply->readAll());
	fileINI.close();	
	reply->deleteLater();
	QSettings settings ( fileINI.fileName(), QSettings::IniFormat);
	QStringList groups = settings.childGroups();
	int numberInGroup = 3;
	QList<HTTPDicomFile *> firstGroup;
	QList<HTTPDicomFile *> curGroup;
	QList<HTTPDicomFile *> prevGroup;

	QStringList UIDS;
	for(int i = 0; i < groups.size(); i++){
		settings.beginGroup(groups[i]);		
		UIDS.append(settings.allKeys());
		settings.endGroup();
	}	

	QList<DicomFile *> recievedListDF;
	HTTPDicomFile * httpDicomFilePrev;
	HTTPDicomFile * httpDicomFileFirst;
	for(int i = 0; i < UIDS.size(); i++)
	{
		HTTPDicomFile * httpDicomFile = new HTTPDicomFile(networkManager, serverSourceURL, UIDS[i]);		
		httpDicomFile->setMetaInfornationXML(fileXML);
		if(i != 0){
			connect(	httpDicomFilePrev,		SIGNAL	(finished	()),
						httpDicomFile,			SLOT	(start		())
					);
		}else{
			httpDicomFileFirst = httpDicomFile;
		}
		if(i == 1)
			httpDicomFileFirst->start();
		
		httpDicomFilePrev = httpDicomFile;
	//	httpDicomFile->start();


		recievedListDF << httpDicomFile;	

	}
	
	emit objectsRecieved(recievedListDF);
				

			//TDownloadFilesThread * myThread = new TDownloadFilesThread (threadUIDS, imageListURL, QCoreApplication::applicationDirPath(), noMessage, curFilter, firstNumber);
			//threads << myThread;
			//myThread->start();		

		////firstNumber += def;	

		//emit startedAllThreads();

}