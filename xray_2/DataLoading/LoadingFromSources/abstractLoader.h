#ifndef ABSTRACTLOADER_H
#define ABSTRACTLOADER_H

#include "DicomFile.h"

typedef QList<DicomFile *> DicomFileList;
class AbstractLoader : public QObject
{
	Q_OBJECT

public:
	AbstractLoader(QObject *parent);
	const QByteArray * get(int objectID, uint size);	
	~AbstractLoader();	
signals:	
	void objectsRecieved(DicomFileList);
protected:
	QHash<int, DicomFile *> dicomFiles;
	int hashKey;
private:
	//QQueue <loadQ> dicomFiles;

};

#endif // ABSTRACTLOADER_H
