#include "stdafx.h"
#include "LocalLoader.h"
#include "LocalDicomFile.h"


QStringList getAllfilesFromSubDirs(const QString & filePath){	
	QStringList lstFiles;	
	int res = 0;
	QFileInfo fileInfo(filePath);
	if( fileInfo.isFile() && fileInfo.size() != 0){
		lstFiles << filePath;
		return lstFiles;
	}
	QDir dir(filePath);
	//�������� ������ ���������
	QStringList lstDirs  = dir.entryList(QDir::Dirs |
		QDir::AllDirs |
		QDir::NoDotAndDotDot);
	//�������� ������ ������   
	QStringList files = dir.entryList(QDir::Files);
	//������� �����
	foreach (QString entry, files)
	{
		QString entryAbsPath = dir.absolutePath() + "/" + entry;
		lstFiles << entryAbsPath;
	}
	//��� ����� ������ ����������� �����    
	foreach (QString entry, lstDirs)
	{
		QString entryAbsPath = dir.absolutePath() + "/" + entry;
		lstFiles << getAllfilesFromSubDirs(entryAbsPath);
	}
	return lstFiles;
}

int removeFolder(QDir & dir)
{
	int res = 0;
	//�������� ������ ���������
	QStringList lstDirs  = dir.entryList(QDir::Dirs  |
		QDir::AllDirs |
		QDir::NoDotAndDotDot);
	//�������� ������ ������   
	QStringList lstFiles = dir.entryList(QDir::Files);
	//������� �����
	foreach (QString entry, lstFiles)
	{
		QString entryAbsPath = dir.absolutePath() + "/" + entry;
		QFile::remove(entryAbsPath);
	}
	//��� ����� ������ ����������� �����    
	foreach (QString entry, lstDirs)
	{
		QString entryAbsPath = dir.absolutePath() + "/" + entry;
		removeFolder(QDir(entryAbsPath));
	}
	//������� �������������� �����
	if (!QDir().rmdir(dir.absolutePath()))
	{
		res = 1;
	}
	return res;
}



LocalLoader::LocalLoader( QObject * parent )
	:AbstractLoader(parent)
{
	//qRegisterMetaType< DicomFileList >( "DicomFileList" );
}
LocalLoader::~LocalLoader(void)
{
}



void LocalLoader::load( const QStringList &fileList )
{	
	//QDir dir(filePath);
	QStringList files;
	for(int i = 0; i < fileList.size(); i++){			
		files  << getAllfilesFromSubDirs(fileList[i]);				
	}

	QList<uint> recievedList;
	QList<DicomFile *> recievedListDF;
	for(int i = 0; i < files.size(); i++){		
		//DicomFile * DF = new DicomFile(files[i]);		
		LocalDicomFile * DF = new LocalDicomFile(files[i]);		
		recievedListDF << DF;		
	}	
	emit objectsRecieved(recievedListDF);

}

void LocalLoader::startedLoop()
{

}
