#pragma once

struct jpeg_error_mgr;
struct jpeg_decompress_struct;
struct jpeg_source_mgr;


/*
** Define enclosures for include files with C linkage (mostly system headers)
*/
#ifdef __cplusplus
#define BEGIN_EXTERN_C extern "C" {
#define END_EXTERN_C }
#else
#define BEGIN_EXTERN_C
#define END_EXTERN_C
#endif


/*
BEGIN_EXTERN_C
//#undef boolean
#define boolean ijg_boolean
#include "jpeglib8.h"
#include "jerror8.h"
#include "jpegint8.h"
#undef boolean
END_EXTERN_C
*/



class JpegDecoderXX
{
public:
	JpegDecoderXX(void);
	JpegDecoderXX(void * inputData, int length, void * outputData);
	virtual ~JpegDecoderXX(){

	}
	void initData(void * inputData, int length, void * outputData);
	virtual void decompress();
	virtual void initJpeg();
	virtual void destroyJpeg();
protected:
	void * inputData;
	int length;
	void * outputData;
	jpeg_error_mgr * jpegErr;
	jpeg_decompress_struct * jpegInfo;
	BOOLEAN isJpegStart;
	jpeg_source_mgr * jpegSrcManager;	
	void * buffer;
	int bufsize;
	int suspension;
};
