#pragma once
#include "jpegdecoderxx.h"
class JpegDecoder16 
	:public JpegDecoderXX
{
public:
	JpegDecoder16(void);
	~JpegDecoder16(void);
	JpegDecoder16(void * inputData, int length, void * outputData);
	virtual void decompress();	
	virtual void initJpeg();
	virtual void destroyJpeg();
};

