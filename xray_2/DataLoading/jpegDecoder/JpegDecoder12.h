#pragma once
#include "jpegdecoderxx.h"
class JpegDecoder12 
	:public JpegDecoderXX
{
public:
	JpegDecoder12(void);	
	JpegDecoder12(void * inputData, int length, void * outputData);
	~JpegDecoder12(void);	
	virtual void decompress();
	virtual void initJpeg();
	virtual void destroyJpeg();
};

