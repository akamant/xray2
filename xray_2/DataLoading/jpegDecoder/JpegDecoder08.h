#pragma once
#include "jpegdecoderxx.h"
class JpegDecoder08 :
	public JpegDecoderXX
{
public:
	JpegDecoder08(void);
	~JpegDecoder08();
	JpegDecoder08(void * inputData, int length, void * outputData);
	virtual void decompress();
	virtual void initJpeg();
	virtual void destroyJpeg();
};

