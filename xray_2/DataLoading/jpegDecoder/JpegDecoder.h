#pragma once
#include "JpegDecoderxx.h"
#include "JpegDecoder08.h"
#include "JpegDecoder12.h"
#include "JpegDecoder16.h"
class JpegDecoder
{
public:
	JpegDecoder(void);
	JpegDecoder(void * inputData, int length, void * outputData, int depth);
	void initData(void * inputData, int length, void * outputData,int depth);
	BOOLEAN decompress();
	~JpegDecoder(void);	
private:
	int depth;
	JpegDecoderXX * jpegDecoderXX;
	BOOLEAN createPlanarConfigurationWord( unsigned short *imageFrame, unsigned short columns, unsigned short rows);
	unsigned char scanJpegDataForBitDepth( const unsigned char *data, const unsigned int fragmentLength);
	unsigned short readshort(const unsigned char *data);
	void * inputData;
	int length;
	void * outputData;
};

