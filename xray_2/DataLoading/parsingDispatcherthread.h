#ifndef DICOMPARSERTHREAD_H
#define DICOMPARSERTHREAD_H

#include "parsingdispatcher.h"

class ParsingDispatcherThread : public QThread
{
	Q_OBJECT

public:
	ParsingDispatcherThread(QObject *parent);
	~ParsingDispatcherThread();
	ParsingDispatcher * dicomParser;
private:
	
protected:
	void run();

	public slots:
void startedLoop();
private:
	bool isLoopEventStarted;
//	QMutex mutex;
};

#endif // DICOMPARSERTHREAD_H
