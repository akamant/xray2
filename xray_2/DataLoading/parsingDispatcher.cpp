#include "stdafx.h"
#include "parsingdispatcher.h"
#include "dicomtask.h"
#include "treedatamodel.h"

ParsingDispatcher::ParsingDispatcher(QThread * _thread, TreeDataModel * _treeDataModel, QObject *parent)
	: QObject(parent)
	, startedStudyProcessingQueue(false)
	, thread(_thread)
	, treeDataModel(_treeDataModel)
{
//	thread->setPriority(QThread::LowestPriority);
	qRegisterMetaType< DicomDataPtr >( "DicomDataPtr" );
}

ParsingDispatcher::~ParsingDispatcher()
{
	
}

//��������� ������ ������� ��� ������ � ��������� ��������� �� ��������� � � �������������� ��������


void ParsingDispatcher::processStudyTasks(QQueue<StudyParsingTask *> studyParsingTaskQueue )
{
	emit startLoading();
	thread->setPriority(QThread::LowestPriority);
	DicomData * dicomData = NULL;
	while(studyParsingTaskQueue.size())
	{
		if(studyParsingTaskQueue.size() != 0)
		{			
			StudyParsingTask * task = studyParsingTaskQueue.first();
			if(dicomData = task->parseUnit())
			{				
				DicomDataPtr ptr (dicomData);
				emit newDicomDataPtr (ptr);
			}else
			{
				studyParsingTaskQueue.dequeue();
				delete task;
			}
		}else{
			thread->setPriority(QThread::LowestPriority);
			emit endLoading();
			return;
		}
	}
	thread->setPriority(QThread::LowestPriority);
	emit endLoading();	
}


/*
void ParsingDispatcher::processStartedRecieveObjects(DicomFileList startedRecieveObjects)
{
	///QString a = dicomFile->fileName();
	emit startLoading();
	for(int i = 0; i < startedRecieveObjects.size(); i ++)
	{
		QQueue<StudyParsingTask *> studyParsingTaskQueue;
		DicomFile * dicomFile = startedRecieveObjects[i];
		DicomTask * dicomTask = new DicomTask(dicomFile);
		StudyParsingTask * studyParsingTask = new StudyParsingTask(dicomTask->getID(), this);
		studyParsingTask->addDicomTask(dicomTask);
		studyParsingTaskQueue << studyParsingTask;
		processStudyTasks(studyParsingTaskQueue);
		studyParsingTaskQueue.clear();
	}
	emit endLoading();
}
*/



//	��������������� ����� ���������� �������� ��������(��������) �������� �� ������
//	����� ���������� � ��� ���� ������������ �� ����������.. ������� ��� ������ � ����
//	������� - studyParsingTaskQueue.size() > 0


void ParsingDispatcher::processStartedRecieveObjects(DicomFileList startedRecieveObjects)
{
	///QString a = dicomFile->fileName();
	emit startLoading();
	//thread->setPriority(QThread::NormalPriority);
	QQueue<StudyParsingTask *> studyParsingTaskQueue;
	for(int i = 0; i < startedRecieveObjects.size(); i ++)
	{
		DicomFile * dicomFile = startedRecieveObjects[i];
		DicomTask * dicomTask = new DicomTask(dicomFile);
		bool isNewDicomTaskBegin = true;
		for(int i = 0; i <  studyParsingTaskQueue.size(); i++)
		{
			if(dicomTask->getID() == studyParsingTaskQueue[i]->getID()) // 
			{
				studyParsingTaskQueue[i]->addDicomTask(dicomTask);
				isNewDicomTaskBegin = false;
			}
		}
		// ����
		if(isNewDicomTaskBegin)
		{
			if(studyParsingTaskQueue.size() > 0)
			{
				StudyParsingTask * task = studyParsingTaskQueue.last();
				//���� ������� ��� ������������ ��� � ��� � ������. ������� �� 
				// ������ ������� ����������� ������ � ��� ��� ��� ������� ���������
				if(task && task->isSorted())
				{
					for(int j = 0; j < task->preParsingList().size(); j ++)
					{				
						DicomData * dicomData = task->preParsingList()[j];			
						if(dicomData)
						{
						//	if(!treeDataModel->haveDicomStructInstance())
								//emit newDicomData (dicomData);							
						}
					}
				}
				//this->studyParsingTaskQueue << studyParsingTaskQueue;
 				processStudyTasks(studyParsingTaskQueue);

				studyParsingTaskQueue.clear();
// 				taskQueue.clear();
			}
			//after processing last study task add new task

			if(!dicomTask->getID().contains("noNameStudy")){
				StudyParsingTask * studyParsingTask = new StudyParsingTask(dicomTask->getID(), this);
				studyParsingTask->addDicomTask(dicomTask);
				studyParsingTaskQueue << studyParsingTask;
			}
		}
	}

	if(studyParsingTaskQueue.size() > 0)
	{
		StudyParsingTask * task = studyParsingTaskQueue.last();
		//when data come from network, that can not be sorted
		if(task && task->isSorted())
		{
			for(int j = 0; j < task->preParsingList().size(); j ++)
			{
				DicomData * dicomData = task->preParsingList()[j];
				if(dicomData){
					//if(!treeDataModel->haveDicomStructInstance())
						//emit newDicomData (dicomData);
				}
			}
			//this->studyParsingTaskQueue << studyParsingTaskQueue;

			processStudyTasks(studyParsingTaskQueue);
			studyParsingTaskQueue.clear();
		}
	}
	processStudyTasks(studyParsingTaskQueue);
 	studyParsingTaskQueue.clear();
	
	emit endLoading();
}
