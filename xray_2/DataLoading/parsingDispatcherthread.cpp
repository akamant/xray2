#include "StdAfx.h"
#include "parsingdispatcherthread.h"

ParsingDispatcherThread::ParsingDispatcherThread(QObject *parent)
	: QThread(parent)
	, dicomParser(NULL)
	,isLoopEventStarted(false)
{
	//dicomParser = new ParsingDispatcher(this, NULL);
	//return;
	connect	(	this,		SIGNAL	(started		()	),
				this,		SLOT	(startedLoop	()	)
			);
	dicomParser = new ParsingDispatcher(this, NULL);
	dicomParser->moveToThread(this);
	qDebug() << "dicomParser = new ParsingDispatcher";
	start();
	while(!isLoopEventStarted)
		QCoreApplication::processEvents(QEventLoop::AllEvents);
	int a = 3;
}

ParsingDispatcherThread::~ParsingDispatcherThread()
{

}

void ParsingDispatcherThread::run()
{
	
	exec();
}

void ParsingDispatcherThread::startedLoop(){	
	isLoopEventStarted = true;
	qDebug() << "startedLoop";
}


