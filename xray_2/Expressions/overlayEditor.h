#ifndef OVERLAYEDITOR_H
#define OVERLAYEDITOR_H


#include "overlayDict.h"

class TExpressionDict;
class TOverlayDict;
class TOverlayTreeWidget;
class TElemCont;
class TElemContList;

///////////////////////////////////////////////////////////////////////////
// TOverlayEditor /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TOverlayEditor : public QDialog
{
	Q_OBJECT
public:
	TOverlayEditor( TExpressionDict *eDict, TOverlayDict *oDict, TElemContList &cList, const QString &mod );

private slots:
	void slotFocusChanged( TOverlayTreeWidget *tr, bool state );
	//
	void slotCurrentModChanged( const QString &text );
	void slotCornerDataChanged();
	//
	void slotAddExpression();
	void slotInsertEmptyString();
	void slotDelExpression();
	void slotOk();
	void slotSave();

private:
	QList<QVector<QString>> prepareExprList( const QStringList &list );
	//
	bool trySaveMod();
	void setDicts( const QString &mod );
	void fillFields();
	void setChanged( bool changed );
	bool deleteEmptyStrings();
	//
	TOvrMod::EPos getActiveCornerPos();
	TOverlayTreeWidget* getCornerTree( TOvrMod::EPos pos );
	//
	const TOvrMod& getCurrentMod();
	//
	TOvrMod::EPos activeCornerPos;
	bool changed;
	TExpressionDict *exprDict;
	TOverlayDict	*overlayDict;
	QStringList		mods;
	TOvrMod			curMod;
	TElemContList	&contList;
	//
	//left-to-right top-to-bottom
	QVector<TOverlayTreeWidget*> m_trees;
	TOverlayTreeWidget *active;

	QComboBox	*cmbMod;
	QPushButton *ok, *save, *cancel;
	QAction		*actInsertExpr
			,	*actDeleteExpr
			,	*actInsertEmptyString;
};

#endif