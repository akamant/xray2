#ifndef EXPRDICT_H
#define EXPRDICT_H


///////////////////////////////////////////////////////////////////////////
// TArgument ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
struct TArgument
{
	QVariant value;
	int precision;
};

///////////////////////////////////////////////////////////////////////////
// TExpression ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
struct TExpression
{
	void clear();
	//
	QString name;
	QString expression;
	QString description;
	QList<TArgument> args;
};

///////////////////////////////////////////////////////////////////////////
// TExpressionDict ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TExpressionDict
{
//TEMPORARY
public:
	static TExpressionDict *getDict();

public:
//	QString	execExpr( QString name );
	TExpression exprVar( QString name );
	QStringList exprNames();
	bool hasExpr( const QString &name ) const;
	//
	void updateExpr( QString name, TExpression newV );
	void deleteExpr( QString name );
	
	//
	bool loadDict( QString path );
	bool saveDict( QString path );
private:
	QMap <QString, TExpression> exprMap;
};

#endif //EXPRDICT_H