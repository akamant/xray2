#ifndef XMLTAGLOADER_H
#define XMLTAGLOADER_H

///////////////////////////////////////////////////////////////////////////
// TXmlTagLoader //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TDicomDict;
class TXmlTagHandler;
//
class TXmlTagLoader
{
public:
	TXmlTagLoader( QMap <uint, TDicomElement> *tagMp
				 , QMap <QString, EType> *typeMp );
	//
	bool loadAll(  QFile &file );
	bool loadOne(  QFile &file, uint t );
	//
	void saveDict( QFile &file );
	
private:
	QXmlSimpleReader *reader;
	TXmlTagHandler	 *handler;
	//
	QMap <uint, TDicomElement>		*tagMap;
	QMap <QString, EType>	*typeMap;
};



#endif //XMLTAGLOADER_H