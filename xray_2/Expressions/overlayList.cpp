#include "stdafx.h"
#include "overlayList.h"
#include "overlayDict.h"
#include "exprDict.h"
#include "exprBuilder.h"

///////////////////////////////////////////////////////////
// TOverlayMimeData ///////////////////////////////////////
///////////////////////////////////////////////////////////
//
class TOverlayMimeData : public QMimeData
{
public:
	TOverlayMimeData( TOvrCorner *cor, int ind )
		: QMimeData(), corner(cor), index(ind)
	{
		setData("application/x-qabstractitemmodeldatalist", "");
	}
	//
	TOvrCorner *corner;
	int index;
};

///////////////////////////////////////////////////////////
// TOverlayTreeWidget /////////////////////////////////////
///////////////////////////////////////////////////////////
//
TOverlayTreeWidget::TOverlayTreeWidget( TExpressionDict *eDict, TElemContList &contList, QWidget *parent )
: QTreeWidget(parent)
, active(false)
, exprDict(eDict)
, m_contList(contList)
{
	setHeaders();

	setFocusPolicy( Qt::StrongFocus );
	setRootIsDecorated( false );
	setSelectionBehavior( QAbstractItemView::SelectRows );

	setDragEnabled( true );
	setDefaultDropAction( Qt::MoveAction );
	setDragDropMode( QAbstractItemView::DragDrop );

	setMouseTracking( true );
}

//
void TOverlayTreeWidget::setHeaders()
{
	QTreeWidgetItem *headerItem = new QTreeWidgetItem;

	headerItem->setIcon( 0, QIcon(":/images/gray/overlay/display.png") );
	headerItem->setToolTip( 0, "���������� �� ������" );

	headerItem->setText( 1, "���������" );

	headerItem->setText( 2, "��������" );

	headerItem->setIcon( 3, QIcon(":/images/gray/overlay/printer.png") );
	headerItem->setToolTip( 3, "���������� ��� ������" );

	headerItem->setTextAlignment( 0, Qt::AlignHCenter );
	headerItem->setTextAlignment( 3, Qt::AlignHCenter );

	setHeaderItem( headerItem );

	header()->setResizeMode( 0, QHeaderView::Fixed );
	header()->setResizeMode( 3, QHeaderView::Fixed );

	header()->setResizeMode( 1, QHeaderView::Stretch );
	header()->setResizeMode( 2, QHeaderView::Stretch );

	header()->setStretchLastSection( false );

	resizeColumnToContents( 0 );
	resizeColumnToContents( 3 );
}

//
void TOverlayTreeWidget::setOvrCorner( TOvrCorner *cor )
{
	corner = cor;
	fillFields();
}

//
void TOverlayTreeWidget::fillFields()
{
	clear();
	foreach( const TOvrExpression &expr, corner->eList )
		addItemFromExpr( expr );
}

//
void TOverlayTreeWidget::setActive( bool state )
{
	if( active != state ){
		active = state;
		viewport()->update(rect());
	}
}

//
void TOverlayTreeWidget::addExpression(TOvrExpression &expr)
{
	corner->addExpr( expr );
	addItemFromExpr(expr);

	emit cornerDataChanged();
}

//
void TOverlayTreeWidget::addItemFromExpr( const TOvrExpression &expr )
{
	QTreeWidgetItem *item = new QTreeWidgetItem;
	addTopLevelItem(item);
	
	QCheckBox *cbS = new QCheckBox;
	cbS->setChecked( expr.m_show );
	cbS->setFocusProxy( this );
	cbS->setCursor( Qt::ArrowCursor );
	cbS->setObjectName( "overlayCB" );
	connect( cbS, SIGNAL(stateChanged(int)), SLOT(slotItemChanged(int)) );

	QCheckBox *cbP = new QCheckBox;
	cbP->setChecked( expr.m_onPrinter);
	cbP->setFocusProxy( this );
	cbP->setCursor( Qt::ArrowCursor );
	cbP->setObjectName( "overlayCB" );
	connect( cbP, SIGNAL(stateChanged(int)), SLOT(slotItemChanged(int)) );

	if( exprDict->hasExpr(expr.m_name) ){
		item->setText( 1, exprDict->exprVar(expr.m_name).description );

		TExprBuilder builder( exprDict, m_contList );
		item->setText( 2, builder.execExpr(expr.m_name) );
	}

	item->setText( 4, expr.m_name );

	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addStretch();
	hl1->addWidget( cbS );
	hl1->addStretch();
	QWidget *tmp1 = new QWidget;
	tmp1->setLayout( hl1 );
	setItemWidget( item, 0, tmp1 );

	QHBoxLayout *hl2 = new QHBoxLayout;
	hl2->addStretch();
	hl2->addWidget( cbP );
	hl2->addStretch();
	QWidget *tmp2 = new QWidget;
	tmp2->setLayout( hl2 );
	setItemWidget( item, 3, tmp2 );

	item->setTextAlignment( 0, Qt::AlignHCenter );
	item->setTextAlignment( 3, Qt::AlignHCenter );
	
	checkExprExistance(item);
}

//
void TOverlayTreeWidget::checkExprExistance( QTreeWidgetItem *item )
{
	QString name = item->text(4);
	if( name.isEmpty() || exprDict->hasExpr(name) )
		item->setTextColor( 1, Qt::black );
	else
		item->setTextColor( 1, Qt::red );
}

//
void TOverlayTreeWidget::removeCurrentExpression()
{
	int n = currentIndex().row();
	delete takeTopLevelItem( n );
	corner->eList.removeAt(n);
	
	emit cornerDataChanged();
}

//
QTreeWidgetItem* TOverlayTreeWidget::itemOfItemWidget( QWidget *widget, int column )
{
	int count = topLevelItemCount(), i = 0;
	for(; i < count; i++){
		QTreeWidgetItem *item = topLevelItem( i );
		if( widget->parentWidget() == itemWidget(item,column) )
			return item;
	}
	return NULL;
}

//
void TOverlayTreeWidget::slotItemChanged( int state )
{
	QWidget *w =  qobject_cast<QWidget*>(sender());
	QTreeWidgetItem *item = itemOfItemWidget( qobject_cast<QWidget*>(sender()), 0 );
	if( item == NULL )
		item = itemOfItemWidget( qobject_cast<QWidget*>(sender()), 3 );
	
	if( item )
		updateData( item );
	else
		qDebug() << "TOverlayTreeWidget::slotItemChanged: item = NULL";
}

//
void TOverlayTreeWidget::updateData( QTreeWidgetItem *item )
{
	QCheckBox *cbS = qobject_cast<QCheckBox*>( itemWidget(item, 0)->layout()->itemAt(1)->widget() );
	QCheckBox *cbP = qobject_cast<QCheckBox*>( itemWidget(item, 3)->layout()->itemAt(1)->widget() );

	int index = indexOfTopLevelItem( item );
	corner->eList[index].m_show = cbS->checkState() == Qt::Checked;
	corner->eList[index].m_onPrinter = cbP->checkState() == Qt::Checked;

	emit cornerDataChanged();
}

//
void TOverlayTreeWidget::mouseDoubleClickEvent( QMouseEvent *event )
{
	if( itemAt(event->pos()) == NULL )
		emit itemDoubleClicked( NULL, 0 );
	QTreeWidget::mouseDoubleClickEvent( event );
}

//
void TOverlayTreeWidget::mouseMoveEvent( QMouseEvent *event )
{
	QModelIndex index = indexAt(event->pos());
	if( index.isValid() && (index.column()==1 || index.column()==2) )
		setCursor( Qt::OpenHandCursor );
	else
		setCursor( Qt::ArrowCursor );

	QTreeWidget::mouseMoveEvent( event );
}

//
void TOverlayTreeWidget::focusInEvent( QFocusEvent *event )
{
	emit focusChanged(this, true);
	QTreeWidget::focusInEvent(event);
}

//
bool TOverlayTreeWidget::dropMimeData( QTreeWidgetItem * parent, int index, const QMimeData * data, Qt::DropAction action )
{
	const TOverlayMimeData *mmd = dynamic_cast<const TOverlayMimeData*>(data);
	TOvrExpression exp = mmd->corner->eList.takeAt(mmd->index);
	emit cornerDataChanged();

	if( parent ){
		int n = indexOfTopLevelItem( parent );
		if( above )
			corner->eList.insert( n, exp );
		else
			corner->eList.insert( n+1, exp );
	}
	else
		corner->eList.insert( index, exp );

	fillFields();
	
	setFocus();

	return true;
}

//
void TOverlayTreeWidget::dropEvent(QDropEvent *event)
{
	emit cornerDataChanged();

	QTreeWidgetItem *item = itemAt( event->pos() );
	if( item )
		if( event->pos().y() < visualItemRect(item).center().y() )
			above = true;

	if( event->source() == this && event->dropAction() == Qt::MoveAction ){
		QTreeWidgetItem *itemDD = currentItem();
		int indItemDD = indexOfTopLevelItem( itemDD ), indToPaste;
	
		if( item ){
			int indItemAbove = indexOfTopLevelItem( item );
		
			indToPaste = above ? indItemAbove : indItemAbove + 1;
			if( indItemDD < indToPaste ) indToPaste--;
		}
		else
			indToPaste = topLevelItemCount() - 1;
		
		const TOverlayMimeData *mmd = dynamic_cast<const TOverlayMimeData*>(event->mimeData());
		
		TOvrExpression exp = corner->eList.takeAt(mmd->index);

		corner->eList.insert( indToPaste, exp );
		
		event->accept();
		event->setDropAction(Qt::CopyAction);

		fillFields();
	}
	QTreeWidget::dropEvent( event );
};

//
QMimeData* TOverlayTreeWidget::mimeData( const QList<QTreeWidgetItem*> items ) const
{
	return new TOverlayMimeData( corner, indexOfTopLevelItem(items.first()) );
}

//
void TOverlayTreeWidget::paintEvent(QPaintEvent* ev)
{	
	QPainter painter(viewport());
	
	QRect rect = this->rect();
		rect.setRight(rect.right()-1);
		rect.setBottom(rect.bottom()-1);
		QBrush brush( Qt::SolidPattern );

	if( active )
		brush.setColor( QColor(200,200,250) );
	else
		brush.setColor( QColor(204,199,186) );
	
	painter.fillRect(rect,brush);
	painter.setPen(QColor(0,0,0));
	painter.drawRect(rect);

	QTreeWidget::paintEvent(ev);
}