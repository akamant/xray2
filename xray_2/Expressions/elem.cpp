#include "stdafx.h"
#include "elem.h"

///////////////////////////////////////////////////////////////////////////
// TElement ///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
void TElement::addChild(TElement &elem)
{
	TElementList list = variant.value<TElementList>();
	list << elem;
	variant.setValue<TElementList>( list );
}

//
char *TElement::toNewChar() const
{
	QByteArray tb = variant.toString().toAscii();
	int n = tb.count() + 1;
	char *tc = new char[n];
	strncpy_s( tc, n, tb.constData(), n );
	return tc;
}

//
float TElement::toFloat() const
{
	if( variant.type() == QMetaType::Float )
		return variant.toFloat();
	else
		return variant.toList()[0].toFloat();
}

//
bool TElement::hasChildren() const
{
	return variant.canConvert<TElementList>();
}