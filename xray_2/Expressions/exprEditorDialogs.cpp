#include "stdafx.h"
#include "exprEditorDialogs.h"

///////////////////////////////////////////////////////////////////////////
// TElementsDialog ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TElementsDialog::TElementsDialog( TElemContList &cList )
: QDialog(NULL)
, contList(cList)
{
	setWindowTitle( "������ ���������:" );

	list = new QListWidget(this);
	connect( list, SIGNAL(itemDoubleClicked(QListWidgetItem*)), SLOT(accept()) ); 

	QPushButton	*ok		= new QPushButton("��");
	connect( ok,		SIGNAL(clicked()), SLOT(accept()) );
	QPushButton	*cancel = new QPushButton("������");
	connect( cancel,	SIGNAL(clicked()), SLOT(reject()) );

	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addWidget( ok );
	hl1->addWidget( cancel );

	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addWidget( list );
	vl1->addLayout( hl1 );

	setLayout( vl1 );
	setGeometry( 400, 300, 400, 550 );
		
	fillFields();
}

//
void TElementsDialog::fillFields()
{
	QList<uint> tagList = contList.tagList();
	foreach( uint tag, tagList ){
		QString str("%1:\t%2");
		str = str.arg( tag, 8, 16, QChar('0') ).arg( contList.get(tag).toString() );
		list->addItem( str );
	}
	list->setCurrentRow( 0 );
}

//
uint TElementsDialog::tag()
{
	QString str = list->currentItem()->text();
	return str.left(8).toUInt( NULL, 16 );
}

///////////////////////////////////////////////////////////////////////////
// TExpressionsDialog ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TExpressionsDialog::TExpressionsDialog( TExpressionDict *eDict, const QString &exceptExpr )
: QDialog(NULL)
, exprDict(eDict)
{
	setWindowTitle( "������ ���������:" );

	list = new QListWidget(this);
	connect( list, SIGNAL(itemDoubleClicked(QListWidgetItem*)), SLOT(accept()) ); 

	QPushButton	*ok		= new QPushButton("��");
	connect( ok,		SIGNAL(clicked()), SLOT(accept()) );
	QPushButton	*cancel = new QPushButton("������");
	connect( cancel,	SIGNAL(clicked()), SLOT(reject()) );

	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addWidget( ok );
	hl1->addWidget( cancel );

	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addWidget( list );
	vl1->addLayout( hl1 );

	setLayout( vl1 );
	setGeometry( 400, 300, 400, 550 );
		
	fillFields(exceptExpr);
}

//
void TExpressionsDialog::fillFields( const QString &exceptExpr )
{
	QStringList eList = exprDict->exprNames();
	eList.removeAll( exceptExpr );
	foreach( QString expr, eList ){
		list->addItem( expr );
	}
	list->setCurrentRow( 0 );
}

//
QString TExpressionsDialog::expression()
{
	return list->currentItem()->text();
}

///////////////////////////////////////////////////////////////////////////
// TArgChangeDialog ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TArgChangeDialog::TArgChangeDialog(	TArgument var, TElemContList &cList, TDicomDict *dDict, TExpressionDict *eDict )
: QDialog(NULL)
, argument(var)
, contList(cList)
, dicomDict(dDict)
, exprDict(eDict)
{
	setWindowTitle( "��������" );

	QPushButton *ok		= new QPushButton( "��",		this );
	connect( ok,		SIGNAL(clicked()), SLOT(accept()) );
	QPushButton *cancel	= new QPushButton( "��������",	this );
	connect( cancel,	SIGNAL(clicked()), SLOT(reject()) );
	QPushButton *list	= new QPushButton( "�������",	this );
	connect( list,		SIGNAL(clicked()), SLOT(slotTakeFromList()) );

	tag  = new QRadioButton( "���",		this );
//	connect( tag, SIGNAL(clicked(bool)), list, SLOT(setEnabled(bool)) ); 
	expr = new QRadioButton( "���������",	this );
//	connect( expr, SIGNAL(clicked(bool)), list, SLOT(setDisabled(bool)) ); 
	
	value = new QLineEdit( this );
	connect( value, SIGNAL(textChanged(const QString&)), SLOT(slotTextChanged(const QString&)) ); 

	precision = new QSpinBox( this );
	precision->setEnabled(false);
	precision->setMinimum( -1 );
	precision->setMaximum( 5 );
	precision->setSingleStep( 1 );

	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addWidget( tag );
	vl1->addWidget( expr );
	
	QGroupBox *gb = new QGroupBox( "���:", this );
	gb->setLayout( vl1 );
	
	QVBoxLayout *vl2 = new QVBoxLayout;
	vl2->addWidget( new QLabel("��������:") );
	vl2->addWidget( value );
	vl2->addWidget( list );
	vl2->addWidget( precision );
	vl2->addStretch( 1 );

	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addWidget( gb );
	hl1->addLayout( vl2 );

	QHBoxLayout *hl2 = new QHBoxLayout;
	hl2->addWidget( ok );
	hl2->addWidget( cancel );

	QVBoxLayout *vl3 = new QVBoxLayout;
	vl3->addLayout( hl1 );
	vl3->addLayout( hl2 );

	setLayout( vl3 );

	tag->setChecked(true);
	fillFields();

	value->setFocus();
}

//
void TArgChangeDialog::fillFields()
{
	if( argument.value.type() == QVariant::UInt ){
		tag->setChecked( true );
		value->setText( QString("%1").arg(argument.value.toUInt(), 8, 16, QChar('0')) );
		updatePrecision(argument.value.toUInt());
	}
	else
	if( argument.value.type() == QVariant::String ){
		expr->setChecked( true );
		value->setText( argument.value.toString() );
	}
}

//
void TArgChangeDialog::accept()
{
	QString str = value->text();
	
	if( tag->isChecked() ){
		bool ok;
		uint n = str.toUInt( &ok, 16 );
		if( ok ){
			argument.value = n;
			if( precision->isEnabled() ){
				uint p = precision->text().toUInt(&ok);
				if( ok )	
					argument.precision = p;
				else{
					QMessageBox::warning( this, "������", "������������ �������� ��������" );
					return;
				}
			}
//			else argument.precision = -2;
		}
	}
	else{
		if( !str.isEmpty() )
			argument.value = str;
	}
	
	QDialog::accept();
}

//
void TArgChangeDialog::slotTakeFromList()
{
	if( tag->isChecked() ){
		TElementsDialog dlg(contList);
		if( dlg.exec() == QDialog::Accepted ){
			argument.value = dlg.tag();
			fillFields();
		}
	}
	else
	if( expr->isChecked() ){
		TExpressionsDialog dlg(exprDict);
		if( dlg.exec() == QDialog::Accepted ){
			argument.value = dlg.expression();
			fillFields();
		}
	}
}

//
void TArgChangeDialog::slotTextChanged(const QString &text)
{
	if( tag->isChecked() ){
		bool ok;
		uint n = value->text().toUInt( &ok, 16 );
		if( ok )
			updatePrecision(n);
	}
}

//
void TArgChangeDialog::updatePrecision(uint precTag )
{
	EType type = dicomDict->getType(precTag);
	if( type == DS ){
		precision->setEnabled(true);
		if( argument.precision == -2 )
			argument.precision = 2;
		precision->setValue( argument.precision );
	}
	else{
		precision->setEnabled(false);
		precision->cleanText();
	}
}

//
bool TArgChangeDialog::checkValue()
{
	bool ok;
	uint n = value->text().toUInt( &ok, 16 );
	if( ok ){
		argument.value = n;
		return true;
	}
	return false;
}