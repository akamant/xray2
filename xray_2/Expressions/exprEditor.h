#ifndef EXPREDITOR_H
#define EXPREDITOR_H


#include "elemCont.h"
#include "dicomDict.h"
#include "exprDict.h"

//class TDicomDict;

///////////////////////////////////////////////////////////////////////////
// TExprEditor ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TExprEditor : public QDialog
{
	Q_OBJECT
public:
	TExprEditor( TExpressionDict *dict, TElemContList &cList,  TDicomDict *dDict );

private slots:
	void slotCurrentItemChanged( QListWidgetItem *item, QListWidgetItem *prev );
	void slotItemChanged( QListWidgetItem *item );
	void slotTempChanged(const QString &text );
	void slotDescriptionChanged(const QString &text );
	void slotArgClicked(QListWidgetItem *item);
	//
	void slotAddExpr();
	void slotDeleteExpr();
	void slotShowElements();
	void slotSave();
	void slotNSave();
	void slotOk();
	void slotCancel();

private:
	void loadDict();
	void updateExpression();
	void updateTagAndResult();
	void parseTemplate();
	//
	void setChanged( bool changed = true );
	//
	bool isChanged;
	bool creatingNewExpr;
	int realArgCount;
//	TElemCont *elemCont;
	TElemContList &contList;
	TExpressionDict *exprDict;
	TDicomDict *dicomDict;
	TExpression curExpr, origExpr;
	//
	QListWidgetItem *newItem;
	QListWidget *exprList
			,	*argsList;
	QLineEdit	*tempString
			,	*resultString
			,	*descriptionString;
	QPushButton *ok
			,	*cancel
			,	*add
			,	*del
			,	*save
			,	*nsave
			,	*showElements;
	QAction		*actAdd
			,	*actDel
			,	*actSaveExpr;
};

#endif