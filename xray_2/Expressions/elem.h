#ifndef ELEM_H
#define ELEM_H
///////////////////////////////////////////////////////////////////////////
// TElement ///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
typedef unsigned __int16 uint16;
typedef unsigned __int32 uint32;

struct TTagPair
{
	uint16 gID
	,	   eID;
	TTagPair() : gID(0), eID(0) {}
	TTagPair( uint16 groupID, uint16 elemID ) : gID(groupID), eID(elemID) {}

	bool operator == ( const TTagPair &v ) const { return gID == v.gID && eID == v.eID; }
	bool operator != ( const TTagPair &v ) const { return !operator==(v); }
};
Q_DECLARE_METATYPE( TTagPair );
Q_DECLARE_METATYPE( uint16 );
Q_DECLARE_METATYPE( uint32 );

struct TElement;
typedef QList<TElement> TElementList;
//
struct TElement
{
	TElement(){}
	TElement( QVariant var ): variant(var) {}
	TElement( uint t, QVariant var ): variant(var), tag(t) {}
	uint tag;
	QVariant variant;
	//
	bool isValid() { return variant.isValid(); }
	template <class T>
	bool canConvert( T ) const { return variant.canConvert<T>(); }
	//
	int 		toInt()				const { return variant.toInt();		}
	uint16		toUShort()			const { return variant.value<uint16>(); }
	uint32		toULong()			const { return variant.value<uint32>(); }
	float		toFloat()			const;
	double		toDouble()			const { return variant.toDouble();	}
	QString 	toString()			const { return variant.toString();	}
	QDate		toDate()			const { return variant.toDate();	}
	QTime		toTime()			const { return variant.toTime();	}
	QDateTime	toDateTime()		const { return variant.toDateTime();}

	QVariantList toList()			const { return variant.toList();	}
	char *		toNewChar()			const;
	//
	QList<TElement>	getChildren()	const { return variant.value<TElementList>(); }
	void			setChildren( TElementList &list)	{ variant.setValue<TElementList>(list); }
	void			addChild( TElement &elem );
	bool			hasChildren() const;
};

Q_DECLARE_METATYPE( TElement );
Q_DECLARE_METATYPE( TElementList );

#endif //ELEM_H