#include "stdafx.h"
#include "overlayAddExpressionDialog.h"
#include "expressions/exprDict.h"

///////////////////////////////////////////////////////////////////////////
// TAddExpressionDialog ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TAddExpressionDialog::TAddExpressionDialog( QList<QVector<QString>> &exprList
										, const QList<TOvrExpression> &cornerExpr
										, QStringList &values )
: QDialog(NULL)
, m_changed(false)
, m_expressions(cornerExpr)
{
	setWindowTitle( "�������������� �������" );

	QPushButton *ok		= new QPushButton( "��" );
	QPushButton *cancel = new QPushButton( "������" );

	m_moveToCorner = new QPushButton( ">>" );
	m_moveToCorner->setToolTip( "�������� � �������" );
	m_moveToCorner->setEnabled( false );
	connect( m_moveToCorner, SIGNAL(clicked()), SLOT(slotMoveTo()) );

	m_removeFromCorner = new QPushButton( "<<" );
	m_removeFromCorner->setToolTip( "������� �� �������" );
	m_removeFromCorner->setEnabled( false );
	connect( m_removeFromCorner, SIGNAL(clicked()), SLOT(slotRemoveFrom()) );

	m_insertEmpty = new QPushButton( "" );
	m_insertEmpty->setToolTip( "�������� ������ ������" );
	m_insertEmpty->setEnabled( false );
	connect( m_insertEmpty, SIGNAL(clicked()), SLOT(slotInsertEmpty()) );

/**
	QPushButton *moveUp = new QPushButton( "�����" );
	moveUp->setToolTip( "����������� �����" );
	connect( moveUp, SIGNAL(clicked()), SLOT(slotMoveUp()) );
	QPushButton *moveDown = new QPushButton( "����" );
	moveDown->setToolTip( "����������� ����" );
	connect( moveDown, SIGNAL(clicked()), SLOT(slotMoveDown()) );
	QPushButton *remove = new QPushButton( "�������" );
	moveDown->setToolTip( "������� �� �������" );
	connect( remove, SIGNAL(clicked()), SLOT(slotRemove()) );
/**/

	m_treeAll = new QTreeWidget;
	m_treeAll->setHeaderLabels( QStringList() << "���������" << "��������" );
	m_treeAll->setRootIsDecorated(false);
	m_treeAll->setSelectionBehavior( QAbstractItemView::SelectRows);
	connect( m_treeAll, SIGNAL(itemSelectionChanged()), SLOT(slotSelectionChanges()) );

	m_treeCorner = new QTreeWidget;
	m_treeCorner->setHeaderLabels( QStringList() << "���������" << "��������" );
	m_treeCorner->setRootIsDecorated(false);
	m_treeCorner->setSelectionBehavior( QAbstractItemView::SelectRows);
	connect( m_treeCorner, SIGNAL(itemSelectionChanged()), SLOT(slotSelectionChanges()) );

	fillData( exprList, cornerExpr, values );

	connect( ok,		SIGNAL(clicked()), SLOT(accept()) );
	connect( cancel,	SIGNAL(clicked()), SLOT(reject()) );
	
	QVBoxLayout *vl11 = new QVBoxLayout;
	vl11->addWidget( new QLabel("��� ���������:") );
	vl11->addWidget( m_treeAll );

	QVBoxLayout *vl12 = new QVBoxLayout;
	vl12->addStretch();
	vl12->addWidget( m_moveToCorner );
	vl12->addWidget( m_removeFromCorner );
	vl12->addWidget( m_insertEmpty );
	vl12->addStretch();
/**
	vl12->addWidget( moveUp );
	vl12->addWidget( moveDown );
	vl12->addWidget( remove );
	vl12->addStretch();
/**/

	QVBoxLayout *vl13 = new QVBoxLayout;
	vl13->addWidget( new QLabel("��������� � ��������� �������:") );
	vl13->addWidget( m_treeCorner );

	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addLayout( vl11 );
	hl1->addLayout( vl12 );
	hl1->addLayout( vl13 );

	QHBoxLayout *hl2 = new QHBoxLayout;
	hl2->addWidget( ok );
	hl2->addWidget( cancel );

	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addLayout( hl1 );
	vl1->addLayout( hl2 );

	setLayout( vl1 );

	m_treeAll->setFocus();
}

//
void TAddExpressionDialog::fillData( QList<QVector<QString>> &exprList
									, const QList<TOvrExpression> &cornerExpr
									, QStringList &values )
{
	foreach( const QVector<QString> &vec, exprList ){
		QTreeWidgetItem *item = new QTreeWidgetItem;
		item->setText( 0, vec[1] );
		item->setText( 1, vec[2] );
		item->setText( 2, vec[0] );
		m_treeAll->addTopLevelItem( item );
	}

	TExpressionDict *exprDict = TExpressionDict::getDict();

	QStringList::iterator beg = values.begin();
	foreach( const TOvrExpression &exp, cornerExpr ){
		QTreeWidgetItem *item = new QTreeWidgetItem;
		if( exprDict->hasExpr(exp.m_name) )
			item->setText( 0, exprDict->exprVar(exp.m_name).description );
		item->setText( 1, *beg );
		item->setText( 2, exp.m_name );

		m_treeCorner->addTopLevelItem( item );
		++beg;
	}

	m_treeAll->header()->setResizeMode( QHeaderView::Stretch );
	m_treeCorner->header()->setResizeMode( QHeaderView::Stretch );
}

//
void TAddExpressionDialog::slotMoveTo()
{
	QTreeWidgetItem *item = m_treeAll->currentItem();
	if( item == NULL )
		return;

	TOvrExpression expr( item->text(2), "", true, false, false );
	m_expressions << expr;

	QTreeWidgetItem *newItem = new QTreeWidgetItem;
	newItem->setText( 0, item->text(0) );
	newItem->setText( 1, item->text(1) );
	newItem->setText( 2, item->text(2) );
	m_treeCorner->addTopLevelItem( newItem );

	m_changed = true;
}

//
void TAddExpressionDialog::slotRemoveFrom()
{
	QTreeWidgetItem *item = m_treeCorner->currentItem();
	if( item == NULL )
		return;

	delete m_treeCorner->takeTopLevelItem( m_treeCorner->indexOfTopLevelItem(item) );
	
	m_changed = true;
}

//
void TAddExpressionDialog::slotSelectionChanges()
{
	bool enableMove = ( qobject_cast<QTreeWidget*>(QObject::sender()) == m_treeAll );

	m_moveToCorner->setEnabled( enableMove );
	m_removeFromCorner->setEnabled( !enableMove );
	m_insertEmpty->setEnabled( !enableMove );
}

//
void TAddExpressionDialog::slotInsertEmpty()
{

}