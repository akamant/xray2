#include "stdafx.h"

#include "overlayDict.h"

///////////////////////////////////////////////////////////////////////////
// TOvrExpression /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TOvrExpression::TOvrExpression()
: m_show(true)
, m_onlyFirst(false)
, m_onPrinter(true)
{}

//
TOvrExpression::TOvrExpression( const QString &str, const QString &desc, bool show, bool first, bool printer )
: m_name(str)
, m_description(desc)
, m_show(show)
, m_onlyFirst(first)
, m_onPrinter(printer)
{}

///////////////////////////////////////////////////////////////////////////
// TExprXmlhandler ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TOvrXmlhandler : public QXmlDefaultHandler 
{ 
public: 
	TOvrXmlhandler( QMap <QString, TOvrMod> *modMp ):modMap(modMp) {}
	//
	bool startElement(	const QString &namespaceURI, 
						const QString &localName, 
						const QString &qName, 
						const QXmlAttributes &attribs)
	{
		if( qName == "modality" ){
			curMod.modality	= attribs.value("name");
			curMod.description = attribs.value("description");
		}
		else
		if( qName == "corner" )
			pos = TOvrMod::stringToEPos( attribs.value("hpos"), attribs.value("vpos") );
		else
		if( qName == "expression" ){
			bool show = (attribs.value("display") == "true" || attribs.value("display").isEmpty()) ? true : false;
//			bool onlyFirst = attribs.value("show") == "true" ? true : false;
			bool onPrinter = attribs.value("onPrinter") == "false" ? false : true;
			curMod.corner(pos).eList << TOvrExpression( attribs.value("name"), "", show, true, onPrinter );
		}
		return true; 
	}
	//
	bool endElement( const QString &namespaceURI, const QString &localName, const QString &qName )
	{
		if( qName == "modality" ){
			modMap->insert( curMod.modality, curMod );
			curMod.clear();
		}
		return true;
	}

private:
	TOvrMod curMod;
	TOvrMod::EPos pos;
	QMap <QString, TOvrMod> *modMap;
};

///////////////////////////////////////////////////////////////////////////
// TOvrMod ////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TOvrMod::TOvrMod()
{
	genCorners();
}

//
TOvrMod::TOvrMod(QString name)
: modality(name)
{
	genCorners();
}

//
TOvrMod::EPos TOvrMod::stringToEPos(QString &hPos, QString &vPos)
{
	if( hPos == "left"	&& vPos == "top" )		return ELeftTop;
	if( hPos == "left"	&& vPos == "bottom" )	return ELeftBottom;
	if( hPos == "right" && vPos == "top" )		return ERightTop;
	if( hPos == "right" && vPos == "bottom" )	return ERightBottom;
	
	if( hPos == "mid"	&& vPos == "top" )		return ETop;
	if( hPos == "left"	&& vPos == "mid" )		return ELeft;
	if( hPos == "right" && vPos == "mid" )		return ERight;
	if( hPos == "mid" && vPos == "bottom" )		return EBottom;

	return EInvalidPos;
}

//
QStringList TOvrMod::EPosToString(TOvrMod::EPos pos)
{
	QStringList list;
	switch(pos){
		case ELeftTop:		return list << "left"	<< "top";
		case ELeftBottom:	return list << "left"	<< "bottom";
		case ERightTop:		return list << "right"	<< "top";
		case ERightBottom:	return list << "right"	<< "bottom";

		case ETop:		return list << "mid"	<< "top";
		case ELeft:		return list << "left"	<< "mid";
		case ERight:	return list << "right"	<< "mid";
		case EBottom:	return list << "mid"	<< "bottom";
	}
	return list;
}

//
QString TOvrMod::EPosToLitString(TOvrMod::EPos pos)
{
	switch(pos){
		case ELeftTop:		return "����� �������";
		case ELeftBottom:	return "����� ������";
		case ERightTop:		return "������ �������";
		case ERightBottom:	return "������ ������";
		case ETop:		return "�������";
		case ELeft:		return "�����";
		case ERight:	return "������";
		case EBottom:	return "������";
	}
	return "";
}

//
void TOvrMod::genCorners()
{
	for( int i = 0; i < 8; ++i )
		corners.insert( (TOvrMod::EPos)i, TOvrCorner() );
}

//
TOvrCorner &TOvrMod::corner( EPos pos )
{
	return corners[pos];
}

//
void TOvrMod::clear()
{
	QMap<EPos,TOvrCorner>::iterator it = corners.begin(), end = corners.end();
	while( it != end ){
		it.value().eList.clear();
		++it;
	}
}

//
TOvrMod::EPos TOvrMod::findName( const QString &name ) const
{
	QMap<EPos,TOvrCorner>::const_iterator it = corners.begin(), end = corners.end();
	while( it != end ){
		if( it.value().hasName( name ) )
			return it.key();
		++it;
	}
	return EInvalidPos;
}

//
QStringList TOvrMod::expressions(TOvrMod::EPos pos, bool firstPage, bool printer)
{
	TOvrCorner &cor = corners[pos];
	QStringList exprList;
	foreach( const TOvrExpression &expr, cor.eList )
		if( (printer && expr.m_onPrinter) || (!printer && expr.m_show) )	
			exprList << expr.m_name;

	return exprList;
}

///////////////////////////////////////////////////////////////////////////
// TOvrCorner /////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
void TOvrCorner::addExpr( TOvrExpression &expr )
{
	eList << expr;
}

//
bool TOvrCorner::hasName( QString name ) const
{
	foreach(const TOvrExpression &exp,	eList)
		if( exp.m_name == name )
			return true;

	return false;
}

//
QStringList TOvrCorner::getNames() const
{
	QStringList list;
	foreach(const TOvrExpression &exp,	eList)
		list << exp.m_name;
	return list;
}

//
TOvrExpression TOvrCorner::takeExpr( int index )
{
	return eList.takeAt(index);
}

//
TOvrExpression TOvrCorner::takeExpr( QString name )
{
	int n = 0;
	QList<TOvrExpression>::iterator it = eList.begin();
	while( it != eList.end() ){
		if( it->m_name == name ){
			return eList.takeAt(n);
		}
		++it, ++n;
	}
	return TOvrExpression();
}

///////////////////////////////////////////////////////////////////////////
// TOverlayDict ///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//TEMPORARY
TOverlayDict *TOverlayDict::getDict()
{
	static TOverlayDict instance;
	return &instance;
}

//
TOverlayDict::TOverlayDict()
: changed(false)
{
}

//
bool TOverlayDict::loadDict( QString path )
{
	QFile file(path);
	file.open( QIODevice::ReadOnly );
	
	QXmlSimpleReader reader;
	TOvrXmlhandler handler( &modMap );
	
	reader.setContentHandler( &handler );
	reader.parse( &file );
	return true;
}

//
bool TOverlayDict::saveDict( QString path )
{
	if( !changed ) return true;
	
	QFile outF( path );
	outF.open( QIODevice::WriteOnly );
	QTextStream out( &outF );
	out.setCodec( "UTF-8" );

	out	<< "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; 
	out	<< "<overlay>\n"; 
	
	QMap<QString,TOvrMod>::const_iterator it_mod = modMap.constBegin();	
	while (it_mod != modMap.constEnd() ){
		TOvrMod mod = it_mod.value();
		out << "\t<modality name=\"" + it_mod.key() + "\"";
		out << " description=\"" + mod.description + "\">\n";
		
		QMap <TOvrMod::EPos,TOvrCorner>::const_iterator it = mod.corners.begin(), end = mod.corners.end();
		while( it != end ){
			QStringList pos = TOvrMod::EPosToString( it.key() );
			out << "\t\t<corner hpos=\"" + pos[0] + "\" vpos=\"" + pos[1] + "\">\n";
			foreach(TOvrExpression exp, it.value().eList ){
				out << "\t\t\t<expression name=\"" + exp.m_name 
					+ "\" display = \"" + (exp.m_show ? "true" : "false") 
					+ "\" onPrinter = \"" + (exp.m_onPrinter ? "true" : "false") + "\"/>\n";
			}
			++it;
			out << "\t\t</corner>\n";
		}
		++it_mod;
		out << "\t</modality>\n";			
	}

	out	<< "</overlay>\n";
	outF.close();
	return true;
}

/**
QList<TOvrCorner> TOverlayDict::getCorners( QString mod )
{
	return modMap[mod].cList;
}
/**/

//
QStringList TOverlayDict::getModalityList()
{
	return modMap.keys();
}

//
void TOverlayDict::updateMod(TOvrMod &newV)
{
	modMap[newV.modality] = newV;

	changed = true;
}

//
void TOverlayDict::createMod( QString name )
{
	if( modMap.contains(name) )
		return;

	TOvrMod newMod;
	newMod.modality = name;
	modMap[name] = newMod;

	changed = true;
}

//
void TOverlayDict::deleteMod( QString name )
{
	if( !modMap.contains(name) )
		return;
	modMap.take( name );
}
