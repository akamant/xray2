//#include "dicomDict.h"
#include "stdafx.h"
#include "elem.h"
#include "dicomTagDialog.h"

///////////////////////////////////////////////////////////////////////////
// TDicomTagDialog //////////////////..////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TDicomTagDialog::TDicomTagDialog( TDicomDict *dDict, const TElemContList &cList, EMode mode )
: QDialog(NULL)
, contList(cList)
, dicomDict(dDict)
{
	setWindowTitle( "������ ���������" );

	tree = new QTreeWidget(this);

	QPushButton	*ok	= new QPushButton("��");
	connect( ok, SIGNAL(clicked()), SLOT(accept()) );
	
	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addStretch(1);
	hl1->addWidget( ok );
		if( mode == EChose ){
			connect( tree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*)), SLOT(accept()) ); 
		
			QPushButton	*cancel = new QPushButton("������");
			connect( cancel, SIGNAL(clicked()), SLOT(reject()) );
			hl1->addWidget( cancel );
		}
	hl1->addStretch(1);

	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addWidget( tree );
	vl1->addLayout( hl1 );

	setLayout( vl1 );
	setGeometry( 400, 300, 400, 550 );
		
	fillFields();

	tree->resizeColumnToContents(0);
	tree->resizeColumnToContents(1);
	tree->resizeColumnToContents(2);
	tree->resizeColumnToContents(3);
}

//
void TDicomTagDialog::fillFields()
{
	QStringList header;
	header << "���" << "���" << "��������" << "��������";
	tree->setHeaderLabels( header );

	QList<uint> tagList = contList.tagList();
	foreach( uint tag, tagList ){
		TElement elem = contList.get(tag);
		QTreeWidgetItem *item = makeNewTreeItem( elem );
		tree->addTopLevelItem(item);
	}
}

//
QTreeWidgetItem* TDicomTagDialog::makeNewTreeItem( const TElement &elem )
{
	uint tag = elem.tag;
	TDicomElement dicomElem = dicomDict->getElement(tag);

	QTreeWidgetItem *item = new QTreeWidgetItem;
	item->setText(0, QString("%1").arg(tag, 8, 16, QChar('0')) );
	item->setText(1, dicomDict->getStringEType(dicomElem.type) );
	item->setText(2, elem.toString() );
	item->setText(3, dicomElem.description );

	QString aa = elem.variant.toString();

	if( elem.hasChildren() ){
		QList<TElement> childs = elem.getChildren();
		foreach( const TElement &chld, childs )
			item->addChild( makeNewTreeItem(chld) );
	}

	return item;
}

//
uint TDicomTagDialog::tag()
{
	QString str = tree->currentItem()->text(0);
	return str.left(8).toUInt( NULL, 16 );
}
