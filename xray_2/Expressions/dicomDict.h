#ifndef DICOMDICT_H
#define DICOMDICT_H


enum EType
{
	Empty = -1,
	AE,	AS, AT, CS, DA, DS, DT, FL, FD, IS, LO, LT, OB, 
	OF, OW, PN, SH, SL, SQ, SS, ST, TM, UI, UL, UN, US, UT
};

///////////////////////////////////////////////////////////////////////////
// TDicomElement //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
struct TDicomElement
{
	TDicomElement(): type(Empty) {}
	TDicomElement( EType tp ): type(tp) {}
	//
	EType type;
	QString description;
};

///////////////////////////////////////////////////////////////////////////
// TDicomDict /////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TDicomDict
{
public:
	static TDicomDict *getDict();

public:
	TDicomDict();
	//
	void loadDict( const QString &bigDictPath, const QString &locDictPath );
	void saveDict( const QString &path );
	//
	void addElement( uint tag );
	TDicomElement getElement( uint tag );
	EType getType( uint tag );
	EType getType( uint group, uint element );
	EType getEType( char t[2] );
	QString getStringType( uint tag );
	QString getStringType( uint group, uint element );
	QString getStringEType( EType type );
	//
	bool filterContains( uint group, uint element );

private:
	uint cTag( uint group, uint element ) const			{ return (group<<16) + element; }
	bool isHardTag( uint tag ) const					{ return HIWORD(tag)%2; }
	bool isHardTag( uint group, uint element ) const	{ return group%2; }
		
	void loadMapFromLocalFile();
//	void loadFilter();
	void makeHardTags();
	//
	QFile globalDictFile
		, localDictFile;
//		, filterFile;
	//
//	QMap <uint, TDicomElement> filter;
	QMap <uint, TDicomElement> tagMap;
	QMap <uint, TDicomElement> specTagMap;
	QMap <QString, EType>	typeMap;
	QMap <EType, QString>	invTypeMap;
	//
	bool needLocalDictUpdate;
};


#endif //DICOMDICT_H