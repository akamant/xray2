#include "stdafx.h"
#include "exprBuilder.h"
#include "elemCont.h"
#include "exprDict.h"
#include "dicomDict.h"

///////////////////////////////////////////////////////////////////////////
// TExprBuilder ///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
QString TExprBuilder::execExpr( const QString &exprName ) const
{
	TExpression expr = exprDict->exprVar(exprName);
	QString exprStr = expr.expression;
	
	foreach( TArgument var, expr.args ){
		if( var.value.type() == QVariant::String ){
			exprStr = exprStr.arg( execExpr(var.value.toString()) );
		}
		else{
			if( contList.hasElement(var.value.toUInt()) && var.value.toUInt() != 0x0001000AA ){
				TElement elem = contList.get( var.value.toUInt() );
				if( elem.variant.type() == QVariant::Double )
					exprStr = exprStr.arg( elem.toDouble(), 0, 'g', var.precision );
				else
					exprStr = exprStr.arg( elem.toString() );
			}
			else{
				exprStr = exprStr.arg("<not found>");
				TDicomDict::getDict()->getElement( var.value.toUInt() );
			}
		}
	}
	return  exprStr;
}

//
QStringList TExprBuilder::execExprList( const QStringList &list ) const
{
	QStringList resList;
	foreach( QString str, list ){
		resList << execExpr(str);
	}
	return  resList;
}