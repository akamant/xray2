#ifndef ELEMCONT_H
#define ELEMCONT_H

#include "elem.h"


///////////////////////////////////////////////////////////////////////////
// TElemCont //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TElemCont
{
public:
	void add( TElement elem );
	void add( uint tag, TElement elem );
	void add( uint group, uint element, TElement elem );
	TElement get( uint tag ) const;
	bool hasElement( uint tag ) const;
	QList<uint> tagList() const;
	bool isEmpty();
	void setFilter( const QSet<uint> &set );
	void makeHardTags();
	//QString getNumber();
	QString getNumberSeries();
	void setNumberForImagesFrames();
	void makeHardTag(uint tag, QVariant value);
private:
	QMap< uint, TElement > elemMap;
	QSet< uint > filter;
};

///////////////////////////////////////////////////////////////////////////
// TElemContList //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TElemContList
{
public:
	TElemContList(){}
	TElemContList( TElemCont *c1 );
	TElemContList( TElemCont *c1, TElemCont *c2 );
	TElemContList( TElemCont *c1, TElemCont *c2, TElemCont *c3 );
	TElemContList( TElemCont *c1, TElemCont *c2, TElemCont *c3, TElemCont *c4 );
	//
	void addCont( TElemCont *c );
	bool isEmpty();
	//
	TElement get( uint tag ) const;
	bool hasElement( uint tag ) const;
	QList<uint> tagList() const;

//private:
	QList<TElemCont*> list;
};

#endif //ELEMCONT_H