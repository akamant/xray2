#ifndef OVERLAYADDEXPRESSIONDIALOG_H
#define OVERLAYADDEXPRESSIONDIALOG_H

#include "overlayDict.h"

class TExpressionDict;

///////////////////////////////////////////////////////////////////////////
// TAddExpressionDialog ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TAddExpressionDialog : public QDialog
{
	Q_OBJECT
public:
	TAddExpressionDialog( QList<QVector<QString>> &exprList
		, const QList<TOvrExpression> &cornerExpr
		, QStringList &values );
	//
	QList<TOvrExpression> expressions() { return m_expressions; }
	bool nothingChanged() { return !m_changed; }

private slots:
	void slotMoveTo();
	void slotRemoveFrom();
	void slotInsertEmpty();
	void slotSelectionChanges();

private:
	void fillData( QList<QVector<QString>> &exprList
				, const QList<TOvrExpression> &cornerExpr
				, QStringList &values );
	//
	QTreeWidget *m_treeAll, *m_treeCorner;
	QPushButton *m_moveToCorner, *m_removeFromCorner, *m_insertEmpty;
	QList<TOvrExpression> m_expressions;
	bool m_changed;
};

#endif