#ifndef DICOMTAGDIALOG_H
#define DICOMTAGDIALOG_H

#include "elemCont.h"
#include "dicomDict.h"

///////////////////////////////////////////////////////////////////////////
// TDicomTagDialog ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TDicomTagDialog : public QDialog
{
	Q_OBJECT
public:
	enum EMode
	{
		ESimple = 0
	,	EChose
	};
	//
	TDicomTagDialog( TDicomDict *dDict, const TElemContList &cList, EMode mode );
	//
	uint tag();

private:
	QTreeWidgetItem* makeNewTreeItem( const TElement &elem );
	void fillFields();
	QTreeWidget	*tree;
	const TElemContList &contList;
	TDicomDict *dicomDict;
};

#endif