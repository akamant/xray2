#ifndef EXPREDITORDIALOGS_H
#define EXPREDITORDIALOGS_H


#include "elemCont.h"
#include "exprDict.h"
#include "dicomDict.h"

///////////////////////////////////////////////////////////////////////////
// TElementsDialog ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TElementsDialog : public QDialog
{
	Q_OBJECT
public:
	TElementsDialog( TElemContList &cList );
	//
	uint tag();

private:
	void fillFields();
	QListWidget	*list;
	TElemContList &contList;
};

///////////////////////////////////////////////////////////////////////////
// TExpressionsDialog ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TExpressionsDialog : public QDialog
{
	Q_OBJECT
public:
	TExpressionsDialog( TExpressionDict *eDict, const QString &exceptExpr = "" );
	//
	QString expression();

private:
	void fillFields( const QString &excrptExpr );
	QListWidget	*list;
	TExpressionDict *exprDict;
};

///////////////////////////////////////////////////////////////////////////
// TArgChangeDialog ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TArgChangeDialog : public QDialog
{
	Q_OBJECT

public:
	TArgChangeDialog( TArgument a, TElemContList &cList, TDicomDict *dDict, TExpressionDict *eDict );
	//
	TArgument arg() { return argument; }

public slots:
	virtual void accept();

private slots:
	void slotTakeFromList();
	void slotTextChanged( const QString &text );
	
private:
	void updatePrecision(uint precTag);
	bool checkValue();
	void fillFields();
	//
	QRadioButton *tag, *expr;
	QLineEdit *value;
	QSpinBox  *precision;
	//
	TArgument argument;
	TElemContList &contList;
	TDicomDict *dicomDict;
	TExpressionDict *exprDict;
};

#endif //EXPREDITORDIALOGS_H