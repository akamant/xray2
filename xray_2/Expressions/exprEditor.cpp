//#include "dicomDict.h"
#include "stdafx.h"
#include "exprEditor.h"
#include "exprBuilder.h"
#include "exprEditorDialogs.h"


///////////////////////////////////////////////////////////////////////////
//
QString getVarText( const TArgument &var )
{
	if( var.value.type() == QVariant::UInt )
		if( var.precision != -2 )
			return QString( QString("���: %1 ��������: %2").arg(var.value.toUInt(), 8, 16, QChar('0')).arg(var.precision) );
		else
			return QString( QString("���: %1").arg(var.value.toUInt(), 8, 16, QChar('0')) );
	else
	if( var.value.type() == QVariant::String )
		return QString( "���������: " + var.value.toString() );

	return QString( "<�����>" );
}

///////////////////////////////////////////////////////////////////////////
// TExprEditor ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TExprEditor::TExprEditor( TExpressionDict *dict, TElemContList &cList, TDicomDict *dDict )
: QDialog()
, exprDict(dict)
//, elemCont(cont)
, contList(cList)
, creatingNewExpr(false)
, newItem(NULL)
, dicomDict(dDict)
{
	setWindowTitle("�������� ���������");

	actAdd = new QAction("������� ���������", this);
	actDel = new QAction("������� ���������",  this);
	QList<QAction*> actList;
	actList << actAdd << actDel;

	actAdd->setShortcut( Qt::Key_Insert );
	actDel->setShortcut( Qt::Key_Delete );

	connect( actAdd, SIGNAL(triggered(bool)), SLOT(slotAddExpr()) );
	connect( actDel, SIGNAL(triggered(bool)), SLOT(slotDeleteExpr()) );

	actSaveExpr = new QAction("���������", this);
	actSaveExpr->setShortcut( Qt::Key_F5 );
	connect( actSaveExpr, SIGNAL(triggered(bool)), SLOT(slotSave()) );
	addAction( actSaveExpr );

	exprList = new QListWidget(this);
	exprList->setEditTriggers( QAbstractItemView::DoubleClicked | QAbstractItemView::EditKeyPressed );
	exprList->addActions( actList );
	connect( exprList, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*))
					 , SLOT(slotCurrentItemChanged(QListWidgetItem*,QListWidgetItem*)) );
	
	argsList = new QListWidget(this);
	connect( argsList, SIGNAL(itemActivated(QListWidgetItem*)), SLOT(slotArgClicked(QListWidgetItem*)) );

	tempString  = new QLineEdit(this);
	connect( tempString,	SIGNAL(textEdited(const QString&))
						,	SLOT(slotTempChanged(const QString&)) );

	descriptionString = new QLineEdit(this);
	connect( descriptionString, SIGNAL(textEdited(const QString&))
							,	SLOT(slotDescriptionChanged(const QString&)) );

	resultString = new QLineEdit(this);
	resultString->setReadOnly(true);

	ok		= new QPushButton("��");
	connect( ok, SIGNAL(clicked()), SLOT(slotOk()) );
	cancel	= new QPushButton("������");
	connect( cancel, SIGNAL(clicked()), SLOT(slotCancel()) );
	add		= new QPushButton("��������");
	connect( add, SIGNAL(clicked()), SLOT(slotAddExpr()) );
	del		= new QPushButton("�������");
	connect( del, SIGNAL(clicked()), SLOT(slotDeleteExpr()) );

	save	= new QPushButton("���������");
	save->addAction( actSaveExpr );
	connect( save, SIGNAL(clicked()), SLOT(slotSave()) );
//	connect( save, SIGNAL(clicked()), actSaveExpr, SIGNAL(triggered(bool)) );
	nsave	= new QPushButton("��������");
	connect( nsave, SIGNAL(clicked()), SLOT(slotNSave()) );

	showElements = new QPushButton("�������� ��������");
//	if( elemCont )
	if( contList.isEmpty() )
		showElements->setEnabled( false );
	else
		connect( showElements, SIGNAL(clicked()), SLOT(slotShowElements()) );

	QHBoxLayout *�hl = new QHBoxLayout;
	�hl->addWidget( ok );
	�hl->addWidget( cancel );
	�hl->addWidget( add );
	�hl->addWidget( del );
	�hl->addWidget( save );
	�hl->addWidget( nsave );

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addWidget( new QLabel("������ �������:") );
	vl->addWidget(tempString);
	vl->addWidget( new QLabel("���������:") );
	vl->addWidget(argsList);
	vl->addWidget(showElements);
	vl->addWidget( new QLabel("��������:") );
	vl->addWidget(descriptionString);
	vl->addWidget( new QLabel("������ ����������:") );
	vl->addWidget(resultString);
	vl->addLayout(�hl);
	
	QVBoxLayout *�vl = new QVBoxLayout;
	�vl->addWidget( new QLabel("���������:") );
	�vl->addWidget(exprList);

	QHBoxLayout *ghl = new QHBoxLayout;
	ghl->addLayout(�vl);
	ghl->addLayout(vl);

	setLayout( ghl );
	setGeometry( 300, 150, 1200, 600 );

	setChanged( false );
	loadDict();

	setTabOrder( exprList, tempString );
	setTabOrder( tempString, argsList );
//	setTabOrder( argList, tempString );
}

//
void TExprEditor::loadDict()
{
	disconnect( exprList, SIGNAL(itemChanged(QListWidgetItem*))
			  , this	, SLOT(slotItemChanged(QListWidgetItem*)) );

	exprList->clear();
	QStringList names = exprDict->exprNames();
	exprList->addItems( names );

	for( int i = 0; i < exprList->count(); i++){
		QListWidgetItem *item = exprList->item(i);
		item->setFlags( item->flags() | Qt::ItemIsEditable );
	}

	exprList->setCurrentRow( 0 );

	connect( exprList, SIGNAL(itemChanged(QListWidgetItem*))
					 , SLOT(slotItemChanged(QListWidgetItem*)) );
}

//
void TExprEditor::updateExpression()
{
	tempString->setText( origExpr.expression );
	descriptionString->setText( origExpr.description );

	updateTagAndResult();
}

//
void TExprEditor::updateTagAndResult()
{
	argsList->clear();
	for( int i = 0; i < realArgCount; ++i ){
		TArgument arg = curExpr.args[i];
		argsList->addItem( getVarText(arg) );
	}
	
	int n = contList.list.count();
	if( !contList.isEmpty() ){
		TExprBuilder exprBuilder( exprDict, contList );
		resultString->setText( exprBuilder.execExpr(curExpr.name) );
	}
	else{
		resultString->setText("<�� ���������� ��������� ���������>");
	}

}

//
void TExprEditor::setChanged( bool changed )
{
	isChanged = changed;
	save->setEnabled( changed );
	nsave->setEnabled( changed );
}

//
void TExprEditor::slotCurrentItemChanged(QListWidgetItem* item, QListWidgetItem *prev)
{
	if( !item ) return;

	if( isChanged && prev )
		prev->setText( origExpr.name );
	
	setChanged( false );

	if( creatingNewExpr ){
		if( item == newItem ){
			origExpr.clear();
			curExpr.clear();
			curExpr.expression = "%1";
			setChanged( true );
			realArgCount = 0;
		}
		else
		if( prev == newItem ){
			delete exprList->takeItem( exprList->count()-1 );
			newItem = NULL;
			creatingNewExpr = false;
			curExpr = origExpr = exprDict->exprVar( item->text() );
			realArgCount = curExpr.args.count();
		}
	}
	else{
		curExpr = origExpr = exprDict->exprVar( item->text() );
		realArgCount = curExpr.args.count();
	}

	updateExpression();
}

//
void TExprEditor::slotItemChanged(QListWidgetItem* item)
{
	curExpr.name = item->text();
	setChanged( true );
}

//
void TExprEditor::slotTempChanged(const QString &text)
{
	curExpr.expression = text;
	setChanged( true );
	parseTemplate();
}

//
void TExprEditor::slotDescriptionChanged(const QString &text)
{
	curExpr.description = text;
	setChanged( true );
}

//
void TExprEditor::slotAddExpr()
{
	if( creatingNewExpr )
		return;	
	int lastRow = exprList->count();
	newItem = new QListWidgetItem("");
	newItem->setFlags( newItem->flags() | Qt::ItemIsEditable );

	exprList->addItem(newItem);
	creatingNewExpr = true;
	setChanged( true );

	exprList->setCurrentItem( newItem );
	exprList->editItem( newItem );
}

//
void TExprEditor::slotDeleteExpr()
{
	QListWidgetItem *item = exprList->currentItem();
	
	if( item ){
		exprDict->deleteExpr( item->text() );
		int row = exprList->row( item );
		exprList->takeItem( row );
	
		exprList->setCurrentRow( row ? row-1 : row );
	}
}

//
void TExprEditor::slotSave()
{
	curExpr.args.erase( curExpr.args.begin()+realArgCount, curExpr.args.end() );

	exprDict->updateExpr( origExpr.name, curExpr );

	origExpr = curExpr;

	newItem = NULL;
	creatingNewExpr = false;
	setChanged( false );
}

//
void TExprEditor::slotNSave()
{
	if( creatingNewExpr ){
//		exprList->takeItem( exprList->count()-1 );
		exprList->setCurrentRow( 0 );
	}
	else{
		curExpr = origExpr;
		realArgCount = curExpr.args.count();
		updateExpression();
	}
	newItem = NULL;
	setChanged( false );
}

//
void TExprEditor::slotOk()
{
	slotSave();
	accept();
}

//
void TExprEditor::slotCancel()
{
	slotNSave();
	reject();
}

//
void TExprEditor::parseTemplate()
{
	int argCount = 0, start = 0, ind, origArgCount = curExpr.args.count();
	while( (ind = curExpr.expression.indexOf(QRegExp("\\%[123456789]{1}[0-9]?"), start)) != -1 ){
		++argCount;
		start = ind + 1;
	}

	realArgCount = argCount;

//	if( argCount < origArgCount )
//		curExpr.args.erase( curExpr.args.begin()+argCount, curExpr.args.end() );

	if( realArgCount > origArgCount ){
		for( int i = 0; i < realArgCount - origArgCount; i++ )
			curExpr.args.append( TArgument() );
	}
	
	updateTagAndResult();
}

//
void TExprEditor::slotArgClicked(QListWidgetItem* item)
{
	int row = argsList->row(item);
	TArgument arg = curExpr.args[row];

	TArgChangeDialog dlg( arg, contList, dicomDict, exprDict );

	if( dlg.exec() == QDialog::Accepted ){
		TArgument res = dlg.arg();
		curExpr.args[row] = res;
		item->setText( getVarText(res) );
		setChanged( true );
	}
}

//
void TExprEditor::slotShowElements()
{
	TElementsDialog dlg(contList);
	dlg.exec();
}