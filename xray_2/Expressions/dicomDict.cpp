#include "stdafx.h"
#include "dicomDict.h"
#include "xmlTagLoader.h"

//////////////////////////////////////////////////////////////////////////
// TDicomDict /////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
//
TDicomDict *TDicomDict::getDict()
{
	static TDicomDict instance;
	return &instance;
}
//
TDicomDict::TDicomDict()
: needLocalDictUpdate(false)
{
	QStringList types;
	types << "AE" << "AS" << "AT" << "CS" << "DA" << "DS" << "DT" << "FL" << "FD" 
		  << "IS" << "LO" << "LT" << "OB" << "OF" << "OW" << "PN" << "SH" << "SL" 
		  << "SQ" << "SS" << "ST" << "TM" << "UI" << "UL" << "UN" << "US" << "UT";
	int i = 0;  
	foreach( const QString &str, types ){
		typeMap[str] = static_cast<EType>(i);
		invTypeMap[static_cast<EType>(i)] = str;
		++i;
	}
}

//
void TDicomDict::saveDict(const QString &path)
{
	QFile f(path);
	if( needLocalDictUpdate && f.open(QIODevice::WriteOnly) ){
		TXmlTagLoader xmlLoader(&tagMap, &typeMap);
		xmlLoader.saveDict( f );
		f.close();
	}
}

//
void TDicomDict::loadDict( const QString &globalDictPath
						 , const QString &localDictPath )
{
	globalDictFile.setFileName( globalDictPath );
	localDictFile.setFileName( localDictPath );

	loadMapFromLocalFile();

	makeHardTags();
}

//
void TDicomDict::makeHardTags()
{
	tagMap[0x00010000] = CS; //SERIESDESCRIPTION_PARAMETER
	tagMap[0x00010001] = DS; //EXPOSURE_TIME_PARAMETER
	tagMap[0x00010003] = DS; //SCALE_PARAMETER
	tagMap[0x00010004] = CS; //NUMBER_PARAMETER
	tagMap[0x00010005] = CS; //SERIES_NUMBER_PARAMETER
	tagMap[0x00010006] = CS; //SLICE_LOCATION_PARAMETER
	tagMap[0x00010007] = DS; //FRAME_NUMBER_PARAMETER
	tagMap[0x00010008] = DS; //CHANGED_WINDOW_PARAMETER
	tagMap[0x00010009] = DS; //CHANGED_LEVEL_PARAMETER

	tagMap[0x0001000A] = IS; //FRAME_NUMBER_PARAMETER
	tagMap[0x0001000B] = IS; //TOTAL_FRAME_NUMBER_PARAMETER
	tagMap[0x0001000C] = IS; //_IMAGE_NUMBER_PARAMETER
	tagMap[0x0001000D] = IS; //TOTAL_IMAGE_NUMBER_PARAMETER
	tagMap[0x000100AA] = IS; //ruler
}

//
void TDicomDict::loadMapFromLocalFile()
{
	if( localDictFile.open( QIODevice::ReadOnly ) ){
		TXmlTagLoader xmlLoader(&tagMap, &typeMap);
		xmlLoader.loadAll( localDictFile );
		localDictFile.close();
	}
}

//
EType TDicomDict::getType( uint tag )
{
	if( !LOWORD(tag) )	return UL; //��� ����� ������

	if( isHardTag(tag) ) //������������� ������
		if( tagMap.contains(tag) )
			return tagMap.value(tag).type;
		else
			return UN;
	else{
		if( tagMap.contains(tag) )
			return tagMap.value(tag).type;
		else{
			TXmlTagLoader xmlLoader(&tagMap, &typeMap);

			xmlLoader.loadOne( globalDictFile, tag );
			needLocalDictUpdate = true;
			return tagMap.value(tag).type;
		}
	}
}

//
EType TDicomDict::getType(uint group, uint element)
{
	return getType( cTag(group,element) );
}

/**/ //����������, == TDicomDict::hasTag
bool TDicomDict::filterContains( uint group, uint element )
{
	uint tag = (group<<16) + element;
	return tagMap.contains(tag);
}
/**/

//
QString TDicomDict::getStringType(uint group, uint element)
{
	return invTypeMap[ getType(group,element) ];
}

//
QString TDicomDict::getStringType(uint tag)
{
	return invTypeMap[ getType(tag) ];
}

//
QString TDicomDict::getStringEType( EType type )
{
	return invTypeMap[type];
}

/**/
void TDicomDict::addElement( uint tag )
{
	if( tagMap.contains(tag) )
		return;
	
	if( isHardTag(tag) ) return;

	TXmlTagLoader xmlLoader(&tagMap, &typeMap);

	xmlLoader.loadOne( globalDictFile, tag );
	needLocalDictUpdate = true;
}
/**/

//
TDicomElement TDicomDict::getElement(uint tag)
{
	if( LOWORD(tag) == 0 ){
		TDicomElement elem;
		elem.type = UL;
		elem.description = "Group Length";
		return elem;
	}
	
	if( tagMap.contains(tag) )
		return tagMap[tag];

	if( isHardTag(tag) ){
		TDicomElement elem;
		elem.type = UN;
		QString str = "Unknown tag: %1";
		elem.description = str.arg(tag);
		return elem;
	}

	TXmlTagLoader xmlLoader(&tagMap, &typeMap);

	xmlLoader.loadOne( globalDictFile, tag );
	needLocalDictUpdate = true;
	return tagMap.value(tag).type;
}

//
EType TDicomDict::getEType( char t[2] )
{
	if( t[0] == 'A' ){
		if( t[1] == 'E' ) return AE;
		if( t[1] == 'S') return AS;
		if( t[1] == 'T' ) return AT;
	}
	if( t[0] == 'C' ) return CS;
	if( t[0] == 'D' ){
		if( t[1] == 'A' ) return DA;
		if( t[1] == 'S' ) return DS;
		if( t[1] == 'T' ) return DT;
	}
	if( t[0] == 'F' ){
		if( t[1] == 'L' ) return FL;
		if( t[1] == 'D' ) return FD;
	}
	if( t[0] == 'I' ) return IS;
	if( t[0] == 'L' ){
		if( t[1] == 'O' ) return LO;
		if( t[1] == 'T' ) return LT;
	}
	if( t[0] == 'O' ){
		if( t[1] == 'B' ) return OB;
		if( t[1] == 'F' ) return OF;
		if( t[1] == 'W' ) return OW;
	}
	if( t[0] == 'P' ) return PN;
	if( t[0] == 'S' ){
		if( t[1] == 'H' ) return SH;
		if( t[1] == 'L' ) return SL;
		if( t[1] == 'Q' ) return SQ;
		if( t[1] == 'S' ) return SS;
		if( t[1] == 'T' ) return ST;
	}
	if( t[0] == 'T' ) return TM;
	if( t[0] == 'U' ){
		if( t[1] == 'I' ) return UI;
		if( t[1] == 'L' ) return UL;
		if( t[1] == 'N' ) return UN;
		if( t[1] == 'S' ) return US;
		if( t[1] == 'T' ) return UT;
	}
	return Empty;
}