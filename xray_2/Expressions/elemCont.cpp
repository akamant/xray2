#include "stdafx.h"
#include "elemCont.h"
#include "dicomDict.h"
//#include "structs.h"
///////////////////////////////////////////////////////////////////////////
// TElemCont //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//

void TElemCont::makeHardTags()
{
	//[0x00010000] = CS; //SERIESDESCRIPTION_PARAMETER
	
	//[0x00010001] = DS; //EXPOSURE_TIME_PARAMETER
	if( elemMap.contains(0x00181150) && !elemMap.contains(0x00010001)){
		TElement elem = elemMap[0x00181150];
		add( TElement( 0x00010001, float(elem.toInt()) * 0.001f ) );
	}

	//[0x00010003] = DS; //SCALE_PARAMETER	 
	if(!elemMap.contains(0x00010003)){	
		add( TElement( 0x00010003, 1.0f ) );
	}
	
	//tagMap[0x0001000A] = IS; //FRAME_NUMBER_PARAMETER
	
	//tagMap[0x0001000B] = IS; //TOTAL_FRAME_NUMBER_PARAMETER
	
	//tagMap[0x0001000C] = IS; //_IMAGE_NUMBER_PARAMETER

	//tagMap[0x0001000D] = IS; //TOTAL_IMAGE_NUMBER_PARAMETER
	if( elemMap.contains(0x00200013)){ 
		setNumberForImagesFrames();
		//elemMap[0x00010004] = TElement newElem( 0x00010004, getNumber() );
	}

	//[0x00010005] = CS; //TOTAL_SERIES_PARAMETER
	add( TElement( 0x00010005, getNumberSeries() ) );

	//[0x00010006] = DS; //SLICE_LOCATION_PARAMETER
	if( elemMap.contains(0x00201041) && !elemMap.contains(0x00010006)){
		TElement elem = elemMap[0x00201041];
		double sliceLocation = elem.toDouble();
		if( sliceLocation < 0){
			QString sliceStr = QString("I%1").arg(-sliceLocation, 0, 'g', 2);
			add( TElement( 0x00010006, sliceStr ) );
		}else{
			QString sliceStr = QString("S%1").arg(sliceLocation, 0, 'g', 2);
			add( TElement( 0x00010006, sliceStr) );
		}
	}

	//    [0x00010007] = IS; //FRAME_NUMBER_PARAMETER
}


void TElemCont::makeHardTag(uint tag, QVariant value)
{
	
	add( TElement( tag, value ) );


}

QString TElemCont::getNumberSeries(){
	/*
	DICOMStruct * saveDicomStruct = NULL;
	if(!Panel::currentDicomFile)
		return QString("");
	saveDicomStruct = Panel::currentDicomFile->current;
	Panel::setFirst(Panel::firstDicomFile->first);
	DICOMStruct * dicomStruct = Panel::currentDicomFile->current;	
	while(dicomStruct != NULL){
		if(&dicomStruct->getELEM_CONT() == this 
		//	&& dicomStruct->getELEM_CONT().get(0x00010007).variant.toInt() == this->get(0x00010007).variant.toInt()
			)
			break;
		dicomStruct = Panel::getNext();	
	}

	if(dicomStruct == NULL)
		return QString("");
	Panel::setFirst(Panel::firstDicomFile->first);	
	DICOMStruct * dicomStructFind = Panel::currentDicomFile->current;	
	int j = 0;	
	QString curSeries("");
	while(dicomStructFind != NULL){		
		if(dicomStructFind->getELEM_CONT().get(StudyUID).toString().compare(dicomStruct->getELEM_CONT().get(StudyUID).toString()) == 0){
			if(curSeries.compare(dicomStructFind->getELEM_CONT().get(SeriesUID).toString()) != 0){
				curSeries = dicomStructFind->getELEM_CONT().get(SeriesUID).toString();				
				j++;
			}
		}				
		dicomStructFind = Panel::getNext();			
	}	

	Panel::setFirst(Panel::firstDicomFile->first);	
	dicomStructFind = Panel::currentDicomFile->current;			
	while(dicomStructFind != saveDicomStruct){	
		dicomStructFind = Panel::getNext();			
	}
	return QString("%1").arg(j);
	*/
	return QString ("");
}

void TElemCont::setNumberForImagesFrames(){
	/*
	DICOMStruct * saveDicomStruct = NULL;
	if(!Panel::currentDicomFile)
		return;
		saveDicomStruct = Panel::currentDicomFile->current;
	Panel::setFirst(Panel::firstDicomFile->first);
	DICOMStruct * dicomStruct = Panel::currentDicomFile->current;	
	while(dicomStruct != NULL){
		if(&dicomStruct->getELEM_CONT() == this 
			//&& dicomStruct->getELEM_CONT().get(0x00010007).variant.toInt() == this->get(0x00010007).variant.toInt()
			)
			break;
		dicomStruct = Panel::getNext();	
	}
	if(dicomStruct == NULL)
		return;
	Panel:: setFirst(Panel::firstDicomFile->first);
	DICOMStruct * dicomStructFind = Panel::currentDicomFile->current;				
	int j = 1;
	int i = 1;
	int curImage = 1;
	int curFrame = 1;
	int number = -2;
	int numberFrames = 0;
	while(dicomStructFind != NULL){		
		if(dicomStructFind->getELEM_CONT().get(StudyUID).toString().compare(dicomStruct->getELEM_CONT().get(StudyUID).toString()) == 0 &&
		   dicomStructFind->getELEM_CONT().get(SeriesUID).toString().compare(dicomStruct->getELEM_CONT().get(SeriesUID).toString()) == 0 && 
		   number != dicomStructFind->getELEM_CONT().get(0x00200013).variant.toInt()){
			j++;
			if(	//dicomStructFind == dicomStruct && 
				dicomStructFind->getELEM_CONT().get(0x00200013).variant.toInt() == dicomStruct->getELEM_CONT().get(0x00200013).variant.toInt()){
					int number = dicomStructFind->getELEM_CONT().get(0x00200013).variant.toInt();
					i = j;
			}
			number = dicomStructFind->getELEM_CONT().get(0x00200013).variant.toInt();
		}
		if(dicomStructFind->getELEM_CONT().get(StudyUID).toString().compare(dicomStruct->getELEM_CONT().get(StudyUID).toString()) == 0 &&
			dicomStructFind->getELEM_CONT().get(SeriesUID).toString().compare(dicomStruct->getELEM_CONT().get(SeriesUID).toString()) == 0){					
				numberFrames++;
				if(dicomStructFind == dicomStruct)
					curFrame = numberFrames;
		}
		dicomStructFind = Panel::getNext();			
	}			

	Panel::setFirst(Panel::firstDicomFile->first);	
	dicomStructFind = Panel::currentDicomFile->current;			
	while(dicomStructFind != saveDicomStruct){	
		dicomStructFind = Panel::getNext();			
	}
	QVariant var;
	if(dicomStruct->getELEM_CONT().get(0x00010007).toInt() == -1)
		var.setValue(QString("Im: %1/%2").arg(i - 1).arg(j - 1));		
	else
		var.setValue(QString("Fr: %1/%2 Im: %3/%4").arg(curFrame).arg(numberFrames).arg(i - 1).arg(j - 1));		
	TElement newElem( 0x00010004, var);	
	elemMap[0x00010004] = newElem;
	{	
		var.setValue(curFrame);
		TElement newElem( 0x0001000A, var);	
		elemMap[0x0001000A] = newElem;
	}
	{	
		var.setValue(numberFrames);
		TElement newElem( 0x0001000B, var);	
		elemMap[0x0001000B] = newElem;
	}
	{	
		var.setValue(i - 1);
		TElement newElem( 0x0001000C, var);	
		elemMap[0x0001000C] = newElem;
	}
	{	
		var.setValue(j - 1);
		TElement newElem( 0x0001000D, var);	
		elemMap[0x0001000D] = newElem;
	}
	*/
}


void TElemCont::add(TElement elem)
{
//	if( filter.contains(tag) )
		elemMap[elem.tag] = elem;
}

//
void TElemCont::add(uint tag, TElement elem)
{
//	if( filter.contains(tag) )
		elemMap[tag] = elem;
}

//
void TElemCont::add(uint group, uint element, TElement elem)
{
	add( (group<<16) + element, elem );
}

//
TElement TElemCont::get(uint tag) const
{
	if( !elemMap.contains(tag) ){
		//qDebug() << QString("� ���������� ����� ����������� ��� 0x%1").arg(tag, 8, 16, QChar('0') );
		TElement elem;
		elem.tag = tag;
		//���� ����������� ���, �� ��������� ��� ��� � globalDict
		//� �������� � ��������� �������
		if( HIWORD(tag) % 2 ){ 
			TDicomDict *dicomDict = TDicomDict::getDict();
			dicomDict->addElement( tag );
		}
		return elem;	
	}

	return elemMap[tag];
}

//
bool TElemCont::hasElement( uint tag ) const
{
	return elemMap.contains(tag);
}

//
QList<uint> TElemCont::tagList() const
{
	return elemMap.keys();
}

//
bool TElemCont::isEmpty()
{
	return elemMap.isEmpty();
}

//
void TElemCont::setFilter(const QSet<uint> &set)
{
	filter = set;
}

///////////////////////////////////////////////////////////////////////////
// TElemContList //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TElemContList::TElemContList( TElemCont *c1 )
{ if(c1)	list << c1; }
TElemContList::TElemContList( TElemCont *c1, TElemCont *c2 )
{ list << c1 << c2; }
TElemContList::TElemContList( TElemCont *c1, TElemCont *c2, TElemCont *c3 )
{ list << c1 << c2 << c3; }
TElemContList::TElemContList( TElemCont *c1, TElemCont *c2, TElemCont *c3, TElemCont *c4 )
{ list << c1 << c2 << c3 << c4; }

//
void TElemContList::addCont( TElemCont *c )
{
	list << c;
}

//
bool TElemContList::isEmpty()
{
	foreach( TElemCont *cont, list )
		if( !cont->isEmpty() )
			return false;
	return true;
}

//
TElement TElemContList::get( uint tag ) const
{
	foreach( TElemCont *cont, list )
		if( cont->hasElement(tag) )
			return cont->get(tag);

	Q_ASSERT(true);
	return TElement();
}

//
bool TElemContList::hasElement( uint tag ) const
{
	foreach( TElemCont *cont, list )
		if( cont->hasElement(tag) )
			return true;

	return false;
}

//
QList<uint> TElemContList::tagList() const
{
	QList<uint> tList;
	foreach( TElemCont *cont, list )
		tList << cont->tagList();

	return tList;
}
