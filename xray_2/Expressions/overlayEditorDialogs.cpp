#include "stdafx.h"
#include "overlayEditorDialogs.h"

///////////////////////////////////////////////////////////////////////////
// TCloneModDialog ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TCloneModDialog::TCloneModDialog( QStringList &modList, QStringList &descList )
: QDialog(NULL)
, m_modList(modList)
{
	setWindowTitle( "�������� �������" );

	QPushButton *ok		= new QPushButton( "��", this );
	QPushButton *cancel = new QPushButton( "������", this );

	list = new QListWidget( this );
	list->addItems( descList );

	connect( list, SIGNAL(itemactiveated(QListWidgetItem*)), SLOT(accept()) );

	connect( ok,		SIGNAL(clicked()), SLOT(accept()) );
	connect( cancel,	SIGNAL(clicked()), SLOT(reject()) );
	
	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addWidget( ok );
	hl1->addWidget( cancel );

	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addWidget( new QLabel("���������� ��������� ��:") );
	vl1->addWidget( list );
	vl1->addLayout( hl1 );

	setLayout( vl1 );
	list->setFocus();
}

//
void TCloneModDialog::accept()
{
	name = m_modList[ list->currentRow() ];
	QDialog::accept();
}

///////////////////////////////////////////////////////////////////////////
// TPreviewDialog /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TPreviewDialog::TPreviewDialog( QStringList &ltList, QStringList &rtList
							,	QStringList &lbList, QStringList &rbList )
: QDialog(NULL)
{
	setWindowTitle( "��������������� ��������" );

	QLabel *w		= new QLabel(this);
	QPushButton *ok	= new QPushButton( "��", this );
	connect( ok, SIGNAL(clicked()), SLOT(accept()) );

	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addStretch(1);
	hl1->addWidget( ok );
	hl1->addStretch(1);
	
	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addWidget( w );
	vl1->addLayout( hl1 );
	setLayout( vl1 );

	QSize sz( 600, 450 );
	QPixmap pic(sz);
	QPainter p(&pic);

	p.fillRect( pic.rect(),	Qt::gray );

	p.drawLine( sz.width()/2, 0, sz.width()/2, sz.height()*0.1 );
	p.drawLine( sz.width()/2, sz.height()*0.9, sz.width()/2, sz.height() );
	p.drawLine( 0, sz.height()/2, sz.width()*0.1, sz.height()/2 );
	p.drawLine( sz.width()*0.9, sz.height()/2, sz.width(), sz.height()/2 );

	QRect rect;

	rect.setCoords( 5, 10, sz.width()/2-5, 25 );
	foreach( QString str, ltList ){
		p.drawText( rect, Qt::AlignVCenter | Qt::AlignLeft, str );
		rect.moveBottom( rect.bottom() + 15 );
	}

	rect.setCoords( 5, sz.height()-25, sz.width()/2-5, sz.height()-10 );
	foreach( QString str, lbList ){
		p.drawText( rect, Qt::AlignVCenter | Qt::AlignLeft, str );
		rect.moveBottom( rect.bottom() - 15 );
	}

	rect.setCoords( sz.width()/2+5, 10, sz.width()-5, 25 );
	foreach( QString str, rtList ){
		p.drawText( rect, Qt::AlignVCenter | Qt::AlignRight, str );
		rect.moveBottom( rect.bottom() + 15 );
	}
	
	rect.setCoords( sz.width()/2+5, sz.height()-25, sz.width()-5, sz.height()-10 );
	foreach( QString str, rbList ){
		p.drawText( rect, Qt::AlignVCenter | Qt::AlignRight, str );
		rect.moveBottom( rect.bottom() - 15 );
	}

	w->setPixmap(pic);
}