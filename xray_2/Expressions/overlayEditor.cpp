#include "stdafx.h"
#include "overlayEditor.h"
#include "overlayEditorDialogs.h"
#include "overlayList.h"
#include "exprDict.h"
#include "elemCont.h"
#include "exprBuilder.h"
#include "overlayAddExpressionDialog.h"

///////////////////////////////////////////////////////////////////////////
// TOverlayEditor /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TOverlayEditor::TOverlayEditor( TExpressionDict *eDict, TOverlayDict *oDict, TElemContList &cList, const QString &mod )
: QDialog()
, active(NULL)
, exprDict(eDict)
, overlayDict(oDict)
, contList(cList)
{
	setWindowTitle("��������� �������� ������");

	actInsertExpr = new QAction("������������� �������", this);
	actDeleteExpr = new QAction("������� ���������",  this);
	actInsertEmptyString = new QAction("�������� ������ ������", this);

	actInsertExpr->setShortcut( Qt::Key_Insert );
	actDeleteExpr->setShortcut( Qt::Key_Delete );
	
	connect( actInsertExpr, SIGNAL(triggered(bool)), SLOT(slotAddExpression()) );
	connect( actDeleteExpr, SIGNAL(triggered(bool)), SLOT(slotDelExpression()) );
	connect( actInsertEmptyString, SIGNAL(triggered(bool)), SLOT(slotInsertEmptyString()) );

	QList<QAction*> actList;
	actList << actInsertExpr << actDeleteExpr << actInsertEmptyString;

	QVector<int> colWidth;
	for( int i = 0; i < 8; ++i ){
		TOverlayTreeWidget* tree = new TOverlayTreeWidget( exprDict, contList, this );
		tree->addActions( actList );
		tree->setContextMenuPolicy( Qt::ActionsContextMenu );

		connect( tree, SIGNAL(focusChanged(TOverlayTreeWidget*,bool)), SLOT(slotFocusChanged(TOverlayTreeWidget*,bool)) );
		connect( tree, SIGNAL(cornerDataChanged()), SLOT(slotCornerDataChanged()) );
		connect( tree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), SLOT(slotAddExpression()) );

		m_trees << tree;

		if( i < 3 )
			for( int j = 0; j < 4; ++j )
				colWidth << tree->columnWidth( j );
		else{
			tree->header()->hide();
			for( int j = 0; j < 4; ++j )
				tree->setColumnWidth( j, colWidth[j] );
		}
	}

	cmbMod	= new QComboBox;
	cmbMod->setMinimumContentsLength( 20 );
	cmbMod->setObjectName( "overlayModalityCmb" );

	ok			= new QPushButton("��");
	save		= new QPushButton("���������");
	cancel		= new QPushButton("������");

	connect( cmbMod, SIGNAL(currentIndexChanged(const QString&))
				   , SLOT(slotCurrentModChanged(const QString&)) );

	connect( ok, SIGNAL(clicked()), SLOT(slotOk()) );
	connect( save, SIGNAL(clicked()), SLOT(slotSave()) );
	connect( cancel, SIGNAL(clicked()), SLOT(close()) );

	QLabel *info =  new QLabel( "�������������� �������: ������� ������ ������� ����� � ������ ���� ��� ������ ������������ ����." );
	info->setWordWrap( true );

	QHBoxLayout *hl = new QHBoxLayout;
	hl->addStretch();
	hl->addWidget( cmbMod );
	hl->addStretch();
	
	QVBoxLayout *vl111 = new QVBoxLayout;
	vl111->addStretch();
	vl111->addLayout( hl );
	vl111->addStretch();
	vl111->addWidget( info );
	vl111->addStretch();

	QGridLayout *gl = new QGridLayout;
	gl->addWidget( m_trees[0], 0, 0 );
	gl->addWidget( m_trees[5], 2, 0 );
	gl->addWidget( m_trees[2], 0, 2 );
	gl->addWidget( m_trees[7], 2, 2 );
	gl->addWidget( m_trees[1], 0, 1 );
	gl->addWidget( m_trees[6], 1, 2 );
	gl->addWidget( m_trees[3], 1, 0 );
	gl->addWidget( m_trees[4], 2, 1 );
	gl->addLayout( vl111, 1, 1 );

	QHBoxLayout *hl3 = new QHBoxLayout;
	hl3->addStretch(1);
	hl3->addWidget( ok );
	hl3->addWidget( save );
	hl3->addWidget( cancel );

	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addLayout( gl );
	vl1->addLayout( hl3 );

	setLayout( vl1 );
	
	setChanged( false );
	setDicts( mod );
}

//
const TOvrMod& TOverlayEditor::getCurrentMod()
{
	int idx = cmbMod->currentIndex();
	QString mod = overlayDict->getModalityList()[idx];
	return overlayDict->getMod( mod );
}

//
void TOverlayEditor::setDicts( const QString &mod )
{
	disconnect( cmbMod, SIGNAL(currentIndexChanged(const QString&))
			,	this,	SLOT(slotCurrentModChanged(const QString&)) );
		
	QStringList mods = overlayDict->getModalityList();
	foreach( const QString &mod, mods )
		cmbMod->addItem( overlayDict->getMod(mod).description );

	if( mods.contains(mod) ){
		cmbMod->setCurrentIndex( mods.indexOf(mod) );
		curMod = overlayDict->getMod( mod );
	}
	else{
		cmbMod->setCurrentIndex( mods.indexOf("Unknown") );
		curMod = overlayDict->getMod( "Unknown" );
	}

	connect( cmbMod, SIGNAL(currentIndexChanged(const QString&))
		   , SLOT(slotCurrentModChanged(const QString&)) );
	
	fillFields();
}

//
TOvrMod::EPos TOverlayEditor::getActiveCornerPos()
{
	int i = 0;
	foreach( TOverlayTreeWidget *tree, m_trees ){
		if( tree->isActive() )
			return (TOvrMod::EPos)i;
		++i;
	}

	return TOvrMod::EInvalidPos;
}

//
TOverlayTreeWidget* TOverlayEditor::getCornerTree( TOvrMod::EPos pos )
{
	return (pos == TOvrMod::EInvalidPos) ? NULL : m_trees[pos];
}

//
void TOverlayEditor::slotFocusChanged( TOverlayTreeWidget *tr, bool state )
{
	if( state ){
		foreach( TOverlayTreeWidget *tree, m_trees )
			tree->setActive( false );
		tr->setActive(true);
		active = tr;
	}
}

//
void TOverlayEditor::slotCurrentModChanged( const QString &text )
{
	if( trySaveMod() ){
		curMod = getCurrentMod();
		fillFields();
	}
	else{
		disconnect( cmbMod, SIGNAL(currentIndexChanged(const QString&))
				,	this,	SLOT(slotCurrentModChanged(const QString&)) );

		QStringList mods = overlayDict->getModalityList();
		int ind = mods.indexOf( curMod.modality );
		cmbMod->setCurrentIndex( ind );

		connect( cmbMod, SIGNAL(currentIndexChanged(const QString&))
			   , SLOT(slotCurrentModChanged(const QString&)) );
	}
}

//
QList<QVector<QString>> TOverlayEditor::prepareExprList( const QStringList &list )
{
	TExprBuilder builder( exprDict, contList );
	QList<QVector<QString>> exprList;

	foreach( const QString &expr, list ){
		QVector<QString> entry(3);
		entry[0] = expr;
		entry[1] = exprDict->exprVar(expr).description;
		entry[2] = builder.execExpr(expr);
		exprList << entry;
	}

	return exprList;
}

//
void TOverlayEditor::slotAddExpression()
{
	if( active == NULL ){
		QMessageBox::warning( NULL, "�� ������� �������", "����� �������� ����� ���������, ������� �������� �������." );
		return;
	}
	
	TOvrCorner *corner = const_cast<TOvrCorner*>( active->treeCorner() );

	QStringList exprNames = exprDict->exprNames();

	QList<QVector<QString>> fullList = prepareExprList( exprNames );

	TExprBuilder builder( exprDict, contList );

	TAddExpressionDialog dlg( fullList, corner->eList
							, builder.execExprList(corner->getNames()) );
	QDesktopWidget desktopWidget;
	dlg.setGeometry( desktopWidget.screenGeometry().width()*0.2+50, desktopWidget.screenGeometry().height()*0.2+50
		, desktopWidget.screenGeometry().width()*0.7-50, desktopWidget.screenGeometry().height()*0.7-50 );

	if( dlg.exec() != QDialog::Accepted || dlg.nothingChanged() )
		return;

	QList<TOvrExpression> newExprNames =  dlg.expressions();

	corner->eList = newExprNames;

	active->setOvrCorner( corner );

	setChanged( true );
}

//
void TOverlayEditor::slotInsertEmptyString()
{
	if( active == NULL ){
		QMessageBox mb( QMessageBox::Warning, "�� ������� �������", "����� �������� ����� ���������, �������� �������.", QMessageBox::Ok );
		mb.exec();
		return;
	}

	active->addExpression( TOvrExpression() );
}

//
void TOverlayEditor::slotDelExpression()
{
	active->removeCurrentExpression();
}

//
void TOverlayEditor::fillFields()
{
	for( uint i = 0; i < 8; ++i )
		m_trees[i]->setOvrCorner( &curMod.corner((TOvrMod::EPos)i) );

	setChanged(false);

	m_trees[0]->setFocus();
	m_trees[0]->setCurrentItem( m_trees[0]->topLevelItem(0) );
}

//
bool TOverlayEditor::trySaveMod()
{
	if( changed ){
		QMessageBox dlg( QMessageBox::Warning, "��������� �������� ������"
						, "������� ����� ��� �������.\n��������� ���������?" );
		QAbstractButton *y = dlg.addButton( "��", QMessageBox::YesRole )
					,	*n = dlg.addButton( "���", QMessageBox::NoRole )
					,	*c = dlg.addButton( "������", QMessageBox::RejectRole )
					,	*res;
		dlg.exec();
		res = dlg.clickedButton();
		if( res == y ){
			slotSave();
			return true;
		}
		return res == n;
	}
	return true;
}

//
void TOverlayEditor::slotOk()
{
	if( trySaveMod() )
		close();
}

//
void TOverlayEditor::slotSave()
{
	if( changed ){
		bool someStringsDeleted = deleteEmptyStrings();
		overlayDict->updateMod( curMod );
		if( someStringsDeleted ) fillFields();
		setChanged(false);
	}
}

//
void TOverlayEditor::setChanged( bool changed )
{
	this->changed = changed;
	save->setEnabled( changed );
}

//
void TOverlayEditor::slotCornerDataChanged()
{
	setChanged(true);
}

//
bool deleteEmptyStringsInExprList( QList<TOvrExpression> &exprList )
{
	bool res = false;

	while( !exprList.isEmpty() && exprList.back().m_name.isEmpty() ){
		exprList.pop_back();
		res = true;
	}

	return res;
}

//
bool TOverlayEditor::deleteEmptyStrings()
{
	bool res = false;

	for( int i = 0; i < 8; ++i )
		res |= deleteEmptyStringsInExprList( curMod.corner((TOvrMod::EPos)i).eList );

	return res;
}