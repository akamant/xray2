#include "stdafx.h"
#include "dicomDict.h"
#include "elementParser.h"


///////////////////////////////////////////////////////////////////////////
// TElementParser /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
TElementParser::TElementParser( TDicomDict *dDict )
: dict(dDict)
{
	codec = QTextCodec::codecForName( "Windows-1251" );  ///US-ASCII
}

//
void TElementParser::setCodec( const QString &codecName )
{
	codec = QTextCodec::codecForName( codecName.toAscii().constData() );
}

//
TElement TElementParser::getElement( int grp, int elem, const char *m, int len, char eType[2] )
{
	group	= grp;
	element = elem;
	mem		= m;
	length	= len;
	type = dict->getEType( eType );
	return loadElement();
}

//
TElement TElementParser::getElement( int grp, int elem, const char *m, int len )
{
	group	= grp;
	element = elem;
	mem		= m;
	length	= len;
	type = dict->getType( group, element );
	return loadElement();
}

//
TElement TElementParser::loadElement()
{
	TElement elem;
	switch(type){
		case AE: //+
		case CS: //+
			elem = loadAE(); break;
		case ST: //? ==AE==LT
		case LT: //? ==AE, leading space may be significant
		case UT: //?
			elem = loadST(); break;
		case UI: //?
			elem = loadUI(); break;
		case LO: //?
		case PN: //?
		case SH: //?
			elem = loadLO(); break;
//		case TDicomDict::AS: elem = loadAS();
		case AT: elem = loadAT(); break;
		case DA: elem = loadDA(); break;
		case DS: elem = loadDS(); break;
		case DT: elem = loadDT(); break; //?
		case FL: elem = loadFL(); break; //?
		case FD: elem = loadFD(); break; //?
		case IS: elem = loadIS(); break;
		case OF: elem = loadOF(); break; //?
//		case TDicomDict::OW: elem = loadOW(); //? ������ �� OB/OW
		case SL: elem = loadSL(); break; //?
//		case TDicomDict::SQ: elem = loadSQ();
		case SS: elem = loadSS(); break; //?
		case TM: elem = loadTM(); break;
		case UL: elem = loadUL(); break;
		case UN: elem = loadUN(); break; //? Unknown, parse as string
		case US: elem = loadUS(); break;
	}

	elem.tag = (group<<16) + element;
	return elem;
}

//
QString TElementParser::fromSpecificCodec( const char *mem, int length )
{
	QByteArray encodedString(mem, length);
	return codec->toUnicode(encodedString);
}

//application entry, code string, 
TElement TElementParser::loadAE()
{
	return TElement( QString::fromAscii(mem, strnlen(mem,length)).trimmed() );
}

TElement TElementParser::loadST()
{
	return TElement( fromSpecificCodec(mem, strnlen(mem,length)).trimmed() );
}

//application entry, code string, 
TElement TElementParser::loadUI()
{
//	if( mem[length-1] == 0 ) length--;
	return TElement( QString::fromAscii(mem, strnlen(mem,length)).trimmed() );
}

//age string
TElement TElementParser::loadAS()
{
	int val = QString::fromAscii(mem,3).toInt(NULL); // 'nnnX', X = Y|M|W|D
	switch( *(mem + 3) ){
		case 'D': return TElement( val );
		case 'W': return TElement( val*7 );
		case 'M': return TElement( val*30 );
		case 'Y': return TElement( val*365 );
	}
	Q_ASSERT(true);
	return TElement();
}

//attribute tag
TElement TElementParser::loadAT()
{
	uint g = *(uint16*)(mem);
	uint e = *(uint16*)(mem+2);
	QVariant var;
	var.setValue<TTagPair>( TTagPair(g,e) );
	return TElement( var );
}

//date
TElement TElementParser::loadDA()
{
	return TElement( QDate::fromString( QString::fromAscii(mem,strnlen(mem,length)), "yyyyMMdd" ) );
}

//decimal string: ���� 0 ��� 1 ��������, ������� QVariant<float>, ���� ������ - QVariant< QVariantList<float> >
TElement TElementParser::loadDS()
{
	QString str = QString::fromAscii(mem,strnlen(mem,length)).trimmed();
	QStringList lst = str.split( '\\', QString::SkipEmptyParts );

	if( lst.isEmpty() )
		return TElement( 0.0f );
	else
	if( lst.count() == 1 )
		return TElement( lst[0].toFloat() );
	else{
		QVariantList varList;
		foreach( QString s, lst )
			varList << s.toFloat();
		return TElement( varList );
	}
}

//long string: ���� 0 ��� 1 ��������, ������� QVariant<qstring>, ���� ������ - QVariant< QVariantList<qstring> >
TElement TElementParser::loadLO()
{	
	QString str = fromSpecificCodec(mem,strnlen(mem,length)).trimmed();
//	QString str = QString::fromAscii(mem,strnlen(mem,length)).trimmed();
	QStringList lst = str.split( '\\', QString::SkipEmptyParts );

	if( lst.isEmpty() )
		return TElement( "" );
	else
	if( lst.count() == 1 )
		return TElement( lst[0] );
	else{
		QVariantList varList;
		foreach( QString s, lst )
			varList << s;
		return TElement( varList );
	}
}


/*QString TElementParser::fromSpecificCodec(const char *mem, int length){
	QString dicomCodecName = getELEM_CONT().get(0x00080005).toString();
	dicomCodecName.replace("-". "_").replace(" ". "_").replace("^". "_");
	QString codecName =...
	QByteArray encodedString(mem, length);
	QTextCodec codec = QTextCodec::codecForName(codecName);
	return codec.toUnicode(encodedString);
}*/
//date time YYYYMMDDHHMMSS.FFFFFF&ZZXX
TElement TElementParser::loadDT()
{
//	return TElement( QDateTime::fromString( QString::fromAscii(mem,length), "yyyyMMddhhmmss" ) );
	return TElement( QDateTime::fromString( QString::fromAscii(mem,14), "yyyyMMddhhmmss" ) );
}

//floating point single
TElement TElementParser::loadFL()
{
	float val = *(float*)(mem);
	return TElement( val );
}

//floating point double
TElement TElementParser::loadFD()
{
	double val = *(double*)(mem);
	return TElement( val );
}

//integer string
TElement TElementParser::loadIS()
{
	QString str = QString::fromAscii(mem,strnlen(mem,length)).trimmed();
	return TElement( str.toInt() );
}

//other float string
TElement TElementParser::loadOF()
{
	int count = length >> 2;
	if( count == 0 )
		return TElement( 0.0f );
	if( count == 1 ){
		float val = *(float*)(mem);
		return TElement( val );
	}
	
	QVariantList varList;
	for( int i = 0; i < count; ++i, mem +=4  ){
		float val = *(float*)(mem);
		varList << val;
	}
	return TElement( varList );		
}

//other word string
TElement TElementParser::loadOW()
{
	int count = length >> 1;
	if( count == 0 )
		return TElement( (__int16)0 );
	if( count == 1 ){
		__int16 val = *(__int16*)(mem);
		return TElement( val );
	}
	
	QVariantList varList;
	for( int i = 0; i < count; ++i, mem +=4  ){
		__int16 val = *(__int16*)(mem);
		varList << val;
	}
	return TElement( varList );		
}

//signed long
TElement TElementParser::loadSL()
{
	__int32 val = *(__int32*)(mem);
	return TElement( val );
}

//sequence of items ??????????????????????????????????????
//TElement TElementParser::loadSQ()
//{
//	return TElement( QDateTime::fromString( QString::fromAscii(mem,length), "yyyyMMddhhmmss" ) );
//}

//signed short
TElement TElementParser::loadSS()
{
	__int16 val = *(__int16*)(mem);
	return TElement( val );
}

//time			???????????????????????????????????????????????????????? ������ ����-������-�������
TElement TElementParser::loadTM(  )
{
	if( length >= 6 )
		return TElement( QTime::fromString( QString::fromAscii(mem,6), "hhmmss" ) );
	if( length >= 4 )
		return TElement( QTime::fromString( QString::fromAscii(mem,4), "hhmm" ) );
	if( length >= 2 )
		return TElement( QTime::fromString( QString::fromAscii(mem,2), "hh" ) );
	return TElement();
}

//unsigned long
TElement TElementParser::loadUL()
{
	uint32 val = *(uint32*)(mem);
	return TElement( val );
}

//unknown, parse as string
TElement TElementParser::loadUN()
{
	return TElement( QString::fromAscii(mem, strnlen(mem,length)) );
}

//binary unsigned short
TElement TElementParser::loadUS()
{
	uint16 val = *(uint16*)(mem);
	return TElement( val );
}
