#ifndef OVERLAYLIST_H
#define OVERLAYLIST_H

#include "elemCont.h"

class TExpressionDict;
class TOvrExpression;
class TOvrCorner;

///////////////////////////////////////////////////////////
// TOverlayTreeWidget /////////////////////////////////////
///////////////////////////////////////////////////////////
//
class TOverlayTreeWidget : public QTreeWidget
{
	Q_OBJECT
public:
	TOverlayTreeWidget( TExpressionDict *eDict, TElemContList &contList, QWidget *parent = NULL );
	//
	bool isActive(){ return active; }
	void setActive( bool active );
	//
	void setOvrCorner( TOvrCorner *cor);
 	void addExpression( TOvrExpression &expr );
//	QStringList getExprList();
	void fillFields();
	//
	void paintEvent(QPaintEvent* ev);
	
	const TOvrCorner *treeCorner(){ return corner; }

public slots:
	void removeCurrentExpression();

signals:
	void cornerDataChanged();
	void focusChanged( TOverlayTreeWidget *me, bool state );
	//
//	void needRepaint();

protected:
	virtual QMimeData* mimeData( const QList<QTreeWidgetItem*> items ) const;
	virtual bool dropMimeData( QTreeWidgetItem * parent, int index, const QMimeData * data, Qt::DropAction action );
	virtual void dropEvent(QDropEvent *event);
	//
	virtual void focusInEvent( QFocusEvent *event );
	//
	virtual void mouseDoubleClickEvent( QMouseEvent *event );
	virtual void mouseMoveEvent( QMouseEvent *event );

protected slots:
	void slotItemChanged( int state );

private:
	void addInsertButton();
	void setHeaders();
	void checkExprExistance(QTreeWidgetItem *item);
	void addItemFromExpr( const TOvrExpression &expr );
	void updateData( QTreeWidgetItem *item );
	QTreeWidgetItem* itemOfItemWidget( QWidget *widget, int column );
	//
	bool above;
	bool active;
	TOvrCorner *corner;
	TExpressionDict *exprDict;
	TElemContList &m_contList;
};

#endif