#ifndef ELEMENTPARSER_H
#define ELEMENTPARSER_H

#include "elem.h"
#include "dicomDict.h"
class TDicomDict;

///////////////////////////////////////////////////////////////////////////
// TElementParser /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TDicomDict;
struct	 TElement;
//
class TElementParser
{
public:
	TElementParser( TDicomDict *dDict );
	//
	void setCodec( const QString &codecName );
	//
	TElement getElement( int grp, int elem, const char *m, int len );
	TElement getElement( int grp, int elem, const char *m, int len, char type[2]  );

private:
	EType type;
	int group, element, length;
	const char *mem;
	TDicomDict *dict;
	QTextCodec *codec;
	//
	QString fromSpecificCodec( const char *mem, int length );
	TElement loadElement();
	//
	TElement loadAE();
	TElement loadAS();
	TElement loadAT();
	TElement loadCS();
	TElement loadDA();
	TElement loadDS();
	TElement loadDT();
	TElement loadFL();
	TElement loadFD();
	TElement loadIS();
	TElement loadLO();
	TElement loadLT();
	TElement loadOB();
	TElement loadOF();
	TElement loadOW();
	TElement loadPN();
	TElement loadSH();
	TElement loadSL();
	TElement loadSQ();
	TElement loadSS();
	TElement loadST();
	TElement loadTM();
	TElement loadUI();
	TElement loadUL();
	TElement loadUN();
	TElement loadUS();
	TElement loadUT();
	//QString fromSpecificCodec(const char *mem, int length);
};


#endif //ELEMENTPARSER_H