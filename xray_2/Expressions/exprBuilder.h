#ifndef EXPRBUILDER_H
#define EXPRBUILDER_H

class TElemCont;
class TElemContList;
struct TElement;
class TExpressionDict;

///////////////////////////////////////////////////////////////////////////
// TExprBuilder ///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TExprBuilder
{
public:
	TExprBuilder( TExpressionDict *dict, TElemContList &cList ): contList(cList), exprDict(dict) {}
	//
	QString execExpr( const QString &exprName ) const;
	QStringList execExprList( const QStringList &list ) const;

private:
	TElemContList &contList;
	TExpressionDict *exprDict;
};



#endif //EXPRBUILDER_H