#ifndef OVERLAYDICT_H
#define OVERLAYDICT_H

///////////////////////////////////////////////////////////////////////////
// TOvrExpression /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TOvrExpression
{
public:
	TOvrExpression();
	TOvrExpression( const QString &str, const QString &desc, bool show = true, bool first = false, bool printer = true );
	//
	QString m_name, m_description;
	bool m_onlyFirst;
	bool m_onPrinter;
	bool m_show;
};

///////////////////////////////////////////////////////////////////////////
// TOvrCorner /////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TOvrCorner
{
public:
	bool hasName( QString name )	const;
	QStringList getNames()			const;
	//
	void addExpr( TOvrExpression &expr );
	int indexOf( QString name );
	TOvrExpression takeExpr( int index );
	TOvrExpression takeExpr( QString name );
	//
	QList<TOvrExpression> eList;
};

///////////////////////////////////////////////////////////////////////////
// TOvrMod ////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TOvrMod
{
public:
	enum EPos
	{
		ELeftTop = 0
	,	ETop
	,	ERightTop
	,	ELeft
	,	ERight
	,	ELeftBottom
	,	EBottom
	,	ERightBottom
	,	EInvalidPos
	};
	//
	static EPos			stringToEPos( QString &hPos, QString &vPos );
	static QStringList	EPosToString( EPos );
	static QString		EPosToLitString( EPos );
	//
	TOvrMod();
	TOvrMod( QString name );
	//
	EPos	findName( const QString &name ) const;
	TOvrCorner &corner( EPos pos);
	QStringList expressions( EPos pos, bool firstPage, bool printer = false );
	void clear();
	//
	QString modality, description;
	QMap< EPos, TOvrCorner > corners;

private:
	void genCorners();
};

///////////////////////////////////////////////////////////////////////////
// TOverlayDict ///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
class TOverlayDict
{
public:
	static TOverlayDict *getDict();
public:
	TOverlayDict();
	//
	bool loadDict( QString path );
	bool saveDict( QString path );
	void updateMod( TOvrMod &newV );
	void createMod( QString name );
	void deleteMod( QString name );
	bool haveChanged() { return changed; }
	//
	QStringList getModalityList();
	const TOvrMod &getMod( QString mod ) { return modMap[mod]; }

private:
	QMap <QString, TOvrMod> modMap;
	bool changed;
};


#endif //OVERLAYDICT_H