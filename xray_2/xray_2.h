#ifndef XRAY2_H
#define XRAY2_H

//#include "ui_xray_2.h"
class BranchFrame;
class TreeDataViewModel;
class TreeDataModel;
class PrintDialog;
class PrintTreeDataModel;


class bb : public QPushButton	
{
	Q_OBJECT
		~bb(){
			int a = 4;
	};
};

class LocalLoader;
class HttpLoader;
class TAddFilmDialog;
class xray_2 : public QMainWindow
{
	Q_OBJECT

public:
	xray_2(QString _VrefFilePath = "", QWidget *parent = 0, Qt::WFlags flags = 0);
	~xray_2();
	void makeToolbars();
	static PrintTreeDataModel * printDataModel;
public slots:

	void onEXPORTTOINVOLS();
	void startLoadingSlot();
	void endLoadingSlot();

	void onCHANGEPOSITIONMAIN1();
	void onCHANGEPOSITIONMAIN2();
	void onCHANGEPOSITIONMAIN3();
	void onCHANGEPOSITIONMAIN4();
	void onGONEXTIMAGE();
	void onGOPREVIMAGE();
	void onCONTRASTIMAGE();	 
	void onRESETALL();
	void onSetWL();	 	
	void onREFLECT();	 	
	void onMOVEIMAGE();	 	
	void onHIDEOVERLAY();	 	
	void onTURNVERT();
	void onTURNHOR();
	void onROTATERIGHT();
	void onROTATELEFT();	
	void onDICOMDIRECTORY();
	void onDOWNLOADFILES(QStringList files);
	void onADDFILES(QStringList files);
	void onCREATETREE(QString dirName);
	void changeSplitMenu(QAction *);
	void onExpressionEditor();
	void onOverlayEditor();	

	void onANGLE();
	void onCURVE();
	void onSPOT();
	void onTEXT();
	void onDISTANCE();
	void onDELETEMARKS();
	void onHIDEMARKS();

	void onHELP();
	void onABOUT();
	void onSaveWindowState();
	void onPRINT();

private:
	bool isFirstShow;
	QMenu * splitMenu;
	QToolButton * menuAction;
	void setPositionOnMonitors();
	int primaryScreen;
	int secondScreen;
	int thirdScreen;
	QString VrefFilePath;

	LocalLoader * localLoader;
	HttpLoader *  httpLoader;
	TAddFilmDialog * addFilmDialog;
	//StudySplitter * studySplitter;
	//Ui::xray_2Class ui;
	QWidget centralWidget;
	void hideEvent(QHideEvent * event);
	void keyPressEvent(QKeyEvent * event);
	//void keyWheelEvent(QWheelEvent * event);
	void wheelEvent(QWheelEvent * event);
	void showEvent(QShowEvent * event);
	bool winEvent(MSG *message, long *result);
	void processWM_COPYDATA(MSG *message, long *result);
	QToolBar *makeStudyLayoutToolbar( QMenu *splitMainMenu, QMenu *navigateMainMenu, QActionGroup *studyLayoutGroup /* = NULL */ );

	QAction * createAction(QString name, QString iconName, const char* member, QActionGroup* actionGroup = NULL, QMenu* menu = NULL, QToolBar* toolBar = NULL);
	QAction * createAction(QString name, QString iconName, const char* member, Qt::Key key, QActionGroup* actionGroup = NULL, QMenu* menu = NULL, QToolBar* toolBar = NULL);
	void loadStyles();
	void initVewAndModels();
	TreeDataViewModel * treeDataViewModel;
	TreeDataModel * model ;
	BranchFrame * branchFrame;

	QActionGroup *mouseModeGroup; // ��������, �������� ��������� ���� (����� ������). ������������������ 
	QActionGroup *measureGroup; // ������������ mouseModeGroup, �������� �� ����������
	QActionGroup *studyLayoutGroup; // 
	QActionGroup *seriesLayoutGroup;
	QActionGroup *selectionModeGroup; 
	PrintDialog * printDialog;

};


class AboutDialog : public QDialog{
	Q_OBJECT	
public:
	AboutDialog();
protected:		

	void paintEvent(		QPaintEvent * event);
	void mousePressEvent(	QMouseEvent * event);
};

#endif // XRAY2_H
