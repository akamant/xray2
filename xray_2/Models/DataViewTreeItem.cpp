#include "StdAfx.h"
#include "DataViewTreeItem.h"
#include "dicomdata.h"


DataViewTreeItem::DataViewTreeItem( const QList<QVariant> &data, TreeItem *parent /*= 0*/ , QObject * parentObject)
	:TreeItem(data, parent, parentObject)
	,dicomData(NULL)		
	,isActive(false)
	,_isViewOne(false)
{	
	division = QVector2D(1, 1);
}
DataViewTreeItem::~DataViewTreeItem(void)
{	
}

QModelIndex DataViewTreeItem::getIndex()
{	
	return index;
}

void DataViewTreeItem::setDicomData( DicomData * dicomData )
{	
	_viewParameters.setDicomData(dicomData);
	this->dicomData = dicomData;
	connect	(	dicomData->getEmiter(),		SIGNAL	(bodyCreated		(DicomData *) ),
				this,						SLOT	(processBodyCreated	(DicomData *) ),
				Qt::UniqueConnection
			);	
}

void DataViewTreeItem::processBodyCreated(DicomData * dd)
{
	emit bodyCreated(this);
}

void DataViewTreeItem::setIndex( const QModelIndex & i )
{	
	index = i;
}

void DataViewTreeItem::setDivision(QVector2D division)
{
	this->division = division;

	while(childItems.size() > division.x() * division.y())
	{
		delete childItems[childItems.size() - 1];
		childItems.removeLast();
	}
	emit changeDivision(division.x(), division.y());
}

void DataViewTreeItem::setRows( int rows )
{
	division.setX(rows);
	emit changeDivision(division.x(), division.y());
}

void DataViewTreeItem::setCols( int cols )
{
	division.setY(cols);
	emit changeDivision(division.x(), division.y());
}

void DataViewTreeItem::setActivity( bool i )
{
	isActive = i;
}
