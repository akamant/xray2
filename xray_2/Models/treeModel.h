#ifndef TREEMODEL_H
#define TREEMODEL_H
#include <QAbstractItemModel>

class TreeItem;

class TreeModel : public QAbstractItemModel
{
	Q_OBJECT

signals:
	void modelChanged();

public:
	TreeModel(QObject *parent = 0);
	~TreeModel();	
	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QVariant headerData(int section, Qt::Orientation orientation,
		int role = Qt::DisplayRole) const;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &index) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	int numberChildren(const QModelIndex &index );
protected:	
	virtual TreeItem * getTreeItemByIndex(const QModelIndex &index);
	void setupModelData(const QStringList &lines, TreeItem *parent);	
	
	//virtual TreeItem * getNewTreeItem(const QList<QVariant> &data, TreeItem *parent = 0);	
	//void addDicomData(DicomData * dicomData);
	//void addDicomData(DicomData * dicomData, QModelIndex modelIndexParent = QModelIndex());
	TreeItem *rootItem;


};

#endif // STUDYDATAMODEL_H
