#pragma once
//#include "threadedttreemodel.h"
#include "treemodel.h"


class TreeDataModel;
class DataViewTreeItem;
class DicomData;
class Command;
class ViewParameters;
class DataViewModelDispatcher;
class TreeDataViewModel : public TreeModel
{
Q_OBJECT
signals:
	void changedDataModelIndex(const QModelIndex & index);
	void changeSelectedItemSignal (const QModelIndex & index);
	void changingParameter(const QModelIndex & index);
	void onlyOneItemActive();
	void changeActivitySignal(const QModelIndex & index);
	void changeDivision(const QModelIndex & index,  int, int);
	void command(const QModelIndex &, const Command & command);
	void brothersCreated( const QModelIndex & );
	void changedViewOne();
	void overlaySwitched();
	

public:	
	void switchOverlay();
	DataViewModelDispatcher * modelDispatcher() {return dispatcher;};
	enum Direction{Back, Forward};
	TreeDataViewModel(TreeDataModel * model, QObject *parent = 0);
	Q_ENUMS(Direction)	
	virtual void setIsViewOne(const QModelIndex & index, bool i);
	virtual void setIsViewOne( const QModelIndex & index);
	virtual bool isIndexViewOne(const QModelIndex & index);
	virtual void emitCommand( const Command & _command, const QModelIndex &modelIndexParent  = QModelIndex());
	QString getModality(DicomData * dicomData);
	QString getModality(const QModelIndex & index);	
	~TreeDataViewModel(void);			
	void fillDataForAllItem(const QModelIndex & modelIndexParent = QModelIndex());
	TreeItem * findTreeItem(const QModelIndex & index, TreeItem * treeItem  = NULL);
	bool addNewTreeItem(const QModelIndex & indexParent, int rowNumber);
	int addNewTreeItem( DataViewTreeItem * treeItem, const QModelIndex & modelIndexChild);
	virtual bool setData(const QModelIndex &index, const QVariant &value, int role  = Qt::EditRole);	
	QVariant * data( const QModelIndex & index, int role = Qt::DisplayRole);
	QModelIndex getDataModelIndex(const QModelIndex & index);
	QVector2D getDivisionForItem(const QModelIndex &index);
	bool getActivityItem(const QModelIndex &index);
	void setActivityForItem(const QModelIndex &index, bool activity);
	uint getIndexTag (const QModelIndex & index);
	virtual ViewParameters getViewParameters( const QModelIndex &index);
	bool isSelectedItemIndex (const QModelIndex & index);
	virtual QModelIndex selectedItem() {return selectedItemIndex;};

	void setDispatcher (DataViewModelDispatcher * _dispatcher){dispatcher = _dispatcher;};

public slots:	
	void reCountIndexes( const QModelIndex & parentModelIndex, int rowNumberStart, int rowNumberEnd );
	void deleteChildrenSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd );
	void selectNextPrevToLowLevel(TreeDataViewModel::Direction, QModelIndex);	
	void addNewBranchToParent(const QModelIndex &  indexParent, const QModelIndex &  modelIndexChild);
	void setDivisionForItem(const QModelIndex &index, const QVector2D & division);
	void addNewTreeItemSlot( const QModelIndex & index, int rowNumberStart, int rowNumberEnd);		
	void processChanging(const QModelIndex & topLeft, const QModelIndex & bottomRight );
	void processBodyCreated( TreeItem * treeItem );		
	void changeDataModelIndex(QModelIndex index, QModelIndex modelIndex);
	void changeDataModelAndFillIndex(QModelIndex index, QModelIndex modelIndex);
	void selectNextChildren(const QModelIndex &  index);
	void selectPrevChildren(const QModelIndex &  index);

	void changeChildrenModelIndexesToParent(const QModelIndex & index, const QModelIndex & firstChildIndex);

	TreeDataModel * getDataModel(){return treeDataModel;};
	virtual void selectTreeItem(const QModelIndex & index, bool isShifted);
	DicomData * getDicomData(const QModelIndex & index);
	void setDivisionForItemSlot(DataViewTreeItem * item);
	void changeDivisionSlot(int rows, int cols);

private:		
	DataViewModelDispatcher * dispatcher;
	QString settingsDir;
	bool firstFilling;
	QModelIndex getNextAfterLastChildren(const QModelIndex & index);
	QModelIndex getFirstPrevSetChildren(const QModelIndex & index);
	void fillBranch(const QModelIndex & index);
	void fillInFromModel(const QModelIndex & modelIndexParent = QModelIndex());
	void setAllInactive(const QModelIndex & index  = QModelIndex());
	
	DicomData * dicomData;		
	bool isFilling;
protected:
	TreeDataModel * treeDataModel;
	QModelIndex selectedItemIndex;
	virtual bool applyForGroup(const QModelIndex & index);
	void fillTreeItemsByEmptyTags (DataViewTreeItem * treeItem, const QList<uint> & tags, int curTagNumber);
	virtual QVariant getDataForItem( TreeItem * treeItem);	
	bool findActivity(const QModelIndex & modelIndexParent = QModelIndex());	
	bool existIndexInModel(const QModelIndex & index, const QModelIndex & modelIndexParent = QModelIndex ());
	void startInit();
	void setActiveTreeItem(const QModelIndex & index);
	void setActiveTreeItem(DataViewTreeItem * item);
	void setInActiveTreeItem(const QModelIndex & modelIndex);
	void setInActiveTreeItem(DataViewTreeItem * item);
	virtual TreeItem * getTreeItemByIndex(const QModelIndex &index );
	void createEmptyStructureForTreeItem(DataViewTreeItem * treeItem);
	void serializeTreeStructure (DataViewTreeItem * treeItem, const QString &modality);
	void serializeTreeItem(QDomDocument &doc, QDomNode & docNode, DataViewTreeItem * treeItem);
	void deSerializeTreeItem(QDomDocument &element, QDomElement & docNode, DataViewTreeItem * treeItem, const QList<uint> & tags, uint curTagNumber);
	void deSerializeTreeStructure (DataViewTreeItem * treeItem, const QString &modality, const QList<uint> & tags);
	void tryDeSerializeTreeStructure(DataViewTreeItem * item, const QModelIndex & modelIndex);	
	DataViewTreeItem * addChildItem(DataViewTreeItem * treeItem, const QModelIndex & index, uint tag);
	void setModelIndexToItem(DataViewTreeItem * treeItem, const QModelIndex & modelIndexChild);
	bool haveActiveInBot ( DataViewTreeItem * item);
	void tryFillActiveLine(DataViewTreeItem * item = NULL);
	bool haveActiveChildren(DataViewTreeItem * item);
	DataViewTreeItem * viewOneItem(const QModelIndex & index);
	void checkForNoOneActiveChildren(DataViewTreeItem * item);
	virtual void emitChangeSelectedItemSignal(QModelIndex index);
	void flushItem(TreeItem * item );
};
