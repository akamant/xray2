#include "StdAfx.h"

#include "TreeDataViewModel.h"
#include "treedatamodel.h"
#include "DataViewTreeItem.h"
#include "TLUT.h"
#include "dicombitmappresenter.h"
#include "ModalityShemeElements.h"
#include "qslog.h"


/*
TreeDataViewModel::TreeDataViewModel( DicomData * dicomData, QObject *parent  )
:TreeDataModel(dicomData, parent)
{

}*/

TreeDataViewModel::TreeDataViewModel( TreeDataModel * model, QObject *parent /*= 0*/ )
	:TreeModel(parent)
	,treeDataModel(model)
	,dicomData(NULL)
	,isFilling(false)
	,firstFilling(true)	

{	
	selectedItemIndex = treeDataModel->emptyDicomDataIndex();
	qRegisterMetaType<TreeDataViewModel::Direction>( "TreeDataViewModel::Direction" );
	settingsDir = "settings";
	QList <QVariant> list;
	list << "rootItem";		
	this;
	rootItem = new DataViewTreeItem (list, NULL);	
	((DataViewTreeItem *)rootItem)->setIndex(QModelIndex());

	connect(rootItem,	SIGNAL(changeDivision(int, int)),
			this,		SLOT (changeDivisionSlot(int, int)),
			Qt::UniqueConnection);

	QList <uint> tags = treeDataModel->getTags(QModelIndex());

	QString modality = this->getModality(QModelIndex());

	startInit();
	tags.clear();
	tags << StudyUID;

	fillTreeItemsByEmptyTags((DataViewTreeItem *)rootItem, tags, 0);

// 	connect	(	model,		SIGNAL	( rowsAboutToBeRemoved 	( const QModelIndex &, int, int ) ),
// 				this,		SLOT	( deleteChildrenSlot	( const QModelIndex &, int, int ) )
// 				);

 	connect	(	model,		SIGNAL	( RowsIsGoingToRemove 	( const QModelIndex &, int, int ) ),
 		this,				SLOT	( deleteChildrenSlot	( const QModelIndex &, int, int ) )
 		);

// 	connect	(	model,		SIGNAL	( rowsAboutToBeRemoved 	( const QModelIndex &, int, int ) ),
// 				this,		SLOT	( deleteChildrenSlot	( const QModelIndex &, int, int ) )
// 		);
	
	connect	(	model,		SIGNAL	( rowsRemoved 		( const QModelIndex &, int, int ) ),
				this,		SLOT	( reCountIndexes	( const QModelIndex &, int, int ) )
		);
		
/*	connect(		treeDataModel,		SIGNAL	( rowsInserted			( const QModelIndex &, int, int ) ),
					this,				SLOT	( addNewTreeItemSlot	( const QModelIndex &, int, int ) )
			);*/

	//fillDataForAllItem();
	
	
}

void TreeDataViewModel::processChanging(const QModelIndex & topLeft, const QModelIndex & bottomRight )
{	
	TreeItem * treeItem = findTreeItem(topLeft);

	DicomData * dicomData = treeDataModel->getDicomDataByIndex(topLeft);

	if(treeItem && dicomData)
	{
		((DataViewTreeItem *)treeItem)->setDicomData(dicomData);
	}else
	{
		return;
		QLOG_ERROR() << "null dicomData in setDicomData in treedataviewmodel3";

		if(!treeItem)
			QLOG_ERROR() << "tree";
	}

	if(dicomData && dicomData->getBody() && treeItem)
		processBodyCreated(treeItem);	

	if(treeItem && dicomData)
	{
		((DataViewTreeItem *)treeItem)->setDicomData(dicomData) ;
		connect(treeItem,		SIGNAL(bodyCreated(TreeItem *)),
				this,			SLOT(processBodyCreated(TreeItem *)),
				Qt::UniqueConnection);
	}	
}

void TreeDataViewModel::processBodyCreated( TreeItem * treeItem )
{
	QModelIndex index;
	if(treeItem == rootItem)
		index = QModelIndex();
	else
		index = createIndex(treeItem->row(), 0, treeItem);	
	QVariant var = getDataForItem(treeItem);
	setData(index, var);	
}

QVariant TreeDataViewModel::getDataForItem(TreeItem * treeItem)
{
	return NULL;
}

QVector2D TreeDataViewModel::getDivisionForItem(const QModelIndex &index)
{
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	return item->getDivision();
}

void TreeDataViewModel::setDivisionForItemSlot(DataViewTreeItem * item)
{
	emit changeDivision(createIndex(item->row(), 0, item) , item->getDivision().x(), item->getDivision().y());
}

void TreeDataViewModel::setDivisionForItem(const QModelIndex &index, const QVector2D & division)
{		
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	int prevCount = item->childCount();
	int newCount = division.x() * division.y();
	
	
	if(newCount < prevCount)
	{
 		int startRow = newCount;
 		int deleteCount = prevCount - startRow; 		
		for(int r = prevCount - 1; r >= newCount; r --)
		{

			TreeItem * findItem = getTreeItemByIndex(selectedItemIndex);
			while(selectedItemIndex != treeDataModel->emptyDicomDataIndex() && findItem  &&  findItem != rootItem)
			{
				if(findItem == getTreeItemByIndex(this->index(r, 0, index)))
					selectedItemIndex = treeDataModel->emptyDicomDataIndex();
					
				findItem = (DataViewTreeItem *)findItem->parent();
			}
  			item->removeChild(getTreeItemByIndex(this->index(r, 0, index)));
		}			

		beginRemoveRows(index, startRow, startRow + deleteCount);
 		bool result = removeRows ( startRow, deleteCount, index);
 		endRemoveRows();		
	}

	item->setDivision(division);

	fillBranch(index);	
	checkForNoOneActiveChildren((DataViewTreeItem *) rootItem);

	emit brothersCreated(this->index(0, 0, index));
	
	while(item  &&  item != rootItem && item->parent() != rootItem)
		item = (DataViewTreeItem *)item->parent();

	if(item && item != rootItem)
		serializeTreeStructure(item, getModality(index));

	emit modelChanged();
}

bool TreeDataViewModel::getActivityItem(const QModelIndex &index)
{
	if(rootItem == NULL)
		return false;
	if(index == QModelIndex()){
		return findActivity();
	}else{
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
		return item->getActivity();
	}	
}


bool TreeDataViewModel::findActivity(const QModelIndex & modelIndexParent)
{
	int rowC = rowCount(modelIndexParent);
	for(int r = 0; r < rowC; r ++)
	{		
		QModelIndex modelIndex = index(r, 0, modelIndexParent);
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(modelIndex);
		if(item->getActivity())
			return true;		
	}
	return false;
}

void TreeDataViewModel::setActivityForItem(const QModelIndex &index, bool activity)
{
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	item->setActivity(activity);
	return;
}


bool TreeDataViewModel::setData( const QModelIndex &index, const QVariant &value, int role /*= Qt::EditRole*/ )
{
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);	
	DicomData * dicomData = item->getDicomData();
	//if(treeDataModel->getTags(index).empty() || getIndexTag(index) != treeDataModel->getTags(index)[treeDataModel->getTags(index).size() - 1] )
	//	return false;	

	DataViewTreeItem * childItem = (DataViewTreeItem * ) item;
	if(firstFilling)
	{		
		if(treeDataModel->getTags(index).indexOf (getIndexTag(index)) ==  treeDataModel->getTags(index).count() - 1){

			setActiveTreeItem(childItem);
			firstFilling = false;
		}
	}

	if(!dicomData->getisNormalized())
	{
		dicomBitMapPresenter presenter(NULL);
		presenter.representDicomData(dicomData);		
	}
			
	emit dataChanged(index, index);
	return true;
}

QVariant * TreeDataViewModel::data( const QModelIndex & index, int role /*= Qt::DisplayRole */ )
{
	if (!index.isValid())
		return NULL;
	DataViewTreeItem *item = (DataViewTreeItem *)getTreeItemByIndex(index);	
	return NULL;
}


QModelIndex TreeDataViewModel::getDataModelIndex(const QModelIndex & index)
{
	DataViewTreeItem *item;
	if(index == QModelIndex())
		item = (DataViewTreeItem *)rootItem;
	else
		item = (DataViewTreeItem *)getTreeItemByIndex(index);
	return item->getIndex();
}

uint TreeDataViewModel::getIndexTag (const QModelIndex & index)
{
	DataViewTreeItem *item;
	if(index == QModelIndex())
		item = (DataViewTreeItem *)rootItem;
	else
		item = (DataViewTreeItem *)getTreeItemByIndex(index);

	if(item->getIndex() == treeDataModel->emptyDicomDataIndex())
		return item->tag;
	return treeDataModel->getTagByIndex(item->getIndex());
}




void TreeDataViewModel::fillInFromModel(const QModelIndex & modelIndexParent)
{
	int rowCount = treeDataModel->rowCount(modelIndexParent);
	for(int r = 0; r < rowCount; r ++){
		addNewTreeItem(modelIndexParent, r);
		QModelIndex modelIndex =  treeDataModel->index(r, 0, modelIndexParent);
		fillInFromModel(modelIndex);
	}
}

bool TreeDataViewModel::existIndexInModel(const QModelIndex & index, const QModelIndex & modelIndexParent)
 {	
	 if(index == QModelIndex())
		 return true;
 	int rowCount = this->rowCount(modelIndexParent);
 	for(int r = 0; r < rowCount; r ++){ 	
		QModelIndex modelIndex =  this->index(r, 0, modelIndexParent);
		if(modelIndex == index)
			return true;
 		if(existIndexInModel(index, modelIndex))
			return true;
	}
	return false;
 }


DicomData * TreeDataViewModel::getDicomData(const QModelIndex & index)
{
	if(index == QModelIndex())
		return NULL;
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	if(!item || treeDataModel->emptyDicomDataIndex() == item->getIndex() || 	!existIndexInModel(index))
		return NULL;
	return treeDataModel->getDicomDataByIndex(item->getIndex());
}

void TreeDataViewModel::changeDataModelIndex(QModelIndex index, QModelIndex modelIndex)
{
	//this;
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	tryDeSerializeTreeStructure(item, modelIndex);
	if(item->getIndex() != modelIndex)
	{
		if(item->getIndex() != treeDataModel->emptyDicomDataIndex() || item->parent() == rootItem
			&& item->getIndex() != treeDataModel->emptyDicomDataIndex())
		{

			for(int i = 0; i < item->childCount(); i ++)
				flushItem(item->child(i));

			item->removeChildren();
		}
		tryDeSerializeTreeStructure(item, modelIndex);
		setModelIndexToItem(item, modelIndex);		
	}
	checkForNoOneActiveChildren((DataViewTreeItem *) rootItem);
}


void TreeDataViewModel::changeDataModelAndFillIndex(QModelIndex index, QModelIndex modelIndex)
{
	changeDataModelIndex(index, modelIndex);
	fillBranch(index);
}


void TreeDataViewModel::tryDeSerializeTreeStructure(DataViewTreeItem * item, const QModelIndex & modelIndex)
{
	if(item->parent() == rootItem)
	{
		QString modality = treeDataModel->getModality(modelIndex);
		QList<uint> tags = treeDataModel->getTags(modelIndex);

		int tag = treeDataModel->getTagByIndex(modelIndex);

		while(tags.size()!= 0 && tags.first() != tag )
			tags.removeFirst();
		if(tags.size())
			tags.removeFirst();

		deSerializeTreeStructure(item, modality ,tags);
	}
}

void TreeDataViewModel::addNewTreeItemSlot(const QModelIndex & index, int rowNumberStart, int rowNumberEnd)
{	
	addNewTreeItem(index, rowNumberStart);	
}


bool TreeDataViewModel::addNewTreeItem( const QModelIndex & modelIndexParent, int rowNumber)
{
	QList<QVariant> list;	
	DataViewTreeItem * treeItem;
	if(modelIndexParent == QModelIndex())	
		 treeItem = (DataViewTreeItem *) rootItem;
	else
		 treeItem = (DataViewTreeItem *) findTreeItem(modelIndexParent);	

	if(treeItem == NULL)	
		return false;
	
	if(//treeItem->childCount() >= treeItem->getDivision().x() * treeItem->getDivision().y()||
			findTreeItem(treeDataModel->index(rowNumber, 0, modelIndexParent))
		)
		return false;

	return addNewTreeItem(treeItem, treeDataModel->index(rowNumber, 0, modelIndexParent));
		
}


int TreeDataViewModel::addNewTreeItem( DataViewTreeItem * treeItem, const QModelIndex & modelIndexChild)
{	
	

	if(treeItem == NULL)
	{
		qDebug() << "null tree item";
		return -1;
	}	
	
	QString modality = getModality(treeDataModel->getDicomDataByIndex(modelIndexChild));
	DataViewTreeItem * childItem = NULL;
	for(int i = 0; i < treeItem->childCount(); i ++)
	{
		DataViewTreeItem * item = (DataViewTreeItem *) treeItem->child(i);
		//if( item->getIndex() == treeDataModel->emptyDicomDataIndex(treeItem->getIndex()))
		//if( item->getIndex() == treeDataModel->emptyDicomDataIndex())
		if( treeDataModel->isEmptyDicomDataIndex(item->getIndex())) // == treeDataModel->emptyDicomDataIndex())
		{
			childItem = item;
			break;
		}
	}
	
	if(!childItem)
	{		
		if(treeItem->childCount() >= treeItem->getDivision().x() * treeItem->getDivision().y()
			//|| treeItem != rootItem
			)
			return -1;

		childItem = addChildItem(treeItem, modelIndexChild, treeDataModel->getTagByIndex(modelIndexChild));
		//createEmptyStructureForTreeItem(childItem);

		if(treeItem == rootItem)
		{
			tryDeSerializeTreeStructure(childItem, modelIndexChild);
		}
	}else
	{
		//return false;
		//if(treeItem == rootItem)	

		
		 changeDataModelIndex(createIndex(childItem->row(), 0, childItem), modelIndexChild);
	}

	if(childItem)
		return childItem->row();
	
	return -1;
}


void TreeDataViewModel::fillDataForAllItem(const QModelIndex & modelIndexParent)
{			
	int rowC = rowCount(modelIndexParent);
	for(int r = 0; r < rowC; r ++)
	{		
		QModelIndex modelIndex = index(r, 0, modelIndexParent);
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(modelIndex);
		DicomData * dicomData = treeDataModel->getDicomDataByIndex(item->getIndex());				
		if(dicomData)
		{
			item->setDicomData(dicomData) ;		
			if(dicomData->getBody()){
				processBodyCreated(item);
			}else{
			connect(	item,		SIGNAL(bodyCreated(TreeItem *)),
						this,			SLOT(processBodyCreated(TreeItem *)),
						Qt::UniqueConnection
					);
			}
		}else{
			QLOG_ERROR() << "null dicomData in setDicomData in in treedataviewmodel1";
		}
	fillDataForAllItem(modelIndex);		
	}
}


TreeItem * TreeDataViewModel::findTreeItem(const QModelIndex & index, TreeItem * treeItem  /* = rootItem*/){	
	if(index == QModelIndex())
		return rootItem;
	if(!treeItem)
		treeItem = rootItem;
	int childCount = treeItem->childCount();
	if(treeItem != rootItem &&((DataViewTreeItem *)treeItem)->getIndex() == index)
		return treeItem;
	for(int c = 0; c < childCount; c ++){		
		TreeItem * childItem = treeItem->child(c);
		TreeItem * item = findTreeItem(index, childItem);
		if(item != NULL)
			return item;
	}
	return NULL;
}

TreeDataViewModel::~TreeDataViewModel(void)
{
	
}

void TreeDataViewModel::selectTreeItem(const QModelIndex & index, bool isShifted)
{			
	DataViewTreeItem * item = static_cast<DataViewTreeItem *>(getTreeItemByIndex(index));
	bool activity = false;
	if(item){
		activity = item->getActivity();			
	}
	if(!isShifted)
	{
		if(!activity)
			setAllInactive();		
		setActiveTreeItem(index);		
	}else{
		if(activity)
			setInActiveTreeItem(index);
		else
			setActiveTreeItem(index);

		//checkForNoOneActiveChildren(item);
	}
	if(!item->childCount() && item->getIndex() != treeDataModel->emptyDicomDataIndex())
	{
		selectedItemIndex = index;
		emitChangeSelectedItemSignal(index);
	}
	checkForNoOneActiveChildren((DataViewTreeItem *) rootItem);
	emit changeActivitySignal(QModelIndex());
}
void TreeDataViewModel::emitChangeSelectedItemSignal(QModelIndex index)
{
	emit changeSelectedItemSignal(index);
}

void TreeDataViewModel::setActiveTreeItem(const QModelIndex & modelIndexParent)
{		
	DataViewTreeItem * item = static_cast<DataViewTreeItem *>(getTreeItemByIndex(modelIndexParent));
	setActiveTreeItem(item);
		
}
void TreeDataViewModel::setActiveTreeItem ( DataViewTreeItem * item)
{
	if(true || item && item->getIndex() != treeDataModel->emptyDicomDataIndex())
	{
		bool activity = item->getActivity();
		item->setActivity(true);
		if(item->parent())
			setActiveTreeItem((DataViewTreeItem *)item->parent());
	}else{
		return;
	}		
}

void TreeDataViewModel::checkForNoOneActiveChildren(DataViewTreeItem * item)
{
	int rowC = item->childCount();		
	bool noActive = true;
	for(int r = 0; r < rowC; r ++)
	{	
 		DataViewTreeItem * itemChild = (DataViewTreeItem *)item->child(r);
		if( itemChild->getActivity() )
			noActive = false;		
	}
	if(noActive)
	{
		for(int r = 0; r < rowC; r ++)
		{	
			DataViewTreeItem * itemChild = (DataViewTreeItem *)item->child(r);
			if(itemChild->getIndex() != treeDataModel->emptyDicomDataIndex())
			{
				itemChild->setActivity(true);	
				break;
			}
		}		
	}

	if	( 
		//rowC &&
		item != rootItem
		&& treeDataModel->getTagByIndex(item->getIndex()) == treeDataModel->getTags(item->getIndex()).last()
		&& item->parent()
		&& ((DataViewTreeItem * )item->parent())->getActivity()  
		&& ((DataViewTreeItem * )item->parent())->getIndex() != treeDataModel->emptyDicomDataIndex()
		&& ((DataViewTreeItem * )item)->getIndex() != treeDataModel->emptyDicomDataIndex()

		&&( selectedItemIndex != treeDataModel->emptyDicomDataIndex() && !getActivityItem(parent(selectedItemIndex)) 
			|| selectedItemIndex == treeDataModel->emptyDicomDataIndex()
			)		
		)
	{
		QModelIndex index = createIndex(item->row(), 0, item);
		selectedItemIndex = index;
		emitChangeSelectedItemSignal(index);
	}

	for(int r = 0; r < rowC; r ++)
	{	
		DataViewTreeItem * itemChild = (DataViewTreeItem *)item->child(r);		
		if(itemChild->getActivity())				
			checkForNoOneActiveChildren(itemChild);
	}

	emit changeActivitySignal(QModelIndex());
}


void TreeDataViewModel::tryFillActiveLine(DataViewTreeItem * item)
{
	return;
	if(!item)
		item = (DataViewTreeItem * )rootItem;
	if(!haveActiveInBot(item) && item->childCount())
		setActiveTreeItem( (DataViewTreeItem *)item->child(0) );
	if(item->childCount())
		tryFillActiveLine((DataViewTreeItem *)item->child(0));
}

bool TreeDataViewModel::haveActiveInBot ( DataViewTreeItem * item)
{
	int rowC = item->childCount();		
	bool noActive = true;
	if(!rowC)
		return item->getActivity();
	for(int r = 0; r < rowC; r ++)
	{	
		DataViewTreeItem * itemChild = (DataViewTreeItem *)item->child(r);
		if(haveActiveInBot(itemChild))
			return true;
	}
	return false;
}

bool TreeDataViewModel::haveActiveChildren(DataViewTreeItem * item)
{
	int rowC = item->childCount();	
	for(int r = 0; r < rowC; r ++)
	{	
		DataViewTreeItem * itemChild = (DataViewTreeItem *)item->child(r);	
		if(itemChild->getActivity())
			return true;
	}
	return false;
}


void TreeDataViewModel::setInActiveTreeItem(const QModelIndex & modelIndex)
{
	DataViewTreeItem * item = static_cast<DataViewTreeItem *>(getTreeItemByIndex(modelIndex));
	setInActiveTreeItem(item);
}

void TreeDataViewModel::setInActiveTreeItem(DataViewTreeItem * item)
{		
	if(!item)
		return;		
	int rowC = item->childCount();		
	bool noActive = true;
	for(int r = 0; r < rowC; r ++)
	{	
		DataViewTreeItem * itemChild = (DataViewTreeItem *)item->child(r);	
		if(itemChild->childCount() == 0)
			itemChild->setActivity(false);
	}

	if(item)
	{
		if(!haveActiveChildren( (DataViewTreeItem *)item) )
		{
			item->setActivity(false);	
			if(item == rootItem)
				tryFillActiveLine();
		}
		if(item->parent())
			setInActiveTreeItem((DataViewTreeItem *)item->parent());		
	}else{
		return;
	}
	emit changeActivitySignal(QModelIndex());
}

void TreeDataViewModel::setAllInactive(const QModelIndex & modelIndexParent){
	int rowC = rowCount(modelIndexParent);
	for(int r = 0; r < rowC; r ++)
	{		
		QModelIndex modelIndex = index(r, 0, modelIndexParent);
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(modelIndex);
		item->setActivity(false);
		setAllInactive(modelIndex);
	}
	emit changeActivitySignal(QModelIndex());
}

QString TreeDataViewModel::getModality(DicomData * dicomData)
{
	if(dicomData)
		return dicomData->getELEM_CONT().get(0x00080060).variant.toString();
	else
		return "";
}

QString TreeDataViewModel::getModality(const QModelIndex & index)
{
	DicomData * dicomData = getDicomData(index);
	if(dicomData)
		return dicomData->getELEM_CONT().get(0x00080060).variant.toString();
	else 
		return "";
}


void TreeDataViewModel::fillBranch(const QModelIndex & index)//, const QModelIndex & modelIndexForFirst)
{

	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	QModelIndex modelIndex = item->getIndex();

	///// ���� �������� ��� ������� ������ �����������. ������������� ��� �������� ���� ������ � ������� ������ � �� ��������
	bool isFindedPrev = false;
	//bool haveEmpty = 
	int rowCountModel = treeDataModel->rowCount(modelIndex);
	for(int rModel = 0; rModel < rowCountModel; rModel ++)
	{ 		
		QModelIndex childModelIndex = treeDataModel->index(rModel, 0, modelIndex);	
		bool isFindedCur = false;
		int rowCount = this->rowCount(index);
		if(rowCount == 0)
			isFindedPrev = true;

		for(int r = 0; r < rowCount; r ++)
		{ 		
			QModelIndex childIndex = this->index(r, 0, index);
			DataViewTreeItem * childItem = (DataViewTreeItem *)getTreeItemByIndex(childIndex);
			if(childModelIndex == childItem->getIndex())
			{
				isFindedPrev = true;
				isFindedCur = true;
			}

			if(childItem->getIndex() == treeDataModel->emptyDicomDataIndex())
				isFindedPrev = true;
		}
		if((isFindedPrev) && !isFindedCur)				
			addNewBranchToParent(index, childModelIndex);		
				
	}

	createEmptyStructureForTreeItem(item);
}


void TreeDataViewModel::addNewBranchToParent(const QModelIndex & indexParent, const QModelIndex & modelIndexChild)
{
	DataViewTreeItem * itemParent = (DataViewTreeItem *)getTreeItemByIndex(indexParent);
	int numberIndex = addNewTreeItem(itemParent, modelIndexChild);
	if(numberIndex != -1)
	{	
		int rowCount = treeDataModel->rowCount(modelIndexChild);
		for(int r = 0; r < rowCount; r ++)
		{ 		
 			QModelIndex modelIndex = treeDataModel->index(r, 0, modelIndexChild);		
			addNewBranchToParent( index(numberIndex, 0, indexParent), modelIndex);					
 		}
	}
}

void TreeDataViewModel::selectNextChildren( const QModelIndex & index )
{		
	QModelIndex nextAfterLastChildIndex = getNextAfterLastChildren(index);
	if(nextAfterLastChildIndex != QModelIndex())
	{
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);	
		changeChildrenModelIndexesToParent(index, nextAfterLastChildIndex);
		
	}
}


void TreeDataViewModel::selectNextPrevToLowLevel(TreeDataViewModel::Direction direction, QModelIndex index )
{	
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	if(rowCount(index)!= 0 && rowCount(this->index(0, 0, index)) == 0 && item->getActivity() && index != QModelIndex())
	{
		if(direction == Back)
			selectPrevChildren(index);
		else
			selectNextChildren(index);
		return;
	}
	int rowCount = this->rowCount(index);
	for(int r = 0; r < rowCount; r ++)
	{
		QModelIndex childIndex = this->index(r, 0, index);		
		selectNextPrevToLowLevel(direction, childIndex);
	}
}


void TreeDataViewModel::selectPrevChildren( const QModelIndex & index )
{	

	QModelIndex firstPrevSetChildIndex = getFirstPrevSetChildren(index);

	if(firstPrevSetChildIndex != getDataModelIndex(this->index(0, 0, index)))
	{
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);		
		changeChildrenModelIndexesToParent(index, firstPrevSetChildIndex);		
	}
}


DataViewTreeItem * TreeDataViewModel::viewOneItem(const QModelIndex & index)
{
	int rowCount = this->rowCount(index);
	for(int r = 0; r < rowCount; r ++)	
	{
		QModelIndex childIndex = this->index(r, 0, index);	
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(childIndex);
		if( item->isViewOne() )
			return item;	
		//if(viewOneItem(childIndex))
		//	return viewOneItem(childIndex);
	}
	return NULL;
}


void TreeDataViewModel::changeChildrenModelIndexesToParent(const QModelIndex & index, const QModelIndex & firstChildIndex)
{

	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);
	QModelIndex modelIndex = item->getIndex();

	bool isFindedPrev = false;
	int rowCountModel = treeDataModel->rowCount(modelIndex);
	int rModel = 0;
	for(rModel = 0; rModel < rowCountModel; rModel ++)
	{
		QModelIndex childModelIndex = treeDataModel->index(rModel, 0, modelIndex);
		if(firstChildIndex == childModelIndex)
			break;
	}
	
	int rowCount = this->rowCount(index);	
	int r = 0;
	for(r = 0; r < rowCount && rModel < rowCountModel; r ++)
	{
		QModelIndex childIndex = this->index(r, 0, index);
		DataViewTreeItem * childItem = (DataViewTreeItem *)getTreeItemByIndex(childIndex);

		QModelIndex childModelIndex = treeDataModel->index(rModel, 0, modelIndex);

		QModelIndex curIndex = childItem->getIndex();

		setModelIndexToItem(childItem, childModelIndex);
		if(curIndex == treeDataModel->emptyDicomDataIndex())
		emit changedDataModelIndex(this->index(r, 0, index));
	
		rModel ++;
	}
	int startRow = r;
	int deleteCount = rowCount - r;
	
	for(r = rowCount - 1; r >= startRow; r --)
		changeDataModelIndex	(this->index(r, 0, index), treeDataModel->emptyDicomDataIndex());	

	checkForNoOneActiveChildren((DataViewTreeItem *) rootItem);

}

QModelIndex TreeDataViewModel::getNextAfterLastChildren(const QModelIndex & index)
{	
	int rowCount= this->rowCount(index);
	QModelIndex lastIndexModelIndex;
	DataViewTreeItem * dataViewItem = viewOneItem(index);
	
	lastIndexModelIndex = getDataModelIndex(this->index(rowCount - 1, 0, index));
	QModelIndex firstIndexModelIndex = getDataModelIndex(this->index(0, 0, index));

	if(lastIndexModelIndex == treeDataModel->emptyDicomDataIndex() && !dataViewItem)
		return QModelIndex();

	QModelIndex dataModelParentIndex = treeDataModel->parent(firstIndexModelIndex);
	int rowCountModel = treeDataModel->rowCount(dataModelParentIndex);

// 	if(dataViewItem)
// 		if(dataViewItem->getIndex() == treeDataModel->index(rowCountModel - 1, 0, dataModelParentIndex))
// 			return QModelIndex();
// 		else
// 			return  getDataModelIndex(this->index(1, 0, index));

	bool findexLastDataModelInex = false;
	for(int rModel = 0; rModel < rowCountModel; rModel ++)
	{ 		
		QModelIndex childModelIndex = treeDataModel->index(rModel, 0, dataModelParentIndex);
		
		if(findexLastDataModelInex)
			return childModelIndex;
		if(! dataViewItem && childModelIndex == lastIndexModelIndex || dataViewItem && getDataModelIndex(this->index(0, 0, index)) == childModelIndex)
			findexLastDataModelInex = true;
	}
	return QModelIndex();
}

QModelIndex TreeDataViewModel::getFirstPrevSetChildren(const QModelIndex & index){

	int rowCount = this->rowCount(index);
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);

	int divisionNumber =  item->getDivision().x() * item->getDivision().y();
	if(viewOneItem(index))
		divisionNumber = 1;

	QModelIndex firstIndexModelIndex = getDataModelIndex(this->index(0, 0, index));	
	QModelIndex dataModelParentIndex = treeDataModel->parent(firstIndexModelIndex);
	int rowCountModel = treeDataModel->rowCount(dataModelParentIndex);
	bool findexLastFirstModelInex = false;
	int offset = 0;
	for(int rModel = rowCountModel; !(rModel < 0); rModel --)
	{ 		
		QModelIndex childModelIndex = treeDataModel->index(rModel, 0, dataModelParentIndex);
		if(findexLastFirstModelInex)
		{
			offset ++;		
			if(offset == divisionNumber)			
				return childModelIndex;
		}
		if(childModelIndex == firstIndexModelIndex)
			findexLastFirstModelInex = true;
	}
	return treeDataModel->index(0, 0, dataModelParentIndex);
}

void TreeDataViewModel::startInit()
{
	fillInFromModel();
	fillDataForAllItem();
	isFilling = true;
	connect	(	treeDataModel,		SIGNAL	( rowsInserted			( const QModelIndex &, int, int ) ),
				this,				SLOT	( addNewTreeItemSlot	( const QModelIndex &, int, int ) )
			);
	connect	(	treeDataModel,		SIGNAL	( dataChanged			( const QModelIndex & , const QModelIndex &  ) ),
				this,				SLOT	( processChanging		( const QModelIndex & , const QModelIndex &  ) ),
				Qt::UniqueConnection
			);
}

void TreeDataViewModel::emitCommand( const Command & _command, const QModelIndex &modelIndexParent )
{	
	DataViewTreeItem * parentItem = (DataViewTreeItem *)getTreeItemByIndex(modelIndexParent);
	int rowC = rowCount(modelIndexParent);
	for(int r = 0; r < rowC; r ++)
	{		
		QModelIndex modelIndex = index(r, 0, modelIndexParent);		
		DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(modelIndex);
		if(true && (item->getActivity() || (applyForGroup(modelIndexParent) && parentItem->getActivity())) )
		{
			item->viewParameters()->processCommand(_command);
			emit changingParameter(modelIndex);
		}
		emitCommand(_command, modelIndex);
	}
}

ViewParameters TreeDataViewModel::getViewParameters( const QModelIndex &index)
{	
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);	
	return *item->viewParameters();
}

TreeItem * TreeDataViewModel::getTreeItemByIndex(const QModelIndex &index )
{
	if(index == QModelIndex())
		return TreeDataViewModel::rootItem;
	else
		return static_cast<TreeItem *>( index.internalPointer() );
}

void TreeDataViewModel::createEmptyStructureForTreeItem(DataViewTreeItem * treeItem)
{
	QList<uint> tags = treeDataModel->getTags(treeItem->getIndex());
	QString modality = treeDataModel->getModality(treeItem->getIndex());
	int tag = treeDataModel->getTagByIndex(treeItem->getIndex());

	while(tags.size()!= 0 && tags.first() != tag )
		tags.removeFirst();
	if(tags.size())
		tags.removeFirst();

 	fillTreeItemsByEmptyTags(treeItem, tags, 0);

}

void TreeDataViewModel::fillTreeItemsByEmptyTags (DataViewTreeItem * treeItem, const QList<uint> & tags, int curTagNumber)
{
	while(treeItem->childCount() < treeItem->getDivision().x() * treeItem->getDivision().y() 
		&& curTagNumber < tags.size())
	{
		DataViewTreeItem * childItem = addChildItem(treeItem,treeDataModel->emptyDicomDataIndex(), tags[curTagNumber]);
		fillTreeItemsByEmptyTags(childItem, tags, curTagNumber + 1);	
	}	
}

DataViewTreeItem * TreeDataViewModel::addChildItem(DataViewTreeItem * treeItem, const QModelIndex & modelIndexChild, uint tag)
{
	QList<QVariant> list;	
	DataViewTreeItem * childItem = new DataViewTreeItem(list, treeItem);

	list << QString("%1").arg((int)childItem, 0, 16);
	childItem->setData(list);
	
	childItem->tag = tag;
	QModelIndex parent;
	if(treeItem == rootItem)
	{
		//parent = QModelIndex();
	}
	else{
		parent = createIndex(treeItem->row(), 0, treeItem);	
	}
	
	setModelIndexToItem(childItem, modelIndexChild);	

	beginInsertRows ( parent, treeItem->childCount(),  treeItem->childCount());
	treeItem->appendChild(childItem);
	endInsertRows();	
		
	connect(treeItem, SIGNAL(changeDivision(int, int)),
			this,		SLOT (changeDivisionSlot(int, int)),
			Qt::UniqueConnection);

	connect(childItem, SIGNAL(changeDivision(int, int)),
			this,		SLOT (changeDivisionSlot(int, int)),
			Qt::UniqueConnection);

	checkForNoOneActiveChildren((DataViewTreeItem *) rootItem);
	tryFillActiveLine(treeItem);
	return childItem;
}

void TreeDataViewModel::changeDivisionSlot(int rows, int cols)
{
	DataViewTreeItem * item = (DataViewTreeItem *)QObject::sender();
	if(item == rootItem)
		emit changeDivision (QModelIndex(), item->rows(), item->cols());
	else
		emit changeDivision (createIndex(item->row(), 0, item), item->rows(), item->cols());
}

void TreeDataViewModel::setModelIndexToItem(DataViewTreeItem * treeItem, const QModelIndex & modelIndex)
{
	treeItem->setIndex(modelIndex);
	if(modelIndex != treeDataModel->emptyDicomDataIndex())
	{
		DicomData * dicomData = treeDataModel->getDicomDataByIndex(modelIndex);

		if(dicomData)			
			treeItem->setDicomData(dicomData);	
		else
			QLOG_ERROR() << "null dicomData in setDicomData in in treedataviewmodel4";

		if(dicomData && dicomData->getBody())				
			processBodyCreated(treeItem);	

		connect	(	treeItem,				SIGNAL(bodyCreated(TreeItem *)),
					this,					SLOT(processBodyCreated(TreeItem *)),
					Qt::UniqueConnection
				);
		if( treeItem->childCount() == 0 &&  treeItem->parent() != rootItem)
		{
			TreeItem * p = treeItem->parent();
			if(p->childCount() != 0)
			{
				DataViewTreeItem * firstItem = (DataViewTreeItem *) p->child(0);
				ViewParameters params = *firstItem->viewParameters();
				treeItem->setViewParameters( params );				
			}
		}
	}
	checkForNoOneActiveChildren((DataViewTreeItem *) rootItem);	
	QModelIndex index = createIndex(treeItem->row(), 0, treeItem);		
	emit changedDataModelIndex(index);	
	emit modelChanged();
	
	
}


void TreeDataViewModel::serializeTreeStructure (DataViewTreeItem * treeItem, const QString &modality)
{

	QDomDocument doc("Sheme");

	QDomElement root = doc.createElement("modality");

	root.setAttribute("name", modality);

	doc.appendChild(root);
	serializeTreeItem(doc, root, treeItem);	
	
	QFile output(settingsDir + "\\" + modality + ".xml");
	//QFile output("note.xml");
	output.open(QIODevice::WriteOnly);
	QTextStream stream(&output);
	doc.save(stream, 2);
}

void TreeDataViewModel::serializeTreeItem(QDomDocument &doc, QDomNode & docNode, DataViewTreeItem * treeItem)
{
	QDomElement el = doc.createElement(treeItem->metaObject()->className());

	docNode.appendChild(el);

	bool haveXrayProp = false;

	for(int i = 0; i < treeItem->metaObject()->propertyCount(); i++)
	{
		QMetaProperty prop = treeItem->metaObject()->property(i);
		QString propName = prop.name();
		if(propName == "objectName" )
			continue;
		if(propName.contains("xray_"))
		{
			QVariant value = treeItem->property(propName.toAscii().data());		
			el.setAttribute(propName.remove("xray_"), value.toString());
			haveXrayProp = true;
		}

	}

	DataViewTreeItem * childItem = NULL;
	for(int i = 0; i < treeItem->childCount(); i ++)
	{
		DataViewTreeItem * item = (DataViewTreeItem *) treeItem->child(i);	
		serializeTreeItem(doc, el, item);	
	}
}


void TreeDataViewModel::deSerializeTreeStructure (DataViewTreeItem * treeItem, const QString &modality,  const QList<uint> & tags)
{
	//return;
	QDomDocument doc( "imgInfo" );

	QString errorStr;
	int row, column;
	QDir dir;
	dir.mkdir(settingsDir);
	QFile output(settingsDir + "\\" + modality + ".xml");

	if( !doc.setContent( &output , false, &errorStr, &row, &column ) ){
		QLOG_ERROR() << QString("������ xml �� ������ %1, ������� %2:").arg(row).arg(column);
		QLOG_ERROR() << errorStr;
		//QMessageBox::warning (NULL, "������ ��������� ������", "�������� ���������� XML-�����." );
		return;
	}

	QDomElement root = doc.documentElement();
	QString  a = root.tagName();

	deSerializeTreeItem(doc, root.firstChildElement(), treeItem, tags, 0);	
}


void TreeDataViewModel::deSerializeTreeItem(	QDomDocument & docNode, 
												QDomElement & element, 
												DataViewTreeItem * treeItem, 
												const QList<uint> & tags, uint curTagNumber)
{
	//treeItem->setDivision();
	QString  a = element.nodeName();

	for(int i = 0; i < treeItem->metaObject()->propertyCount(); i++)
	{
		QMetaProperty prop = treeItem->metaObject()->property(i);
		QString propName = prop.name();
		if(propName == "objectName" )
			continue;
		if(propName.contains("xray_"))
		{		
			QVariant val = QVariant( element.attribute(propName.remove("xray_")));
			treeItem->setProperty(QString("xray_" + propName).toAscii().data(), val);				
		}

	}

	QDomElement childElement = element.firstChildElement();
	while( !childElement.isNull() && treeItem->childCount() < treeItem->getDivision().x() * treeItem->getDivision().y() 
			&& curTagNumber < (uint)tags.size())
	{
		
		DataViewTreeItem * childItem = addChildItem(treeItem,treeDataModel->emptyDicomDataIndex(), tags[curTagNumber]);

		deSerializeTreeItem( docNode, childElement, childItem, tags, curTagNumber + 1);
		childElement = childElement.nextSiblingElement();
	}
}

void TreeDataViewModel::reCountIndexes( const QModelIndex & parentModelIndex, 
										int rowNumberStart, 
										int rowNumberEnd )
{
	TreeItem * parentItem = findTreeItem(parentModelIndex);

	TreeItem * childItem = findTreeItem(treeDataModel->index(rowNumberStart, 0, parentModelIndex));

	for(int i = 0; i < parentItem->childCount(); i ++)
	{
		DataViewTreeItem * item = (DataViewTreeItem *) parentItem->child(i);
		QModelIndex dataModelIndex = item->getIndex();

		if(dataModelIndex != treeDataModel->emptyDicomDataIndex())
		{		
			int dataModelIndexRow = dataModelIndex.row();
			if(	dataModelIndexRow > rowNumberStart )				
				item->setIndex(treeDataModel->index(dataModelIndexRow - 1, 0, parentModelIndex));
		}
	}	

	checkForNoOneActiveChildren((DataViewTreeItem *) rootItem);
	tryFillActiveLine();
	fillBranch(parentModelIndex);
}


void TreeDataViewModel::deleteChildrenSlot( const QModelIndex & parentModelIndex, int rowNumberStart, int rowNumberEnd )
{
	TreeItem * childItem = findTreeItem(treeDataModel->index(rowNumberStart, 0, parentModelIndex));

	if(childItem)
	{
		flushItem(childItem);
	}
}

void TreeDataViewModel::flushItem(TreeItem * item )
{	
	int count = item->childCount();
	for(int i = 0; i < item->childCount(); i ++)
	{
		flushItem(item->child(i));
	}

	QModelIndex index = createIndex(item->row(), 0, item);	

	changeDataModelIndex(index, treeDataModel->emptyDicomDataIndex());

	setIsViewOne(index, false);
	setActivityForItem(index, false);
	if(selectedItemIndex == index)
		selectedItemIndex = treeDataModel->emptyDicomDataIndex();

	item->viewParameters().resetParameters();

}



void TreeDataViewModel::setIsViewOne( const QModelIndex & index, bool i )
{
	DataViewTreeItem * item = (DataViewTreeItem * )getTreeItemByIndex(index);
	item->setIsViewOne(i);
	emit changedViewOne();

	if(getIndexTag(parent(index)) != StudyUID && parent(index)!= QModelIndex())
		setIsViewOne(parent(index), i);

}

void TreeDataViewModel::setIsViewOne( const QModelIndex & index)
{
	if(index == QModelIndex())
		return;
	DataViewTreeItem * item = (DataViewTreeItem * )getTreeItemByIndex(index);	
	item->setIsViewOne(!item->isViewOne());
	emit changedViewOne();	
	//if(getIndexTag(parent(index)) != StudyUID)
		setIsViewOne(parent(index));

}

bool TreeDataViewModel::isIndexViewOne(const QModelIndex & index)
{
	DataViewTreeItem * item = (DataViewTreeItem * )getTreeItemByIndex(index);
	return item->isViewOne();
}

bool TreeDataViewModel::isSelectedItemIndex( const QModelIndex & index )
{
	if(index == selectedItemIndex)
		return true;
	else
		return false;
}

bool TreeDataViewModel::applyForGroup( const QModelIndex & index )
{
	QString modality = getModality(index);

	if (getIndexTag(index) == SeriesUID )
		return true;
	else 
		return false;
}


void TreeDataViewModel::switchOverlay()
{
	emit overlaySwitched();
}