#include "StdAfx.h"
#include "printtreedataviewmodel.h"
#include "DataViewTreeItem.h"
#include "treedatamodel.h"

PrintTreeDataViewModel::PrintTreeDataViewModel(TreeDataModel * model,QObject *parent)
	: TreeDataViewModel(model, parent)
{

}

PrintTreeDataViewModel::~PrintTreeDataViewModel()
{

}

void PrintTreeDataViewModel::emitCommand( const Command & _command, const QModelIndex &modelIndexParent )
{
	QModelIndex modelIndex = selectedItemIndex;
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(modelIndex);
	ViewParameters params = treeDataModel->viewParameters(item->getIndex());	
	params.processCommand(_command);
	treeDataModel->setViewParameters(item->getIndex(), params);
	emit changingParameter(modelIndex);
}


ViewParameters PrintTreeDataViewModel::getViewParameters( const QModelIndex &index)
{	
	DataViewTreeItem * item = (DataViewTreeItem *)getTreeItemByIndex(index);	
	return treeDataModel->viewParameters(item->getIndex());	
}

void PrintTreeDataViewModel::emitChangeSelectedItemSignal(QModelIndex index)
{
}

bool PrintTreeDataViewModel::isIndexViewOne(const QModelIndex & index)
{
	return false;
}

void PrintTreeDataViewModel::setIsViewOne( const QModelIndex & index )
{
	return;
}

void PrintTreeDataViewModel::setIsViewOne( const QModelIndex & index, bool i )
{
	return;
}
