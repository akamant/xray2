#include "stdafx.h"
#include "treemodel.h"
//#include "treemodel.h"
#include "TreeItem.h"


TreeModel::TreeModel(QObject *parent)
: QAbstractItemModel(parent)
, rootItem(NULL)
{
	
}


TreeModel::~TreeModel()
{
	delete rootItem;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////not my realization//////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
int TreeModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
	else
		return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole)
		return QVariant();

	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

	return item->data(index.column());
}
/*
DicomData * TreeModel::getDicomDataByIndex(const QModelIndex &index) const
{
	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	return item->getDicomData();
}*/

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return 0;

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
									int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return rootItem->data(section);

	return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	TreeItem *parentItem;

	if (!parent.isValid())
		parentItem = rootItem;
	else
		parentItem = static_cast<TreeItem*>(parent.internalPointer());

	TreeItem *childItem = parentItem->child(row);
	if (childItem)
		return createIndex(row, column, childItem);
	else
		return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
	TreeItem *parentItem = childItem->parent();

	if (!parentItem || parentItem == rootItem)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}


int TreeModel::rowCount(const QModelIndex &parent) const
{
	TreeItem *parentItem;
 	if (parent.column() > 0)
 		return 0;

	if (!parent.isValid())
		parentItem = rootItem;
	else
		parentItem = static_cast<TreeItem*>(parent.internalPointer());

	return parentItem->childCount();
}

void TreeModel::setupModelData(const QStringList &lines, TreeItem *parent)
{
	QList<TreeItem*> parents;
	QList<int> indentations;
	parents << parent;
	indentations << 0;

	int number = 0;

	while (number < lines.count()) {
		int position = 0;
		while (position < lines[number].length()) {
			if (lines[number].mid(position, 1) != " ")
				break;
			position++;
		}

		QString lineData = lines[number].mid(position).trimmed();

		if (!lineData.isEmpty()) {
			// ��������� �������� �������� �� ������� ������.
			QStringList columnStrings = lineData.split("\t", QString::SkipEmptyParts);
			QList<QVariant> columnData;
			for (int column = 0; column < columnStrings.count(); ++column)
				columnData << columnStrings[column];

			if (position > indentations.last()) 
			{
				// ��������� ������� �������� �������� ������ ����� ��������
				// ���� ������� �������� �� ����� ��������.

				if (parents.last()->childCount() > 0) {
					parents << parents.last()->child(parents.last()->childCount()-1);
					indentations << position;
				}
			} else {
				while (position < indentations.last() && parents.count() > 0) 
				{
					parents.pop_back();
					indentations.pop_back();
				}
			}

			// ��������� ����� ������� � ������ �������� �������� ��������.
			parents.last()->appendChild(new TreeItem(columnData, parents.last()));
		}

		number++;
	}
}

TreeItem * TreeModel::getTreeItemByIndex(const QModelIndex &index )
{
	if(index == QModelIndex())
		return rootItem;
	else
		return static_cast<TreeItem *>( index.internalPointer() );
}

int TreeModel::numberChildren(const QModelIndex &index ){
	int row = rowCount(parent(index));
	for(int i = 0; i < row; i ++){
		if (index == this->index(i, 0, parent(index)))
			return i + 1;
	}	
	return 1;
}