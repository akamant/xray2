#ifndef STUDYDATAMODEL_H
#define STUDYDATAMODEL_H

#include <treemodel.h>
#include "dicomData.h"

//#include <threadedttreemodel.h>


class DicomData;
class TreeItem;
class ThreadedTtreeModel;

class TreeDataModel : public TreeModel
{
	Q_OBJECT

public:

	TreeDataModel(QObject *parent = NULL);
	~TreeDataModel();
	
	ViewParameters viewParameters(QModelIndex index);
	void setViewParameters(QModelIndex index, ViewParameters params);
	
	uint findTagTreeItem(const QModelIndex & tagIndex, int level = 0, TreeItem * treeItem = NULL);
	QString getModality(const QModelIndex & index);	
	DicomData * getDicomDataByIndex (const QModelIndex & index);
	virtual QList<uint> getTags(const DicomDataPtr & dicomDataPtr);
	virtual QList<uint> getTags(const QModelIndex & index);
	uint TreeDataModel::getTagByIndex (const QModelIndex & index);
	virtual void startInit();	
	bool haveDicomStructInstance( DicomDataPtr dicomDataPtr, TreeItem * treeItem = NULL);
	QModelIndex emptyDicomDataIndex();
	bool isEmptyDicomDataIndex(const QModelIndex &index);
	virtual void addDicomDataPtr(const DicomDataPtr & dicomDataPtr, const ViewParameters & viewParameters = ViewParameters(), int numberKeyTag = 1, TreeItem * treeItem = NULL);
	DicomDataPtr getDicomDataPtrByIndex (const QModelIndex & index);

signals:
	   void RowsIsGoingToRemove(const QModelIndex &parent, int first, int last);
public slots:
	void removeBranchByIndex(const QModelIndex & index );
	void processBodyCreated(TreeItem * treeItem);
	void rowsInsertedSlot ( const QModelIndex & i, int start , int end);
	void processDicomDataPtr(const DicomDataPtr & dicomDataPtr);
	
private:
	uint getTagByParentIndex (const QModelIndex & parentIndex);
	TreeItem * emptyTreeItem;
	int rowsForEmpty;
	void processDicomDataFromQueue();		
	QQueue<DicomDataPtr> queueDicomData;
		
};

#endif // STUDYDATAMODEL_H
