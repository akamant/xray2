#ifndef PRINTTREEDATAMODEL_H
#define PRINTTREEDATAMODEL_H

#include <TreeDataModel.h>
#include "dicomData.h"

class PrintTreeDataModel : public TreeDataModel
{
	Q_OBJECT

public:
	PrintTreeDataModel(QObject *parent = NULL);
	~PrintTreeDataModel();
	virtual QList<uint> getTags(const DicomDataPtr & dicomDataPtr);	
	virtual QList<uint> getTags(const QModelIndex & index);
private:
	
};

#endif // PRINTTREEDATAMODEL_H
