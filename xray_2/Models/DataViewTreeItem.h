#pragma once

#include "treeitem.h"

class DataViewTreeItem :
	public TreeItem
{
Q_OBJECT
Q_PROPERTY(int xray_rows READ rows WRITE setRows)
Q_PROPERTY(int xray_cols READ cols WRITE setCols)

public:
	//DataViewTreeItem(void);
	DataViewTreeItem(const QList<QVariant> &data, TreeItem *parent = 0, QObject * parentObject = NULL);
	~DataViewTreeItem(void);
	QModelIndex getIndex();
	void setIndex(const QModelIndex & i);
	void setDicomData( DicomData * dicomData);
	DicomData * getDicomData(){return dicomData;};
	
	bool getActivity(){return isActive;};
	void setActivity(bool i);

	QVector2D getDivision(){ return division;};
	void setDivision(QVector2D _division);

	int rows(){return division.x();};
	int cols(){return division.y();};

	void setRows(int rows);
	void setCols(int cols);

	void setViewParameters( const ViewParameters & params) {_viewParameters = params;}
	ViewParameters * viewParameters() {return &_viewParameters;};
	
	bool isViewOne() {return _isViewOne;};

	void DataViewTreeItem::setIsViewOne( bool i ){_isViewOne = i;};

public slots:	
	void processBodyCreated(DicomData *);

signals:
	void bodyCreated(TreeItem * viewModelTreeItem);
	void changeDivision(int, int);
	


private:
	ViewParameters _viewParameters;
	QVector2D division;
	bool isActive;
	DicomData * dicomData;
	QModelIndex index;
	bool _isViewOne;
};
