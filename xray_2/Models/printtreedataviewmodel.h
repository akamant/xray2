#ifndef PRINTTREEDATAVIEWMODEL_H
#define PRINTTREEDATAVIEWMODEL_H

#include <TreeDataViewModel.h>

class PrintTreeDataViewModel : public TreeDataViewModel
{
	Q_OBJECT

public:
	PrintTreeDataViewModel(TreeDataModel * model, QObject *parent = 0);
	~PrintTreeDataViewModel();
	virtual void emitCommand( const Command & _command, const QModelIndex &modelIndexParent = QModelIndex());
	ViewParameters getViewParameters( const QModelIndex &index);	
	void emitChangeSelectedItemSignal(QModelIndex index);
	bool isIndexViewOne(const QModelIndex & index);
	void setIsViewOne( const QModelIndex & index);
	void setIsViewOne(const QModelIndex & index, bool i);
private:
	
};

#endif // PRINTTREEDATAVIEWMODEL_H
