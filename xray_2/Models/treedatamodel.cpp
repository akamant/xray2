#include "stdafx.h"
#include "treedatamodel.h"
#include "treeitem.h"
#include "dicomdata.h"


TreeDataModel::TreeDataModel(QObject *parent)
	: TreeModel(parent)
	, rowsForEmpty(0xFFFF)
{
	this;
	QList<QVariant> emptyItemList;	
	emptyTreeItem = new TreeItem (emptyItemList << "emptyItem");
	QList<QVariant> rootData;	
	QString id;
	rootData << id;	
	connect(this, SIGNAL	(rowsInserted	( const QModelIndex &, int, int ) ),
			this, SLOT		(rowsInsertedSlot ( const QModelIndex &, int, int )));		
	rootItem = new TreeItem(rootData);	
}


void TreeDataModel::rowsInsertedSlot ( const QModelIndex & i, int start , int end){
	int  a = 5;
}

TreeDataModel::~TreeDataModel()
{
	delete rootItem;
}

void TreeDataModel::processDicomDataPtr(const DicomDataPtr & dicomDataPtr)
{
	if(haveDicomStructInstance(dicomDataPtr))
		return;

	addDicomDataPtr(dicomDataPtr);
	return;

	queueDicomData << dicomDataPtr;
	if(queueDicomData.size() == 1)
	{
		queueDicomData.dequeue();
		addDicomDataPtr(dicomDataPtr);
	}
}

void TreeDataModel::processDicomDataFromQueue()
{
 	//QVector<int> testsData { 1, 2, 10, 42, 50, 123  };
	return;
	if(!queueDicomData.isEmpty())
		queueDicomData.dequeue();
	if(!queueDicomData.isEmpty())
		addDicomDataPtr(queueDicomData.first());
}

void TreeDataModel::addDicomDataPtr(const DicomDataPtr & dicomDataPtr,  const ViewParameters & viewParameters, int numberKeyTag, TreeItem * treeItem /* = rootItem*/)
{
	if(!treeItem)
	{
		QString path;
		treeItem = rootItem;	
		treeItem->setDicomDataPtr(dicomDataPtr);		
		QString modality = dicomDataPtr.data()->getELEM_CONT().get(0x00080060).variant.toString();
		//if(modality == "CR")
		//	path = dicomDataPtr.data()->ID;
	}	
	uint keyTag;
	if (getTags(dicomDataPtr).size() > 0)
		keyTag = getTags(dicomDataPtr)[numberKeyTag];
	int size = getTags(dicomDataPtr).size();

	if (numberKeyTag >= getTags(dicomDataPtr).size() - 1 || treeItem->exitCondition())  //exit condition of recursion
	{
		QList<QVariant> list;
		list << dicomDataPtr.data()->getELEM_CONT().get(keyTag).variant.toString() << QByteArray("");	

		TreeItem * childItem = new TreeItem( list, treeItem );	

		childItem->setDicomDataPtr(dicomDataPtr);	
		//if(viewParameters)
			childItem->setParameters(viewParameters);

		QModelIndex parent;
		if(treeItem == rootItem)
			parent = QModelIndex();
		else
			parent = createIndex(treeItem->row(), 0, treeItem);

		beginInsertRows ( parent, treeItem->childCount(),  treeItem->childCount());
		treeItem->appendChild(childItem);
		endInsertRows();			
		emit modelChanged();
		
		emit dataChanged (index(childItem->row(), 0, parent), index(childItem->row(), 0, parent));	

		processDicomDataFromQueue();
		return;
	}

	int childCount = treeItem->childCount();
	for(int c = 0; c < childCount; c ++){
		TreeItem * childItem = treeItem->child(c);
		if(childItem->data(0).toString() == dicomDataPtr.data()->getELEM_CONT().get(keyTag).variant.toString()){				
			return addDicomDataPtr(dicomDataPtr, viewParameters, numberKeyTag + 1, childItem);
		}
	}
	QList<QVariant> list;
	list << dicomDataPtr.data()->getELEM_CONT().get(keyTag).variant.toString();
	int r = treeItem->row();
	TreeItem * childItem =  new TreeItem( list, treeItem );
	childItem->setDicomDataPtr(dicomDataPtr);
	QModelIndex parent;
	if(treeItem == rootItem)
	{
		parent = QModelIndex();

		int maxNumber = 33;

		if(rootItem->childCount() > maxNumber - 1)
			removeBranchByIndex(createIndex(0, 0, rootItem->child(0)));

	}else
	{
		parent = createIndex(treeItem->row(), 0, treeItem);	
	}

	beginInsertRows ( parent,  treeItem->childCount(),  treeItem->childCount());		
	treeItem->appendChild(childItem);
	endInsertRows();

	return addDicomDataPtr(dicomDataPtr,viewParameters, numberKeyTag + 1, childItem);
}

uint TreeDataModel::findTagTreeItem(const QModelIndex & tagIndex, int level, TreeItem * treeItem){
	if(!treeItem){
		treeItem = rootItem;		
	}
	TreeItem * tagTreeItem = getTreeItemByIndex(tagIndex);

	if(tagIndex == QModelIndex() || tagTreeItem == NULL)
		return -1;	

	if(treeItem == tagTreeItem)
		return getTags(tagIndex)[level];
	
	int childCount = treeItem->childCount();
	for(int c = 0; c < childCount; c ++)
	{
		TreeItem * childItem = treeItem->child(c);
		uint tag = findTagTreeItem(tagIndex, level + 1, childItem);
		if(tag)
			return tag;
	}
	return NULL;
}


void TreeDataModel::processBodyCreated( TreeItem * treeItem )
{	
	QModelIndex parent;
	if(treeItem == rootItem)
		parent = QModelIndex();
	else
		parent = createIndex(treeItem->row(), 0, treeItem);		

}


DicomData * TreeDataModel::getDicomDataByIndex (const QModelIndex & index)
{	
	 if(index == emptyDicomDataIndex())
		 return NULL;
	TreeItem * item = getTreeItemByIndex(index);
	return item->dicomDataPtr().data();
}

DicomDataPtr TreeDataModel::getDicomDataPtrByIndex (const QModelIndex & index)
{	
	if(index == emptyDicomDataIndex())
		return DicomDataPtr(NULL);
	TreeItem * item = getTreeItemByIndex(index);
	return item->dicomDataPtr();
}

uint TreeDataModel::getTagByIndex (const QModelIndex & index)
{
	return findTagTreeItem(index);
}

QString TreeDataModel::getModality(const QModelIndex & index)
{
	TreeItem * item = getTreeItemByIndex(index);
	return item->modality();	
}


QList<uint> TreeDataModel::getTags(const QModelIndex & index)
{
	QList<uint> keyTags;
	if(index == QModelIndex())
		return keyTags << -1 << StudyUID;
	QString modality = getTreeItemByIndex(index)->modality();
	if(modality.contains("US"))
	{
		keyTags <<-1 << StudyUID << 0x00080018 << 0x00200052;
	}else{
		keyTags <<-1 << StudyUID << SeriesUID << 0x00080018;
	}		
	return keyTags;
}


QList<uint> TreeDataModel::getTags(const DicomDataPtr & dicomDataPtr)
{
	QList<uint> keyTags;
	if(!dicomDataPtr)
		return keyTags << -1;
	QString modality = dicomDataPtr.data()->getELEM_CONT().get(0x00080060).variant.toString();
	
	if(modality.contains("US"))
	{
		keyTags <<-1 << StudyUID << 0x00080018 << 0x00200052;
	}else{
		keyTags <<-1 << StudyUID << SeriesUID << 0x00080018;
	}		
	
	return keyTags;
}


void TreeDataModel::startInit(){
}



bool TreeDataModel::haveDicomStructInstance( DicomDataPtr dicomDataPtr, TreeItem * treeItem )
{
	//return false;

	if(treeItem == NULL)
		treeItem = rootItem;
	if(!treeItem)
		return false;
	int childCount = treeItem->childCount();
	for(int c = 0; c < childCount; c ++)
	{
		TreeItem * childItem = treeItem->child(c);
		if	(	dicomDataPtr.data()->getELEM_CONT().get(0x00080018).toString() == childItem->dicomDataPtr().data()->getELEM_CONT().get(0x00080018).toString()
			&&	dicomDataPtr.data()->getELEM_CONT().get(0x0001000A).toString() == childItem->dicomDataPtr().data()->getELEM_CONT().get(0x0001000A).toString()
			)
			return true;
		if(haveDicomStructInstance(dicomDataPtr, childItem))
			return true;
	}

	return false;
}


bool TreeDataModel::isEmptyDicomDataIndex(const QModelIndex &index)
{	
	if(index == emptyDicomDataIndex())
		return true;
	else
		false;

	return false;
}

uint TreeDataModel::getTagByParentIndex (const QModelIndex & parentIndex)
{
	QList<uint> tags = getTags(parentIndex);
	if(tags.size() == 1)
		return StudyUID;

	int tag = getTagByIndex(parentIndex);

	while(tags.size()!= 0 && tags.first() != tag )
		tags.removeFirst();
	if(tags.size())
		tags.removeFirst();
	if(tags.size())		
		return tags[0];	
	else return 0;
}

QModelIndex TreeDataModel::emptyDicomDataIndex()
{	
		return createIndex(0, 0, emptyTreeItem);		
}

void TreeDataModel::removeBranchByIndex(const QModelIndex & index)
{

	TreeItem * item = getTreeItemByIndex(index);

	TreeItem * parentItem = item->parent();

	QModelIndex parentIndex = parent(index);
	int row = item->row();

	emit RowsIsGoingToRemove (parentIndex, row, row);
	beginRemoveRows(parentIndex, row, row );
	bool result = removeRows ( row, 1, index);
	parentItem->removeChild(item);
	endRemoveRows();
	delete item;

}

ViewParameters TreeDataModel::viewParameters( QModelIndex index )
{
	TreeItem * item = getTreeItemByIndex(index);
	return item->viewParameters();
}

void TreeDataModel::setViewParameters( QModelIndex index, ViewParameters params )
{
	TreeItem * item = getTreeItemByIndex(index);
	item->setParameters(params);
}