#include "stdafx.h"

#include "treeitem.h"

TreeItem::TreeItem(const QList<QVariant> &data, TreeItem *parent, QObject * parentObject):
	//dicomDataPtrVariant(NULL)
	 _dicomDataPtr(NULL)
	,_modality("")
	,parentItem(parent)
	,itemData(data)
	,QObject(parentObject)	
{
	
}

TreeItem::~TreeItem(void)
{
	
// 	for(int i = 0; i < childItems.size(); i ++ ){
// 		TreeItem * item = childItems[i];
// 		item->removeChildren();
// 		delete item;
// 	}
	if (childCount() == 0 && _dicomDataPtr)
	{
		//delete _dicomDataPtr;
		//_dicomDataPtr = NULL;
	}

	qDeleteAll(childItems);
	childItems.clear();
	int a = 3;
	
}

bool TreeItem::appendChild(TreeItem *item)
{
	childItems.append(item);
	return true;
}


void TreeItem::removeChildren(){
	for(int i = 0; i < childItems.size(); i ++ ){
		TreeItem * item = childItems[i];
		//item->removeChildren();
		delete item;
	}
	childItems.clear();
}

void TreeItem::removeChild(TreeItem * item){		
		childItems.removeOne(item);			
	}


TreeItem *TreeItem::child(int row)
{
	return childItems.value(row);
}

int TreeItem::childCount() const
{
	return childItems.count();
}

int TreeItem::columnCount() const
{
	return itemData.count();
}

QVariant TreeItem::data(int column) const
{
	return itemData.value(column);
}

TreeItem *TreeItem::parent()
{
	return parentItem;
}

int TreeItem::row() const
{
	if (parentItem)
		return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

	return 0;
}

bool TreeItem::exitCondition()
{
	return false;
}

void TreeItem::setDicomDataPtr( const DicomDataPtr & dicomDataPtr )
{
	_dicomDataPtr = dicomDataPtr;
	 _modality = dicomDataPtr.data()->getELEM_CONT().get(0x00080060).variant.toString();
}

// void TreeItem::setDicomDataVariant( DicomDataPtr dicomDataPtr )
// {
// 	this->dicomDataPtrVariant = DicomDataVariant(dicomDataPtr);
// 		
// }

// void TreeItem::setDicomDataPtr( DicomDataPtr dicomDataPtr )
// {
// 	_dicomDataPtr = dicomDataPtr;
// 
// }

/*
void TreeItem::processBodyCreated( DicomDataPtr dicomDataPtr )
{
	emit bodyCreated(this);
}
*/