#include "StdAfx.h"
#include "printtreedatamodel.h"
//#include "treedatamodel.h"

PrintTreeDataModel::PrintTreeDataModel(QObject *parent )
	: TreeDataModel(parent)
{

}

PrintTreeDataModel::~PrintTreeDataModel()
{

}

QList<uint> PrintTreeDataModel::getTags(const QModelIndex & index)
{	
	QList<uint> keyTags;	
	keyTags <<-1 << 0x00080018;			
	return keyTags;
}


QList<uint> PrintTreeDataModel::getTags(const DicomDataPtr & dicomDataPtr)
{
	QList<uint> keyTags;	
	keyTags <<-1 << 0x00080018;			
	return keyTags;
}