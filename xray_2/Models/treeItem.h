#ifndef TREEITEM_H
#define TREEITEM_H

#include <QList>
#include <QVariant>
#include "dicomdata.h"
//#include "dicomdatavariant.h"

class DicomData;

class TreeItem : public QObject
{
Q_OBJECT


public:	
	TreeItem(const QList<QVariant> &data, TreeItem * parent = 0, QObject * parentObject = NULL);
	~TreeItem(void);
	bool exitCondition();
	bool appendChild(TreeItem *child);
	void removeChildren();
	TreeItem *child(int row);
	int childCount() const;
	int columnCount() const;
	QVariant data(int column) const;
	int row() const;
	TreeItem *parent();
	void setDicomDataPtr(const DicomDataPtr & dicomDataPtr);
	DicomDataPtr dicomDataPtr(){return _dicomDataPtr;};
	QString modality(){return _modality;};
	QString setModality( QString modality){_modality = modality;};
	void setData (QList <QVariant> & data){itemData = data;};
	void removeChild(TreeItem * item);
	uint tag;

	ViewParameters viewParameters(){return _viewParameters;};
	void setParameters(const ViewParameters viewParams){_viewParameters = viewParams;};

protected:
	QList<TreeItem*> childItems;
	QList<QVariant> itemData;
	TreeItem *parentItem;
	DicomDataPtr _dicomDataPtr;
	QString _modality;
	ViewParameters _viewParameters;
};

#endif